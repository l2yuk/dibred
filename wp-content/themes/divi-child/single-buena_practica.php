<?php
/*
Template Name: Detalle Buenas Practicas
*/
get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

$show_navigation = get_post_meta( get_the_ID(), '_et_pb_project_nav', true );
$bp_institucion_educativa =get_post_meta( get_the_ID(), 'institucion_educativa', true );

$bp_video_bp=get_post_meta( get_the_ID(), 'video_bp', true );
$bp_descripcion=get_post_meta( get_the_ID(), 'descripcion_buena_practica', true );


$bp_slider = get_field('slider_buena_practica');
$bp_slider_imagen_0=$bp_slider['slider_imagen_1'];
$bp_slider_imagen_1=$bp_slider['slider_imagen_2'];
$bp_slider_imagen_2=$bp_slider['slider_imagen_3'];

$bp_etapas = get_field('etapas');
$bp_imagen_etapa_1=$bp_etapas['imagen_etapa_1'];
$bp_descripcion_etapa_1=$bp_etapas['descripcion_etapa_1'];
$bp_imagen_etapa_2=$bp_etapas['imagen_etapa_2'];
$bp_descripcion_etapa_2=$bp_etapas['descripcion_etapa_2'];
$bp_imagen_etapa_3=$bp_etapas['imagen_etapa_3'];
$bp_descripcion_etapa_3=$bp_etapas['descripcion_etapa_3'];
$bp_imagen_etapa_4=$bp_etapas['imagen_etapa_4'];
$bp_descripcion_etapa_4=$bp_etapas['descripcion_etapa_4'];

$bp_seleccionar_opcion_1=$bp_etapas['seleccionar_opcion_1'];
$bp_seleccionar_opcion_2=$bp_etapas['seleccionar_opcion_2'];
$bp_seleccionar_opcion_3=$bp_etapas['seleccionar_opcion_3'];
$bp_seleccionar_opcion_4=$bp_etapas['seleccionar_opcion_4'];
if ($bp_seleccionar_opcion_1=='imagen_1'){
  $bp_imagen_etapa_1=$bp_etapas['imagen_etapa_1'];
}else{
  $bp_imagen_etapa_1=$bp_etapas['video_etapa1'];
}
if ($bp_seleccionar_opcion_2=='imagen_2'){
  $bp_imagen_etapa_2=$bp_etapas['imagen_etapa_2'];
}else{
  $bp_imagen_etapa_2=$bp_etapas['video_etapa2'];
}
if ($bp_seleccionar_opcion_3=='imagen_3'){
  $bp_imagen_etapa_3=$bp_etapas['imagen_etapa_3'];
}else{
  $bp_imagen_etapa_3=$bp_etapas['video_etapa3'];
}
if ($bp_seleccionar_opcion_4=='imagen_4'){
  $bp_imagen_etapa_4=$bp_etapas['imagen_etapa_4'];
}else{
  $bp_imagen_etapa_4=$bp_etapas['video_etapa4'];
}



$bp_archivo_bp=get_field('archivo_bp');
$bp_post_id=get_the_ID();

$bp_evidencias = get_field('evidencias');

$bp_terms_ne = wp_get_post_terms(get_the_ID(), 'nivel-educativo', array("fields" => "names"));
//var_dump(the_title());
//$bp_seleccionar_opcion_1=get_field_object('seleccionar_opcion_1');

//$bp_seleccionar_opcion_1=get_post_meta( get_the_ID(), 'seleccionar_opcion_1', true );
//var_dump($bp_seleccionar_opcion_1);
//var_dump($bp_etapas);

$style_linea_bpractica = "";

switch ($bp_terms_ne[0]) {
    case 'Inicial':
        $titulo_inicial='color: #ffa8b8!important;';
        $fondo_pagina='background-color:#ffffff!important;';
        $titulo_equipo='color: #d66679!important;';
        $linea_tiempo='background-color: #ffbdc8;';
        $linea_circulo='background-color: #e6697d;';
        $linea_separacion='border-top-color: #ffbdc8;border-bottom-color: #f9f2f2;';

        $style_background='background-color: #d66679!important;';
        $style_background_linea='background-color: #ffbdc8!important;';
        $style_color='color: #d66679!important;';
        $style_border_color='border-top-color: #ffbdc8;';
        $style_background_image='
            background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNkNjY2NzkiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);
        ';
        $style_linea='#ff9baa!important;';
		$style_linea_bpractica = " style = \"border-bottom: 5px solid #d66679;\"";
        break;
    case 'Primaria':
        $titulo_inicial='color: #ffbb28!important;';
        $fondo_pagina='background-color:#ffffff!important';
        $titulo_equipo='color: #f7973d!important;';
        $linea_tiempo='background-color: #edbf60;';
        $linea_circulo='background-color: #f17718;';
        $linea_separacion='border-top-color: #edbf60;border-bottom-color: #f9f2f2;';

        $style_background='background-color: #c66f23!important;';
        $style_background_linea='background-color: #edbf60!important;';
        $style_color='color: #c66f23!important;';
        $style_border_color='border-top-color: #edbf60;';
        $style_background_image='
            background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNjNjZmMjMiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);
        ';
        $style_linea='#ff9900!important;';
		$style_linea_bpractica = " style = \"border-bottom: 5px solid #f7973d;\"";
        break;
    case 'Secundaria':
        $titulo_inicial='color: #9ccc00!important;';
        $fondo_pagina='background-color:#ffffff!important';
        $titulo_equipo='color: #62a30d!important;';
        $linea_tiempo='background-color: #a3d35b;';
        $linea_circulo='background-color: #43940d;';
        $linea_separacion='border-top-color: #a2d35b;border-bottom-color: #f9f2f2;';


        $style_background='background-color: #5d7c2a!important;';
        $style_background_linea='background-color: #a3d35b!important;';
        $style_color='color: #5d7c2a!important;';
        $style_border_color='border-top-color: #a2d35b;';
        $style_background_image='
        background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiM1ZDdjMmEiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);
        ';
        $style_linea='#a1c912!important;';
		$style_linea_bpractica = " style = \"border-bottom: 5px solid #62a30d;\"";
        break;
}
?>
<!--
	<div id="page-container" style="padding-top: 120px; overflow-y: hidden; margin-top: -1px;" class="et-animated-content">



		<div id="et-main-area">

<div id="main-content">
-->

<?php while ( have_posts() ) : the_post(); ?>
<!--
<article id="post-55254" class="post-55254 page type-page status-publish hentry odd">
-->
<article id="post-<?php the_ID(); ?>" class="post-<?php the_ID(); ?> " <?php post_class(); ?>>
<?php

?>
  <div class="entry-content">

    <div id="et-boc" class="et-boc">

      <div class="et-l et-l--post">

        <div class="et_builder_inner_content et_pb_gutters3">

          <div class="et_pb_section et_pb_section_0 et_pb_fullwidth_section et_section_regular section_has_divider et_pb_bottom_divider" <?php echo $style_linea_bpractica; ?>>
            <div class="et_pb_module et_pb_fullwidth_slider_0 et_pb_slider et_slider_auto et_slider_speed_7000 et_slider_auto_ignore_hover et_pb_bg_layout_dark et_slide_transition_to_next" data-active-slide="et_pb_slide_1">
              <div class="et_pb_slides">

                <?php if(!empty($bp_slider_imagen_0)){ ?>
                <div class="et_pb_slide et_pb_slide_0 et_pb_bg_layout_dark et_pb_media_alignment_center et-pb-moved-slide et_slide_transition" data-slide-id="et_pb_slide_0" >
                  <div class="et_pb_container clearfix" style="height: 700px;">
                    <div class="et_pb_slider_container_inner">
                      <div class="et_pb_slide_description">
                        <h2 class="et_pb_slide_title_bp" style="<?php echo $titulo_inicial; ?>"><?php the_title(); ?></h2><div class="et_pb_slide_content"><p>( <?php echo $bp_institucion_educativa;?> )</p></div>
                      </div> <!-- .et_pb_slide_description -->
                    </div>
                  </div> <!-- .et_pb_container -->
                </div> <!-- .et_pb_slide -->
                <?php } ?>
                <?php if(!empty($bp_slider_imagen_1)){ ?>
                <div class="et_pb_slide et_pb_slide_1 et_pb_bg_layout_dark et_pb_media_alignment_center et-pb-active-slide" data-slide-id="et_pb_slide_1" >
                  <div class="et_pb_container clearfix" style="height: 700px;">
                    <div class="et_pb_slider_container_inner">
                      <div class="et_pb_slide_description">
                        <h2 class="et_pb_slide_title_bp" style="<?php echo $titulo_inicial; ?>"><?php the_title(); ?></h2><div class="et_pb_slide_content"><p>( <?php echo $bp_institucion_educativa;?> )</p></div>
                      </div> <!-- .et_pb_slide_description -->
                    </div>
                  </div> <!-- .et_pb_container -->
                </div> <!-- .et_pb_slide -->
                <?php } ?>
                <?php if(!empty($bp_slider_imagen_2)){ ?>
                <div class="et_pb_slide et_pb_slide_2 et_pb_bg_layout_dark et_pb_media_alignment_center" data-slide-id="et_pb_slide_2" >
                  <div class="et_pb_container clearfix" style="height: 700px;">
                    <div class="et_pb_slider_container_inner">
                      <div class="et_pb_slide_description">
                        <h2 class="et_pb_slide_title_bp" style="<?php echo $titulo_inicial; ?>"><?php the_title(); ?></h2><div class="et_pb_slide_content"><p>( <?php echo $bp_institucion_educativa;?> )</p></div>
                      </div> <!-- .et_pb_slide_description -->
                    </div>
                  </div> <!-- .et_pb_container -->
                </div> <!-- .et_pb_slide -->
                  <?php } ?>

              </div> <!-- .et_pb_slides -->

              <div class="et-pb-slider-arrows"><a class="et-pb-arrow-prev" href="#"><span>Anterior</span></a><a class="et-pb-arrow-next" href="#"><span>Siguiente</span></a></div>
              <div class="et-pb-controllers"><a href="#" class="">1</a><a href="#" class="et-pb-active-control">2</a><a href="#" class="">3</a></div>
            </div> <!-- .et_pb_slider -->

            <div class="et_pb_bottom_inside_divider" ></div>

          </div> <!-- .et_pb_section -->

          <div class="et_pb_section et_pb_section_1_bp et_pb_with_background et_section_regular section_has_divider et_pb_bottom_divider" style="padding-top: 16px;<?php echo $fondo_pagina;?>">
            <div class="et_pb_with_border et_pb_row et_pb_row_0" style="padding-bottom: 0px!important;">
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">
                <div class="et_pb_module et_pb_code et_pb_code_0">
                  <div class="et_pb_code_inner">
                  </div>
                </div> <!-- .et_pb_code -->
                <div class="et_pb_module et_pb_code et_pb_code_1  et_pb_text_align_center">
                  <div class="et_pb_code_inner">
                    <!--Yasr Visitor Votes Shortcode-->
                    <?php echo do_shortcode('[yasr_visitor_votes]'); ?>
                  </div>
                </div> <!-- .et_pb_code -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">
                <div class="et_pb_module et_pb_code et_pb_code_2">
                  <div class="et_pb_code_inner">
                    <!-- compartir redes sociales -->
                      <?php echo do_shortcode('[csbwfs_buttons buttons="fb,tw,li,wa"] ');?>
                  </div>
                </div> <!-- .et_pb_code -->
              </div> <!-- .et_pb_column -->

            </div> <!-- .et_pb_row -->

            <div class="et_pb_with_border et_pb_row et_pb_row_6">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child AverageRating">

                  <?php
                    //echo getAverageRating($bp_post_id);

                  ?>

              </div> <!-- .et_pb_column -->
            </div>


            <div class="et_pb_row et_pb_row_1">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">
                <div class="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light">
                  <div class="et_pb_text_inner"><h1>Descripción</h1></div>
                </div> <!-- .et_pb_text -->
                <div class="et_pb_module et_pb_divider et_pb_divider_0 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
              </div> <!-- .et_pb_column -->
            </div> <!-- .et_pb_row -->

            <div class="et_pb_with_border et_pb_row et_pb_row_2">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough et-last-child">
                <div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_1  et_pb_text_align_left et_pb_bg_layout_light">
                  <div class="et_pb_text_inner" style="color: #575756!important;    font-family: 'Calibri Regular',Helvetica,Arial,Lucida,sans-serif!important;
    font-size: 18px!important;line-height: 25px;">
                    <p style="line-height: 25px!important;">
                    <?php echo $bp_descripcion;?>
                    </p>

                  </div>
                  </div> <!-- .et_pb_text -->

                  <div class="et_pb_module et_pb_code et_pb_code_3">
                    <div class="et_pb_code_inner">
                      <link  rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?libdco">
                      <?php //WP_CSP::ob_start(); ?>
                      <style >
                      li.et_pb_social_icon.et-social-youtube a.icon:before {
                        font-family: "Socicon" !important;
                        content: 'e099' !important;
                      }
                    </style>
                    <?php //WP_CSP::ob_end_flush(); ?>
                    <?php //WP_CSP::ob_start(); ?>
                    <script >
                    jQuery(function($){
                      $('.et-social-youtube a').attr('title', 'Whatsapp');
                    });
                  </script>
                  <?php //WP_CSP::ob_end_flush(); ?>

                </div>
              </div> <!-- .et_pb_code -->
            </div> <!-- .et_pb_column -->

          </div> <!-- .et_pb_row -->
          <?php if (!empty($bp_video_bp)){ ?>
          <div class="et_pb_row et_pb_row_3">
            <div class="et_pb_column et_pb_column_4_4 et_pb_column_4  et_pb_css_mix_blend_mode_passthrough et-last-child" style="">
              <div class="et_pb_module et_pb_video et_pb_video_0">
                <div class="et_pb_video_box">
                  <!--
                  <video controls="">
                    <source type="video/mp4" src="https://observatorio.minedu.gob.pe/almacenamiento/2019/10/convertido3-video-muestra.mp4">
                    </video>
                  -->
                    <?php echo $bp_video_bp;?>
                  </div>
                </div>
              </div> <!-- .et_pb_column -->
            </div> <!-- .et_pb_row -->
            <?php } ?>

            <div class="et_pb_bottom_inside_divider"></div>
          </div> <!-- .et_pb_section -->
          <div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular" style="padding-top: 20px;">
            <div class="et_pb_row et_pb_row_4">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_5  et_pb_css_mix_blend_mode_passthrough et-last-child">
                <div class="et_pb_module et_pb_text et_pb_text_2  et_pb_text_align_left et_pb_bg_layout_light">
                  <div class="et_pb_text_inner"><h1>Equipo Docente</h1></div>
                </div> <!-- .et_pb_text -->
                <div class="et_pb_module et_pb_divider et_pb_divider_1 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
              </div> <!-- .et_pb_column -->
            </div> <!-- .et_pb_row -->
            <div class="et_pb_row et_pb_row_5">

              <?php echo do_shortcode('[ntrk-carousel-cposts id='.$bp_post_id.']'); ?>


            </div> <!-- .et_pb_row -->
          </div> <!-- .et_pb_section -->

          <div class="et_pb_section et_pb_section_3 ds-timeline et_pb_with_background et_section_regular section_has_divider et_pb_bottom_divider" style="padding-top: 85px; ">
            <div class="et_pb_row et_pb_row_6">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child">
                <div class="et_pb_module et_pb_text et_pb_text_3  et_pb_text_align_left et_pb_bg_layout_light">
                  <div class="et_pb_text_inner"><h1>Etapas</h1></div>
                </div> <!-- .et_pb_text --><div class="et_pb_module et_pb_divider et_pb_divider_2 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
              </div> <!-- .et_pb_column -->
            </div> <!-- .et_pb_row -->
            <div class="et_pb_row et_pb_row_7 timeline-item speak-soon even et_animated et_pb_gutters1" data-animation-style="slideTop" data-animation-repeat="" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-intensity="17%" data-animation-starting-opacity="0%" data-animation-speed-curve="ease-in-out">
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_10 col-date  et_pb_css_mix_blend_mode_passthrough">
                <div class="et_pb_module et_pb_text et_pb_text_4 tm-date  et_pb_text_align_left et_pb_bg_layout_light">
                  <div class="et_pb_text_inner" style="margin-top: -40px;"><h4>Contexto Social y propósito</h4></div>
                </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_11 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">
                <div class="et_pb_module et_pb_image et_pb_image_0">
                  <span class="et_pb_image_wrap ">
                  <?php if ($bp_seleccionar_opcion_1=='imagen_1'){ ?>
                    <img src="<?php echo $bp_imagen_etapa_1; ?>" alt="" title="">
                  <?php }else{?>
                  <?php echo $bp_imagen_etapa_1;?>
                  <?php }?>
                  </span>
                </div><div class="et_pb_module et_pb_text et_pb_text_5 tm-desc  et_pb_text_align_left et_pb_bg_layout_light">
                  <div class="et_pb_text_inner">
                    <p>
                      <span>
                        <?php echo $bp_descripcion_etapa_1;?>
                      </span>
                    </p>
                  </div>
                </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->

            </div> <!-- .et_pb_row -->
            <div class="et_pb_row et_pb_row_8 timeline-item speak-soon odd et_animated et_pb_gutters1" data-animation-style="fade" data-animation-repeat="" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-intensity="50%" data-animation-starting-opacity="0%" data-animation-speed-curve="ease-in-out">
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_12 col-date  et_pb_css_mix_blend_mode_passthrough">
                <div class="et_pb_module et_pb_text et_pb_text_6 tm-date  et_pb_text_align_left et_pb_bg_layout_light">
                  <div class="et_pb_text_inner"><h4>Desarrollo, desafíos y<br> lecciones aprendidas</h4></div>
                </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->

              <div class="et_pb_column et_pb_column_1_2 et_pb_column_13 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">
                <div class="et_pb_module et_pb_image et_pb_image_1">
                  <span class="et_pb_image_wrap ">

                    <?php if ($bp_seleccionar_opcion_2=='imagen_2'){ ?>
                      <img src="<?php echo $bp_imagen_etapa_2; ?>" alt="" title="">
                    <?php }else{?>
                    <?php echo $bp_imagen_etapa_2;?>
                    <?php }?>

                  </span>
                  </div>
                  <div class="et_pb_module et_pb_text et_pb_text_7 tm-desc  et_pb_text_align_left et_pb_bg_layout_light">
                    <div class="et_pb_text_inner">
                      <p>
                        <span>
                          <?php echo $bp_descripcion_etapa_2;?>
                        </span>
                      </p>
                    </div>
                  </div> <!-- .et_pb_text -->
                </div> <!-- .et_pb_column -->

              </div> <!-- .et_pb_row -->
              <div class="et_pb_row et_pb_row_9 timeline-item tm-soon even et_animated et_pb_gutters1" data-animation-style="fade" data-animation-repeat="" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-intensity="50%" data-animation-starting-opacity="0%" data-animation-speed-curve="ease-in-out">
                <div class="et_pb_column et_pb_column_1_2 et_pb_column_14 col-date  et_pb_css_mix_blend_mode_passthrough">
                  <div class="et_pb_module et_pb_text et_pb_text_8 tm-date  et_pb_text_align_left et_pb_bg_layout_light">
                    <div class="et_pb_text_inner"><h4>Logros y resultados</h4></div>
                  </div> <!-- .et_pb_text -->
                </div> <!-- .et_pb_column -->

                <div class="et_pb_column et_pb_column_1_2 et_pb_column_15 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">
                  <div class="et_pb_module et_pb_video et_pb_video_1">
                    <div class="et_pb_video_box">
                      <!--
                      <video controls="">
                        <source type="video/mp4" src="https://observatorio.minedu.gob.pe/almacenamiento/2019/10/convertido7-para-timeline.mp4">
                        </video>
                      -->
                      <span class="et_pb_image_wrap ">

                        <?php if ($bp_seleccionar_opcion_3=='imagen_3'){ ?>
                          <img src="<?php echo $bp_imagen_etapa_3; ?>" alt="" title="">
                        <?php }else{?>
                        <?php echo $bp_imagen_etapa_3;?>
                        <?php }?>

                      </span>
                      </div>
                    </div>

                    <div class="et_pb_module et_pb_text et_pb_text_9 tm-desc  et_pb_text_align_left et_pb_bg_layout_light">
                      <div class="et_pb_text_inner">
                        <p>
                          <span>
                            <?php echo $bp_descripcion_etapa_3;?>
                          </span>
                        </p>
                        </div>
                      </div> <!-- .et_pb_text -->
                    </div> <!-- .et_pb_column -->
                  </div> <!-- .et_pb_row -->

                  <div class="et_pb_row et_pb_row_10 timeline-item speak-soon odd et_animated et_pb_gutters1" data-animation-style="slideBottom" data-animation-repeat="" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-intensity="20%" data-animation-starting-opacity="0%" data-animation-speed-curve="ease-in-out">
                    <div class="et_pb_column et_pb_column_1_2 et_pb_column_16 col-date  et_pb_css_mix_blend_mode_passthrough">
                      <div class="et_pb_module et_pb_text et_pb_text_10 tm-date  et_pb_text_align_left et_pb_bg_layout_light">
                        <div class="et_pb_text_inner">
                          <h4>Oportunidades de mejora (recomendaciones)</h4>
                        </div>
                      </div> <!-- .et_pb_text -->
                    </div> <!-- .et_pb_column -->
                    <div class="et_pb_column et_pb_column_1_2 et_pb_column_17 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">
                      <div class="et_pb_module et_pb_image et_pb_image_2">
                        <span class="et_pb_image_wrap ">


                          <?php if ($bp_seleccionar_opcion_4=='imagen_4'){ ?>
                            <img src="<?php echo $bp_imagen_etapa_4; ?>" alt="" title="">
                          <?php }else{?>
                          <?php echo $bp_imagen_etapa_4;?>
                          <?php }?>

                        </span>


                        </div>

                        <div class="et_pb_module et_pb_text et_pb_text_11 tm-desc  et_pb_text_align_left et_pb_bg_layout_light">
                          <div class="et_pb_text_inner"><p>
                            <span>
                              <?php echo $bp_descripcion_etapa_4;?>
                            </span>
                          </p>
                          </div>
                        </div> <!-- .et_pb_text -->
                      </div> <!-- .et_pb_column -->

                    </div> <!-- .et_pb_row -->

                    <div class="et_pb_bottom_inside_divider"></div>
                  </div> <!-- .et_pb_section -->
                  <?php //if (is_user_logged_in()){ ?>

                  <?php
                  if (is_user_logged_in()){
                    $class_evidencia="bp_button_evidencia";
                    $class_descargar="bp_update_descargar";
                    $bp_archivo_bp=$bp_archivo_bp;
                    $class_me_interesa="bp_update_datos";
                  }else{
                    $class_evidencia="bp_button_evidencia_out";
                    $class_descargar="bp_descargar_out";
                    //$class_descargar="";
                    $bp_archivo_bp="";
                    $class_me_interesa="bp_update_datos_out";
                  }
                  ?>
                  <div class="et_pb_with_border et_pb_section et_pb_section_4 et_section_regular section_has_divider et_pb_bottom_divider et_pb_top_divider">
                    <div class="et_pb_top_inside_divider"></div>
                    <div class="et_pb_with_border et_pb_row et_pb_row_11 et_pb_gutters1">
                  <!--    <div class="et_pb_column et_pb_column_1_3 et_pb_column_18  et_pb_css_mix_blend_mode_passthrough">

                        <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
                          <a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_pb_bg_layout_light   <?php echo $class_evidencia;?>" href="" data-icon="">EVIDENCIAS</a>
                        </div>
                      </div> --> <!-- .et_pb_column -->

                      <div class="et_pb_column et_pb_column_1_3 et_pb_column_19  et_pb_css_mix_blend_mode_passthrough" style="float:left">
                        <div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_center et_pb_module ">
                          <a class="et_pb_button et_pb_custom_button_icon et_pb_button_1 et_pb_bg_layout_light <?php echo $class_descargar;?>" target="_blank" href="<?php echo $bp_archivo_bp;?>" data-icon="">DESCARGAR</a>
                        </div>
                      </div> <!-- .et_pb_column -->

                      <div class="et_pb_column et_pb_column_1_3 et_pb_column_20  et_pb_css_mix_blend_mode_passthrough et-last-child"  style="float:right">
                        <?php
                          $bp_array=wp_get_current_user();
                          //$auth = SwpmAuth::get_instance();
                          //echo $auth->details->id_perfil;
                          //echo $id_perfil;
                        //  echo $bp_array->ID;
                         ?>
                        <?php //if ($bp_array->ID <> '0'){?>
                        <div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_center et_pb_module " >
                          <a class="et_pb_button et_pb_custom_button_icon et_pb_button_2 et_pb_bg_layout_light  <?php echo $class_me_interesa;?> bp_disable" href="#dato" data-icon="R">ME INTERESA</a>

                          <input type="hidden" id="bp_id_page" name='bp_id_page' value="<?php echo esc_attr(get_the_ID()); ?>" size="50" />
                          <input type="hidden" id="bp_id_user"  name='bp_id_user' value="<?php echo esc_attr($bp_array->ID); ?>" size="50"  />
                          <input type="hidden" id="bp_nro_doc"  name='bp_nro_doc' value="<?php echo esc_attr($bp_array->user_login); ?>" size="50"  />
                          <!--
                          <input type="hidden" id="bp_perfil" value="<?php //echo esc_attr($last_name); ?>" size="50" name="bp_perfil" />
                          -->
                          <div  class="bp_msg_mi" >Gracias por mostrar su interés en la implementación de la Buena Práctica.</div>

                        </div>
                        <?php //} ?>
                      </div> <!-- .et_pb_colum  n -->

                      <div class="bp_listar_evidencias">
                        <button type="button" class="pum-close popmake-close bp_evidencia_close" aria-label="Cerrar">
			                         ×
                        </button>
      									<?php
                          // Creamos un instancia de la clase ZipArchive
                          $zip = new ZipArchive();
                          // Creamos y abrimos un archivo zip temporal
                          $zip->open("miarchivo.zip",ZipArchive::CREATE);

        									foreach ($bp_evidencias as $bp_key) {
                            if(!empty($bp_key)){
                              $bp_get_name=getNameFile($bp_key);
                              $bp_get_extension=getExtensionFile($bp_key);
                              $bp_ruta_imagen=getIconoArchivo(getExtensionFile($bp_get_extension));
                              $partes_ruta = pathinfo($bp_key);
                              //echo $partes_ruta['dirname'];
                              $ruta_file_bp=substr($partes_ruta['dirname'], -8);
                              //$zip->addFile("wp-content/uploads/2019/10/dato-prueba.docx",'dato-prueba.docx');
                              $zip->addFile("wp-content/uploads".$ruta_file_bp."/".$bp_get_name,$bp_get_name);
                              echo '<div class="bp_listar_imagenes"><a class="" href="'.$bp_key.'" download="'.$bp_get_name.'">
                              <img class="alignnone size-full wp-image-53920" src="'.$bp_ruta_imagen.'" alt="" width="62" height="81">
                              <div class="bp_titulo_archivo">'.$bp_get_name.'</div>
                              </a></div>';
                            }
        									}
                          echo '<div class="bp_listar_imagenes"><a class="" href="https://observatorio.minedu.gob.pe/miarchivo.zip" >
                          <img class="alignnone size-full wp-image-53920" src="https://observatorio.minedu.gob.pe/almacenamiento/2019/11/zip_bp.png" alt="" width="75" height="81">
                          <div class="bp_titulo_archivo">ArchivoZip</div>
                          </a></div>';
                          //var_dump($bp_evidencias);
                           //echo getcwd() . "\n";
                          $zip->close();

      									?>

      								</div>


                    </div> <!-- .et_pb_row -->
                  </div> <!-- .et_pb_section -->


                  <div class=" et_pb_section_5 et_section_regular bp_mensaje_out" style="padding-top:30px;">




          					<div class="et_pb_with_border et_pb_row et_pb_row_12">
          				<div class="et_pb_column et_pb_column_4_4 et_pb_column_21  et_pb_css_mix_blend_mode_passthrough et-last-child">


          				<div class="et_pb_module et_pb_text et_pb_text_12  et_pb_text_align_center et_pb_bg_layout_light">


          				<div class="et_pb_text_inner"><p>Para poder acceder a esta opción, primero debe <span style="text-decoration: underline;"><a href="https://observatorio.minedu.gob.pe/membership-login/">iniciar sesión</a></span> en el Observatorio.</p></div>
          			</div> <!-- .et_pb_text -->
          			</div> <!-- .et_pb_column -->


          			</div> <!-- .et_pb_row -->


			             </div>

                  <?php echo do_shortcode('[shortcode_listar_evidencias id='.$bp_post_id.']'); ?>
                  <?php //} ?>

                </div><!-- .et_builder_inner_content -->
              </div><!-- .et-l -->
            </div><!-- #et-boc -->
          </div> <!-- .entry-content -->
        </article> <!-- .et_pb_post -->

        <!-- .et_pb_post -->
        <?php //if (is_user_logged_in()){
            //comments_template()
          ?>

          <div class="bp_comments">

          <?php
              //comments_template();
              if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_postcomments', 'on' ) )
              comments_template( '', true );
          ?>
        <?php //} ?>
          <?php endwhile; ?>
          <?php
            if ( in_array( $page_layout, array( 'et_full_width_page', 'et_no_sidebar' ) ) ) {
              et_pb_portfolio_meta_box();
            }
          ?>
          </div>

<!--
</div>
-->
<!-- #main-content -->

<!--
		</div> --><!-- #et-main-area -->

<!--
	</div>--> <!-- #page-container -->

<?php //WP_CSP::ob_start(); ?>
  <script  type="text/javascript">
				var et_animation_data = [{"class":"et_pb_row_7","style":"slideTop","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"17%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_row_8","style":"fade","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_row_9","style":"fade","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_row_10","style":"slideBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"0%","speed_curve":"ease-in-out"}];
	</script>
<?php //WP_CSP::ob_end_flush(); ?>


<?php //WP_CSP::ob_start(); ?>
  <style>
  .et_pb_section_0.et_pb_section{
  	padding-top: 0px!important ;
  	padding-bottom:  0px!important ;
  	}
</style>
<?php //WP_CSP::ob_end_flush(); ?>
<?php //WP_CSP::ob_start(); ?>
<style  id="et-builder-module-design-55254-cached-inline-styles">
@font-face{font-family:"Gotham Rounded Medium";src:url("https://observatorio.minedu.gob.pe/almacenamiento/et-fonts/Gotham-Rounded-Medium-1.otf") format("opentype")}
@font-face{font-family:"calibri";src:url("https://observatorio.minedu.gob.pe/almacenamiento/et-fonts/calibri-1.ttf") format("truetype")}
@font-face{font-family:"Gotham Rounded Light";src:url("https://observatorio.minedu.gob.pe/almacenamiento/et-fonts/Gotham-Rounded-Light-1.otf") format("opentype")}
@font-face{font-family:"Gotham Rounded Bold";src:url("https://observatorio.minedu.gob.pe/almacenamiento/et-fonts/Gotham-Rounded-Bold-1.otf") format("opentype")}
@font-face{font-family:"calibri bold";src:url("https://observatorio.minedu.gob.pe/almacenamiento/et-fonts/calibri-bold.ttf") format("truetype")}
.et_pb_section_0{z-index:8}.et_pb_row_17.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}
.et_pb_text_18.et_pb_text{color:#b5b5b5!important}.et_pb_text_18{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}
.et_pb_text_18 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_19{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_text_19 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_20{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_button_4_wrapper .et_pb_button_4,.et_pb_button_4_wrapper .et_pb_button_4:hover{padding-bottom:0px!important}.et_pb_button_4_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}body #page-container .et_pb_section .et_pb_button_4{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}body #page-container .et_pb_section .et_pb_button_4:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_4:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}body #page-container .et_pb_section .et_pb_button_4:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_button_4,.et_pb_button_4:after{transition:all 300ms ease 0ms}.et_pb_divider_5{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_text_17{font-family:'Calibri Bold',sans-serif}.et_pb_divider_5:before{border-top-color:#ffd3f5;border-top-style:dotted;border-top-width:2px}.et_pb_row_18.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_image_6{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_text_21{font-family:'Calibri Bold',sans-serif}.et_pb_text_21 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_22.et_pb_text{color:#b5b5b5!important}.et_pb_text_22{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}.et_pb_text_22 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_23{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_text_23 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_24{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_button_5_wrapper .et_pb_button_5,.et_pb_button_5_wrapper .et_pb_button_5:hover{padding-bottom:0px!important}.et_pb_button_5_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}body #page-container .et_pb_section .et_pb_button_5{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}.et_pb_text_17 h1{font-family:'Calibri Bold',sans-serif}.et_pb_image_5{margin-right:-34px!important;text-align:right;margin-right:0}body #page-container .et_pb_section .et_pb_button_5:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}.et_pb_text_14.et_pb_text{color:#b5b5b5!important}.et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label,.et_pb_contact_form_0 .input[type="radio"]:active+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked:active+label i:before{color:#575756}.et_pb_contact_form_0 p .input:focus::-webkit-input-placeholder{color:#575756}.et_pb_contact_form_0 p .input:focus::-moz-placeholder{color:#575756}.et_pb_contact_form_0 p .input:focus:-ms-input-placeholder{color:#575756}.et_pb_contact_form_0 p textarea:focus::-webkit-input-placeholder{color:#575756}.et_pb_contact_form_0 p textarea:focus::-moz-placeholder{color:#575756}.et_pb_contact_form_0 p textarea:focus:-ms-input-placeholder{color:#575756}.et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{transition:background-color 300ms ease 0ms}.et_pb_contact_form_0 .input[type="radio"]:checked+label i:before{background-color:#575756}.et_pb_contact_form_0 .input[type="radio"]:checked:active+label i:before{background-color:#575756}.et_pb_row_14.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_image_4{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_text_13{font-family:'Calibri Bold',sans-serif}.et_pb_text_13 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_14{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}.et_pb_row_16.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_text_14 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_15{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_text_15 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_16{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_button_3_wrapper .et_pb_button_3,.et_pb_button_3_wrapper .et_pb_button_3:hover{padding-bottom:0px!important}.et_pb_button_3_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-9px!important}body #page-container .et_pb_section .et_pb_button_3{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}body #page-container .et_pb_section .et_pb_button_3:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_3:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}body #page-container .et_pb_section .et_pb_button_3:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_button_3,.et_pb_button_3:after{transition:all 300ms ease 0ms}.et_pb_row_15.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_divider_4{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_divider_4:before{border-top-color:#ffd3f5;border-top-style:dotted;border-top-width:2px}body #page-container .et_pb_section .et_pb_button_5:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_5:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_contact_form_0 .input::-moz-placeholder{color:#575756}.et_pb_text_34.et_pb_text{color:#c1afea!important}.et_pb_button_7_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}body #page-container .et_pb_section .et_pb_button_7{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}body #page-container .et_pb_section .et_pb_button_7:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_7:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}body #page-container .et_pb_section .et_pb_button_7:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_button_7,.et_pb_button_7:after{transition:all 300ms ease 0ms}.et_pb_section_6{border-top-color:#f8ae0d}.et_pb_section_6.et_pb_section{padding-top:1.5vw;padding-right:0px;padding-bottom:1.5vw;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px}.et_pb_section_7{border-top-color:#f8ae0d}.et_pb_section_7.et_pb_section{padding-top:1.5vw;padding-right:0px;padding-bottom:1.5vw;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px;background-color:#270054!important}.et_pb_row_23.et_pb_row{padding-top:0px!important;padding-bottom:8px!important;margin-top:-6px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:8px}.et_pb_text_33.et_pb_text{color:#ffffff!important}.et_pb_text_33 p{line-height:20px}.et_pb_text_33{font-family:'calibri',Helvetica,Arial,Lucida,sans-serif;font-size:15px;letter-spacing:0.2px;line-height:20px}.et_pb_text_34 p{line-height:20px}.et_pb_text_32{padding-top:0px!important;margin-top:-33px!important;margin-bottom:3px!important}.et_pb_text_34{font-family:'calibri',Helvetica,Arial,Lucida,sans-serif;font-size:15px;letter-spacing:0.2px;line-height:20px;padding-top:0px!important;margin-top:-31px!important}.et_pb_text_35.et_pb_text{color:#ffffff!important}.et_pb_text_35 p{line-height:20px}.et_pb_text_35{font-family:'calibri',Helvetica,Arial,Lucida,sans-serif;font-size:15px;letter-spacing:0.2px;line-height:20px;margin-top:-22px!important}.et_pb_text_36.et_pb_text{color:#c1afea!important}.et_pb_text_36 p{line-height:20px}.et_pb_text_36{font-family:'calibri',Helvetica,Arial,Lucida,sans-serif;font-size:15px;line-height:20px;padding-top:0px!important;margin-top:-30px!important}.et_pb_blurb_0.et_pb_blurb h4,.et_pb_blurb_0.et_pb_blurb h4 a,.et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header a{color:#ffffff!important}.et_pb_blurb_0.et_pb_blurb p{line-height:20px}.et_pb_blurb_0.et_pb_blurb .et_pb_blurb_description{text-align:left}.et_pb_blurb_0.et_pb_blurb{font-family:'calibri',Helvetica,Arial,Lucida,sans-serif;font-size:15px;color:#ffffff!important;letter-spacing:0.5px;line-height:20px;padding-top:40px!important;margin-left:36px!important}.et_pb_blurb_0 .et-pb-icon{font-size:31px;color:#c1afea}.et_pb_text_37.et_pb_text{color:#ffffff!important}.et_pb_text_37 p{line-height:20px}.et_pb_button_7_wrapper .et_pb_button_7,.et_pb_button_7_wrapper .et_pb_button_7:hover{padding-bottom:0px!important}.et_pb_text_31 h1{font-family:'Calibri Bold',sans-serif}.et_pb_button_5,.et_pb_button_5:after{transition:all 300ms ease 0ms}.et_pb_button_6_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}.et_pb_row_19.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_divider_6{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_divider_6:before{border-top-color:#ffd3f5;border-top-style:dotted;border-top-width:2px}.et_pb_row_20.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_image_7{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_text_25{font-family:'Calibri Bold',sans-serif}.et_pb_text_25 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_26.et_pb_text{color:#b5b5b5!important}.et_pb_text_26{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}.et_pb_text_26 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_27{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_text_27 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_28{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_button_6_wrapper .et_pb_button_6,.et_pb_button_6_wrapper .et_pb_button_6:hover{padding-bottom:0px!important}body #page-container .et_pb_section .et_pb_button_6{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}.et_pb_text_31{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}body #page-container .et_pb_section .et_pb_button_6:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_6:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}body #page-container .et_pb_section .et_pb_button_6:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_button_6,.et_pb_button_6:after{transition:all 300ms ease 0ms}.et_pb_row_21.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_divider_7{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_divider_7:before{border-top-color:#ffd3f5;border-top-style:dotted;border-top-width:2px}.et_pb_row_22.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_image_8{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_text_29{font-family:'Calibri Bold',sans-serif}.et_pb_text_29 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_30.et_pb_text{color:#b5b5b5!important}.et_pb_text_30{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}.et_pb_text_30 h1{font-family:'Calibri Bold',sans-serif}.et_pb_contact_form_0 .input::-ms-input-placeholder{color:#575756}.et_pb_contact_form_0 .input::-webkit-input-placeholder{color:#575756}
.et_pb_section_0.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{
  /*
  background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiM1ZDdjMmEiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);
  */
  <?php //echo $style_background_image;?>
  background-size:100% 100px;
  bottom:0;
  height:100px;
  z-index:10}

  .et_pb_team_member_2.et_pb_team_member h4,.et_pb_team_member_2.et_pb_team_member h1.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h2.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h3.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h5.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h6.et_pb_module_header{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-align:center}.et_pb_text_2{margin-top:20px!important}.et_pb_divider_1{background-color:rgba(0,0,0,0);padding-top:0px;margin-top:-28px!important;width:14%;max-width:100%}.et_pb_divider_1:before{border-top-color:#eab2dd;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_team_member_0.et_pb_team_member h4,.et_pb_team_member_0.et_pb_team_member h1.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h2.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h3.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h5.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h6.et_pb_module_header{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-align:center}.et_pb_team_member_0.et_pb_team_member{text-align:left}.et_pb_team_member_0.et_pb_team_member .et_pb_member_position{font-family:'calibri bold',Helvetica,Arial,Lucida,sans-serif;font-size:16px;color:#62a30d!important;text-align:center}.et_pb_team_member_0 .et_pb_member_social_links a{color:#a9cec7!important}.et_pb_team_member_0 .et_pb_member_social_links .et_pb_font_icon{font-size:18px}.et_pb_team_member_1.et_pb_team_member h4,.et_pb_team_member_1.et_pb_team_member h1.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h2.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h3.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h5.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h6.et_pb_module_header{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-align:center}.et_pb_team_member_1.et_pb_team_member{text-align:left}.et_pb_team_member_1.et_pb_team_member .et_pb_member_position{font-family:'Calibri Bold',sans-serif;font-size:16px;color:#62a30d!important;text-align:center}.et_pb_team_member_1 .et_pb_member_social_links a{color:#a9cec7!important}.et_pb_team_member_1 .et_pb_member_social_links .et_pb_font_icon{font-size:18px}.et_pb_team_member_2.et_pb_team_member{text-align:left}.et_pb_text_2.et_pb_text{color:#ffffff!important}.et_pb_team_member_2.et_pb_team_member .et_pb_member_position{font-family:'Calibri Bold',sans-serif;font-size:16px;color:#62a30d!important;text-align:center}.et_pb_team_member_2 .et_pb_member_social_links a{color:#a9cec7!important}.et_pb_team_member_2 .et_pb_member_social_links .et_pb_font_icon{font-size:18px}.et_pb_section_3.et_pb_section{padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;background-color:#f4f2de!important}.et_pb_row_6.et_pb_row{padding-top:0px!important;padding-bottom:10px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:10px}.et_pb_text_3.et_pb_text{color:#575756!important}.et_pb_text_3{font-family:'Calibri',sans-serif;margin-top:60px!important}.et_pb_text_3 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#7f6ba5!important}.et_pb_divider_2{background-color:rgba(0,0,0,0);padding-top:0px;padding-bottom:0px;margin-top:-28px!important;margin-bottom:0px!important;width:14%;max-width:100%}.et_pb_divider_2:before{border-top-color:#eab2dd;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_text_4.et_pb_text{color:#575756!important}.et_pb_text_4{font-family:'Calibri',sans-serif;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_text_4 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_image_0{width:100%;max-width:100%!important;text-align:left;margin-left:0}.et_pb_text_2 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#7f6ba5!important}.et_pb_row_4.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label,.et_pb_contact_form_0 .input[type="radio"]+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked+label i:before,.et_pb_contact_form_0 .input::placeholder{color:#575756}.et_pb_code_2{padding-top:0px;padding-bottom:0px;margin-top:0px!important;margin-bottom:0px!important;margin-left:148px!important}.et_pb_slide_0{background-color:#7EBEC5}.et_pb_slide_1{background-color:#7EBEC5}.et_pb_slide_2{background-color:#7EBEC5}.et_pb_fullwidth_slider_0.et_pb_slider .et_pb_slide_description .et_pb_slide_title{font-family:'Gotham Rounded Bold',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;font-size:40px!important;color:#9ccc00!important;line-height:40px}.et_pb_fullwidth_slider_0.et_pb_slider.et_pb_module .et_pb_slides .et_pb_slide_content{font-family:'Calibri Bold',sans-serif}.et_pb_fullwidth_slider_0,.et_pb_fullwidth_slider_0 .et_pb_slide{height:700px;max-height:700px}
.et-l .et_pb_section_1_bp{
  padding-top:2px;/*padding-bottom:110px*/;margin-top:0px;margin-bottom:0px;background-color:#5d7c2a!important
}
.et_pb_section_1_bp{z-index:9}
.et_pb_section_1_bp.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{

  background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);

  <?php //echo $style_background_image;?>
  background-size:100% 100px;bottom:0;height:100px;z-index:1
}

.section_has_divider .et_pb_row_0{
  padding-top: 40px!important;
  /*border-top-width:1.5px;*/
  border-bottom-width:1.5px;
  border-style:dashed;


  border-top-color:<?php echo $style_linea?>;
  border-bottom-color:<?php echo $style_linea?>;
}

.section_has_divider.et_pb_row_0.et_pb_row{padding-top:2px!important;padding-bottom:2px!important;padding-top:2px;padding-bottom:2px}

.section_has_divider .et_pb_code_0{padding-top:0px;padding-bottom:0px;margin-top:0px!important;margin-bottom:0px!important}


.et_pb_code_1{padding-top:2px;padding-bottom:0px}.et_pb_row_1.et_pb_row{padding-top:0px!important;margin-top:0px!important;padding-top:0px}.et_pb_section_2.et_pb_section{padding-top:0px;padding-bottom:22px;margin-top:0px;background-color:#ffffff!important}.et_pb_text_0.et_pb_text{color:#ffffff!important}
.et_pb_text_0 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#7f6ba5!important}

.et_pb_text_0{margin-top:30px!important}.et_pb_divider_0{background-color:rgba(0,0,0,0);padding-top:0px;margin-top:-28px!important;width:14%;max-width:100%}.et_pb_divider_0:before{border-top-color:#ff89e1;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_row_2{background-image:linear-gradient(1deg,rgba(137,196,0,0) 0%,rgba(199,226,43,0) 100%);border-bottom-color:#e0f21d}.et_pb_row_2.et_pb_row{padding-top:0px!important;padding-right:0px!important;padding-bottom:0px!important;padding-left:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px}.et_pb_text_1.et_pb_text{color:#ffffff!important}.et_pb_text_1 p{line-height:25px}.et_pb_text_1{font-family:'Calibri',sans-serif;line-height:25px;border-left-color:#e5f45f;padding-top:0px!important;padding-bottom:13px!important;margin-top:-28px!important;margin-bottom:0px!important}.et_pb_row_3{background-image:linear-gradient(1deg,rgba(137,196,0,0) 0%,rgba(199,226,43,0) 100%);border-radius:20px 20px 0 0;overflow:hidden}.et_pb_row_3.et_pb_row{padding-top:22px!important;padding-right:0px!important;padding-bottom:0px!important;padding-left:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:22px;padding-right:0px;padding-bottom:0px;padding-left:0px}.et_pb_video_0{padding-right:0px;padding-left:0px;max-width:100%}.et_pb_video_0 .et_pb_video_overlay_hover:hover{background-color:rgba(0,0,0,.6)}.et_pb_image_0 .et_pb_image_wrap,.et_pb_image_0 img{width:100%}.et_pb_text_5.et_pb_text{color:#575756!important}.et_pb_text_5{font-family:'Calibri',sans-serif}.et_pb_divider_3:before{border-top-color:#eab2dd;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_button_1,.et_pb_button_1:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_2{color:#ffffff!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:15px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:#ffbf0f}body #page-container .et_pb_section .et_pb_button_2:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_2:after{color:rgba(255,255,255,0.43);line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_2:hover{background-image:initial!important;background-color:#f79220!important}.et_pb_button_2,.et_pb_button_2:after{transition:all 300ms ease 0ms}.et_pb_section_5{border-top-width:0px;border-top-color:rgba(0,0,0,0)}.et_pb_section_5.et_pb_section{padding-top:0px;margin-top:0px;background-color:#ffffff!important}.et_pb_row_12.et_pb_row{padding-top:25px!important;margin-top:0px!important;padding-top:25px}.et_pb_text_12.et_pb_text{color:#ffffff!important}.et_pb_text_12 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#7f6ba5!important}.et_pb_text_12{margin-top:40px!important}.et_pb_divider_3{background-color:rgba(0,0,0,0);padding-top:0px;padding-bottom:0px;margin-top:-28px!important;margin-bottom:0px!important;width:14%;max-width:100%}.et_pb_row_13.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_text_5.et_pb_text a{color:#00a99d!important}.et_pb_image_3{margin-top:38px!important;margin-left:27px!important;text-align:left;margin-left:0}.et_pb_contact_form_0.et_pb_contact_form_container h1,.et_pb_contact_form_0.et_pb_contact_form_container h2.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h3.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h4.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h5.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h6.et_pb_contact_main_title{font-family:'Calibri Bold',sans-serif;font-size:21px;color:#575756!important}.et_pb_contact_form_0.et_pb_contact_form_container .input,.et_pb_contact_form_0.et_pb_contact_form_container .input::placeholder,.et_pb_contact_form_0.et_pb_contact_form_container .input[type=checkbox]+label,.et_pb_contact_form_0.et_pb_contact_form_container .input[type=radio]+label{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input::-webkit-input-placeholder{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input::-moz-placeholder{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input:-ms-input-placeholder{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input,.et_pb_contact_form_0.et_pb_contact_form_container .input[type="checkbox"]+label i,.et_pb_contact_form_0.et_pb_contact_form_container .input[type="radio"]+label i{border-width:1px;border-color:#ffd3f5}.et_pb_contact_form_0.et_pb_contact_form_container{margin-left:-160px!important}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{color:#603e00!important;border-width:0px!important;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;background-color:#ffc30f}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:1.6em}body.et_button_custom_icon #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:12px}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{background-image:initial!important;background-color:#ffdb0f!important}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label i,.et_pb_contact_form_0 .input[type="radio"]+label i{background-color:#ffffff}.et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label i,.et_pb_contact_form_0 .input[type="radio"]:active+label i{background-color:#ffffff}body #page-container .et_pb_section .et_pb_button_1:hover{background-image:initial!important;background-color:#f79220!important}body #page-container .et_pb_section .et_pb_button_1:after{color:rgba(255,255,255,0.43);line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_1:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_text_10{font-family:'Calibri',sans-serif;padding-top:0px!important;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_text_6.et_pb_text{color:#575756!important}.et_pb_text_6{font-family:'Calibri',sans-serif;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_text_6 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_image_1{width:100%;max-width:100%!important;text-align:left;margin-left:0}.et_pb_image_1 .et_pb_image_wrap,.et_pb_image_1 img{width:100%}.et_pb_text_7.et_pb_text{color:#575756!important}.et_pb_text_7{font-family:'Calibri',sans-serif}.et_pb_text_8.et_pb_text{color:#575756!important}.et_pb_text_8{font-family:'Calibri',sans-serif;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_text_8 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_video_1 .et_pb_video_overlay_hover:hover{background-color:rgba(0,0,0,0.6)}.et_pb_text_9.et_pb_text{color:#575756!important}.et_pb_text_9{font-family:'Calibri',sans-serif}body #page-container .et_pb_section .et_pb_button_1{color:#ffffff!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:15px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:#ffbf0f}.et_pb_text_10.et_pb_text{color:#575756!important}.et_pb_text_10 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_image_2{width:100%;max-width:100%!important;text-align:left;margin-left:0}.et_pb_button_0,.et_pb_button_0:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_0:hover{background-image:initial!important;background-color:#f79220!important}body #page-container .et_pb_section .et_pb_button_0:after{color:rgba(255,255,255,0.43);line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_0{color:#ffffff!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:15px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:#ffbf0f}.et_pb_row_11.et_pb_row{padding-top:10px!important;padding-bottom:10px!important;margin-top:-28px!important;margin-bottom:0px!important;padding-top:10px;padding-bottom:10px}.et_pb_row_11{border-top-style:dotted;border-top-color:#e6b3e3}
.et_pb_section_4.section_has_divider.et_pb_top_divider .et_pb_top_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2Y0ZjJkZSI+PC9nPjwvc3ZnPg==);background-size:100% 20px;top:0;height:20px;z-index:1}.et_pb_section_4.et_pb_section{padding-top:0px;padding-bottom:0px;margin-top:0px;margin-bottom:0px}
.et_pb_section_4{
  border-top-width:1px;
  <?php echo   $linea_separacion;?>

  }.et_pb_text_11{font-family:'Calibri',sans-serif}.et_pb_text_11.et_pb_text{color:#575756!important}.et_pb_image_2 .et_pb_image_wrap,.et_pb_image_2 img{width:100%}.et_pb_text_37{font-family:'calibri',Helvetica,Arial,Lucida,sans-serif;font-size:15px;letter-spacing:0.5px;line-height:20px;padding-top:40px!important}

.et_pb_slider .et_pb_slide_2{
  background-blend-mode:multiply;
  background-color:initial;
  /*
  background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto2.jpg),linear-gradient(180deg,#ffffff 15%,rgba(0,0,0,0.93) 100%);
*/
  <?php if(!empty($bp_slider_imagen_2)){ ?>
  background-image:url(<?php echo $bp_slider_imagen_2;?>),linear-gradient(180deg,#ffffff 15%,rgba(0,0,0,0.93) 100%);
  <?php } ?>
}

.et_pb_slider .et_pb_slide_1{
  background-blend-mode:multiply;
  background-color:initial;
  /*
  background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto1.jpg),linear-gradient(180deg,#ffffff 15%,rgba(0,0,0,0.93) 100%);
  */
  <?php if(!empty($bp_slider_imagen_1)){ ?>
    background-image:url(<?php echo $bp_slider_imagen_1;?>),linear-gradient(180deg,#ffffff 15%,rgba(0,0,0,0.93) 100%);
  <?php } ?>
}


.et_pb_slider .et_pb_slide_0{
  background-blend-mode:multiply;
  background-color:initial;
  /*
  background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto1.jpg),linear-gradient(180deg,#ffffff 15%,rgba(0,0,0,0.93) 100%);
  */
  <?php if(!empty($bp_slider_imagen_0)){ ?>
    background-image:url(<?php echo $bp_slider_imagen_0;?>),linear-gradient(180deg,#ffffff 15%,rgba(0,0,0,0.93) 100%);
  <?php } ?>
}


@media only screen and (min-width:101px){.et_pb_fullwidth_slider_0.et_pb_slider .et_pb_slide_description{margin-bottom:-22%}}@media only screen and (max-width:100px){.et_pb_text_37{padding-top:0px!important}body #page-container .et_pb_section .et_pb_button_4:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_5:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_5:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_5{padding-left:0.7em;padding-right:2em}.et_pb_image_6{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_4:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_4:before{display:none}body #page-container .et_pb_section .et_pb_button_4:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_4{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_5:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_image_5{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_3:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_3:before{display:none}body #page-container .et_pb_section .et_pb_button_3:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_3:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_3{padding-left:0.7em;padding-right:2em}.et_pb_image_4{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}body #page-container .et_pb_section .et_pb_button_5:before{display:none}.et_pb_image_7{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_7:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_blurb_0.et_pb_blurb{padding-top:0px!important;padding-right:22vw!important;padding-left:22vw!important}.et_pb_blurb_0.et_pb_blurb .et_pb_blurb_description{text-align:center}.et_pb_text_35{margin-top:0px!important}.et_pb_section_7.et_pb_section{padding-top:5vw;padding-bottom:5vw}.et_pb_section_7{border-top-color:#f8ae0d}.et_pb_section_6.et_pb_section{padding-top:5vw;padding-bottom:5vw}.et_pb_section_6{border-top-color:#f8ae0d}body #page-container .et_pb_section .et_pb_button_7:before{display:none}body #page-container .et_pb_section .et_pb_button_6{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_7:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_7:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_7{padding-left:0.7em;padding-right:2em}.et_pb_image_8{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_6:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_6:before{display:none}body #page-container .et_pb_section .et_pb_button_6:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_6:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}.et_pb_row_11{border-top-style:dotted;border-top-color:#e6b3e3}.et_pb_text_2{margin-top:60px!important}.et_pb_section_4{border-top-width:1px;border-top-color:#a2d35b;border-bottom-color:#f9f2f2}.et_pb_image_2{text-align:center;margin-left:auto;margin-right:auto}.et_pb_image_1{text-align:center;margin-left:auto;margin-right:auto}.et_pb_image_0{text-align:center;margin-left:auto;margin-right:auto}.et_pb_text_4{margin-bottom:-12px!important}.et_pb_text_3{margin-top:35px!important}
.et_pb_section_3.section_has_divider.et_pb_bottom_divider
.et_pb_bottom_inside_divider{
  /*
  background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI0OXB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2ZmOWI5YiI+PC9nPjwvc3ZnPg==);
  */
  <?php echo $style_background_image;?>
  background-size:100% 49px;bottom:0;height:49px;z-index:1;transform:rotateY(180deg) rotateX(180deg)}.et_pb_row_3.et_pb_row{padding-right:0px!important;padding-left:0px!important;padding-right:0px!important;padding-left:0px!important}.et_pb_button_0_wrapper{margin-bottom:12px!important}.et_pb_text_1{border-left-color:#e5f45f}.et_pb_row_2.et_pb_row{padding-right:0px!important;padding-left:0px!important;padding-right:0px!important;padding-left:0px!important}.et_pb_row_2{border-bottom-color:#e0f21d}.et_pb_text_0{margin-top:30px!important}.et_pb_text_0 h1{font-size:40px}
/*
.et_pb_row_0{border-top-width:1.5px;border-bottom-width:1.5px;border-top-color:#fff130;border-bottom-color:#fff130}
*/
.et_pb_section_1_bp.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{
  /*
  background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI3N3B4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTAgNDcuNDRMMTcwIDBsNjI2LjQ4IDk0Ljg5TDExMTAgODcuMTFsMTcwLTM5LjY3VjE0MEgwVjQ3LjQ0eiIgZmlsbC1vcGFjaXR5PSIuNSIvPjxwYXRoIGQ9Ik0wIDkwLjcybDE0MC0yOC4yOCAzMTUuNTIgMjQuMTRMNzk2LjQ4IDY1LjggMTE0MCAxMDQuODlsMTQwLTE0LjE3VjE0MEgwVjkwLjcyeiIvPjwvZz48L3N2Zz4=);
  */
  <?php echo $style_background_image;?>
  background-size:100% 77px;bottom:0;height:77px;z-index:1;transform:rotateY(0) rotateX(0)}.et_pb_image_3{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_0{padding-left:0.7em;padding-right:2em}.et_pb_fullwidth_slider_0.et_pb_slider .et_pb_slide_description .et_pb_slide_title{font-size:30px!important;line-height:30px}body #page-container .et_pb_section .et_pb_button_1:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_text_12{margin-top:0px!important}.et_pb_section_5{border-top-width:0px;border-top-color:rgba(0,0,0,0)}body #page-container .et_pb_section .et_pb_button_2:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_2:before{display:none}body #page-container .et_pb_section .et_pb_button_2:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_2:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_2{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_1:before{display:none}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_1:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_1:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_1{padding-left:0.7em;padding-right:2em}.et_pb_button_1_wrapper{margin-bottom:12px!important}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_0:before{display:none}body #page-container .et_pb_section .et_pb_button_0:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}.et_pb_fullwidth_slider_0,.et_pb_fullwidth_slider_0 .et_pb_slide{height:500px}
    .et_pb_slider .et_pb_slide_1{
      background-color:initial;background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto1.jpg),linear-gradient(180deg,#ffffff 29%,rgba(0,0,0,0.93) 100%)
    }
    .et_pb_slider .et_pb_slide_2{
      background-color:initial;background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto2.jpg),linear-gradient(180deg,#ffffff 29%,rgba(0,0,0,0.93) 100%)
    }
    .et_pb_slider .et_pb_slide_0{
      background-color:initial;background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto1.jpg),linear-gradient(180deg,#ffffff 29%,rgba(0,0,0,0.93) 100%)
    }
  }@media only screen and (min-width:78px) and (max-width:98px){.et_pb_fullwidth_slider_0.et_pb_slider .et_pb_slide_description{margin-bottom:-35%}}@media only screen and (max-width:76px){.et_pb_blurb_0.et_pb_blurb{padding-right:12vw!important;padding-left:12vw!important}.et_pb_image_5{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_5{padding-left:0.7em;padding-right:2em}.et_pb_image_6{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_4:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_4:before{display:none}body #page-container .et_pb_section .et_pb_button_4:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_4:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_4{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_3:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_5:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_3:before{display:none}body #page-container .et_pb_section .et_pb_button_3:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_3:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_3{padding-left:0.7em;padding-right:2em}.et_pb_image_4{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_button_5:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_5:before{display:none}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_button_7:hover{padding-left:0.7em;padding-right:2em}.et_pb_blurb_0.et_pb_blurb .et_pb_blurb_description{text-align:center}.et_pb_section_7.et_pb_section{padding-top:8vw;padding-bottom:8vw}.et_pb_section_7{border-top-color:#f8ae0d}.et_pb_section_6{border-top-color:#f8ae0d}body #page-container .et_pb_section .et_pb_button_7:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_7:before{display:none}body #page-container .et_pb_section .et_pb_button_7:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_7{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_5:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_image_8{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_6:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_6:before{display:none}body #page-container .et_pb_section .et_pb_button_6:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_6:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_6{padding-left:0.7em;padding-right:2em}.et_pb_image_7{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}.et_pb_image_3{margin-left:auto;margin-right:auto}.et_pb_text_8{margin-bottom:-12px!important}.et_pb_text_2{margin-top:30px!important}.et_pb_image_1{margin-left:auto;margin-right:auto}.et_pb_text_6{margin-bottom:-12px!important}.et_pb_image_0{margin-left:auto;margin-right:auto}.et_pb_text_3 h1{font-size:30px}
  .et_pb_section_3.section_has_divider.et_pb_bottom_divider.et_pb_bottom_inside_divider{
  /*
  background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI0OXB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2ZmOWI5YiI+PC9nPjwvc3ZnPg==);
  */
  <?php echo $style_background_image;?>
  background-size:100% 49px;bottom:0;height:49px;z-index:1;transform:rotateY(180deg) rotateX(180deg)}.et_pb_row_5.et_pb_row{margin-top:0px!important}.et_pb_text_2 h1{font-size:30px}.et_pb_image_2{margin-left:auto;margin-right:auto}.et_pb_text_1{border-left-color:#e5f45f;padding-bottom:20px!important}.et_pb_row_2{border-bottom-color:#e0f21d}.et_pb_text_0 h1{font-size:30px}.et_pb_row_1.et_pb_row{margin-top:0px!important}
/*
.et_pb_row_0{border-top-width:1.5px;border-bottom-width:1.5px;border-top-color:#fff130;border-bottom-color:#fff130}
*/
.et_pb_section_1_bp.section_has_divider.et_pb_bottom_divider.et_pb_bottom_inside_divider{
  /*
  background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI2OHB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTAgNDcuNDRMMTcwIDBsNjI2LjQ4IDk0Ljg5TDExMTAgODcuMTFsMTcwLTM5LjY3VjE0MEgwVjQ3LjQ0eiIgZmlsbC1vcGFjaXR5PSIuNSIvPjxwYXRoIGQ9Ik0wIDkwLjcybDE0MC0yOC4yOCAzMTUuNTIgMjQuMTRMNzk2LjQ4IDY1LjggMTE0MCAxMDQuODlsMTQwLTE0LjE3VjE0MEgwVjkwLjcyeiIvPjwvZz48L3N2Zz4=);
*/
<?php echo $style_background_image;?>
  background-size:100% 68px;bottom:0;height:68px;z-index:1;transform:rotateY(0) rotateX(0)}.et_pb_fullwidth_slider_0.et_pb_slider .et_pb_slide_description{margin-bottom:-22%}.et_pb_section_5{border-top-width:0px;border-top-color:rgba(0,0,0,0)}.et_pb_section_4{border-top-width:1px;border-top-color:#a2d35b;border-bottom-color:#f9f2f2}.et_pb_fullwidth_slider_0.et_pb_slider .et_pb_slide_description .et_pb_slide_title{font-size:20px!important;line-height:20px}body #page-container .et_pb_section .et_pb_button_1:before{display:none}body #page-container .et_pb_section .et_pb_button_2:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_2:before{display:none}body #page-container .et_pb_section .et_pb_button_2:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_2:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_2{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_1:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_1:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}.et_pb_row_11{border-top-style:dotted;border-top-color:#e6b3e3}body #page-container .et_pb_section .et_pb_button_1:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_1{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_0:before{display:none}body #page-container .et_pb_section .et_pb_button_0:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0{padding-left:0.7em;padding-right:2em}.et_pb_fullwidth_slider_0,.et_pb_fullwidth_slider_0 .et_pb_slide{height:300px}

  .et_pb_slider .et_pb_slide_1{
    background-color:initial;background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto1.jpg),linear-gradient(180deg,#ffffff 11%,rgba(0,0,0,0.93) 100%)
    }
  .et_pb_slider .et_pb_slide_2{
    background-color:initial;background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto2.jpg),linear-gradient(180deg,#ffffff 11%,rgba(0,0,0,0.93) 100%)
    }
  .et_pb_slider .et_pb_slide_0{
    background-color:initial;background-image:url(https://observatorio.minedu.gob.pe/almacenamiento/2019/10/buenapractica-portada-foto1.jpg),linear-gradient(180deg,#ffffff 11%,rgba(0,0,0,0.93) 100%)
    }

  }
  #menu .menu-item-type-custom>a{color:#38edcf}.ds-timeline .timeline-item{display:flex;flex-wrap:wrap;align-items:center;margin-bottom:0;padding:0 0 50px;position:relative}
  .ds-timeline .timeline-item:after{
    content:'';position:absolute;width:3px;height:100%;visibility:visible;top:0;left:0;margin-left:-1.5px;
    /*background-color:#a3d35b
    */
    <?php echo $linea_tiempo;?>
  }
    .ds-timeline .timeline-item .col-date{position:relative;padding-left:40px}
  .ds-timeline .timeline-item .col-date:before{
    content:'';position:absolute;left:-18px;top:50%;
    /*background-color:#43940d;*/
    <?php echo $linea_circulo;?>
    width:26px;height:26px;margin-top:-35px;border:5px solid #f4f2de;-webkit-border-radius:50%;-moz-border-radius:50%;-ms-border-radius:50%;border-radius:50%

    }.ds-timeline .timeline-item .col-details{padding-left:40px;-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 2px 2px 0 rgba(0,0,0,0.12) 0.5em 0px 0px #000000;-moz-box-shadow:0 2px 2px 0 rgba(0,0,0,0.12) 0.5em 0px 0px #000000;box-shadow:0 2px 2px 0 rgba(0,0,0,0.12) 0.5em 0px 0px #000000}.ds-timeline .tm-date{font-size:18px}.ds-timeline .tm-date h4{font-weight:600;font-size:23px;margin:8px 0}.ds-timeline .tm-video .et_pb_video_play{font-size:50px;line-height:56px;margin:-28px auto auto -28px}.ds-timeline .tm-video .et_pb_video_play:before{content:'\45';-webkit-border-radius:50%;-moz-border-radius:50%;-ms-border-radius:50%;border-radius:50%;-webkit-transition:all 0.3s ease-in;-moz-transition:all 0.3s ease-in;-o-transition:all 0.3s ease-in;transition:all 0.3s ease-in;-webkit-transition-delay:0;-moz-transition-delay:0;-ms-transition-delay:0;-o-transition-delay:0;transition-delay:0;display:inline-block;width:56px;height:56px;top:50%;left:50%;border:2px solid #fff;background-color:rgba(255,255,255,0);color:#fff;text-align:center}.ds-timeline .tm-video:hover .et_pb_video_play:before{background:#00a99d;border-color:#00a99d;-webkit-transform:scale(1.25);-moz-transform:scale(1.25);-ms-transform:scale(1.25);-o-transform:scale(1.25);transform:scale(1.25)}.ds-timeline .tm-desc{padding:30px;background-color:#fff}.ds-timeline .tm-title{border-bottom:2px solid #f4f4f4;border-left:3px solid #00a99d;background-color:#fff;padding:15px 30px}.ds-timeline .tm-title h3{padding:0;line-height:1.1;font-weight:600}.ds-timeline .tm-countdown{background-color:transparent!important;padding:0;margin-bottom:10px!important}.ds-timeline .tm-countdown .et_pb_countdown_timer_container{text-align:left;display:flex}.ds-timeline .tm-countdown .section.values{width:auto;max-width:95px;flex-grow:1;background-color:#fff;color:#1d1c21;padding:12px 8px;border-top:3px solid #00a99d;margin-right:10px}.ds-timeline .tm-countdown .section.values:last-child{margin-right:0}.ds-timeline .tm-countdown .section.values .value{font-weight:800;font-size:27px!important;line-height:1!important}.ds-timeline .tm-countdown .section.values .label{text-transform:uppercase}.ds-timeline .tm-countdown .section.sep{display:none}@media (min-width:980px){
      .ds-timeline .timeline-item:after{left:50%!important}.ds-timeline .timeline-item:first-child:after{height:50%;top:50%}.ds-timeline .timeline-item:last-child:after{height:50%}.ds-timeline .timeline-item.even{flex-direction:row-reverse}.ds-timeline .timeline-item.even .col-details{padding-right:40px;padding-left:0!important}.ds-timeline .timeline-item.odd .col-date{padding-right:40px;padding-left:0!important}.ds-timeline .timeline-item.odd .col-date:before{left:auto;right:-18px}.ds-timeline .timeline-item.odd .et_pb_button_module_wrapper{text-align:right}.ds-timeline .timeline-item.odd .tm-date{text-align:right}.ds-timeline .timeline-item.odd .tm-countdown .et_pb_countdown_timer_container{justify-content:flex-end}}
.ntrk-carousel-wrap {
   margin-bottom: -60px !important;
    /* margin-bottom: 30px; */
}
.ntrk-text_institucion{
<?php echo $titulo_equipo;?>
}
#top-menu li.menu-item-buenas-practicas>a{
  color: #f9d800!important;
}
/* estilos de footer */
.et_pb_row .et_pb_column.et-last-child, .et_pb_row .et_pb_column:last-child, .et_pb_row_inner .et_pb_column.et-last-child, .et_pb_row_inner .et_pb_column:last-child {
    margin-right: 0!important;
    margin-top: 30px;
}
.et_pb_column_1_3 .et_pb_module .et_pb_text_inner p{
  margin-top: -25px!important;
}
.et_pb_column_1_3 .et_pb_text_4 .et_pb_text_inner p {
    margin-top: 5px!important;
}
.et_pb_css_mix_blend_mode_passthrough .et_pb_text_1 .et_pb_text_inner p{
  padding-bottom: 20px!important;
}
.et_pb_row .et_pb_column.et-last-child, .et_pb_row .et_pb_column:last-child, .et_pb_row_inner .et_pb_column.et-last-child, .et_pb_row_inner .et_pb_column:last-child {
    margin-right: 0!important;
    margin-top:0px!important;
}

.pum-close {
    position: absolute;
    height: 40px;
    width: 40px;
    left: auto;
    right: -13px;
    bottom: auto;
    top: -13px;
    padding: 0px;
    color: #ffffff;
    font-family: Arial;
    font-weight: 100;
    font-size: 26px;
    line-height: 24px;
    border: 1px solid #f79c25;
    border-radius: 26px;
    box-shadow: 0px 0px 15px 1px rgba( 2, 2, 2, 0.25 );
    text-shadow: 0px 0px 0px rgba( 0, 0, 0, 0.23 );
    background-color: rgba( 247, 156, 37, 1.00 );
}
.et_pb_row_12 {
    border-top-width: 2px;
    border-top-style: dashed;
    border-bottom-style: dashed;
    border-top-color: #ffb5c2;
    border-bottom-color: #ff9b0f;
}
.et_pb_row_12.et_pb_row {
    padding-top: 0.5vw!important;
    padding-right: 0.5vw!important;
    padding-bottom: 0.5vw!important;
    padding-left: 0.5vw!important;
    padding-top: 0.5vw;
    padding-right: 0.5vw;
    padding-bottom: 0.5vw;
    padding-left: 0.5vw;
}
.et_pb_row_12 {
    max-width: 900px;
}
.et_pb_text_12.et_pb_text {
    color: #ba67c6!important;
}

.et_pb_text_12 {
    font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
    line-height: 22px;
    margin-top: -4px!important;
}

.et_pb_slide_title_bp {
    font-family: 'Gotham Rounded Bold',Helvetica,Arial,Lucida,sans-serif;
    text-transform: uppercase;
    font-size: 40px!important;
    color: #9ccc00!important;
    line-height: 40px;
}

.et_pb_section_4 {
    border-top-width: inherit!important;
    border-top-color: inherit!important;
    border-bottom-color: #f9f2f2!important;
    background-color: #f4f2de!important;
    padding-bottom: 10px!important;
}
.slick-track{
  padding-bottom: 30px!important;
}
</style>
<?php


get_footer();

?>
<style>

.et_pb_section_1.et_pb_section{
  /*background-color:#5d7c2a!important*/
  }
  /*
.et_pb_section_0.et_pb_section{
	padding-top: 0px!important ;
	padding-bottom:  0px!important ;
	}
	.et_pb_text_0 {
    padding-bottom: 30px!important;
	}
	.et_pb_text_2 {
    padding-top: 40px!important;
	}
	.et_pb_text_3 {
    padding-top: 30px!important;
	}
*/
</style>
<?php //WP_CSP::ob_end_flush(); ?>
