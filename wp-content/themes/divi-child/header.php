<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();

	//EDITADO PROYECTO_BP 2019-10-16
	//AGREGADO PROYECTO_BP 2019-10-16
	$auth = SwpmAuth::get_instance();
	//print_r(var_dump($auth));
	$bp_estado=$auth->userData->account_state;
	$bp_first_name=$auth->userData->first_name;
 	$array_bp = explode(' ', trim($bp_first_name));
  /*
	var_dump($auth);
  var_dump($array_bp);
  $array_bp[0]
  */
	//$auth->isLoggedIn;
	//TERMINA PROYECTO_BP
  echo $_GET["swpm-logout"];
  /*
  if($_GET["swpm-logout"]=='true'){
    header("Location: ".site_url()."");
    exit;
  }
  */
?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php //WP_CSP::ob_start(); ?>
	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159908999-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-159908999-1');
</script>	
	
		<?php //WP_CSP::ob_end_flush(); ?>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
	$product_tour_enabled = et_builder_is_product_tour_enabled();
	$page_container_style = $product_tour_enabled ? ' style="padding-top: 0px;"' : ''; ?>
	<div id="page-container"<?php echo et_core_intentionally_unescaped( $page_container_style, 'fixed_string' ); ?>>
<?php
	if ( $product_tour_enabled || is_page_template( 'page-template-blank.php' ) ) {
		return;
	}

	$et_secondary_nav_items = et_divi_get_top_nav_items();

	$et_phone_number = $et_secondary_nav_items->phone_number;

	$et_email = $et_secondary_nav_items->email;

	$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;

	$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;

	$et_secondary_nav = $et_secondary_nav_items->secondary_nav;

	$et_top_info_defined = $et_secondary_nav_items->top_info_defined;

	$et_slide_header = 'slide' === et_get_option( 'header_style', 'left' ) || 'fullscreen' === et_get_option( 'header_style', 'left' ) ? true : false;
?>

	<?php if ( $et_top_info_defined && ! $et_slide_header || is_customize_preview() ) : ?>
		<?php ob_start(); ?>



		<div id="top-header"<?php echo $et_top_info_defined ? '' : 'style="display: none;background: #ffffff!important;'; ?>>
			<div class="container clearfix">

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo et_core_esc_previously( et_sanitize_html_input_text( $et_phone_number ) ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
				<?php endif; ?>

				<?php
				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				} ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>
				<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
				<div id="et-secondary-menu">
				<?php
					if ( ! $et_contact_info_defined && true === $show_header_social_icons ) {
						get_template_part( 'includes/social_icons', 'header' );
					} else if ( $et_contact_info_defined && true === $show_header_social_icons ) {
						ob_start();

						get_template_part( 'includes/social_icons', 'header' );

						$duplicate_social_icons = ob_get_contents();

						ob_end_clean();

						printf(
							'<div class="et_duplicate_social_icons">
								%1$s
							</div>',
							et_core_esc_previously( $duplicate_social_icons )
						);
					}
          include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
          //if($_SERVER['REQUEST_URI'] == '/red/' && is_plugin_active('buddypress/bp-loader.php') && is_user_logged_in()){
            global $current_user;
            $url_link=get_bloginfo('url') . '/members/'. $current_user->user_login . '/profile';
            //wp_redirect( get_bloginfo('url') . '/members/'. $current_user->user_login . '/');
          //  exit();
          //}
					// AGREGADO PROYECTO_BP 2019-10-16
					if($bp_estado<>''){

						echo '<ul id="et-secondary-nav" class="menu" style="color: #bd99e5;margin-top: -1px;font-size:15px">';
            /*
						echo '<li style="font-weight:normal!important;margin-right: 5px;">Hola, '. $array_bp[0].' </li><li style="top:7px"> <a href="#" class="submit user-submit" >'.get_avatar( bbp_get_current_user_id(), '25' ).'</a>
            <ul >
              <li style="font-size:13px!important">Mi Perfil</li>
              <li style="font-size:13px!important">Cerrar sesión</li>
            </ul>
            </li></ul>';
            */

						/*
            echo '<li style="font-weight:normal!important;left: -20px;top: 2px;color: #bd99e5;"><span class="secondary-mensaje">Hola, '. $array_bp[0].'
            </span><a href="#" class="submit user-submit" style="position:absolute;width:100%;margin-left:-80px;margin-top:-8px;border-radius: 50%!important;">'.get_avatar( bbp_get_current_user_id(), '25' ).'

						<img alt="" src="https://observatorio.minedu.gob.pe/almacenamiento/group-avatars/uploads/2019/12/ico-usuario-cabecera.png" srcset="" class="avatar avatar-25 photo" height="25" width="25">
						</a>
            <ul style="left: 17px;top: 25px;">
              <li style="font-size:13px!important;border-top-style: solid;border-top-width: 2px;border-top-color: #f9b20c;"><a href="'.$url_link.'">Mi Perfil</a></li>
              <li style="font-size:13px!important"><a href="https://observatorio.minedu.gob.pe/?swpm-logout=true">Cerrar sesión</a></li>
            </ul>
            </li></ul>';
						*/
						/*
						if (trim(bbp_get_current_user_id())<>'92'){
							$dato_i=get_avatar( bbp_get_current_user_id(), '25' );
						}else{
							$dato_i='<img alt="" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/12/ico-usuario-cabecera.png" srcset="" class="avatar avatar-25 photo" height="25" width="25">';
						}
						*/
						//$dato_i='<img alt="" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/12/ico-usuario-cabecera.png" srcset="" class="avatar avatar-25 photo bp_icono" height="25" width="25">';

						/*
						echo '<li style="font-weight:normal!important;left: -20px;top: 2px;color: #bd99e5;"><span class="secondary-mensaje">Hola, '. $array_bp[0].'
						</span><a href="#" class="submit user-submit" style="position:absolute;width:100%;margin-left:-60px;margin-top:-8px;border-radius: 50%!important;">

						'.$dato_i.'
						</a>
						<ul style="left: 17px;top: 25px;">
							<li style="font-size:13px!important;border-top-style: solid;border-top-width: 2px;border-top-color: #f9b20c;"><a href="'.$url_link.'">Mi Perfil</a></li>
							<li style="font-size:13px!important"><a href="https://observatorio.minedu.gob.pe/?swpm-logout=true">Cerrar sesión</a></li>
						</ul>
						</li></ul>';
						*/
						/*
						echo '<a href="#" class="submit user-submit" style="position:absolute;width:100%;margin-left:-60px;margin-top:-8px;border-radius: 50%!important;">

						'.get_avatar_bp( bbp_get_current_user_id(), '25').'
						</a>';
						*/
						echo '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51637" style="padding-top: 4px;">
  					<span style="margin-right: -5px;">Hola, '. $array_bp[0].'</span>
						<span style="padding-left:5px">'.get_avatar_bp( bbp_get_current_user_id(), '25').'</span>
						<ul style="left: 20px;top: 28px;">
							<li style="font-size:13px!important;border-top-style: solid;border-top-width: 2px;border-top-color: #f9b20c;margin-top: -5px;">
							<a href="'.$url_link.'" style="margin-top: -5px;">Mi Perfil</a></li>
							<li style="font-size:13px!important">
							<a href="https://observatorio.minedu.gob.pe/?swpm-logout=true" style="margin-top: -5px;">Cerrar sesión</a></li>
						</ul>
						</li>
						</ul>
						';
						//echo get_avatar(92);
            /*
            <ul>
              <li>Mi Perfil</li>
              <li>Cerrar sesión</li>
            </ul>
						'<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51637">
  					<a href="'.$url_link.'">Mi Perfil</a>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51637">
  					<a href="?swpm-logout=true">Cerrar sesión</a>
						</li></ul>
						';
            */
					}else{

						if ( '' !== $et_secondary_nav ) {
							echo et_core_esc_wp( $et_secondary_nav );
						}
					}
					//Comentado para una proxima version
					//	echo '	<a class="et_pb_button et_pb_button_bp prueba_popup" href="#" style="color: #ffffff !important;font-family: Gotham Rounded Medium,Helvetica,Arial,Lucida,sans-serif!important;top: -2px;padding-left:10px;">SUSCRÍBETE</a>';
					//TERMINA PROYECTO_BP

					et_show_cart_total();
				?>
				<!-- AGREGADO PROYECTO_BP 2019-10-16 -->

				<?php //get_search_form();?>



				<!--//TERMINA-->
				</div> <!-- #et-secondary-menu -->




			</div> <!-- .container -->
			<div class="menu_3_bp">
				<div >
					<span class="menu_imagen_1"   style="float: left;">
						<a href="https://www.gob.pe/minedu" target="_blank">
							<img src="https://observatorio.minedu.gob.pe/almacenamiento/2019/10/logo-minedu-1.jpg" alt="" title="">
						</a>
					</span>
				</div>
				<div >
						<span class="menu_imagen_2">
							<a href="https://observatorio.minedu.gob.pe/">
								<img src="https://observatorio.minedu.gob.pe/almacenamiento/2019/11/lettering-observatorio.png" alt="" title="" style="margin-top: 20px;padding-left: 170px;">
							</a>
						</span>
				</div>
			</div>
			<!--
			<div class="et_pb_row et_pb_row_1 et_pb_gutters1">
							<div class="et_pb_column et_pb_column_1_2 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">


							<div class="et_pb_module et_pb_image et_pb_image_0" style="margin-left: -360px;">


							<span class="et_pb_image_wrap "><img src="https://observatorio.minedu.gob.pe/almacenamiento/2019/10/logo-minedu-1.jpg" alt="" title=""></span>
						</div>
						</div>
						<div class="et_pb_column et_pb_column_1_2 et_pb_column_4  et_pb_css_mix_blend_mode_passthrough et-last-child">


							<div class="et_pb_module et_pb_image et_pb_image_1">


							<span class="et_pb_image_wrap "><img src="https://observatorio.minedu.gob.pe/almacenamiento/2019/11/lettering-observatorio.png" alt="" title=""></span>
						</div>
						</div>

						</div>
					-->


		</div> <!-- #top-header -->




	<?php
		$top_header = ob_get_clean();

		/**
		 * Filters the HTML output for the top header.
		 *
		 * @since 3.10
		 *
		 * @param string $top_header
		 */
		echo et_core_intentionally_unescaped( apply_filters( 'et_html_top_header', $top_header ), 'html' );
	?>
	<?php endif; // true ==== $et_top_info_defined ?>

	<?php if ( $et_slide_header || is_customize_preview() ) : ?>
		<?php ob_start(); ?>



		<div class="et_slide_in_menu_container">
			<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) || is_customize_preview() ) { ?>
				<span class="mobile_menu_bar et_toggle_fullscreen_menu"></span>
			<?php } ?>

			<?php
				if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
					<div class="et_slide_menu_top">

					<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
						<div class="et_pb_top_menu_inner">
					<?php } ?>
			<?php }

				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				}

				et_show_cart_total();
			?>
			<?php if ( false !== et_get_option( 'show_search_icon', true ) || is_customize_preview() ) : ?>
				<?php if ( 'fullscreen' !== et_get_option( 'header_style', 'left' ) ) { ?>
					<div class="clear"></div>
				<?php } ?>
				<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);
					?>
					<button type="submit" id="searchsubmit_header"></button>
				</form>
			<?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo et_core_esc_previously( et_sanitize_html_input_text( $et_phone_number ) ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $et_email ); ?>"><span id="et-info-email"><?php echo esc_html( $et_email ); ?></span></a>
				<?php endif; ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>
			<?php if ( $et_contact_info_defined || true === $show_header_social_icons || false !== et_get_option( 'show_search_icon', true ) || class_exists( 'woocommerce' ) || is_customize_preview() ) { ?>
				<?php if ( 'fullscreen' === et_get_option( 'header_style', 'left' ) ) { ?>
					</div> <!-- .et_pb_top_menu_inner -->
				<?php } ?>

				</div> <!-- .et_slide_menu_top -->
			<?php } ?>

			<div class="et_pb_fullscreen_nav_container">
				<?php
					$slide_nav = '';
					$slide_menu_class = 'et_mobile_menu';

					$slide_nav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
					$slide_nav .= wp_nav_menu( array( 'theme_location' => 'secondary-menu', 'container' => '', 'fallback_cb' => '', 'echo' => false, 'items_wrap' => '%3$s' ) );
				?>

				<ul id="mobile_menu_slide" class="<?php echo esc_attr( $slide_menu_class ); ?>">

				<?php
					if ( '' === $slide_nav ) :
				?>
						<?php if ( 'on' === et_get_option( 'divi_home_link' ) ) { ?>
							<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
						<?php }; ?>

						<?php show_page_menu( $slide_menu_class, false, false ); ?>
						<?php show_categories_menu( $slide_menu_class, false ); ?>
				<?php
					else :
						echo et_core_esc_wp( $slide_nav ) ;
					endif;
				?>

				</ul>
			</div>
		</div>
	<?php
		$slide_header = ob_get_clean();

		/**
		 * Filters the HTML output for the slide header.
		 *
		 * @since 3.10
		 *
		 * @param string $top_header
		 */
		echo et_core_intentionally_unescaped( apply_filters( 'et_html_slide_header', $slide_header ), 'html' );
	?>
	<?php endif; // true ==== $et_slide_header ?>

	<?php ob_start(); ?>




	<!--
	style="height: 68px;"
	-->
		<header id="main-header" data-height-onload="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>" >
			<div class="container clearfix et_menu_container">
			<?php
				$logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && ! empty( $user_logo )
					? $user_logo
					: $template_directory_uri . '/images/logo.png';

				ob_start();
			?>
				<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" />
					</a>
				</div>
			<?php
				$logo_container = ob_get_clean();

				/**
				 * Filters the HTML output for the logo container.
				 *
				 * @since 3.10
				 *
				 * @param string $logo_container
				 */
				echo et_core_intentionally_unescaped( apply_filters( 'et_html_logo_container', $logo_container ), 'html' );
			?>
				<div id="et-top-navigation" data-height="<?php echo esc_attr( et_get_option( 'menu_height', '66' ) ); ?>" data-fixed-height="<?php echo esc_attr( et_get_option( 'minimized_menu_height', '40' ) ); ?>">
					<?php if ( ! $et_slide_header || is_customize_preview() ) : ?>
						<nav id="top-menu-nav">
						<?php
							$menuClass = 'nav';
							if ( 'on' === et_get_option( 'divi_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';
							$primaryNav = '';

							$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );
							if ( empty( $primaryNav ) ) :
						?>
							<ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">
								<?php if ( 'on' === et_get_option( 'divi_home_link' ) ) { ?>
									<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
								<?php }; ?>

								<?php show_page_menu( $menuClass, false, false ); ?>
								<?php show_categories_menu( $menuClass, false ); ?>
							</ul>
						<?php
							else :
								echo et_core_esc_wp( $primaryNav );
							endif;
						?>
						</nav>
					<?php endif; ?>

					<?php
					if ( ! $et_top_info_defined && ( ! $et_slide_header || is_customize_preview() ) ) {
						et_show_cart_total( array(
							'no_text' => true,
						) );
					}
					?>

					<?php if ( $et_slide_header || is_customize_preview() ) : ?>
						<span class="mobile_menu_bar et_pb_header_toggle et_toggle_<?php echo esc_attr( et_get_option( 'header_style', 'left' ) ); ?>_menu"></span>
					<?php endif; ?>

					<?php if ( ( false !== et_get_option( 'show_search_icon', true ) && ! $et_slide_header ) || is_customize_preview() ) : ?>
					<div id="et_top_search">
						<span id="et_search_icon"></span>
					</div>
					<?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

					<?php

					/**
					 * Fires at the end of the 'et-top-navigation' element, just before its closing tag.
					 *
					 * @since 1.0
					 */
					do_action( 'et_header_top' );

					?>
				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);
					?>
					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header> <!-- #main-header -->
	<?php
		$main_header = ob_get_clean();

		/**
		 * Filters the HTML output for the main header.
		 *
		 * @since 3.10
		 *
		 * @param string $main_header
		 */
		echo et_core_intentionally_unescaped( apply_filters( 'et_html_main_header', $main_header ), 'html' );

	?>

		<div id="et-main-area">
	<?php

		/**
		 * Fires after the header, before the main content is output.
		 *
		 * @since 3.10
		 */
		do_action( 'et_before_main_content' );
