<?php
/*
Template Name: Detalle Buenas Practicas
*/
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

$show_navigation = get_post_meta( get_the_ID(), '_et_pb_project_nav', true );
//$bp_institucion_educativa =get_post_meta( get_the_ID(), 'institucion_educativa', true );
$bp_institucion_educativa = get_post_meta( get_the_ID(), 'bp_institucion_s', true );
//$bp_institucion_educativa=bp_listar_institucion($bp_institucion_educativa);
$bp_institucion_educativa=bp_listar_institucion($bp_institucion_educativa);
$bp_video_bp=get_post_meta( get_the_ID(), 'video_bp', true );
$bp_descripcion=get_post_meta( get_the_ID(), 'descripcion_buena_practica', true );

//$bp_slider=get_post_meta( get_the_ID(), 'slider_buena_practica', true );
$bp_slider = get_field('slider_buena_practica');
$bp_slider_imagen_0=$bp_slider['slider_imagen_1'];
$bp_slider_imagen_1=$bp_slider['slider_imagen_2'];
$bp_slider_imagen_2=$bp_slider['slider_imagen_3'];

$bp_etapas = get_field('etapas');
$bp_imagen_etapa_1=$bp_etapas['imagen_etapa_1'];
$bp_descripcion_etapa_1=$bp_etapas['descripcion_etapa_1'];
$bp_imagen_etapa_2=$bp_etapas['imagen_etapa_2'];
$bp_descripcion_etapa_2=$bp_etapas['descripcion_etapa_2'];
$bp_imagen_etapa_3=$bp_etapas['imagen_etapa_3'];
$bp_descripcion_etapa_3=$bp_etapas['descripcion_etapa_3'];
$bp_imagen_etapa_4=$bp_etapas['imagen_etapa_4'];
$bp_descripcion_etapa_4=$bp_etapas['descripcion_etapa_4'];

$bp_archivo_bp=get_field('archivo_bp');
?>

<div id="page-container">
  <div id="et-main-area">
    <div id="main-content">

			<?php while ( have_posts() ) : the_post(); ?>
      <article id="post-<?php the_ID(); ?>" class="post-<?php the_ID(); ?> page type-page status-publish hentry" <?php post_class(); ?>>
        <div class="entry-content">
          <div id="et-boc" class="et-boc">
            <div class="et_builder_inner_content et_pb_gutters3">
              <div class="et_pb_section et_pb_section_0 et_pb_with_background et_pb_fullwidth_section et_section_regular section_has_divider et_pb_bottom_divider">
                <div class="et_pb_module et_pb_fullwidth_slider_0 et_pb_slider">
                  <div class="et_pb_slides">
										<?php if(!empty($bp_slider_imagen_0)){ ?>
                    <div class="et_pb_slide et_pb_slide_0 et_pb_bg_layout_dark et_pb_media_alignment_center et_pb_slider_with_overlay et-pb-active-slide" data-slide-id="et_pb_slide_0">

                      <div class="et_pb_slide_overlay_container"></div>
                      <div class="et_pb_container clearfix">
                        <div class="et_pb_slider_container_inner">

                          <div class="et_pb_slide_description">
                            <h2 class="et_pb_slide_title"><?php the_title(); ?></h2><div class="et_pb_slide_content"><p><?php echo $bp_institucion_educativa;?></p></div>
                          </div> <!-- .et_pb_slide_description -->
                        </div>
                      </div> <!-- .et_pb_container -->

                    </div> <!-- .et_pb_slide -->
										<?php } ?>
										<?php if(!empty($bp_slider_imagen_1)){ ?>
                    <div class="et_pb_slide et_pb_slide_1 et_pb_bg_layout_dark et_pb_media_alignment_center et_pb_slider_with_overlay" data-slide-id="et_pb_slide_1">

                      <div class="et_pb_slide_overlay_container"></div>
                      <div class="et_pb_container clearfix">
                        <div class="et_pb_slider_container_inner">

                          <div class="et_pb_slide_description">
                            <h2 class="et_pb_slide_title"><?php the_title(); ?></h2><div class="et_pb_slide_content"><p><?php echo $bp_institucion_educativa;?></p></div>

                          </div> <!-- .et_pb_slide_description -->
                        </div>
                      </div> <!-- .et_pb_container -->

                    </div> <!-- .et_pb_slide -->
										<?php } ?>
										<?php if(!empty($bp_slider_imagen_2)){ ?>
                    <div class="et_pb_slide et_pb_slide_2 et_pb_bg_layout_dark et_pb_media_alignment_center et_pb_slider_with_overlay" data-slide-id="et_pb_slide_2">

                      <div class="et_pb_slide_overlay_container"></div>
                      <div class="et_pb_container clearfix">
                        <div class="et_pb_slider_container_inner">

                          <div class="et_pb_slide_description">
                            <h2 class="et_pb_slide_title"><?php the_title(); ?></h2><div class="et_pb_slide_content"><p><?php echo $bp_institucion_educativa;?></p></div>

                          </div> <!-- .et_pb_slide_description -->
                        </div>
                      </div> <!-- .et_pb_container -->

                    </div> <!-- .et_pb_slide -->
										<?php } ?>
                  </div> <!-- .et_pb_slides -->

                </div> <!-- .et_pb_slider -->


                <div class="et_pb_bottom_inside_divider et-no-transition"></div>
              </div> <!-- .et_pb_section -->
              <div class="et_pb_section et_pb_section_1 et_pb_with_background et_section_regular section_has_divider et_pb_bottom_divider">

                <div class="et_pb_with_border et_pb_row et_pb_row_0">
                  <div class="et_pb_column et_pb_column_1_2 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">

                    <div class="et_pb_module et_pb_code et_pb_code_0">
                      <div class="et_pb_code_inner">
                        <link  rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?libdco">

                        <style >
	                        li.et_pb_social_icon.et-social-skype a.icon:before {
	                          font-family: "Socicon" !important;
	                          content: '\e099' !important;
	                        }
                      	</style>

                      <script >
                      jQuery(function($){
                        $('.et-social-skype a').attr('title', 'Whatsapp');
                      });
                      </script>
                    </div>
                  </div>
                  <!-- .et_pb_code -->

                  <div class="et_pb_module et_pb_code et_pb_code_1  et_pb_text_align_center">
                    <div class="et_pb_code_inner"><!--Yasr Visitor Votes Shortcode-->
                      <div id="yasr_visitor_votes_53449" class="yasr-visitor-votes">
                        <div id="yasr-visitor-votes-rater-bd540dd29bc3d" class="yasr-rater-stars-visitor-votes" data-rater-postid="53449"data-rating="2" data-rater-starsize="32"data-rater-readonly="false" data-rater-nonce="405aba0c7f" data-issingular="true">
                        </div>
                        <span id="yasr-visitor-votes-container-after-stars-bd540dd29bc3d" class='yasr-visitor-votes-after-stars-class'>
                          <span class="yasr-total-average-container" id="yasr-total-average-text_53449">
                              [Total:1 &nbsp; &nbsp;Promedio:2/5]
                          </span>
                        </span>
                      </div><!--End Yasr Visitor Votes Shortcode-->
                    </div>
                  </div> <!-- .et_pb_code -->
                </div> <!-- .et_pb_column -->

                <div class="et_pb_column et_pb_column_1_2 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">

                  <div class="et_pb_module et_pb_code et_pb_code_2">

                  <div class="et_pb_code_inner">
                    <div id="csbwfs-shortcode" class="">
                      <div id="csbwfs-fb" class="csbwfs-shortcode">
                        <a href="javascript:" onclick="javascript:window.open('//www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title)+'&jump=close', '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" ><img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-noticias-socmedia-facebook-off.png" alt="" width="30" height="30" title="Compartir en Facebook" ></a>
                      </div>
                  <div id="csbwfs-tw" class="csbwfs-shortcode">
                  <a href="javascript:" onclick="window.open('//twitter.com/share?url='+encodeURIComponent(location.href)+'&text='+encodeURIComponent(document.title)+'&jump=close','_blank','width=800,height=300')" ><img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-noticias-socmedia-twitter-off.png" alt="" width="30" height="30" title="Tweet"></a></div>
                  <div id="csbwfs-li" class="csbwfs-shortcode"><a href="javascript:" onclick="javascript:window.open('//www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title)+'&jump=close','','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=800');return false;" ><img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-noticias-socmedia-linkedin-off.png" alt="" width="30" height="30" title="LinkedIn"></a></div>
                  <div id="csbwfs-tw" class="csbwfs-shortcode"><a href="javascript:" onclick="window.open('//api.whatsapp.com/send?url='+encodeURIComponent(location.href)+'&text='+encodeURIComponent(document.title)+'&jump=close','_blank','width=800,height=300')" ><img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-noticias-socmedia-whatsapp-off.png" alt="" width="30" height="30" title="whatsapp"></a>
                  </div>
                  </div>
                  </div>
                  </div> <!-- .et_pb_code -->
                </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->

              <div class="et_pb_with_border et_pb_row et_pb_row_1">
                <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">

                  <div class="et_pb_module et_pb_image et_pb_image_0">

                  <span class="et_pb_image_wrap ">
										<img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/valoracion-por-perfil-strellas.png"
											srcset="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/valoracion-por-perfil-strellas.png 1028w,
											 https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/valoracion-por-perfil-strellas-980x17.png 980w,
											 https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/valoracion-por-perfil-strellas-480x8.png 480w"
											 sizes="((min-width: 0px) and (max-width: 480px)) 480px, ((min-width: 481px) and (max-width: 980px)) 980px, (min-width: 981px) 1028px, 100vw" /></span>
                  </div>
                </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->

              <div class="et_pb_with_border et_pb_row et_pb_row_2">
              <div class="et_pb_column et_pb_column_1_5 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">


              <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_center">


              <div class="et_pb_text_inner"><p>DOCENTE</p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_5 et_pb_column_4  et_pb_css_mix_blend_mode_passthrough">

              <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_center">

              <div class="et_pb_text_inner"><p>ESTUDIANTE</p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_5 et_pb_column_5  et_pb_css_mix_blend_mode_passthrough">


              <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_center">

              <div class="et_pb_text_inner"><p>FUNCIONARIO</p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_5 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough">

              <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_center">

              <div class="et_pb_text_inner"><p>INVESTIGADOR</p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_5 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough et-last-child">

              <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_center">

              <div class="et_pb_text_inner"><p>OTROS</p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->

              </div> <!-- .et_pb_row -->

              <div class="et_pb_row et_pb_row_3">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough et-last-child">

              <div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_light  et_pb_text_align_left">

              <div class="et_pb_text_inner"><h1>Descripción</h1></div>
              </div> <!-- .et_pb_text -->
              <div class="et_pb_module et_pb_divider et_pb_divider_0 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->

              <div class="et_pb_with_border et_pb_row et_pb_row_4">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child">
										<!--
	              <div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_light  et_pb_text_align_left">

	              <div class="et_pb_text_inner">


									-->
									<p style="font-family: 'calibri',Helvetica,Arial,Lucida,sans-serif!important;color: #000000!important;font-size:18px!important;">
										<?php echo $bp_descripcion;?>
									</p>
											<!--


								</div>
	              </div>
									--><!-- .et_pb_text -->
							<style>
								.et_pb_text_2 {
								   font-family: 'calibri',Helvetica,Arial,Lucida,sans-serif!important;color: #000000!important;font-size:18px!important;

								}
								.et_pb_text_2.et_pb_text {
								    color: #000000!important;
								}
							</style>
              <div class="et_pb_module et_pb_code et_pb_code_3">

              <div class="et_pb_code_inner">
              <link  rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?libdco">

              <style >
              li.et_pb_social_icon.et-social-youtube a.icon:before {
                 font-family: "Socicon" !important;
                 content: 'e099' !important;
              }
              </style>

              <script >
              jQuery(function($){
                $('.et-social-youtube a').attr('title', 'Whatsapp');
              });
              </script>
              </div>
              </div> <!-- .et_pb_code -->
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->
							<?php if (!empty($bp_video_bp)){ ?>
              <div class="et_pb_row et_pb_row_5">
                <div class="et_pb_column et_pb_column_4_4 et_pb_column_10  et_pb_css_mix_blend_mode_passthrough et-last-child">


                <div class="et_pb_module et_pb_video et_pb_video_0">
                <div class="et_pb_video_box">
								<!--
                <video controls>
                	<source type="video/mp4" src="https://www.youtube.com/watch?v=F6VwumP0szo" />

                </video>
								-->
								<?php echo $bp_video_bp?>

								</div>

                </div>
                </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->
							<?php } ?>

              <div class="et_pb_bottom_inside_divider et-no-transition"></div>
            </div> <!-- .et_pb_section -->



            <div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">

              <div class="et_pb_row et_pb_row_6">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_11  et_pb_css_mix_blend_mode_passthrough et-last-child">


              <div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left">


              <div class="et_pb_text_inner"><h1>Equipo Docente</h1></div>
              </div> <!-- .et_pb_text --><div class="et_pb_module et_pb_divider et_pb_divider_1 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->
              <div class="et_pb_row et_pb_row_7">
              <div class="et_pb_column et_pb_column_1_3 et_pb_column_12  et_pb_css_mix_blend_mode_passthrough">

              <div class="et_pb_module et_pb_team_member et_pb_team_member_0 et_pb_bg_layout_light clearfix ">
              <div class="et_pb_team_member_image et-waypoint et_pb_animation_off"><img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-3.png" alt="Juana Contreras Chávez" srcset="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-3.png 200w, https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-3-150x150.png 150w" sizes="(max-width: 200px) 100vw, 200px" /></div>
              <div class="et_pb_team_member_description">
              <h4 class="et_pb_module_header">Juana Contreras Chávez</h4>
              <p class="et_pb_member_position">(I. E. 014 Estrellitas de María)</p>


              </div> <!-- .et_pb_team_member_description -->
              </div> <!-- .et_pb_team_member -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_3 et_pb_column_13  et_pb_css_mix_blend_mode_passthrough">


              <div class="et_pb_module et_pb_team_member et_pb_team_member_1 et_pb_bg_layout_light clearfix ">


              <div class="et_pb_team_member_image et-waypoint et_pb_animation_off"><img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-2-fliped.png" alt=" Julio Espinoza Morales" srcset="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-2-fliped.png 200w, https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-2-fliped-150x150.png 150w" sizes="(max-width: 200px) 100vw, 200px" /></div>
              <div class="et_pb_team_member_description">
              <h4 class="et_pb_module_header"> Julio Espinoza Morales</h4>
              <p class="et_pb_member_position">(I. E. 014 Estrellitas de María)</p>


              </div> <!-- .et_pb_team_member_description -->
              </div> <!-- .et_pb_team_member -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_3 et_pb_column_14  et_pb_css_mix_blend_mode_passthrough et-last-child">


              <div class="et_pb_module et_pb_team_member et_pb_team_member_2 et_pb_bg_layout_light clearfix ">


              <div class="et_pb_team_member_image et-waypoint et_pb_animation_off"><img src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-1.png" alt="Bertha Hinojosa Roque" srcset="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-1.png 200w, https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/usuarios-profilepic-1-150x150.png 150w" sizes="(max-width: 200px) 100vw, 200px" /></div>
              <div class="et_pb_team_member_description">
              <h4 class="et_pb_module_header">Bertha Hinojosa Roque</h4>
              <p class="et_pb_member_position">(I. E. 014 Estrellitas de María)</p>
              </div> <!-- .et_pb_team_member_description -->
              </div> <!-- .et_pb_team_member -->
              </div> <!-- .et_pb_column -->
              </div> <!-- .et_pb_row -->

            </div> <!-- .et_pb_section -->

            <div class="et_pb_section et_pb_section_3 ds-timeline et_pb_with_background et_section_regular section_has_divider et_pb_bottom_divider">
              <div class="et_pb_row et_pb_row_8">
              <div class="et_pb_column et_pb_column_4_4 et_pb_column_15  et_pb_css_mix_blend_mode_passthrough et-last-child">
              <div class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light  et_pb_text_align_left">
              <div class="et_pb_text_inner"><h1>Etapas</h1></div>
              </div> <!-- .et_pb_text -->
              <div class="et_pb_module et_pb_divider et_pb_divider_2 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
              </div> <!-- .et_pb_column -->
              </div> <!-- .et_pb_row -->
              <div class="et_pb_row et_pb_row_9 timeline-item speak-soon even et_animated et_pb_gutters1">
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_16 col-date  et_pb_css_mix_blend_mode_passthrough">
              <div class="et_pb_module et_pb_text et_pb_text_9 tm-date et_pb_bg_layout_light  et_pb_text_align_left">
              	<div class="et_pb_text_inner"><h4>Contexto Social y propósito</h4></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_17 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">
              <div class="et_pb_module et_pb_image et_pb_image_1">
              <span class="et_pb_image_wrap "><img src="<?php echo $bp_imagen_etapa_1; ?>" srcset="<?php echo $bp_imagen_etapa_1; ?> 494w, <?php echo $bp_imagen_etapa_1; ?> 480w" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 494px, 100vw" /></span>
              </div>
              <div class="et_pb_module et_pb_text et_pb_text_10 tm-desc et_pb_bg_layout_light  et_pb_text_align_left">
              <div class="et_pb_text_inner"><p><span>
							<?php echo $bp_descripcion_etapa_1;?>

							</span></p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->
              <div class="et_pb_row et_pb_row_10 timeline-item speak-soon odd et_animated et_pb_gutters1">
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_18 col-date  et_pb_css_mix_blend_mode_passthrough">
              <div class="et_pb_module et_pb_text et_pb_text_11 tm-date et_pb_bg_layout_light  et_pb_text_align_left">
              <div class="et_pb_text_inner"><h4>Desarrollo, desafíos y<br /> lecciones aprendidas</h4></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_19 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">

              <div class="et_pb_module et_pb_image et_pb_image_2">
              <span class="et_pb_image_wrap "><img src="<?php echo $bp_imagen_etapa_2; ?>" srcset="<?php echo $bp_imagen_etapa_2; ?> 494w, <?php echo $bp_imagen_etapa_2; ?> 480w" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 494px, 100vw" /></span>
              </div>
              <div class="et_pb_module et_pb_text et_pb_text_12 tm-desc et_pb_bg_layout_light  et_pb_text_align_left">
              <div class="et_pb_text_inner"><p><span>
								<?php echo $bp_descripcion_etapa_2;?>
							</span></p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->
              <div class="et_pb_row et_pb_row_11 timeline-item tm-soon even et_animated et_pb_gutters1">
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_20 col-date  et_pb_css_mix_blend_mode_passthrough">
              <div class="et_pb_module et_pb_text et_pb_text_13 tm-date et_pb_bg_layout_light  et_pb_text_align_left">
              <div class="et_pb_text_inner"><h4>Logros y resultados</h4></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_21 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">
              <div class="et_pb_module et_pb_video et_pb_video_1">
								<div class="et_pb_module et_pb_image et_pb_image_2">
	              <span class="et_pb_image_wrap "><img src="<?php echo $bp_imagen_etapa_3; ?>" srcset="<?php echo $bp_imagen_etapa_3; ?> 494w, <?php echo $bp_imagen_etapa_3; ?> 480w" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 494px, 100vw" /></span>
	              </div>
							<!--
              <div class="et_pb_video_box">
              <video controls>
              <source type="video/mp4" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/convertido7-para-timeline.mp4" />
              </video>
							</div>
							-->

              </div><div class="et_pb_module et_pb_text et_pb_text_14 tm-desc et_pb_bg_layout_light  et_pb_text_align_left">

              <div class="et_pb_text_inner"><p><span>
								<?php echo $bp_descripcion_etapa_3;?>
							</span></p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->
              <div class="et_pb_row et_pb_row_12 timeline-item speak-soon odd et_animated et_pb_gutters1">
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_22 col-date  et_pb_css_mix_blend_mode_passthrough">
              <div class="et_pb_module et_pb_text et_pb_text_15 tm-date et_pb_bg_layout_light  et_pb_text_align_left">

              <div class="et_pb_text_inner"><h4>Oportunidades de mejora (recomendaciones)</h4></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->
              <div class="et_pb_column et_pb_column_1_2 et_pb_column_23 col-details  et_pb_css_mix_blend_mode_passthrough et-last-child">
              <div class="et_pb_module et_pb_image et_pb_image_3">
              <span class="et_pb_image_wrap "><img src="<?php echo $bp_imagen_etapa_4; ?>" srcset="<?php echo $bp_imagen_etapa_4; ?> 494w, <?php echo $bp_imagen_etapa_4; ?> 480w" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 494px, 100vw" /></span>
              </div>
              <div class="et_pb_module et_pb_text et_pb_text_16 tm-desc et_pb_bg_layout_light  et_pb_text_align_left">


              <div class="et_pb_text_inner"><p><span>
								<?php echo $bp_descripcion_etapa_4;?>
							</span></p></div>
              </div> <!-- .et_pb_text -->
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->

              <div class="et_pb_bottom_inside_divider et-no-transition"></div>
            </div> <!-- .et_pb_section -->


            <div class="et_pb_with_border et_pb_section et_pb_section_4 et_pb_with_background et_section_regular section_has_divider et_pb_bottom_divider et_pb_top_divider">
              <div class="et_pb_top_inside_divider et-no-transition"></div>
              <div class="et_pb_row et_pb_row_13">
              <div class="et_pb_column et_pb_column_1_3 et_pb_column_24  et_pb_css_mix_blend_mode_passthrough">


              <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
              <a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 archivos_popup et_pb_bg_layout_light" href="#" data-icon="&#xe059;">EVIDENCIAS</a>
              </div>
              </div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3 et_pb_column_25  et_pb_css_mix_blend_mode_passthrough">


              <div class="et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_center et_pb_module ">
              <a class="et_pb_button et_pb_custom_button_icon et_pb_button_1 et_pb_bg_layout_light" href="<?php echo $bp_archivo_bp;?>" download="Archivo Buena Práctica" data-icon="&#xe092;">DESCARGAR</a>
              </div>
              	</div> <!-- .et_pb_column -->
                <div class="et_pb_column et_pb_column_1_3 et_pb_column_26  et_pb_css_mix_blend_mode_passthrough et-last-child">


              <div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_center et_pb_module ">
              <a class="et_pb_button et_pb_custom_button_icon et_pb_button_2 et_pb_bg_layout_light" href="" data-icon="&#x52;">ME INTERESA</a>
              </div>
              </div> <!-- .et_pb_column -->


              </div> <!-- .et_pb_row -->

            </div> <!-- .et_pb_section -->

          </div>

        </div>

      </div> <!-- .entry-content -->

    </article> <!-- .et_pb_post -->
		<?php
			if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_postcomments', 'on' ) )
				comments_template( '', true );
		?>
		<?php endwhile; ?>
		<?php
			if ( in_array( $page_layout, array( 'et_full_width_page', 'et_no_sidebar' ) ) ) {
				et_pb_portfolio_meta_box();
			}
		?>

		<?php //get_sidebar(); ?>


  </div> <!-- #main-content -->
  </div> <!-- #et-main-area -->
</div> <!-- #page-container -->

<div id="csbwfs-delaydiv"><div class="csbwfs-social-widget"  id="csbwfs-right" title="Share This With Your Friends"  style="top:25%;right:-5px;"><div class="csbwfs-show"><a href="javascript:" title="Show Buttons" id="csbwfs-show"><img src="https://observatorio.minedu.gob.pe/wp-content/plugins/custom-share-buttons-with-floating-sidebar/images/hide-r.png" alt="Show Buttons"></a></div><div id="csbwfs-social-inner"><div class="csbwfs-sbutton csbwfsbtns"><div id="csbwfs-fb" class="csbwfs-fb"><a href="javascript:" onclick="javascript:window.open('//www.facebook.com/sharer/sharer.php?u=https://observatorio.minedu.gob.pe/buena-practica-detalle/', '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share On Facebook" ><i class="csbwfs_facebook"></i></a></div></div><div class="csbwfs-sbutton csbwfsbtns"><div id="csbwfs-tw" class="csbwfs-tw"><a href="javascript:" onclick="window.open('//twitter.com/share?url=https://observatorio.minedu.gob.pe/buena-practica-detalle/&text=Buena%20Practica%20Detalle%20%26%238211%3B%20Secundaria','_blank','width=800,height=300')" title="Share On Twitter" ><i class="csbwfs_twitter"></i></a></div></div><div class="csbwfs-sbutton csbwfsbtns"><div id="csbwfs-li" class="csbwfs-li"><a href="javascript:" onclick="javascript:window.open('//www.linkedin.com/cws/share?mini=true&url=https://observatorio.minedu.gob.pe/buena-practica-detalle/','','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=800');return false;" title="Share On Linkedin" ><i class="csbwfs_linkedin"></i></a></div></div><div class="csbwfs-sbutton csbwfsbtns"><div id="csbwfs-yt" class="csbwfs-yt"><a onclick="window.open('');" href="javascript:void(0);"  title="Share On Youtube"><i class="csbwfs_youtube"></i></a></div></div><div class="csbwfs-sbutton csbwfsbtns"><div id="csbwfs-ml" class="csbwfs-ml"><a href="mailto:?subject=Buena%20Practica%20Detalle%20%26%238211%3B%20Secundaria&body=https://observatorio.minedu.gob.pe/buena-practica-detalle/" title="Contact us"  ><i class="csbwfs_mail"></i></a></div></div></div><div class="csbwfs-hide"><a href="javascript:" title="Hide Buttons" id="csbwfs-hide"><img src="https://observatorio.minedu.gob.pe/wp-content/plugins/custom-share-buttons-with-floating-sidebar/images/show.png" alt="Hide Buttons"></a></div></div></div><script>
	  var windWidth=jQuery( window ).width();
	  //alert(windWidth);
	  var animateWidth;
	  var defaultAnimateWidth;
	jQuery(document).ready(function()
  {
	animateWidth="55";
    defaultAnimateWidth= animateWidth-10;
	animateHeight="49";
	defaultAnimateHeight= animateHeight-2;jQuery("#csbwfs-delaydiv").hide();
	  setTimeout(function(){
	  jQuery("#csbwfs-delaydiv").fadeIn();}, 2);jQuery("div.csbwfsbtns a").hover(function(){
  jQuery(this).animate({width:animateWidth});
  },function(){
    jQuery(this).stop( true, true ).animate({width:defaultAnimateWidth});
  });jQuery("div.csbwfs-show").hide();
  jQuery("div.csbwfs-show a").click(function(){
    jQuery("div#csbwfs-social-inner").show(500);
     jQuery("div.csbwfs-show").hide(500);
    jQuery("div.csbwfs-hide").show(500);
    csbwfsSetCookie("csbwfs_show_hide_status","active","1");
  });

  jQuery("div.csbwfs-hide a").click(function(){
     jQuery("div.csbwfs-show").show(500);
      jQuery("div.csbwfs-hide").hide(500);
     jQuery("div#csbwfs-social-inner").hide(500);
     csbwfsSetCookie("csbwfs_show_hide_status","in_active","1");
  });var button_status=csbwfsGetCookie("csbwfs_show_hide_status");
    if (button_status =="in_active") {
      jQuery("div.csbwfs-show").show();
      jQuery("div.csbwfs-hide").hide();
     jQuery("div#csbwfs-social-inner").hide();
    } else {
      jQuery("div#csbwfs-social-inner").show();
     jQuery("div.csbwfs-show").hide();
    jQuery("div.csbwfs-hide").show();
    }});

</script>

<script>csbwfsCheckCookie();function csbwfsSetCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname+"="+cvalue+"; "+expires;
}

function csbwfsGetCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function csbwfsCheckCookie() {
	var hideshowhide = ""
    var button_status=csbwfsGetCookie("csbwfs_show_hide_status");
    if (button_status != "") {

    } else {
        csbwfsSetCookie("csbwfs_show_hide_status", "active",1);
    }
    if(hideshowhide=="yes")
    {
    csbwfsSetCookie("csbwfs_show_hide_status", "active",0);
    }
}

</script>




<div id="pum-53929" class="pum pum-overlay pum-theme-51838 pum-theme-lightbox popmake-overlay click_open" data-popmake="{&quot;id&quot;:53929,&quot;slug&quot;:&quot;archivos&quot;,&quot;theme_id&quot;:51838,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;.archivos_popup&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;auto&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}" role="dialog" aria-hidden="true" >

	<div id="popmake-53929" class="pum-container popmake theme-51838">







				<div class="pum-content popmake-content">
			<p style="text-align: center;">
<div style="margin-left:10px">Puedes descargar la evidencia, en las siguientes versiones:</div>
</p>
<p style="text-align: center;"><img class="alignnone size-full wp-image-53920" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-file-word.png" alt="" width="62" height="81" /> <img class="alignnone size-full wp-image-53919" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-file-ppt.png" alt="" width="62" height="81" /> <img class="alignnone size-full wp-image-53918" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-file-pdf.png" alt="" width="62" height="81" /> <img class="alignnone size-full wp-image-53917" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-file-html.png" alt="" width="62" height="81" /> <img class="alignnone size-full wp-image-53916" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-file-excel.png" alt="" width="62" height="81" /> <img class="alignnone size-full wp-image-53915" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ico-file-epub.png" alt="" width="62" height="81" /></p>
		</div>




				            <button type="button" class="pum-close popmake-close" aria-label="Close">
			×            </button>

	</div>

</div>
<div id="pum-53631" class="pum pum-overlay pum-theme-51838 pum-theme-lightbox popmake-overlay click_open" data-popmake="{&quot;id&quot;:53631,&quot;slug&quot;:&quot;ubigeo&quot;,&quot;theme_id&quot;:51838,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;.ubigeo_popup&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;tiny&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:false,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}" role="dialog" aria-hidden="true" >

	<div id="popmake-53631" class="pum-container popmake theme-51838 pum-responsive pum-responsive-tiny responsive size-tiny">







				<div class="pum-content popmake-content">
			<p><img class="alignnone  wp-image-53509 aligncenter" src="https://observatorio.minedu.gob.pe/wp-content/uploads/2019/11/ubigeo-300x256.jpg" alt="" width="378" height="319" /></p>
		</div>




				            <button type="button" class="pum-close popmake-close" aria-label="Close">
			×            </button>

	</div>

</div>
	<script type="text/javascript">
				var et_animation_data = [{"class":"et_pb_row_9","style":"slideTop","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"17%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_row_10","style":"fade","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_row_11","style":"fade","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_row_12","style":"slideBottom","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"20%","starting_opacity":"0%","speed_curve":"ease-in-out"}];
			</script>
	<style type="text/css">.csbwfs-shortcode a{box-shadow:inherit}.csbwfs-shortcode a i{display:inline-block;position:relative;width:35px;height:36px;background-image:url(https://observatorio.minedu.gob.pe/wp-content/plugins/custom-share-buttons-with-floating-sidebar/lib/../images/minify-social.png)}.csbwfs-shortcode{display:inline-block;position:relative;width:auto;}.csbwfs-shortcode i.csbwfs_facebook{background-position:68% 4%}.csbwfs-shortcode i.csbwfs_twitter{background-position:14% 4%}.csbwfs-shortcode i.csbwfs_plus{background-position:80% 4%}.csbwfs-shortcode i.csbwfs_linkedin{background-position:92% 4%}.csbwfs-shortcode i.csbwfs_pinterest{background-position:14% 19%}.csbwfs-shortcode i.csbwfs_youtube{background-position:32% 4%}.csbwfs-shortcode i.csbwfs_reddit{background-position:26% 19%}.csbwfs-shortcode i.csbwfs_stumbleupon{background-position:44% 19%}.csbwfs-shortcode i.csbwfs_mail{background-position:8% 19%}.csbwfs-shortcode i.csbwfs_skype {background-position: 38% 19%;}</style><link rel='stylesheet' id='mediaelement-css'  href='https://observatorio.minedu.gob.pe/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.6-78496d1' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='https://observatorio.minedu.gob.pe/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=5.2.3' type='text/css' media='all' />
<link rel='stylesheet' id='rainmaker_form_style-css'  href='https://observatorio.minedu.gob.pe/wp-content/plugins/icegram-rainmaker/classes/../assets/css/form.css?ver=0.37' type='text/css' media='all' />
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"Cerrar","currentText":"Hoy","monthNames":["enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"],"monthNamesShort":["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"],"nextText":"Siguiente","prevText":"Previo","dayNames":["domingo","lunes","martes","mi\u00e9rcoles","jueves","viernes","s\u00e1bado"],"dayNamesShort":["dom","lun","mar","mi\u00e9","jue","vie","s\u00e1b"],"dayNamesMin":["D","L","M","X","J","V","S"],"dateFormat":"d MM, yy","firstDay":1,"isRTL":false});});
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/plugins/yet-another-stars-rating/js/rater-js.js?ver=2.0.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yasrCommonData = {"postid":"53449","ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","loggedUser":"","visitorStatsEnabled":"no","tooltipValues":["mala","pobre","ok","buena","super"],"loaderHtml":"<div id=\"loader-visitor-rating\" style=\"display: inline\">\u00a0  <img src=https:\/\/observatorio.minedu.gob.pe\/wp-content\/plugins\/yet-another-stars-rating\/img\/loader.gif title=\"yasr-loader\" alt=\"yasr-loader\">\n                                 <\/div>"};
/* ]]> */
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/plugins/yet-another-stars-rating/js/yasr-front.js?ver=2.0.7'></script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/plugins/yet-another-stars-rating/js/tippy.all.min.js?ver=3.6.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Anterior","next":"Siguiente"};
var et_pb_custom = {"ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/observatorio.minedu.gob.pe\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/observatorio.minedu.gob.pe\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"c12e1e2292","subscription_failed":"Por favor, revise los campos a continuaci\u00f3n para asegurarse de que la informaci\u00f3n introducida es correcta.","et_ab_log_nonce":"1a462b29a9","fill_message":"Por favor, rellene los siguientes campos:","contact_error_message":"Por favor, arregle los siguientes errores:","invalid":"De correo electr\u00f3nico no v\u00e1lida","captcha":"Captcha","prev":"Anterior","previous":"Anterior","next":"Siguiente","wrong_captcha":"Ha introducido un n\u00famero equivocado de captcha.","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"53449","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""};
var et_pb_box_shadow_elements = [];
/* ]]> */
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/themes/Divi/js/custom.min.js?ver=3.29.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ajaxsearchlite = {"ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","backend_ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","js_scope":"jQuery"};
var ASL = {"ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","backend_ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","js_scope":"jQuery","detect_ajax":"0","scrollbar":"1","js_retain_popstate":"0","version":"4737","fix_duplicates":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/plugins/ajax-search-lite/js/min/jquery.ajaxsearchlite.min.js?ver=4.8'></script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pum_vars = {"version":"1.8.13","ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","restapi":"https:\/\/observatorio.minedu.gob.pe\/wp-json\/pum\/v1","rest_nonce":null,"default_theme":"51837","debug_mode":"","disable_tracking":"","home_url":"\/","message_position":"top","core_sub_forms_enabled":"1","popups":[]};
var ajaxurl = "https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php";
var pum_debug_vars = {"debug_mode_enabled":"Popup Maker: Debug Mode Enabled","debug_started_at":"Debug started at:","debug_more_info":"For more information on how to use this information visit https:\/\/docs.wppopupmaker.com\/?utm_medium=js-debug-info&utm_campaign=ContextualHelp&utm_source=browser-console&utm_content=more-info","global_info":"Global Information","localized_vars":"Localized variables","popups_initializing":"Popups Initializing","popups_initialized":"Popups Initialized","single_popup_label":"Popup: #","theme_id":"Theme ID: ","label_method_call":"Method Call:","label_method_args":"Method Arguments:","label_popup_settings":"Settings","label_triggers":"Triggers","label_cookies":"Cookies","label_delay":"Delay:","label_conditions":"Conditions","label_cookie":"Cookie:","label_settings":"Settings:","label_selector":"Selector:","label_mobile_disabled":"Mobile Disabled:","label_tablet_disabled":"Tablet Disabled:","label_event":"Event: %s","triggers":{"click_open":"Click Open","auto_open":"Time Delay \/ Auto Open"},"cookies":{"on_popup_close":"On Popup Close","on_popup_open":"On Popup Open","pum_sub_form_success":"Subscription Form: Successful","pum_sub_form_already_subscribed":"Subscription Form: Already Subscribed","manual":"Manual JavaScript"}};
var pum_sub_vars = {"ajaxurl":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","message_position":"top"};
var pum_popups = {"pum-51846":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"triggers":[{"type":"click_open","settings":{"cookie_name":"","extra_selectors":".prueba_popup"}}],"theme_id":"51838","size":"small","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height":"380px","animation_type":"fade","animation_speed":"350","animation_origin":"center top","location":"center top","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","zindex":"1999999999","close_button_delay":"0","cookies":[],"theme_slug":"lightbox","id":51846,"slug":"suscribete"},"pum-53929":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"triggers":[{"type":"click_open","settings":{"cookie_name":"","extra_selectors":".archivos_popup"}}],"theme_id":"51838","size":"auto","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height":"380px","animation_type":"fade","animation_speed":"350","animation_origin":"center top","location":"center","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","zindex":"1999999999","close_button_delay":"0","cookies":[],"theme_slug":"lightbox","id":53929,"slug":"archivos"},"pum-53631":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_overlay_click":false,"close_on_esc_press":false,"close_on_f4_press":false,"disable_form_reopen":false,"disable_accessibility":false,"triggers":[{"type":"click_open","settings":{"cookie_name":"","extra_selectors":".ubigeo_popup"}}],"theme_id":"51838","size":"tiny","responsive_min_width":"0%","responsive_max_width":"100%","custom_width":"640px","custom_height":"380px","animation_type":"fade","animation_speed":"350","animation_origin":"center top","location":"center top","position_top":"100","position_bottom":"0","position_left":"0","position_right":"0","zindex":"1999999999","close_button_delay":"0","cookies":[],"theme_slug":"lightbox","id":53631,"slug":"ubigeo"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/plugins/popup-maker/assets/js/site.min.js?defer&#038;ver=1.8.13'></script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/themes/Divi/core/admin/js/common.js?ver=3.29.3'></script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-includes/js/wp-embed.min.js?ver=5.2.3'></script>
<script type='text/javascript'>
var mejsL10n = {"language":"es","strings":{"mejs.install-flash":"Est\u00e1s usando un navegador que no tiene Flash activo o instalado. Por favor, activa el componente del reproductor Flash o descarga la \u00faltima versi\u00f3n desde https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Salir de pantalla completa","mejs.fullscreen-on":"Ver en pantalla completa","mejs.download-video":"Descargar v\u00eddeo","mejs.fullscreen":"Pantalla completa","mejs.time-jump-forward":["Saltar %1 segundo hacia adelante","Salta hacia adelante %1 segundos"],"mejs.loop":"Alternar bucle","mejs.play":"Reproducir","mejs.pause":"Pausa","mejs.close":"Cerrar","mejs.time-slider":"Control de tiempo","mejs.time-help-text":"Usa las teclas de direcci\u00f3n izquierda\/derecha para avanzar un segundo, y las flechas arriba\/abajo para avanzar diez segundos.","mejs.time-skip-back":["Saltar atr\u00e1s 1 segundo","Retroceder %1 segundos"],"mejs.captions-subtitles":"Pies de foto \/ Subt\u00edtulos","mejs.captions-chapters":"Cap\u00edtulos","mejs.none":"Ninguna","mejs.mute-toggle":"Desactivar sonido","mejs.volume-help-text":"Utiliza las teclas de flecha arriba\/abajo para aumentar o disminuir el volumen.","mejs.unmute":"Activar sonido","mejs.mute":"Silenciar","mejs.volume-slider":"Control de volumen","mejs.video-player":"Reproductor de v\u00eddeo","mejs.audio-player":"Reproductor de audio","mejs.ad-skip":"Saltar anuncio","mejs.ad-skip-info":["Saltar en 1 segundo","Saltar en %1 segundos"],"mejs.source-chooser":"Selector de origen","mejs.stop":"Parar","mejs.speed-rate":"Tasa de velocidad","mejs.live-broadcast":"Transmisi\u00f3n en vivo","mejs.afrikaans":"Africano","mejs.albanian":"Albano","mejs.arabic":"\u00c1rabe","mejs.belarusian":"Bielorruso","mejs.bulgarian":"B\u00falgaro","mejs.catalan":"Catal\u00e1n","mejs.chinese":"Chino","mejs.chinese-simplified":"Chino (Simplificado)","mejs.chinese-traditional":"Chino (Tradicional)","mejs.croatian":"Croata","mejs.czech":"Checo","mejs.danish":"Dan\u00e9s","mejs.dutch":"Holand\u00e9s","mejs.english":"Ingl\u00e9s","mejs.estonian":"Estonio","mejs.filipino":"Filipino","mejs.finnish":"Fin\u00e9s","mejs.french":"Franc\u00e9s","mejs.galician":"Gallego","mejs.german":"Alem\u00e1n","mejs.greek":"Griego","mejs.haitian-creole":"Creole haitiano","mejs.hebrew":"Hebreo","mejs.hindi":"Indio","mejs.hungarian":"H\u00fangaro","mejs.icelandic":"Island\u00e9s","mejs.indonesian":"Indonesio","mejs.irish":"Irland\u00e9s","mejs.italian":"Italiano","mejs.japanese":"Japon\u00e9s","mejs.korean":"Coreano","mejs.latvian":"Let\u00f3n","mejs.lithuanian":"Lituano","mejs.macedonian":"Macedonio","mejs.malay":"Malayo","mejs.maltese":"Malt\u00e9s","mejs.norwegian":"Noruego","mejs.persian":"Persa","mejs.polish":"Polaco","mejs.portuguese":"Portugu\u00e9s","mejs.romanian":"Rumano","mejs.russian":"Ruso","mejs.serbian":"Serbio","mejs.slovak":"Eslovaco","mejs.slovenian":"Esloveno","mejs.spanish":"Espa\u00f1ol","mejs.swahili":"Swahili","mejs.swedish":"Sueco","mejs.tagalog":"Tagalo","mejs.thai":"Thai","mejs.turkish":"Turco","mejs.ukrainian":"Ukraniano","mejs.vietnamese":"Vietnamita","mejs.welsh":"Gal\u00e9s","mejs.yiddish":"Yiddish"}};
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1'></script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.2.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.2.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var rm_pre_data = {"ajax_url":"https:\/\/observatorio.minedu.gob.pe\/wp-admin\/admin-ajax.php","rm_nonce_field":"c7f453c323"};
/* ]]> */
</script>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/plugins/icegram-rainmaker/classes/../assets/js/main.js?ver=0.37'></script>
<style id="et-core-unified-cached-inline-styles-2">
@font-face{font-family:"Gotham Rounded Medium";src:url("https://observatorio.minedu.gob.pe/wp-content/uploads/et-fonts/Gotham-Rounded-Medium-1.otf") format("opentype")}@font-face{font-family:"calibri";src:url("https://observatorio.minedu.gob.pe/wp-content/uploads/et-fonts/calibri-1.ttf") format("truetype")}@font-face{font-family:"Gotham Rounded Light";src:url("https://observatorio.minedu.gob.pe/wp-content/uploads/et-fonts/Gotham-Rounded-Light-1.otf") format("opentype")}
@font-face{font-family:"Gotham Rounded Bold";src:url("https://observatorio.minedu.gob.pe/wp-content/uploads/et-fonts/Gotham-Rounded-Bold-1.otf") format("opentype")}.et_pb_section_0.et_pb_section{margin-top:0px;background-color:rgba(0,0,0,0)!important}.et_pb_row_17.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_text_22 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_22{font-family:'Calibri Bold',sans-serif}.et_pb_image_6{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_row_18.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_divider_4:hover:before{border-top-width:px}.et_pb_divider_4:before{border-top-color:#dedef9;border-top-style:dotted;border-top-width:2px}.et_pb_divider_4{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_button_3,.et_pb_button_3:after{transition:all 300ms ease 0ms}.et_pb_text_23{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}body #page-container .et_pb_section .et_pb_button_3:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_3:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}body #page-container .et_pb_section .et_pb_button_3:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_3{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}.et_pb_button_3_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-9px!important}.et_pb_button_3_wrapper .et_pb_button_3,.et_pb_button_3_wrapper .et_pb_button_3:hover{padding-bottom:0px!important}.et_pb_text_21{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_text_23.et_pb_text{color:#b5b5b5!important}.et_pb_text_23 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_20{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_button_4,.et_pb_button_4:after{transition:all 300ms ease 0ms}.et_pb_text_26{font-family:'Calibri Bold',sans-serif}.et_pb_image_7{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_row_20.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_divider_5:hover:before{border-top-width:px}.et_pb_divider_5:before{border-top-color:#dedef9;border-top-style:dotted;border-top-width:2px}.et_pb_divider_5{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_row_19.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}body #page-container .et_pb_section .et_pb_button_4:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_text_24{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}body #page-container .et_pb_section .et_pb_button_4:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}body #page-container .et_pb_section .et_pb_button_4:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_4{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}.et_pb_button_4_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}.et_pb_button_4_wrapper .et_pb_button_4,.et_pb_button_4_wrapper .et_pb_button_4:hover{padding-bottom:0px!important}.et_pb_text_25{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_text_24 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_20 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_19 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_27.et_pb_text{color:#b5b5b5!important}.et_pb_contact_form_0.et_pb_contact_form_container .input::-moz-placeholder{font-size:18px}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{background-image:initial!important;background-color:#ffdb0f!important}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{color:}body.et_button_custom_icon #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:12px}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:1.6em}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{color:#603e00!important;border-width:0px!important;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;background-color:#ffc30f}.et_pb_contact_form_0.et_pb_contact_form_container{margin-left:-160px!important}.et_pb_contact_form_0.et_pb_contact_form_container .input:-ms-input-placeholder{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input::-webkit-input-placeholder{font-size:18px}.et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label i,.et_pb_contact_form_0 .input[type="radio"]:active+label i{background-color:#f4f2de}.et_pb_section_0{z-index:9}.et_pb_contact_form_0.et_pb_contact_form_container h1,.et_pb_contact_form_0.et_pb_contact_form_container h2.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h3.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h4.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h5.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h6.et_pb_contact_main_title{font-family:'Calibri Bold',sans-serif;font-size:21px;color:#575756!important}.et_pb_image_4{margin-top:38px!important;margin-left:27px!important;text-align:left;margin-left:0}.et_pb_divider_3:hover:before{border-top-width:px}.et_pb_divider_3:before{border-top-color:#fce146;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_divider_3{background-color:rgba(0,0,0,0);padding-top:0px;padding-bottom:0px;margin-top:-28px!important;margin-bottom:0px!important;width:14%;max-width:100%}.et_pb_text_17{margin-top:40px!important}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label i,.et_pb_contact_form_0 .input[type="radio"]+label i{background-color:#f4f2de}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label,.et_pb_contact_form_0 .input[type="radio"]+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked+label i:before,.et_pb_contact_form_0 .input::placeholder{color:#575756}.et_pb_text_19{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}.et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{transition:background-color 300ms ease 0ms}.et_pb_text_19.et_pb_text{color:#b5b5b5!important}.et_pb_text_18 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_18{font-family:'Calibri Bold',sans-serif}.et_pb_image_5{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_row_16.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_contact_form_0 .input[type="radio"]:checked:active+label i:before{background-color:#575756}.et_pb_contact_form_0 .input[type="radio"]:checked+label i:before{background-color:#575756}.et_pb_contact_form_0 p textarea:focus:-ms-input-placeholder{color:#575756}.et_pb_contact_form_0 .input::-webkit-input-placeholder{color:#575756}.et_pb_contact_form_0 p textarea:focus::-moz-placeholder{color:#575756}.et_pb_contact_form_0 p textarea:focus::-webkit-input-placeholder{color:#575756}.et_pb_contact_form_0 p .input:focus:-ms-input-placeholder{color:#575756}.et_pb_contact_form_0 p .input:focus::-moz-placeholder{color:#575756}.et_pb_contact_form_0 p .input:focus::-webkit-input-placeholder{color:#575756}.et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label,.et_pb_contact_form_0 .input[type="radio"]:active+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked:active+label i:before{color:#575756}.et_pb_contact_form_0 .input::-ms-input-placeholder{color:#575756}.et_pb_contact_form_0 .input::-moz-placeholder{color:#575756}.et_pb_text_26 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_27{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}.et_pb_text_17.et_pb_text{color:#ffffff!important}body #page-container .et_pb_section .et_pb_button_7:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}.et_pb_section_7{border-top-width:7px;border-top-color:#f8ae0d}div.et_pb_section.et_pb_section_7{background-image:linear-gradient(2deg,#1d0038 0%,#462a66 100%)!important}.et_pb_section_6.et_pb_section{padding-top:2vw;padding-right:0px;padding-bottom:2vw;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px}.et_pb_section_6{border-top-width:7px;border-top-color:#f8ae0d}div.et_pb_section.et_pb_section_6{background-image:linear-gradient(2deg,#1d0038 0%,#462a66 100%)!important}.et_pb_button_7,.et_pb_button_7:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_7:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_7:hover{color:#ffe30f!important}.et_pb_row_25.et_pb_row{padding-top:0px!important;padding-bottom:8px!important;margin-top:-6px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:8px}body #page-container .et_pb_section .et_pb_button_7{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}.et_pb_button_7_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}.et_pb_button_7_wrapper .et_pb_button_7,.et_pb_button_7_wrapper .et_pb_button_7:hover{padding-bottom:0px!important}.et_pb_text_37{padding-top:0px!important;margin-top:-33px!important;margin-bottom:3px!important}.et_pb_text_36 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_36{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_text_35 h1{font-family:'Calibri Bold',sans-serif}.et_pb_section_7.et_pb_section{padding-top:2vw;padding-right:0px;padding-bottom:2vw;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px}.et_pb_text_38.et_pb_text{color:#937ac9!important}.et_pb_text_35.et_pb_text{color:#b5b5b5!important}.et_pb_text_41{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:14px;line-height:20px;padding-top:0px!important;margin-top:-30px!important}.et_pb_text_42 p{line-height:20px}.et_pb_text_42.et_pb_text{color:#c1afea!important}.et_pb_blurb_0 .et-pb-icon{font-size:31px;color:#937ac9}.et_pb_blurb_0.et_pb_blurb{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:14px;color:#c1afea!important;letter-spacing:0.5px;line-height:20px;padding-top:40px!important;margin-left:36px!important}.et_pb_blurb_0.et_pb_blurb .et_pb_blurb_description{text-align:left}.et_pb_blurb_0.et_pb_blurb p{line-height:20px}.et_pb_blurb_0.et_pb_blurb h4,.et_pb_blurb_0.et_pb_blurb h4 a,.et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header a{color:#ffffff!important}.et_pb_text_41 p{line-height:20px}.et_pb_text_38 p{line-height:20px}.et_pb_text_41.et_pb_text{color:#c1afea!important}.et_pb_text_40{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:14px;line-height:20px;margin-top:-19px!important}.et_pb_text_40 p{line-height:20px}.et_pb_text_40.et_pb_text{color:#937ac9!important}.et_pb_text_39{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:14px;line-height:20px;padding-top:0px!important;margin-top:-31px!important}.et_pb_text_39 p{line-height:20px}.et_pb_text_39.et_pb_text{color:#c1afea!important}.et_pb_text_38{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:14px;line-height:20px}.et_pb_text_35{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}.et_pb_text_34 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_27 h1{font-family:'Calibri Bold',sans-serif}body #page-container .et_pb_section .et_pb_button_5:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_image_8{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_row_22.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_divider_6:hover:before{border-top-width:px}.et_pb_divider_6:before{border-top-color:#dedef9;border-top-style:dotted;border-top-width:2px}.et_pb_divider_6{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_row_21.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_button_5,.et_pb_button_5:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_5:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}.et_pb_text_30 h1{font-family:'Calibri Bold',sans-serif}body #page-container .et_pb_section .et_pb_button_5:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_5{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}.et_pb_button_5_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}.et_pb_button_5_wrapper .et_pb_button_5,.et_pb_button_5_wrapper .et_pb_button_5:hover{padding-bottom:0px!important}.et_pb_text_29{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_text_28 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_28{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_text_30{font-family:'Calibri Bold',sans-serif}.et_pb_text_31.et_pb_text{color:#b5b5b5!important}.et_pb_text_34{font-family:'Calibri Bold',sans-serif}body #page-container .et_pb_section .et_pb_button_6:after{color:#ffc30f;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_image_9{margin-right:-34px!important;text-align:right;margin-right:0}.et_pb_row_24.et_pb_row{padding-top:0px!important;padding-bottom:5px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:5px}.et_pb_divider_7:hover:before{border-top-width:px}.et_pb_divider_7:before{border-top-color:#dedef9;border-top-style:dotted;border-top-width:2px}.et_pb_divider_7{background-color:rgba(0,0,0,0);margin-top:0px!important;margin-bottom:0px!important}.et_pb_row_23.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_button_6,.et_pb_button_6:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_6:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}.et_pb_text_31{font-family:'Calibri',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-53px!important;margin-left:184px!important}body #page-container .et_pb_section .et_pb_button_6:hover{color:#ffe30f!important}body #page-container .et_pb_section .et_pb_button_6{color:#ffc30f!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:12px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:rgba(0,0,0,0)}.et_pb_button_6_wrapper{margin-top:0px!important;margin-bottom:0px!important;margin-left:-12px!important}.et_pb_button_6_wrapper .et_pb_button_6,.et_pb_button_6_wrapper .et_pb_button_6:hover{padding-bottom:0px!important}.et_pb_text_33{padding-top:0px!important;padding-bottom:0px!important;margin-top:-33px!important;margin-bottom:0px!important}.et_pb_text_32 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_32{font-family:'Calibri Italic',sans-serif;font-size:13.5px;padding-top:0px!important;margin-top:-36px!important}.et_pb_text_31 h1{font-family:'Calibri Bold',sans-serif}.et_pb_text_17 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#a5d33b!important}.et_pb_contact_form_0.et_pb_contact_form_container .input,.et_pb_contact_form_0.et_pb_contact_form_container .input::placeholder,.et_pb_contact_form_0.et_pb_contact_form_container .input[type=checkbox]+label,.et_pb_contact_form_0.et_pb_contact_form_container .input[type=radio]+label{font-size:18px}.et_pb_row_14.et_pb_row{padding-top:25px!important;margin-top:0px!important;padding-top:25px}.et_pb_row_5{background-image:linear-gradient(1deg,rgba(137,196,0,0) 0%,rgba(199,226,43,0) 100%);border-radius:20px 20px 0 0;overflow:hidden}.et_pb_text_7.et_pb_text{color:#ffffff!important}.et_pb_row_6.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_section_2.et_pb_section{padding-top:0px;padding-bottom:22px;margin-top:0px;background-color:#ffffff!important}.et_pb_video_0 .et_pb_video_overlay_hover:hover{background-color:rgba(0,0,0,.6)}.et_pb_video_0{padding-right:0px;padding-left:0px;max-width:100%}.et_pb_row_5.et_pb_row{padding-top:22px!important;padding-right:0px!important;padding-bottom:0px!important;padding-left:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:22px;padding-right:0px;padding-bottom:0px;padding-left:0px}.et_pb_text_6{font-family:'Calibri',sans-serif;line-height:25px;border-left-color:#e5f45f;padding-top:0px!important;padding-bottom:13px!important;margin-top:-28px!important;margin-bottom:0px!important}.et_pb_text_7{margin-top:20px!important}.et_pb_text_6 p{line-height:25px}.et_pb_text_6.et_pb_text{color:#000000!important}.et_pb_row_4.et_pb_row{padding-top:23px!important;padding-right:0px!important;padding-bottom:0px!important;padding-left:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:23px;padding-right:0px;padding-bottom:0px;padding-left:0px}.et_pb_row_4{border-radius:0 0 0 0;overflow:hidden;border-bottom-color:#e0f21d}.et_pb_divider_0:hover:before{border-top-width:px}.et_pb_divider_0:before{border-top-color:#ffade7;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_divider_0{background-color:rgba(0,0,0,0);padding-top:0px;margin-top:-28px!important;width:14%;max-width:100%}.et_pb_text_7 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#7f6ba5!important}.et_pb_divider_1{background-color:rgba(0,0,0,0);padding-top:0px;margin-top:-28px!important;width:14%;max-width:100%}.et_pb_text_5 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#ffffff!important}.et_pb_team_member_1 .et_pb_member_social_links a{color:#a9cec7!important}.et_pb_section_3.et_pb_section{padding-top:0px;padding-right:0px;padding-bottom:20px;padding-left:0px;background-color:#f4f2de!important}.et_pb_team_member_2 .et_pb_member_social_links a{color:#a9cec7!important}.et_pb_team_member_2.et_pb_team_member .et_pb_member_position{font-family:'Calibri Bold',sans-serif;color:#83bf2f!important;text-align:center}.et_pb_team_member_2.et_pb_team_member{text-align:left}.et_pb_team_member_2.et_pb_team_member h4,.et_pb_team_member_2.et_pb_team_member h1.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h2.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h3.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h5.et_pb_module_header,.et_pb_team_member_2.et_pb_team_member h6.et_pb_module_header{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-align:center}.et_pb_team_member_1 .et_pb_member_social_links .et_pb_font_icon{font-size:18px}.et_pb_team_member_1.et_pb_team_member .et_pb_member_position{font-family:'Calibri Bold',sans-serif;color:#83bf2f!important;text-align:center}.et_pb_divider_1:before{border-top-color:#eab2dd;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_team_member_1.et_pb_team_member{text-align:left}.et_pb_team_member_0 .et_pb_member_social_links .et_pb_font_icon{font-size:18px}.et_pb_team_member_0 .et_pb_member_social_links a{color:#a9cec7!important}.et_pb_team_member_0.et_pb_team_member .et_pb_member_position{font-family:'Calibri Bold',sans-serif;font-size:16px;color:#83bf2f!important;text-align:center}.et_pb_team_member_0.et_pb_team_member{text-align:left}.et_pb_team_member_0.et_pb_team_member h4,.et_pb_team_member_0.et_pb_team_member h1.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h2.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h3.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h5.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h6.et_pb_module_header{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-align:center}.et_pb_divider_1:hover:before{border-top-width:px}.et_pb_text_5{margin-top:30px!important}.et_pb_text_5.et_pb_text{color:#ffffff!important}.et_pb_row_8.et_pb_row{padding-top:0px!important;margin-top:0px!important;padding-top:0px}.et_pb_fullwidth_slider_0.et_pb_slider.et_pb_module .et_pb_slides .et_pb_slide_content{font-family:'Calibri Bold',sans-serif;letter-spacing:0.5px;text-shadow:0em 0em 0.3em #000000}.et_pb_row_0{border-top-width:1.5px;border-bottom-width:1.5px;border-style:dotted dashed;border-top-color:#fff130;border-bottom-color:#fff130}.et_pb_section_1.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiNmZmZmZmYiPjxwYXRoIGQ9Ik0wIDQ3LjQ0TDE3MCAwbDYyNi40OCA5NC44OUwxMTEwIDg3LjExbDE3MC0zOS42N1YxNDBIMFY0Ny40NHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMCA5MC43MmwxNDAtMjguMjggMzE1LjUyIDI0LjE0TDc5Ni40OCA2NS44IDExNDAgMTA0Ljg5bDE0MC0xNC4xN1YxNDBIMFY5MC43MnoiLz48L2c+PC9zdmc+);background-size:100% 100px;bottom:0;height:100px;z-index:1}.et_pb_section_1{z-index:8}.et_pb_section_1.et_pb_section{padding-top:2px;padding-bottom:110px;margin-top:0px;margin-bottom:0px;background-color:rgba(0,0,0,0)!important}div.et_pb_section.et_pb_section_1{background-image:linear-gradient(0deg,#6a820a 0%,#9fcc26 100%)!important}.et_pb_fullwidth_slider_0{width:100%}.et_pb_fullwidth_slider_0.et_pb_slider .et_pb_slide_description .et_pb_slide_title{font-family:'Gotham Rounded Bold',Helvetica,Arial,Lucida,sans-serif;color:#aaff00!important;text-shadow:0em 0em 0.3em #000000}.et_pb_code_0{padding-top:0px;padding-bottom:0px;margin-top:0px!important;margin-bottom:0px!important}.et_pb_slide_2{background-color:#413dba}.et_pb_slide_2.et_pb_slide .et_pb_slide_overlay_container{background-color:rgba(0,0,0,0.4)}.et_pb_slide_1{background-color:#413dba}.et_pb_slide_1.et_pb_slide .et_pb_slide_overlay_container{background-color:rgba(0,0,0,0.4)}.et_pb_slide_0{background-color:#413dba}.et_pb_slide_0.et_pb_slide .et_pb_slide_overlay_container{background-color:rgba(0,0,0,0.4)}.et_pb_section_0.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDBweCIgdmlld0JveD0iMCAwIDEyODAgMTQwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxnIGZpbGw9IiM5ZmNjMjYiPjxwYXRoIGQ9Ik0wIDcwLjM1bDMyMC00OS4yNCA2NDAgOTguNDkgMzIwLTQ5LjI1VjE0MEgwVjcwLjM1eiIvPjwvZz48L3N2Zz4=);background-size:100% 100px;bottom:0;height:100px;z-index:10}.et_pb_row_0.et_pb_row{padding-top:3px!important;padding-bottom:2px!important;padding-top:3px;padding-bottom:2px}.et_pb_code_1{padding-top:2px;padding-bottom:0px}.et_pb_row_3.et_pb_row{padding-top:0px!important;padding-bottom:10px!important;margin-top:0px!important;padding-top:0px;padding-bottom:10px}.et_pb_text_1{font-family:'Calibri Bold',sans-serif;font-size:13px;padding-bottom:0px!important;margin-top:-5px!important;margin-bottom:0px!important}.et_pb_text_4{font-family:'Calibri Bold',sans-serif;font-size:13px;padding-bottom:0px!important;margin-top:-5px!important;margin-bottom:0px!important}.et_pb_text_4.et_pb_text{color:rgba(0,0,0,0.34)!important}.et_pb_text_3{font-family:'Calibri Bold',sans-serif;font-size:13px;padding-bottom:0px!important;margin-top:-5px!important;margin-bottom:0px!important}.et_pb_text_3.et_pb_text{color:rgba(0,0,0,0.34)!important}.et_pb_text_2{font-family:'Calibri Bold',sans-serif;font-size:13px;padding-bottom:0px!important;margin-top:-5px!important;margin-bottom:0px!important}.et_pb_text_2.et_pb_text{color:#000000!important}.et_pb_text_1.et_pb_text{color:rgba(0,0,0,0.34)!important}.et_pb_code_2{padding-top:0px;padding-bottom:0px;margin-top:0px!important;margin-bottom:0px!important;margin-left:148px!important}.et_pb_text_0{font-family:'Calibri Bold',sans-serif;font-size:13px;letter-spacing:1px;padding-bottom:0px!important;margin-top:-5px!important;margin-bottom:0px!important}.et_pb_text_0.et_pb_text{color:rgba(0,0,0,0.34)!important}.et_pb_row_2.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_row_2{background-color:rgba(0,0,0,0);border-bottom-style:dotted;border-bottom-color:#fff130}.et_pb_image_0{text-align:center}.et_pb_row_1.et_pb_row{padding-top:5px!important;padding-bottom:4px!important;padding-top:5px;padding-bottom:4px}.et_pb_row_1{background-color:rgba(0,0,0,0);border-radius:0px 0px 0px 0px;overflow:hidden;border-top-style:dashed;border-bottom-style:dotted;border-top-color:#c7e22b;border-bottom-color:#fff130;z-index:11}.et_pb_section_3.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI0OXB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2FmZDAyZCI+PHBhdGggZD0iTTEyODAgMEw2NDAgNzAgMCAwdjE0MGw2NDAtNzAgNjQwIDcwVjB6IiBmaWxsLW9wYWNpdHk9Ii41Ii8+PHBhdGggZD0iTTEyODAgMEgwbDY0MCA3MCA2NDAtNzB6Ii8+PC9nPjwvc3ZnPg==);background-size:100% 49px;bottom:0;height:49px;z-index:1;transform:rotateY(180deg) rotateX(180deg)}.et_pb_team_member_1.et_pb_team_member h4,.et_pb_team_member_1.et_pb_team_member h1.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h2.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h3.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h5.et_pb_module_header,.et_pb_team_member_1.et_pb_team_member h6.et_pb_module_header{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-align:center}.et_pb_text_8.et_pb_text{color:#575756!important}.et_pb_text_15 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_section_4{border-bottom-width:9px;border-bottom-color:#ecf2bf}div.et_pb_section.et_pb_section_4{background-image:linear-gradient(180deg,#afd02d 0%,#d6e57e 100%)!important}.et_pb_text_16{font-family:'Calibri',sans-serif}.et_pb_text_16.et_pb_text{color:#575756!important}.et_pb_image_3 .et_pb_image_wrap,.et_pb_image_3 img{width:100%}.et_pb_image_3{width:100%;max-width:100%!important;text-align:left;margin-left:0}.et_pb_text_15{font-family:'Calibri',sans-serif;padding-top:0px!important;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_section_4.section_has_divider.et_pb_top_divider .et_pb_top_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2Y0ZjJkZSI+PC9nPjwvc3ZnPg==);background-size:100% 20px;top:0;height:20px;z-index:1}.et_pb_text_15.et_pb_text{color:#575756!important}.et_pb_text_14{font-family:'Calibri',sans-serif}.et_pb_text_14.et_pb_text{color:#575756!important}.et_pb_video_1 .et_pb_video_overlay_hover:hover{background-color:rgba(0,0,0,0.6)}.et_pb_text_13 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_text_13{font-family:'Calibri',sans-serif;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_text_13.et_pb_text{color:#575756!important}.et_pb_section_4.et_pb_section{padding-top:0px;padding-bottom:0px;margin-top:0px;margin-bottom:0px;background-color:#98ba28!important}.et_pb_row_13.et_pb_row{padding-top:8px!important;padding-bottom:30px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:8px;padding-bottom:30px}.et_pb_text_12.et_pb_text{color:#575756!important}.et_pb_button_1,.et_pb_button_1:after{transition:all 300ms ease 0ms}.et_pb_section_5.et_pb_section{padding-top:0px;margin-top:0px;background-color:#ffffff!important}.et_pb_section_5{border-top-width:0px;border-top-color:rgba(0,0,0,0)}.et_pb_button_2,.et_pb_button_2:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_2:hover{background-image:initial!important;background-color:#ff6321!important}body #page-container .et_pb_section .et_pb_button_2:after{color:rgba(255,255,255,0.43);line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_2:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:}body #page-container .et_pb_section .et_pb_button_2{color:#ffffff!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:15px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:#ff9000}body #page-container .et_pb_section .et_pb_button_1:hover{background-image:initial!important;background-color:#ff6321!important}body #page-container .et_pb_section .et_pb_button_0{color:#ffffff!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:15px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:#ff9000}body #page-container .et_pb_section .et_pb_button_1:after{color:rgba(255,255,255,0.43);line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_1:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:}body #page-container .et_pb_section .et_pb_button_1{color:#ffffff!important;border-width:0px!important;border-color:#ffe30f;border-radius:25px;letter-spacing:1px;font-size:15px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:#ff9000}.et_pb_button_0,.et_pb_button_0:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_0:hover{background-image:initial!important;background-color:#ff6321!important}body #page-container .et_pb_section .et_pb_button_0:after{color:rgba(255,255,255,0.43);line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:}.et_pb_text_12{font-family:'Calibri',sans-serif}.et_pb_text_42{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:14px;letter-spacing:0.5px;line-height:20px;padding-top:40px!important}.et_pb_image_2 .et_pb_image_wrap,.et_pb_image_2 img{width:100%}.et_pb_text_11.et_pb_text{color:#575756!important}.et_pb_text_8{font-family:'Calibri',sans-serif;margin-top:60px!important}.et_pb_text_8 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#7f6ba5!important}.et_pb_divider_2{background-color:rgba(0,0,0,0);padding-top:0px;padding-bottom:0px;margin-top:-28px!important;margin-bottom:0px!important;width:14%;max-width:100%}.et_pb_divider_2:before{border-top-color:#eab2dd;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}.et_pb_divider_2:hover:before{border-top-width:px}.et_pb_text_9.et_pb_text{color:#575756!important}.et_pb_text_9{font-family:'Calibri',sans-serif;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_text_9 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_image_1 .et_pb_image_wrap,.et_pb_image_1 img{width:100%}.et_pb_text_10.et_pb_text{color:#575756!important}.et_pb_text_10{font-family:'Calibri',sans-serif}.et_pb_text_10.et_pb_text a{color:#00a99d!important}.et_pb_image_1{width:100%;max-width:100%!important;text-align:left;margin-left:0}.et_pb_image_2{width:100%;max-width:100%!important;text-align:left;margin-left:0}.et_pb_text_11 h4{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;text-transform:uppercase;color:#575756!important;line-height:27px}.et_pb_text_11{font-family:'Calibri',sans-serif;margin-top:-32px!important;margin-bottom:-8px!important}.et_pb_column_40{z-index:9;position:relative}.et_pb_column_29{z-index:9;position:relative}.et_pb_column_39{z-index:9;position:relative}.et_pb_column_11{z-index:9;position:relative}.et_pb_column_0{z-index:9;position:relative}.et_pb_column_49{z-index:9;position:relative}.et_pb_column_25{z-index:9;position:relative}.et_pb_column_38{z-index:9;position:relative}.et_pb_column_9{z-index:9;position:relative}.et_pb_column_14{z-index:9;position:relative}.et_pb_column_16{z-index:9;position:relative}.et_pb_column_26{z-index:9;position:relative}.et_pb_column_28{z-index:9;position:relative}.et_pb_column_37{z-index:9;position:relative}.et_pb_column_35{z-index:9;position:relative}
	.et_pb_column_19{
		z-index:9;position:relative
	}
	.et_pb_slider .et_pb_slide_2{
		<?php if(!empty($bp_slider_imagen_2)){ ?>
		background-image:url(<?php echo $bp_slider_imagen_2;?>)
		<?php } ?>
	}
	.et_pb_column_50{
		z-index:9;position:relative
	}
	.et_pb_column_36{
		z-index:9;position:relative
	}
	.et_pb_column_20{
		z-index:9;position:relative
	}
	.et_pb_slider .et_pb_slide_1{
		/*background-image:url(https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/buenapractica-portada-foto2.jpg)*/
		<?php if(!empty($bp_slider_imagen_1)){ ?>
		background-image:url(<?php echo $bp_slider_imagen_1;?>)
		<?php } ?>
	}
	.et_pb_column_42{
		z-index:9;position:relative
	}
	.et_pb_column_51{
		z-index:9;position:relative
	}
	.et_pb_slider .et_pb_slide_0{
		/*background-image:url(https://observatorio.minedu.gob.pe/wp-content/uploads/2019/10/buenapractica-portada-foto1.jpg)*/
		<?php if(!empty($bp_slider_imagen_0)){ ?>
		background-image:url(<?php echo $bp_slider_imagen_0;?>)
		<?php } ?>
	}
	.et_pb_column_10{
		z-index:9;position:relative
	}
	.et_pb_column_1{
		z-index:9;position:relative
	}
	.et_pb_column_17{
		z-index:9;position:relative
	}
	.et_pb_column_22{
		z-index:9;position:relative
	}
	.et_pb_column_33{z-index:9;position:relative}.et_pb_column_15{z-index:9;position:relative}.et_pb_column_8{z-index:9;position:relative}.et_pb_column_45{z-index:9;position:relative}.et_pb_column_30{z-index:9;position:relative}.et_pb_column_18{z-index:9;position:relative}.et_pb_column_32{z-index:9;position:relative}.et_pb_column_7{z-index:9;position:relative}.et_pb_column_46{z-index:9;position:relative}.et_pb_column_23{z-index:9;position:relative}.et_pb_column_47{z-index:9;position:relative}.et_pb_column_6{z-index:9;position:relative}.et_pb_column_12{z-index:9;position:relative}.et_pb_column_27{z-index:9;position:relative}.et_pb_column_5{z-index:9;position:relative}.et_pb_column_31{z-index:9;position:relative}.et_pb_column_41{z-index:9;position:relative}.et_pb_column_4{z-index:9;position:relative}.et_pb_column_21{z-index:9;position:relative}.et_pb_column_43{z-index:9;position:relative}.et_pb_column_3{z-index:9;position:relative}.et_pb_column_34{z-index:9;position:relative}.et_pb_column_13{z-index:9;position:relative}.et_pb_column_24{z-index:9;position:relative}.et_pb_column_2{z-index:9;position:relative}.et_pb_column_48{z-index:9;position:relative}.et_pb_column_44{z-index:9;position:relative}@media only screen and (max-width:980px){.et_pb_section_0.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI3MnB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iIzlmY2MyNiI+PHBhdGggZD0iTTAgNzAuMzVsMzIwLTQ5LjI0IDY0MCA5OC40OSAzMjAtNDkuMjVWMTQwSDBWNzAuMzV6Ii8+PC9nPjwvc3ZnPg==);background-size:100% 72px;bottom:0;height:72px;z-index:10;transform:rotateY(0) rotateX(0)}body #page-container .et_pb_section .et_pb_button_3:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_4:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_4:before{display:none}body #page-container .et_pb_section .et_pb_button_4:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_4:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_4{padding-left:0.7em;padding-right:2em}.et_pb_image_6{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_3:before{display:none}body #page-container .et_pb_section .et_pb_button_5{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_3:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_3:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_3{padding-left:0.7em;padding-right:2em}.et_pb_image_5{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}.et_pb_image_7{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_5:hover{padding-left:0.7em;padding-right:2em}.et_pb_image_4{margin-left:auto;margin-right:auto}.et_pb_image_9{margin-left:auto;margin-right:auto}.et_pb_section_6{border-top-width:7px;border-top-color:#f8ae0d}body #page-container .et_pb_section .et_pb_button_7:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_7:before{display:none}body #page-container .et_pb_section .et_pb_button_7:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_7:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_7{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_6:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_5:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_6:before{display:none}body #page-container .et_pb_section .et_pb_button_6:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_6:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_6{padding-left:0.7em;padding-right:2em}.et_pb_image_8{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_5:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_5:before{display:none}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}.et_pb_text_17{margin-top:0px!important}.et_pb_fullwidth_slider_0 .et_pb_slide_description,.et_pb_slider_fullwidth_off.et_pb_fullwidth_slider_0 .et_pb_slide_description{padding-top:30vw;padding-bottom:30vw}.et_pb_row_4{border-bottom-color:#e0f21d}.et_pb_text_8{margin-top:35px!important}.et_pb_section_3.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI0OXB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2FmZDAyZCI+PHBhdGggZD0iTTEyODAgMTQwVjBIMGwxMjgwIDE0MHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMTI4MCA5OFYwSDBsMTI4MCA5OHoiLz48L2c+PC9zdmc+);background-size:100% 49px;bottom:0;height:49px;z-index:1;transform:rotateY(180deg) rotateX(180deg)}.et_pb_text_7{margin-top:60px!important}.et_pb_row_5.et_pb_row{padding-right:0px!important;padding-left:0px!important;padding-right:0px!important;padding-left:0px!important}.et_pb_text_6{border-left-color:#e5f45f}.et_pb_row_4.et_pb_row{padding-right:0px!important;padding-left:0px!important;padding-right:0px!important;padding-left:0px!important}.et_pb_text_5{margin-top:30px!important}.et_pb_image_1{text-align:center;margin-left:auto;margin-right:auto}.et_pb_text_5 h1{font-size:40px}.et_pb_row_2{border-bottom-style:dotted;border-bottom-color:#fff130}.et_pb_image_0{margin-left:auto;margin-right:auto}.et_pb_row_1{border-top-style:dashed;border-bottom-style:dotted;border-top-color:#c7e22b;border-bottom-color:#fff130}.et_pb_row_0{border-top-width:1.5px;border-bottom-width:1.5px;border-top-style:dotted;border-bottom-style:dotted;border-top-color:#fff130;border-bottom-color:#fff130}.et_pb_section_1.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI3N3B4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTAgNDcuNDRMMTcwIDBsNjI2LjQ4IDk0Ljg5TDExMTAgODcuMTFsMTcwLTM5LjY3VjE0MEgwVjQ3LjQ0eiIgZmlsbC1vcGFjaXR5PSIuNSIvPjxwYXRoIGQ9Ik0wIDkwLjcybDE0MC0yOC4yOCAzMTUuNTIgMjQuMTRMNzk2LjQ4IDY1LjggMTE0MCAxMDQuODlsMTQwLTE0LjE3VjE0MEgwVjkwLjcyeiIvPjwvZz48L3N2Zz4=);background-size:100% 77px;bottom:0;height:77px;z-index:1;transform:rotateY(0) rotateX(0)}.et_pb_fullwidth_slider_0{margin-bottom:0px!important}.et_pb_text_9{margin-bottom:-12px!important}.et_pb_image_2{text-align:center;margin-left:auto;margin-right:auto}.et_pb_section_5{border-top-width:0px;border-top-color:rgba(0,0,0,0)}body #page-container .et_pb_section .et_pb_button_1:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_2:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_2:before{display:none}body #page-container .et_pb_section .et_pb_button_2:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_2:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_2{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_1:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_1:before{display:none}body #page-container .et_pb_section .et_pb_button_1:hover{padding-left:0.7em;padding-right:2em}.et_pb_image_3{text-align:center;margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_1{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_0:before{display:none}body #page-container .et_pb_section .et_pb_button_0:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0{padding-left:0.7em;padding-right:2em}.et_pb_section_4{border-bottom-width:9px;border-bottom-color:#ecf2bf}.et_pb_section_7{border-top-width:7px;border-top-color:#f8ae0d}}@media only screen and (max-width:767px){.et_pb_section_0.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIzNnB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iIzlmY2MyNiI+PHBhdGggZD0iTTAgNzAuMzVsMzIwLTQ5LjI0IDY0MCA5OC40OSAzMjAtNDkuMjVWMTQwSDBWNzAuMzV6Ii8+PC9nPjwvc3ZnPg==);background-size:100% 36px;bottom:0;height:36px;z-index:10;transform:rotateY(0) rotateX(0)}body #page-container .et_pb_section .et_pb_button_3:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_4:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_4:before{display:none}body #page-container .et_pb_section .et_pb_button_4:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_4:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_4{padding-left:0.7em;padding-right:2em}.et_pb_image_6{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_3:before{display:none}body #page-container .et_pb_section .et_pb_button_5{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_3:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_3:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_3{padding-left:0.7em;padding-right:2em}.et_pb_image_5{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}.et_pb_image_7{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_5:hover{padding-left:0.7em;padding-right:2em}.et_pb_image_4{margin-left:auto;margin-right:auto}.et_pb_image_9{margin-left:auto;margin-right:auto}.et_pb_section_6{border-top-width:7px;border-top-color:#f8ae0d}body #page-container .et_pb_section .et_pb_button_7:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_7:before{display:none}body #page-container .et_pb_section .et_pb_button_7:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_7:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_7{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_6:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_5:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_6:before{display:none}body #page-container .et_pb_section .et_pb_button_6:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_6:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_6{padding-left:0.7em;padding-right:2em}.et_pb_image_8{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_5:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_5:before{display:none}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}.et_pb_section_5{border-top-width:0px;border-top-color:rgba(0,0,0,0)}.et_pb_fullwidth_slider_0 .et_pb_slide_description,.et_pb_slider_fullwidth_off.et_pb_fullwidth_slider_0 .et_pb_slide_description{padding-top:38vw;padding-bottom:38vw}.et_pb_text_6{border-left-color:#e5f45f;padding-bottom:20px!important}.et_pb_image_1{margin-left:auto;margin-right:auto}.et_pb_text_8 h1{font-size:30px}.et_pb_section_3.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI0OXB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2FmZDAyZCI+PHBhdGggZD0iTTEyODAgMTQwVjBIMGwxMjgwIDE0MHoiIGZpbGwtb3BhY2l0eT0iLjUiLz48cGF0aCBkPSJNMTI4MCA5OFYwSDBsMTI4MCA5OHoiLz48L2c+PC9zdmc+);background-size:100% 49px;bottom:0;height:49px;z-index:1;transform:rotateY(180deg) rotateX(180deg)}.et_pb_row_7.et_pb_row{margin-top:0px!important}.et_pb_text_7{margin-top:30px!important}.et_pb_text_7 h1{font-size:30px}.et_pb_row_4{border-bottom-color:#e0f21d}.et_pb_image_2{margin-left:auto;margin-right:auto}.et_pb_text_5 h1{font-size:30px}.et_pb_row_3.et_pb_row{margin-top:0px!important}.et_pb_row_2{border-bottom-style:dotted;border-bottom-color:#fff130}.et_pb_image_0{margin-left:auto;margin-right:auto}.et_pb_row_1{border-top-style:dashed;border-bottom-style:dotted;border-top-color:#c7e22b;border-bottom-color:#fff130}.et_pb_row_0{border-top-width:1.5px;border-bottom-width:1.5px;border-top-style:dotted;border-bottom-style:dotted;border-top-color:#fff130;border-bottom-color:#fff130}.et_pb_section_1.section_has_divider.et_pb_bottom_divider .et_pb_bottom_inside_divider{background-image:url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAwJSIgaGVpZ2h0PSI2OHB4IiB2aWV3Qm94PSIwIDAgMTI4MCAxNDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTAgNDcuNDRMMTcwIDBsNjI2LjQ4IDk0Ljg5TDExMTAgODcuMTFsMTcwLTM5LjY3VjE0MEgwVjQ3LjQ0eiIgZmlsbC1vcGFjaXR5PSIuNSIvPjxwYXRoIGQ9Ik0wIDkwLjcybDE0MC0yOC4yOCAzMTUuNTIgMjQuMTRMNzk2LjQ4IDY1LjggMTE0MCAxMDQuODlsMTQwLTE0LjE3VjE0MEgwVjkwLjcyeiIvPjwvZz48L3N2Zz4=);background-size:100% 68px;bottom:0;height:68px;z-index:1;transform:rotateY(0) rotateX(0)}.et_pb_text_11{margin-bottom:-12px!important}.et_pb_text_13{margin-bottom:-12px!important}body #page-container .et_pb_section .et_pb_button_2:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_1:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_2:before{display:none}body #page-container .et_pb_section .et_pb_button_2:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_2:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_2{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_1:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_1:before{display:none}body #page-container .et_pb_section .et_pb_button_1:hover{padding-left:0.7em;padding-right:2em}.et_pb_image_3{margin-left:auto;margin-right:auto}body #page-container .et_pb_section .et_pb_button_1{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em}body #page-container .et_pb_section .et_pb_button_0:before{display:none}body #page-container .et_pb_section .et_pb_button_0:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0{padding-left:0.7em;padding-right:2em}.et_pb_section_4{border-bottom-width:9px;border-bottom-color:#ecf2bf}.et_pb_section_7{border-top-width:7px;border-top-color:#f8ae0d}}#menu .menu-item-type-custom>a{color:#38edcf}.ds-timeline .timeline-item{display:flex;flex-wrap:wrap;align-items:center;margin-bottom:0;padding:0 0 50px;position:relative}.ds-timeline .timeline-item:after{content:'';position:absolute;width:3px;height:100%;visibility:visible;top:0;left:0;margin-left:-1.5px;background-color:#afd02d}.ds-timeline .timeline-item .col-date{position:relative;padding-left:40px}.ds-timeline .timeline-item .col-date:before{content:'';position:absolute;left:-18px;top:50%;background-color:#08af47;width:26px;height:26px;margin-top:-35px;border:5px solid #f4f4f4;-webkit-border-radius:50%;-moz-border-radius:50%;-ms-border-radius:50%;border-radius:50%}.ds-timeline .timeline-item .col-details{padding-left:40px;-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 2px 2px 0 rgba(0,0,0,0.12) 0.5em 0px 0px #000000;-moz-box-shadow:0 2px 2px 0 rgba(0,0,0,0.12) 0.5em 0px 0px #000000;box-shadow:0 2px 2px 0 rgba(0,0,0,0.12) 0.5em 0px 0px #000000}.ds-timeline .tm-date{font-size:18px}.ds-timeline .tm-date h4{font-weight:600;font-size:23px;margin:8px 0}.ds-timeline .tm-video .et_pb_video_play{font-size:50px;line-height:56px;margin:-28px auto auto -28px}.ds-timeline .tm-video .et_pb_video_play:before{content:'\45';-webkit-border-radius:50%;-moz-border-radius:50%;-ms-border-radius:50%;border-radius:50%;-webkit-transition:all 0.3s ease-in;-moz-transition:all 0.3s ease-in;-o-transition:all 0.3s ease-in;transition:all 0.3s ease-in;-webkit-transition-delay:0;-moz-transition-delay:0;-ms-transition-delay:0;-o-transition-delay:0;transition-delay:0;display:inline-block;width:56px;height:56px;top:50%;left:50%;border:2px solid #fff;background-color:rgba(255,255,255,0);color:#fff;text-align:center}.ds-timeline .tm-video:hover .et_pb_video_play:before{background:#00a99d;border-color:#00a99d;-webkit-transform:scale(1.25);-moz-transform:scale(1.25);-ms-transform:scale(1.25);-o-transform:scale(1.25);transform:scale(1.25)}.ds-timeline .tm-desc{padding:30px;background-color:#fff}.ds-timeline .tm-title{border-bottom:2px solid #f4f4f4;border-left:3px solid #00a99d;background-color:#fff;padding:15px 30px}.ds-timeline .tm-title h3{padding:0;line-height:1.1;font-weight:600}.ds-timeline .tm-countdown{background-color:transparent!important;padding:0;margin-bottom:10px!important}.ds-timeline .tm-countdown .et_pb_countdown_timer_container{text-align:left;display:flex}.ds-timeline .tm-countdown .section.values{width:auto;max-width:95px;flex-grow:1;background-color:#fff;color:#1d1c21;padding:12px 8px;border-top:3px solid #00a99d;margin-right:10px}.ds-timeline .tm-countdown .section.values:last-child{margin-right:0}.ds-timeline .tm-countdown .section.values .value{font-weight:800;font-size:27px!important;line-height:1!important}.ds-timeline .tm-countdown .section.values .label{text-transform:uppercase}.ds-timeline .tm-countdown .section.sep{display:none}@media (min-width:980px){.ds-timeline .timeline-item:after{left:50%!important}.ds-timeline .timeline-item:first-child:after{height:50%;top:50%}.ds-timeline .timeline-item:last-child:after{height:50%}.ds-timeline .timeline-item.even{flex-direction:row-reverse}.ds-timeline .timeline-item.even .col-details{padding-right:40px;padding-left:0!important}.ds-timeline .timeline-item.odd .col-date{padding-right:40px;padding-left:0!important}.ds-timeline .timeline-item.odd .col-date:before{left:auto;right:-18px}.ds-timeline .timeline-item.odd .et_pb_button_module_wrapper{text-align:right}.ds-timeline .timeline-item.odd .tm-date{text-align:right}.ds-timeline .timeline-item.odd .tm-countdown .et_pb_countdown_timer_container{justify-content:flex-end}}


.et_fixed_nav.et_show_nav.et_secondary_nav_enabled #page-container, .et_non_fixed_nav.et_transparent_nav.et_show_nav.et_secondary_nav_enabled #page-container {
      padding-top: 44px !important;
  }

</style>
<?php


//get_footer(); ?>
