<?php
//add_filter( 'bp_email_use_wp_mail', '__return_true' );

// Nuevo avatar misterioso personalizado

function get_avatar_bp( $id_or_email, $size = 96, $default = '', $alt = '', $args = null ) {
    $defaults = array(
        // get_avatar_data() args.
        'size'          => 96,
        'height'        => null,
        'width'         => null,
        'default'       => 'https://observatorio.minedu.gob.pe/wp-content/uploads/2019/12/ico-usuario-cabecera.png',
        'force_default' => false,
        'rating'        => get_option( 'avatar_rating' ),
        'scheme'        => null,
        'alt'           => '',
        'class'         => null,
        'force_display' => false,
        'extra_attr'    => '',
    );

    if ( empty( $args ) ) {
        $args = array();
    }

    $args['size']    = (int) $size;
    $args['default'] = $default;
    $args['alt']     = $alt;

    $args = wp_parse_args( $args, $defaults );

    if ( empty( $args['height'] ) ) {
        $args['height'] = $args['size'];
    }
    if ( empty( $args['width'] ) ) {
        $args['width'] = $args['size'];
    }

    if ( is_object( $id_or_email ) && isset( $id_or_email->comment_ID ) ) {
        $id_or_email = get_comment( $id_or_email );
    }

    /**
     * Filters whether to retrieve the avatar URL early.
     *
     * Passing a non-null value will effectively short-circuit get_avatar(), passing
     * the value through the {@see 'get_avatar'} filter and returning early.
     *
     * @since 4.2.0
     *
     * @param string|null $avatar      HTML for the user's avatar. Default null.
     * @param mixed       $id_or_email The Gravatar to retrieve. Accepts a user_id, gravatar md5 hash,
     *                                 user email, WP_User object, WP_Post object, or WP_Comment object.
     * @param array       $args        Arguments passed to get_avatar_url(), after processing.
     */
    $avatar = apply_filters( 'pre_get_avatar', null, $id_or_email, $args );

    if ( ! is_null( $avatar ) ) {
        /** This filter is documented in wp-includes/pluggable.php */
        return apply_filters( 'get_avatar_bp', $avatar, $id_or_email, $args['size'], $args['default'], $args['alt'], $args );
    }

    if ( ! $args['force_display'] && ! get_option( 'show_avatars' ) ) {
        return false;
    }

    $url2x = get_avatar_url( $id_or_email, array_merge( $args, array( 'size' => $args['size'] * 2 ) ) );

    $args = get_avatar_data( $id_or_email, $args );

    $url = $args['url'];

    if ( ! $url || is_wp_error( $url ) ) {
        return false;
    }

    $class = array( 'avatar', 'avatar-' . (int) $args['size'], 'photo' );

    if ( ! $args['found_avatar'] || $args['force_default'] ) {
        $class[] = 'avatar-default';
    }

    if ( $args['class'] ) {
        if ( is_array( $args['class'] ) ) {
            $class = array_merge( $class, $args['class'] );
        } else {
            $class[] = $args['class'];
        }
    }

    $avatar = sprintf(
        "<img alt='%s' src='%s' srcset='%s' class='%s bp_icono' height='%d' width='%d' %s/>",
        esc_attr( $args['alt'] ),
        esc_url( $url ),
        esc_url( $url2x ) . ' 2x',
        esc_attr( join( ' ', $class ) ),
        (int) $args['height'],
        (int) $args['width'],
        $args['extra_attr']
    );

    /**
     * Filters the avatar to retrieve.
     *
     * @since 2.5.0
     * @since 4.2.0 The `$args` parameter was added.
     *
     * @param string $avatar      &lt;img&gt; tag for the user's avatar.
     * @param mixed  $id_or_email The Gravatar to retrieve. Accepts a user_id, gravatar md5 hash,
     *                            user email, WP_User object, WP_Post object, or WP_Comment object.
     * @param int    $size        Square avatar width and height in pixels to retrieve.
     * @param string $default     URL for the default image or a default type. Accepts '404', 'retro', 'monsterid',
     *                            'wavatar', 'indenticon','mystery' (or 'mm', or 'mysteryman'), 'blank', or 'gravatar_default'.
     *                            Default is the value of the 'avatar_default' option, with a fallback of 'mystery'.
     * @param string $alt         Alternative text to use in the avatar image tag. Default empty.
     * @param array  $args        Arguments passed to get_avatar_data(), after processing.
     */
    return apply_filters( 'get_avatar_bp', $avatar, $id_or_email, $args['size'], $args['default'], $args['alt'], $args );
}

/*
add_filter('avatar_defaults', 'miavatar');
function miavatar($avatar_defaults) {
     // Ruta a nuestra imagen (puede estar en el mismo servidor o en otro)
     $avatar = 'https://observatorio.minedu.gob.pe/wp-content/uploads/2019/12/ico-usuario-cabecera.png';
     // Nombre de nuestro avatar
     $avatar_defaults[$avatar] = "Nuevo";
     return $avatar_defaults;
}
*/
/*
add_action( 'init', 'qmas_remove_avatars' );
if (!function_exists('qmas_remove_avatars')) {
    function qmas_remove_avatars() {
        //add_filter( 'bp_core_fetch_avatar', function () { return ''; } );
        //add_filter( 'get_avatar', function () { return ''; } );
        //add_filter( 'bp_get_signup_avatar', function () { return ''; } );
    }
}
*/
/*

add_filter( 'get_avatar','get_flathash_avatar' , 10, 6 );
function get_flathash_avatar($avatar, $author, $size, $default, $alt, $args) {
  // -------------------------------------------
  // handle user passed by email or by id or obj
  if(isset($author->comment_ID)){
    $code = $author->comment_author_email;
  } else {
    if(stristr($author,"@")) $code = $autore;
      else {
        $autore = get_user_by('ID', $author);
        $code =$autore->user_email;
      }
  }

  // use flathash avatar instead of gravatar
  $flatavatar = "https://observatorio.minedu.gob.pe/wp-content/uploads/2019/12/ico-usuario-cabecera.png";

  return "<img class='avatar' alt=\"".$alt."\" src='https://observatorio.minedu.gob.pe/wp-content/uploads/2019/12/ico-usuario-cabecera.png' data-rel='$flatavatar' width='".$size."' />";
}
*/

function bbp_enable_visual_editor( $args = array() ) {
    $args['tinymce'] = true;
    return $args;
}
add_filter( 'bbp_after_get_the_content_parse_args', 'bbp_enable_visual_editor' );

if (!empty($_GET["swpm-logout"])){
  wp_redirect( site_url() );
  //exit;
}

// Restrict back-end for administrators only

function restrict_admin_area_by_rol() {
  if ( !(current_user_can('edit_pages'))  && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
  //if ( !(current_user_can('Administrator') || current_user_can('Editor')) ) {
    wp_redirect( site_url() );
    exit;
  }
}
add_action( 'admin_init', 'restrict_admin_area_by_rol', 1 );

// Show admin bar only for administrators
function hide_admin_bar($content) {
  return ( current_user_can('edit_pages')) ? $content : false;
}
add_filter( 'show_admin_bar' , 'hide_admin_bar');



function redirect2profile(){
 include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
 if($_SERVER['REQUEST_URI'] == '/red/' && is_plugin_active('buddypress/bp-loader.php') && is_user_logged_in()){
   global $current_user;
   wp_redirect( get_bloginfo('url') . '/members/'. $current_user->user_login . '/activity');
   exit();
 }
}

add_action('init', 'redirect2profile');

function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

//======================================================================
// CUSTOM FOOTER
// Adds a layout to your footer area
//======================================================================
function mp_add_custom_footer() {
	echo do_shortcode('[et_pb_section global_module="52584"][/et_pb_section]');
}
add_action( 'et_after_main_content', 'mp_add_custom_footer' );



function filter_search($query) {
    //---- Don't run in admin area
    //if(!is_admin()) {
        // Limit search to posts
        if($query->is_main_query() && $query->is_search()) {
            $query->set('post_type', array('post'));
        }

        // Return query
        return $query;
    //}
}

add_filter('pre_get_posts', 'filter_search');


add_action( 'wp_enqueue_scripts', 'dcms_load_dashicons_front_end' );

function dcms_load_dashicons_front_end() {
	wp_enqueue_style( 'dashicons' );
}


/*************************************************************/

function bp_misha_filter_function(){
/*
//brands checkboxes
if( $brands = get_terms( array( 'taxonomy' => 'tipo-practica' ) ) ) :
$brands_terms = array();

foreach( $brands as $brand ) {
    if( isset( $_POST['brand_' . $brand->term_id ] ) && $_POST['brand_' . $brand->term_id] == 'on' )
         $brands_terms[] = $brand->slug;
}
endif;

//sizes checkboxes
if( $sizes = get_terms( array( 'taxonomy' => 'nivel-educativo' ) ) ) :
$sizes_terms = array();

foreach( $sizes as $size ) {
    if( isset( $_POST['size_' . $size->term_id ] ) && $_POST['size_' . $size->term_id] == 'on' )
         $sizes_terms[] = $size->slug;
}
endif;
*/
// select
$brands_terms = array();
$sizes_terms = array();
if( isset( $_POST['brand'] ) ){
     $brands_terms[] = $_POST['brand'];
     //echo $_POST['brand'].'<br>';
}
if( isset( $_POST['size'] ) ){
     $sizes_terms[] = $_POST['size'];
     //echo $_POST['size'].'<br>';
}

$currentPage = get_query_var('paged');


if (!empty( $_POST['brand']) && !empty( $_POST['size'] ) && $_POST['brand']!='0' && $_POST['size']!='0'){


  $args = array(
      'orderby' => 'date',
      'post_type' => 'buena_practica',
      'posts_per_page' => 1000,
      'tax_query' => array(
          'relation' => 'and',
          array(
              'taxonomy' => 'tipo-practica',
              'field' => 'slug',
              'terms' => $brands_terms
          ),
          array(
              'taxonomy' => 'nivel-educativo',
              'field' => 'slug',
              'terms' => $sizes_terms
          )
      ),
      'paged' => $currentPage
  );


}else{

  if (!empty( $_POST['brand']) && $_POST['size']=='0'){
    $args = array(
        'orderby' => 'date',
        'post_type' => 'buena_practica',
        'posts_per_page' => 1000,
        'tax_query' => array(
            'relation' => 'and',
            array(
                'taxonomy' => 'tipo-practica',
                'field' => 'slug',
                'terms' => $brands_terms
            )
        ),
        'paged' => $currentPage
    );
  }
  if (!empty( $_POST['size']) && $_POST['brand']=='0'){
    $args = array(
        'orderby' => 'date',
        'post_type' => 'buena_practica',
        'posts_per_page' => 1000,
        'tax_query' => array(
            'relation' => 'and',
            array(
                'taxonomy' => 'nivel-educativo',
                'field' => 'slug',
                'terms' => $sizes_terms
            )
        ),
        'paged' => $currentPage
    );
  }

    if (!empty( $_POST['size']) && $_POST['brand']!='0'){
      $args = array(
          'orderby' => 'date',
          'post_type' => 'buena_practica',
          'posts_per_page' => 1000,
          'tax_query' => array(
              'relation' => 'and',
              array(
                  'taxonomy' => 'nivel-educativo',
                  'field' => 'slug',
                  'terms' => $sizes_terms
              )
          ),
          'paged' => $currentPage
      );
    }

    if (!empty( $_POST['brand']) && $_POST['size']!='0'){
      $args = array(
          'orderby' => 'date',
          'post_type' => 'buena_practica',
          'posts_per_page' => 1000,
          'tax_query' => array(
              'relation' => 'and',
              array(
                  'taxonomy' => 'tipo-practica',
                  'field' => 'slug',
                  'terms' => $brands_terms
              )
          ),
          'paged' => $currentPage
      );
    }
/*
    if ( $_POST['brand']=='0' ){
      $args = array(
        'orderby' => 'date',
          'post_type' => 'buena_practica',
          'posts_per_page' => 10,
          'paged' => $currentPage
      );
  }
    if ($_POST['size']=='0' ){
      $args = array(
        'orderby' => 'date',
          'post_type' => 'buena_practica',
          'posts_per_page' => 10,
          'paged' => $currentPage
      );
    }
*/


}

if (empty( $_POST['brand']) && empty( $_POST['size'] )){
  $args = array(
    'orderby' => 'date',
      'post_type' => 'buena_practica',
      'posts_per_page' => 1000,
      'paged' => $currentPage
  );
}

//si brand es seleccionado
//liste todas las tipo de practica
//si size es selecionado
//liste todos las nivel

$query = new WP_Query( $args );
$count = 1;
$cantReg = 10;
if( $query->have_posts() ) :
    while( $query->have_posts() ): $query->the_post();
        //echo $query->post->ID;
        $bp_terms_tp = wp_get_post_terms($query->post->ID, 'tipo-practica', array("fields" => "names"));
        $bp_terms_ne = wp_get_post_terms($query->post->ID, 'nivel-educativo', array("fields" => "names"));

        $bp_thumbID = get_post_thumbnail_id($query->post->ID);
        $bp_imgDestacada = wp_get_attachment_image_src($bp_thumbID, 'full' ); // Thumbnail, medium, large, full

        $bp_institucion_educativa_bbpp=get_post_meta( get_the_ID(), 'institucion_educativa', true );
        $bp_descripcion_bb=get_post_meta( get_the_ID(), 'descripcion_buena_practica', true );

        $getlength_bbpp = strlen($query->post->post_title);
        $thelength_bbpp = 125;
        $titulo_bbpp=mb_substr($query->post->post_title,0,$thelength_bbpp,'UTF-8');
        if ($getlength_bbpp > $thelength_bbpp) $titulo_bbpp=$titulo_bbpp."...";

        //$post_content_bbpp=$query->post->post_content;
        //$post_content_bbpp=apply_filters('the_content', $post_content_bbpp);
        $post_content_bbpp=$bp_descripcion_bb;
        $getlength_bbpp_2 = strlen($post_content_bbpp);
        $thelength_bbpp_2 = 380;
        $post_content_bbpp=mb_substr($post_content_bbpp,0,$thelength_bbpp_2,'UTF-8');
        if ($getlength_bbpp_2 > $thelength_bbp_2)$post_content_bbpp=$post_content_bbpp."...";

        $bp_link_bbpp=post_permalink($query->post->ID);


        //$style_bbpp_1='style="background-color: #b8d677!important;"';
        //$style_bbpp_2='style="background-color: #b8d677!important;"';
        //echo $bp_terms_tp[0];
        switch ($bp_terms_tp[0]) {
            case 'Directivos':
                $style_bbpp_1='style="background-color: #58c8ce!important;"';
                break;
            case 'Docentes':
                $style_bbpp_1='style="background-color: #689df2 !important;"';
                break;
        }

        switch ($bp_terms_ne[0]) {
            case 'Inicial':
                $style_bbpp_2='style="background-color: #e87c87!important;"';
                break;
            case 'Primaria':
                $style_bbpp_2='style="background-color: #f4a75a!important;"';
                break;
            case 'Secundaria':
                $style_bbpp_2='style="background-color: #b7d378!important;"';
                break;
        }


        $clasBP = '';
        if($count > $cantReg){
          $clasBP = 'hide';
        }

        echo '<div class="bp_resultado_bbpp '.$clasBP.'" data-bp-attr="'.$count.'">';
        echo '<div class="bp_resultado_bbpp_imagen"><a class="bp_estilo_1" href="'.$bp_link_bbpp.'" style="color: #575756!important;"><img src="'.$bp_imgDestacada[0].'" sizes="(max-width: 350px) 100vw, 350px"></a></div>';
        echo '<div class="bp_resultado_bbpp_titulo"><a class="bp_estilo_1" href="'.$bp_link_bbpp.'" style="color: #575756!important;">' . $titulo_bbpp . '</a></div>';
        echo '<div class="bp_resultado_bbpp_terms"><p class="bp_terms_bbpp_0" '.$style_bbpp_1.' >' . $bp_terms_tp[0] . '</p><p class="bp_terms_bbpp_1" '.$style_bbpp_2.'>' . $bp_terms_ne[0] . '</p></div>';
        echo '<div class="bp_resultado_bbpp_institucion">(' . $bp_institucion_educativa_bbpp . ')</div>';
        echo '<div class="bp_resultado_bbpp_contenido"><a class="bp_estilo_1 bp_estilo_conent" href="'.$bp_link_bbpp.'" style="color: #575756!important;">' . $post_content_bbpp. '</a></div>';
        echo '<div class="bp_liena_bbpp"></div>';
        echo '</div>';
        echo '<style>
        .hide{
          display: none;
        }
        .pagination {
          display: inline-block;
        }
        
        .pagination a {
          color: black;
          float: left;
          padding: 8px 16px;
          text-decoration: none;
          transition: background-color .3s;
          border: 1px solid #ddd6d6;
        }
        
        .pagination a.active {
          background-color: #6f3996;
          color: white;
          border: 1px solid #6f3996;
        }
        
        .pagination a:hover:not(.active) {background-color: #ddd;}
        .et_pb_row_3 .et_pb_row {
          margin-top: -170px!important;
          margin-bottom: 0px!important;
        }
        /*
        .et_pb_text_2.et_pb_text p{
          color:#000!important;
          font-size: 18px!important;
          /*float: left;*/
          text-align: left !important;
            color: #575756!important;

        }

        .et_pb_text.et_pb_text_3 p a{
          font-size: 18px!important;
          color: #575756!important;
          font-family: "Calibri",Helvetica,Arial,Lucida,sans-serif18px!important;
          margin-bottom: 60px;
          padding-right: 150px;
          letter-spacing: 0px;
          line-height: 22px;
        }
        .bp_estilo_1:hover{
          text-decoration: underline;
        }
        */
</style>
        ';
        //get_taxonomy();
        //echo '<h2>' . $query->post->terms . '</h2><br>';
          $count++;
        //get_taxonomy();
        //echo '<h2>' . $query->post->terms . '</h2><br>';
    endwhile;
    $paginas = ceil( ($count-1) / $cantReg);
    $index = 1;
    //echo '<br><div class="pagination" style="margin-left: 470px;">';
    echo ' <div class="et_pb_row_inner et_pb_row_inner_14">';
      echo ' <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_22 et-last-child">';
        echo '<div class="et_pb_module et_pb_divider et_pb_divider_6 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div><div style="text-align: center;" class="et_pb_module et_pb_text et_pb_text_20  et_pb_text_align_right et_pb_bg_layout_light">';
          echo ' <div class="et_pb_text_inner"> <p>';
    while ($index <= $paginas) {
      if($index == 1 ){
        echo '<a style="cursor:pointer" class="paginacion-bp " data-indice-pag="'.$index.'">Inicio</a>&nbsp; «&nbsp; &nbsp;';
      }
      $class = '';
      if($index == 1){
        $class = 'active';
      }
      echo ' <a style="cursor:pointer" class="paginacion-bp '.$class.'" data-indice-pag="'.$index.'">'.$index.'</a>&nbsp; &nbsp;';

      

      $index++;  
      
    }

    $extraPage = $paginas+1;
    while ($extraPage <= 7) {
      echo $extraPage.'&nbsp; &nbsp;';
      $extraPage ++;
    }
    echo '»&nbsp; &nbsp;<a style="cursor:pointer" class="paginacion-bp " data-indice-pag="'.$paginas.'">Fin</a>';
              echo  '</p></div>';
            echo  '</div>';
          echo  '</div>';
        echo  '</div>';
    wp_reset_postdata();



else :
    echo 'No se han encontrado publicaciones';
endif;
// Bottom pagination (pagination arguments)


die();



}
add_action('wp_ajax_myfilter', 'bp_misha_filter_function');
add_action('wp_ajax_nopriv_myfilter', 'bp_misha_filter_function');



/*************************************************************/
/**************************************************************/


/*
function myfilterbp(){

// select
$brands_terms = array();
$sizes_terms = array();
if( isset( $_POST['brand2'] ) ){
     $brands_terms[] = $_POST['brand2'];
     //echo $_POST['brand'].'<br>';
}
if( isset( $_POST['size2'] ) ){
     $sizes_terms[] = $_POST['size2'];
     //echo $_POST['size'].'<br>';
}

//$currentPage = get_query_var('paged');


if (!empty( $_POST['brand2']) && !empty( $_POST['size2'] ) && $_POST['brand2']!='0' && $_POST['size2']!='0'){
  $args = array(
      'orderby' => 'date',
      'post_type' => 'buena_practica',
      'posts_per_page' => 10,
      'tax_query' => array(
          'relation' => 'and',
          array(
              'taxonomy' => 'tipo-practica',
              'field' => 'slug',
              'terms' => $brands_terms
          ),
          array(
              'taxonomy' => 'nivel-educativo',
              'field' => 'slug',
              'terms' => $sizes_terms
          )
      ),
      'paged' => $currentPage
  );
}else{

  if ( $_POST['brand2']=='0'){
    $args = array(
      'orderby' => 'date',
        'post_type' => 'buena_practica',
        'posts_per_page' => 10,
        'paged' => $currentPage
    );
  }

  if ($_POST['size2']=='0'){
    $args = array(
      'orderby' => 'date',
        'post_type' => 'buena_practica',
        'posts_per_page' => 10,
        'paged' => $currentPage
    );
  }


    if (!empty( $_POST['size2'] && $_POST['brand2']!='0')){
      $args = array(
          'orderby' => 'date',
          'post_type' => 'buena_practica',
          'posts_per_page' => 10,
          'tax_query' => array(
              'relation' => 'and',
              array(
                  'taxonomy' => 'nivel-educativo',
                  'field' => 'slug',
                  'terms' => $sizes_terms
              )
          ),
          'paged' => $currentPage
      );
    }
    if (!empty( $_POST['brand2'] && $_POST['size2']!='0')){
      $args = array(
          'orderby' => 'date',
          'post_type' => 'buena_practica',
          'posts_per_page' => 10,
          'tax_query' => array(
              'relation' => 'and',
              array(
                  'taxonomy' => 'tipo-practica',
                  'field' => 'slug',
                  'terms' => $brands_terms
              )
          ),
          'paged' => $currentPage
      );
    }


}

if (empty( $_POST['brand2']) && empty( $_POST['size2'] )){
  $args = array(
    'orderby' => 'date',
      'post_type' => 'buena_practica',
      'posts_per_page' => 10,
      'paged' => $currentPage
  );
}

if (!empty( $_POST['search_bp'] )){
  $s=$_POST['search_bp'];
  $args = array(
    'orderby' => 'date',
      //'post_type' => array('buena_practica', 'post'),
      'post_type' => array('buena_practica'),
      's' => $s,
      'post_status' => 'publish',
      'orderby'     => 'title',
      'order'       => 'ASC'
  );
  }
  if (!empty( $_POST['category'] )){
    $args = array(
        'orderby' => 'date',
        'category_name' => 'Noticias'
    );
  }

$query = new WP_Query( $args );

if( $query->have_posts() ) :
    while( $query->have_posts() ): $query->the_post();
        //echo $query->post->ID;
        $bp_terms_tp = wp_get_post_terms($query->post->ID, 'tipo-practica', array("fields" => "names"));
        $bp_terms_ne = wp_get_post_terms($query->post->ID, 'nivel-educativo', array("fields" => "names"));

        $bp_thumbID = get_post_thumbnail_id($query->post->ID);
        $bp_imgDestacada = wp_get_attachment_image_src($bp_thumbID, 'full' ); // Thumbnail, medium, large, full

        $getlength_bbpp = strlen($query->post->post_title);
        $thelength_bbpp = 70;
        $titulo_bbpp=mb_substr($query->post->post_title,0,$thelength_bbpp,'latin');
        if ($getlength_bbpp > $thelength_bbpp) $titulo_bbpp=$titulo_bbpp."...";

        $post_content_bbpp=$query->post->post_content;
        $post_content_bbpp=apply_filters('the_content', $post_content_bbpp);

        $getlength_bbpp_2 = strlen($post_content_bbpp);
        $thelength_bbpp_2 = 550;
        $post_content_bbpp=mb_substr($post_content_bbpp,0,$thelength_bbpp_2,'UTF-8');
        if ($getlength_bbpp_2 > $thelength_bbp_2)$post_content_bbpp=$post_content_bbpp."...";

        $bp_link_bbpp=post_permalink($query->post->ID);

        //$style_bbpp_1='style="background-color: #b8d677!important;"';
        //$style_bbpp_2='style="background-color: #b8d677!important;"';
        //echo $bp_terms_tp[0];
        switch ($bp_terms_tp[0]) {
            case 'Directivos':
                $style_bbpp_1='style="background-color: #58c8ce!important;"';
                break;
            case 'Docentes':
                $style_bbpp_1='style="background-color: #689df2 !important;"';
                break;
        }

        switch ($bp_terms_ne[0]) {
            case 'Inicial':
                $style_bbpp_2='style="background-color: #e87c87!important;"';
                break;
            case 'Primaria':
                $style_bbpp_2='style="background-color: #f4a75a!important;"';
                break;
            case 'Secundaria':
                $style_bbpp_2='style="background-color: #b7d378!important;"';
                break;
        }


        //$link_category=the_permalink();
        if (!empty( $_POST['category'] )){
          echo '<div class="bp_resultado_bbpp_category">';
          echo '<div class="bp_resultado_bbpp_imagen search_imagen_bp"><a class="bp_estilo_1" href="'.$bp_link_bbpp.'" style="color: #575756!important;"><img src="'.$bp_imgDestacada[0].'" sizes="(max-width: 350px) 100vw, 350px"></a></div>';
          echo '<div class="bp_resultado_bbpp_titulo bp_category"><a class="bp_estilo_1" href="'.$bp_link_bbpp.'" style="color: #575756!important;">' . $titulo_bbpp . '</a></div>';
          echo '<div class="bp_resultado_bbpp_contenido "><a class="bp_category_excerpt" href="'.$bp_link_bbpp.'" style="color: #575756!important;">' .wp_trim_excerpt(). '</a></div>';
          echo '<div class="bp_liena_bbpp_category"></div>';
          echo '</div>';
        }else{
          echo '<div class="bp_resultado_bbpp">';
          echo '<div class="bp_resultado_bbpp_imagen search_imagen_bp"><a class="bp_estilo_1" href="https://observatorio.minedu.gob.pe/pagina-detalle-de-buena-practica-v2/" style="color: #575756!important;"><img src="'.$bp_imgDestacada[0].'" sizes="(max-width: 350px) 100vw, 350px"></a></div>';
          echo '<div class="bp_resultado_bbpp_titulo search_titulo_bp"><a class="bp_estilo_1" href="https://observatorio.minedu.gob.pe/pagina-detalle-de-buena-practica-v2/" style="color: #575756!important;">' . $titulo_bbpp . '</a></div>';
          echo '<div class="bp_resultado_bbpp_terms search_terms_bp"><p class="bp_terms_bbpp_0" '.$style_bbpp_1.' >' . $bp_terms_tp[0] . '</p><p class="bp_terms_bbpp_1" '.$style_bbpp_2.'>' . $bp_terms_ne[0] . '</p></div>';
          //echo '<div class="bp_resultado_bbpp_contenido">' . $query->post->post_content . '</div>';
          echo '<div class="bp_resultado_bbpp_contenido "><a class="bp_estilo_1" href="https://observatorio.minedu.gob.pe/pagina-detalle-de-buena-practica-v2/" style="color: #575756!important;">' . $post_content_bbpp. '</a></div>';
          echo '<div class="bp_liena_bbpp2 search_linea_bp2"></div>';
          echo '</div>';

        }

        echo '<style>

        .et_pb_text_2.et_pb_text p{
          color:#000!important;
          font-size: 18px!important;

          text-align: left !important;
            color: #575756!important;

        }

        .et_pb_text.et_pb_text_3 p{
          font-size: 18px!important;
          color: #575756!important;
          font-family: "Calibri",Helvetica,Arial,Lucida,sans-serif18px!important;
          margin-bottom: 25px;

          letter-spacing: 0px;
          line-height: 22px;
          padding-top: 40px;
        }
        .bp_estilo_1:hover{
          text-decoration: underline;
        }
        .et_pb_text.et_pb_text_3 p{

        }

</style>
        ';
        //get_taxonomy();
        //echo '<h2>' . $query->post->terms . '</h2><br>';
    endwhile;
    //wp_reset_postdata();



else :
    echo 'No se han encontrado publicaciones';
endif;
// Bottom pagination (pagination arguments)


die();



}
add_action('wp_ajax_myfilterbp', 'myfilterbp');
add_action('wp_ajax_nopriv_myfilterbp', 'myfilterbp');
*/

/*
**** Shortcode Listar Evidencias ****
*/
// Add Shortcode
function bp_shortcode_listar_evidencias( $atts ) {
	// Return variable
  //Listar archivos

  // Return variable
	$html = "";

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '',
		),
		$atts,
		'shortcode_listar_evidencias'
	);

	// Query
	$args = array(
		'p' => $atts['id'],
		'post_type' => 'buena_practica'
	);
  $carousels = new WP_Query( $args );
  	$html = "";
	while ( $carousels->have_posts() ) : $carousels->the_post();
    //$bp_video_bp=get_post_meta( get_the_ID(), 'video_bp', true );
    $bp_evidencias = get_field('evidencias');
    //var_dump($bp_evidencias);
    foreach ( $bp_evidencias as $evidencia ) {
      	$html .= "<div class='bp_listar_evidencias'>
                    <div class='bp_evidencia'>
                      <a class='' href='".$evidencia."' download='Archivo Buena Práctica' data-icon='&#xe092;''></a>
                      </div>
                  </div> <!-- .ntrk-slide -->";
      $arhivo = basename($evidencia);
    }


  endwhile;


  return   $arhivo;

}
add_shortcode( 'shortcode_listar_evidencias', 'bp_shortcode_listar_evidencias' );

function getNameFile($url){
  $f = explode("/", $url);
  $arch = $f[count($f)-1];
  return $arch;
}

function getExtensionFile($url){
  $f = explode(".", $url);
  $arch = $f[count($f)-1];
  return $arch;
}

function getIconoArchivo($dato){
  $icono="";
  $bp_ruta=content_url().'/uploads/2019/11/';

  switch ($dato) {
    case "docx":
        $icono=$bp_ruta."ico-file-word.png";
        break;
    case "doc":
        $icono=$bp_ruta."ico-file-word.png";
        break;
    case "xls":
        $icono=$bp_ruta."ico-file-excel.png";
        break;
    case "xlsx":
        $icono=$bp_ruta."ico-file-excel.png";
        break;
    case "ppt":
        $icono=$bp_ruta."ico-file-ppt.png";
        break;
    default:
        echo "";
  }
  /*
  http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/11/ico-file-word.png
  http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/11/ico-file-ppt.png
  http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/11/ico-file-ppt.png
  http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/11/ico-file-excel.png
  */
  /*jpg,jpeg,doc,docx,xls,xlsx,ppt,pdf*/


  return $icono;
}

add_action('wp_ajax_getAverageRating', 'getAverageRating');
add_action('wp_ajax_nopriv_getAverageRating', 'getAverageRating');

function getAverageRating($bp_post_id){
  global $wpdb;

  $bp_post_id=$_GET["bp_id_page"];
  $bp_resultados= $wpdb->get_results( "SELECT c.id_perfil,FORMAT((SUM(a.vote) / COUNT(a.vote)),1) as average_rating from wp_yasr_log as a
                                        INNER JOIN wp_users as b
                                        on a.user_id=b.ID
                                        INNER JOIN wp_swpm_members_tbl as c
                                        on b.user_login=c.user_name where a.post_id='$bp_post_id'
                                        GROUP BY c.id_perfil" );

    foreach ($bp_resultados as $bp_key) {
      if(!empty($bp_key)){

          switch ($bp_key->id_perfil) {
            case "1":
              $ar_docente=$bp_key->average_rating;
              break;
            case "2":
              $ar_estudiante=$bp_key->average_rating;
              break;
            case "3":
              $ar_funcioanrio=$bp_key->average_rating;
              break;
            case "4":
              $ar_investigador=$bp_key->average_rating;
              break;
            case "5":
              $ar_otros=$bp_key->average_rating;
              break;
            default:
            echo "";
          }
      }
    }
      //  echo var_dump(getAverageRating($bp_post_id));


      $bp_resultados=do_shortcode('[easyreview title="Resumen de Valoracion"
        cat1title="DOCENTE" cat1detail="" cat1rating="'.$ar_docente.'"
        cat2title="ESTUDIANTE" cat2detail="" cat2rating="'.$ar_estudiante.'"
        cat3title="FUNCIONARIO" cat3detail="" cat3rating="'.$ar_funcioanrio.'"
        cat4title="INVESTIGADOR" cat4detail="" cat4rating="'.$ar_investigador.'"
        cat5title="OTROS" cat5detail="" cat5rating="'.$ar_otros.'"]');

  //return $bp_resultados;
  echo json_encode($bp_resultados);
  die();
  //echo $bp_resultados;
}

add_action( 'wp_ajax_get_update_datos', 'bp_get_update_datos' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_update_datos', 'bp_get_update_datos' ); // Para usuarios no logeados

function bp_get_update_datos() {
  $bp_id_page=$_GET["bp_id_page"];
  $bp_id_user=$_GET["bp_id_user"];
  $bp_nro_doc=$_GET["bp_nro_doc"];

  //$flag=$_GET["flag"];

  global $wpdb;
  $table = $wpdb->prefix.'datos_bp';
  //$data = array('id_page' => $bp_id_page, 'id_user' =>$bp_id_user,'nro_doc'=>$bp_nro_doc,'id_perfil'=>$flag);

  $data = array('id_page' => $bp_id_page, 'id_user' =>$bp_id_user,'nro_doc'=>$bp_nro_doc);

  $format = array('%s','%d');


  //print_r($data);

  //exit;

  $wpdb->insert($table,$data,$format);
  $my_id = $wpdb->insert_id;

  echo json_encode($my_id);
  die(); //Importante finalizar el script
}



add_action( 'wp_ajax_get_update_descargar', 'bp_get_update_descargar' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_update_descargar', 'bp_get_update_descargar' ); // Para usuarios no logeados

function bp_get_update_descargar() {
  $bp_id_page=$_GET["bp_id_page"];
  $bp_id_user=$_GET["bp_id_user"];
  $bp_nro_doc=$_GET["bp_nro_doc"];
  $flag=$_GET["flag"];

  global $wpdb;
  $table = $wpdb->prefix.'datos_bp';
  $data = array('id_page' => $bp_id_page, 'id_user' =>$bp_id_user,'nro_doc'=>$bp_nro_doc,'id_perfil'=>$flag);
  $format = array('%s','%d');
  $wpdb->insert($table,$data,$format);
  $my_id = $wpdb->insert_id;

  echo json_encode($my_id);
  die(); //Importante finalizar el script
}

/*
add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
*/
// funcion listar instituciones

/*
remove_action('load-update-core.php', 'wp_update_plugins');
add_filter('pre_site_transient_update_plugins', '__return_null');
*/

/* funcion para guardar datos del usuario */


/* GENERAMOS UN Shortcode*/

function bp_shortcode_mapa() {
	// Return variable
  if( class_exists ('WP_CSP') ) {
    $nonce= "nonce='".WP_CSP::getNonce() . "' ";
  }
  $dato="
  <div class='tableauPlaceholder' id='viz1573745975650' style='position: relative'><noscript><a href='#'><img alt=' ' src='https://public.tableau.com/static/images/Ma/Mapa20/DASH/1_rss.png' style='border: none' /></a></noscript><object class='tableauViz'  style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='Mapa20/DASH' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https://public.tableau.com/static/images/Ma/Mapa20/DASH/1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='filter' value='publish=yes' /></object></div>
  <script  type='text/javascript' ".$nonce."> var divElement = document.getElementById('viz1573745975650');                    var vizElement = divElement.getElementsByTagName('object')[0];                    if ( divElement.offsetWidth > 800 ) { vizElement.style.width='1120px';vizElement.style.height='887px';} else if ( divElement.offsetWidth > 500 ) { vizElement.style.width='1120px';vizElement.style.height='887px';} else { vizElement.style.width='100%';vizElement.style.height='877px';}                     var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);
  </script>";

/*
$dato="
  <div class='tableauPlaceholder' id='viz1573745975650' style='position: relative'><noscript><a href='#'><img alt=' ' src='https://public.tableau.com/static/images/Ma/Mapa20/DASH/1_rss.png' style='border: none' /></a></noscript><object class='tableauViz'  style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='Mapa20/DASH' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https://public.tableau.com/static/images/Ma/Mapa20/DASH/1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='filter' value='publish=yes' /></object></div>
 ".WP_CSP::ob_start()."
  <script  type='text/javascript' ".$nonce."> var divElement = document.getElementById('viz1573745975650');                    var vizElement = divElement.getElementsByTagName('object')[0];                    if ( divElement.offsetWidth > 800 ) { vizElement.style.width='1120px';vizElement.style.height='887px';} else if ( divElement.offsetWidth > 500 ) { vizElement.style.width='1120px';vizElement.style.height='887px';} else { vizElement.style.width='100%';vizElement.style.height='877px';}                     var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);
  </script>".WP_CSP::ob_end_flush();
*/




  /*
  $dato="
  <div class='tableauPlaceholder' id='viz1573745975650' style='position: relative'><noscript><a href='#'><img alt=' ' src='https://public.tableau.com/static/images/Ma/Mapa20/DASH/1_rss.png' style='border: none' /></a></noscript><object class='tableauViz'  style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='Mapa20/DASH' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https://public.tableau.com/static/images/Ma/Mapa20/DASH/1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='filter' value='publish=yes' /></object></div>

  <script  type='text/javascript'> var divElement = document.getElementById('viz1573745975650');                    var vizElement = divElement.getElementsByTagName('object')[0];                    if ( divElement.offsetWidth > 800 ) { vizElement.style.width='1120px';vizElement.style.height='887px';} else if ( divElement.offsetWidth > 500 ) { vizElement.style.width='1120px';vizElement.style.height='887px';} else { vizElement.style.width='100%';vizElement.style.height='877px';}                     var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);
  </script>";
  */
  return $dato;
}

add_shortcode( 'shortcode_mapa', 'bp_shortcode_mapa' );



/* FUNCIONES DEL REPORTES DE BUENAS PRACTICAS */
add_menu_page(
         __( 'Reportes', 'simple-wp' ),
         __( 'Reportes', 'simple-wp' ),
         'administrator',
         'simplewp',
         'simple_ingresos'
);
/*
// subpagina
add_submenu_page( 'simplewp',
                    __( 'Reporte Me Interesa', 'simple-wp' ),
                    __( 'Ingresos', 'simple-wp' ),
                   'administrator',
                   'simplewp-ingresos',
                   'simple_ingresos'
);


function simple_ingresos() {
?>
	<div class="wrap">
		<h2><?php _e( 'Reporte', 'simple-wp' ); ?></h2>
    <table border="1">
      <tr>
        <th>Perfil</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Usuario</th>
        <th>Email</th>
        <th>Telefono</th>
        <th>Gestión</th>
        <th>Nivel</th>
        <th>DRE</th>
        <th>UGEL</th>
        <th>Institucion Educativa</th>
        <th>Titulo de BP</th>
      </tr>


    <?php
      global $wpdb;

      $bp_reporte= $wpdb->get_results( "SELECT a.id_perfil,a.first_name,a.last_name,a.user_name,a.email,a.phone,
            a.id_centro,a.id_nivel,a.id_dre,a.id_ugel,a.id_institucion_educativa,c.post_title
            from wp_swpm_members_tbl as a
            INNER JOIN wp_datos_bp as b
            on b.nro_doc=a.user_name
            INNER JOIN wp_posts as c
            on b.id_page=c.ID" );
      foreach ($bp_reporte as $bp_key) {
        echo "<tr>";
        echo "<td>".$bp_key->id_perfil."</td>";
        echo "<td>".$bp_key->first_name."</td>";
        echo "<td>".$bp_key->last_name."</td>";
        echo "<td>".$bp_key->user_name."</td>";
        echo "<td>".$bp_key->email."</td>";
        echo "<td>".$bp_key->phone."</td>";
        echo "<td>".$bp_key->id_centro."</td>";
        echo "<td>".$bp_key->id_dre."</td>";
        echo "<td>".$bp_key->id_ugel."</td>";
        echo "<td>".$bp_key->id_institucion_educativa."</td>";
        echo "<td>".$bp_key->post_title."</td>";
        echo "</tr>";
      }

      //var_dump($bp_reporte);

     ?>
   </table>

	</div>
<?php
}
*/
/* shorcode listar datos de perfil*/

function bp_shortcode_datos_perfil() {
	// Return variable

  $auth = SwpmAuth::get_instance();

  $perfil = array(
                  "3"=> "Docente",
                  "2"=> "Estudiante",
                  "4"=>"Funcionario",
                  "1"=>"Investigador",
                  "5"=> "Otros"
                );

  if ($auth->userData->id_perfil=='3'){
    $sub_perfil = array(
            "1"=>"Docente Educación Basica",
            "2"=>"Formato de Docentes",
            "3"=>"Director",
            "4"=>"Especialista de UGEL",
            "5"=>"Especialista de DRE",
            "6"=>"Especialista de DRE"
    );

    foreach ($sub_perfil as $key => $value) {
      if($auth->userData->id_subperfil == $key) {
        $dato_subperfil=$value;
      }
    }
  }

  if ($id_perfil=='4'){
    $sub_perfil = array(
            "7"=>"Minedu",
            "8"=>"DRE",
            "9"=>"UGEL"
    );
    foreach ($sub_perfil as $key => $value) {
      if($auth->userData->id_subperfil == $key) {
        $dato_subperfil=$value;
      }
    }
  }

  foreach ($perfil as $key => $value) {
    if($auth->userData->id_perfil == $key) {
      $dato_perfil=$value;
    }
  }


  $html="";
  $html.="<div class='bp_perfil_content'>";
  $html.="<div class='bp_perfil_titulo'><p>Datos Personales</p></div>";
  $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Nombre de usuario:</span> ".$auth->userData->first_name." ".$auth->userData->last_name."</p></div>";
  if(!empty($dato_perfil)){
    $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Perfil de Usuario:</span> ".$dato_perfil."</p></div>";
  }
  if(!empty($dato_subperfil)){
  $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>SubPerfil de Usuario:</span> ".$dato_subperfil."</p></div>";
  }
  $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Número de Documento:</span> ".$auth->userData->user_name."</p></div>";
  $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Ubigeo:</span> ".$auth->userData->ubigeo."</p></div>";
  $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Correo electrónico: </span>".$auth->userData->email."</p></div>";
  $html.="<div class='bp_perfil_divider'><div class='bp_perfil_linea'></div></div>";
  $html.="<div class='bp_perfil_titulo bp_perfil_text_1'><p>Información centro laboral / Centro de estudios</p></div>";
  

  switch ($auth->userData->id_perfil) {
    case '1'://Investigador
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Gestión :</span> ".SwpmMiscUtils::get_centro_datos($auth->userData->id_centro)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Departamento:</span> ".get_region_dropdown_datos($auth->userData->id_departamento)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Provincia:</span> ".bp_listar_provincia_datos($auth->userData->id_departamento,$auth->userData->id_provincia)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Distrito:</span> ".bp_listar_distrito_datos($auth->userData->id_departamento."".$auth->userData->id_provincia,$auth->userData->id_distrito)."</p></div>";
       // $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Centro Laboral :</span> ".bp_listar_distrito_datos($centro_laboral)."</p></div>";
		$html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Centro Laboral :</span> ".$auth->userData->centro_laboral."</p></div>";
        break;
    case '2'://Estudiante
      $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Departamento:</span> ".get_region_dropdown_datos($auth->userData->id_departamento)."</p></div>";
      $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Provincia:</span> ".bp_listar_provincia_datos($auth->userData->id_departamento,$auth->userData->id_provincia)."</p></div>";
      $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Distrito:</span> ".bp_listar_distrito_datos($auth->userData->id_departamento."".$auth->userData->id_provincia,$auth->userData->id_distrito)."</p></div>";
      $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Institución:</span> ".SwpmMiscUtils::get_institucion_datos($auth->userData->id_institucion)."</p></div>";
      $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Carrera Profesional:</span> ".SwpmMiscUtils::get_carrera_datos($auth->userData->id_carrera)."</p></div>";
        break;
    case '3'://Docente
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Gestión :</span> ".SwpmMiscUtils::get_centro_datos($auth->userData->id_centro)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Nivel Educativo :</span> ".SwpmMiscUtils::get_estudios_dato($auth->userData->id_nivel)."</p></div>";
        if($auth->userData->id_nivel=='1' OR $auth->userData->id_nivel=='2'  OR $auth->userData->id_nivel=='3'){
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>DRE:</span>  ".get_dre_dropdown_dato($auth->userData->id_dre)."</p></div>";
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>UGEL:</span> ".bp_listar_ugel_datos($auth->userData->id_dre,$auth->userData->id_ugel)."</p></div>";
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Institución Educativa:</span> ".bp_listar_ie_datos($auth->userData->id_ugel,$auth->userData->id_institucion_educativa)."</p></div>";
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Código modular IE:</span> ".$auth->userData->codigo_modular."</p></div>";
        }
        if($auth->userData->id_nivel=='4' OR $auth->userData->id_nivel=='5'){
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Departamento:</span> ".get_region_dropdown_datos($auth->userData->id_departamento)."</p></div>";
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Provincia:</span> ".bp_listar_provincia_datos($auth->userData->id_departamento,$auth->userData->id_provincia)."</p></div>";
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Distrito:</span> ".bp_listar_distrito_datos($auth->userData->id_departamento."".$auth->userData->id_provincia,$auth->userData->id_distrito)."</p></div>";
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Institución:</span> ".SwpmMiscUtils::get_institucion_datos($auth->userData->id_institucion)."</p></div>";
          $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Carrera Profesional:</span> ".SwpmMiscUtils::get_carrera_datos($auth->userData->id_carrera)."</p></div>";
        }
        break;
    case '4'://Funcionario

        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Ministerio de Educación :</span> ".SwpmMiscUtils::get_tipo_oficina_datos($auth->userData->tipo_oficina)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Gestión :</span> ".SwpmMiscUtils::get_centro_datos($auth->userData->id_centro)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>DRE:</span>  ".get_dre_dropdown_dato($auth->userData->id_dre)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>UGEL:</span> ".bp_listar_ugel_datos($auth->userData->id_dre,$auth->userData->id_ugel)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Institución Educativa:</span> ".bp_listar_ie_datos($auth->userData->id_ugel,$auth->userData->id_institucion_educativa)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Cargo:</span> ".SwpmMiscUtils::get_tipo_oficina_datos($auth->userData->id_cargo)."</p></div>";

        break;
    case '5'://Otros

        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Gestión :</span> ".SwpmMiscUtils::get_centro_datos($auth->userData->id_centro)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Departamento:</span> ".get_region_dropdown_datos($auth->userData->id_departamento)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Provincia:</span> ".bp_listar_provincia_datos($auth->userData->id_departamento,$auth->userData->id_provincia)."</p></div>";
        $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Distrito:</span> ".bp_listar_distrito_datos($auth->userData->id_departamento."".$auth->userData->id_provincia,$auth->userData->id_distrito)."</p></div>";
   //     $html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Centro Laboral :</span> ".bp_listar_distrito_datos($centro_laboral)."</p></div>";
		$html.="<div class='bp_perfil_datos'><p><span style='color: #999999;'>Centro Laboral :</span> ".$auth->userData->centro_laboral."</p></div>";
        break;
  }



  $html.="</div><style>#page-container{
    margin-top:-40px!important;
  }</style>";


  return $html;
}

add_shortcode( 'shortcode_datos_perfil', 'bp_shortcode_datos_perfil' );
//bp_core_new_subnav_item();



function bp_shortcode_update_perfil() {
  $auth = SwpmAuth::get_instance();
  //var_dump($auth);
  $perfil = array(
                  "3"=> "Docente",
                  "2"=> "Estudiante",
                  "4"=>"Funcionario",
                  "1"=>"Investigador",
                  "5"=> "Otros"
                );

  foreach ($perfil as $key => $value) {
    if($auth->userData->id_perfil == $key) {
      $dato_perfil=$value;
    }
  }
$html='<div class="bp_content_perfil"><div class="bp_content_perfil_1">
        <p class="bp-feedback info bp_info">
        <span class="bp-icon" aria-hidden="true"></span>
        <span class="bp-help-text mensaje_perfil">

        </span>
        </p>
        <div class="descripcion_titulo">Los cambios de perfil de usuario solamente se pueden realizar 2 veces al año.</div>
        <div class="">(Esta operación puede tardar algunos minutos.)</div>
        <form class="form" id="" action="#" style="padding-left:40px;padding-top:20px">
        <div class="swpm-registration-perfil-row bp_frm_registration" >
          <label for="perfil">'.SwpmUtils::_('Perfil Actual').': <span class="text_perfil">'.$dato_perfil.'</span></label>
        </div>
        <div class="swpm-mi-perfil-row bp_frm_registration" style="margin-bottom:10px;">
          <label for="perfil">'.SwpmUtils::_('Nuevo Perfil').': </label>
          <select id="id_perfil_bp" name="id_perfil_bp" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil" required>'.SwpmMiscUtils::get_perfil_dropdown($auth->userData->id_perfil).'</select>
        </div>';

        //echo $auth->userData->id_subperfil;
//if(!empty($auth->userData->id_subperfil)){
      $html.='<div class="swpm-mi-subperfil-row bp_frm_registration" style="margin-bottom:20px;">
                <label for="subperfil">'.SwpmUtils::_('SubPerfil de Usuario').': </label>
                <select id="id_subperfil_bp" name="id_subperfil_bp" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil">'.SwpmMiscUtils::get_subperfil_dropdown($auth->userData->id_subperfil).'</select>
              </div>';
//}
      $html.='
        <button type="Button" class="btn_perfil button bp-primary-action" style="cursor: pointer;" >GUARDAR</button>
        </form></div></div>';
  return $html;
}
add_shortcode( 'shortcode_update_perfil', 'bp_shortcode_update_perfil' );

add_action('wp_ajax_change_miperfil', 'bp_change_miperfil');
add_action('wp_ajax_nopriv_change_miperfil', 'bp_change_miperfil');

function bp_change_miperfil()
{
  global $wpdb;
  $auth = SwpmAuth::get_instance();
  //echo $_POST['name_w'];
  $bp_id_perfil=$_GET['bp_id_perfil'];
  $bp_id_subperfil=$_GET['bp_id_subperfil'];

  $table = $wpdb->prefix.'swpm_members_tbl';
  if(empty($bp_id_subperfil)){
    $data = array('id_perfil' => $bp_id_perfil);
  }else{
    $data = array('id_perfil' => $bp_id_perfil,'id_subperfil'=>$bp_id_subperfil);
  }
  $where = array('member_id' => $auth->userData->member_id);
  $updated = $wpdb->update( $table, $data, $where );
  if ( false === $updated ) {
      // There was an error.
      $dato=" Error al actualizar el perfil";
  } else {
      // No error. You can check updated to see how many rows were changed.
      $dato=" Se actualizó el Perfil";
      $fecha = date('Y-m-d');
      $table_1 = $wpdb->prefix.'temp_perfil';
      $data_1 = array('id_user' => $auth->userData->member_id, 'fec_reg' =>$fecha);
      $wpdb->insert($table_1,$data_1);
      $my_id = $wpdb->insert_id;
  }

  echo json_encode($dato);
  die(); //Importante finalizar el script
}

add_action('wp_ajax_miperfil', 'bp_miperfil');
add_action('wp_ajax_nopriv_miperfil', 'bp_miperfil');


function bp_miperfil()
{
  global $wpdb;
  $auth = SwpmAuth::get_instance();
  //SELECT COUNT(id) as total FROM wp_temp_perfil where id_user='96' and YEAR(fec_reg)='2020'
  $year = date('Y');
  $result = $wpdb->get_results('SELECT COUNT(id) as total FROM '.$wpdb->prefix.'temp_perfil where id_user="'.$auth->userData->member_id.'" and YEAR(fec_reg)="'.$year.'"');
  echo json_encode($result['0']->total);
  die();
}




function xprofile_screen_cambiar_perfil(){
	add_action( 'bp_template_content', 'bp_cambiar_perfil' );
	//bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
  bp_core_load_template( 'template_content' );
}
function bp_cambiar_perfil() {
	echo 'Function to Generate the displayed users posts.';
	// echo do_shortcode('[mb_feedburner count=5 user="MaximusBusiness"]'); // Displays a shortcode output.
}

// Set up Cutsom BP navigation
function my_setup_nav() {
      global $bp;
      // Change name of menu item
      //$bp->bp_nav['members']['profile'] = "Mi Perfil";
      $bp->bp_nav['profile']['name'] = "Mi Perfil";
}

add_action( 'bp_setup_nav', 'my_setup_nav' );

remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );




/*
function profile_tab_jobs() {
  global $bp;
  //bp_core_new_subnav_item(array('name' => __('Cambiar Perfil', 'dbem'), 'slug' => 'profile', 'parent_slug' => $bp->profile->slug, 'parent_url' => $em_link, 'screen_function' => 'bp_em_events', 'position' => 10, 'user_has_access' => bp_is_my_profile()));
  //bp_core_new_subnav_item ("Cambiar Perfil","Cambiar_Perfil","Profile","");

    bp_core_new_subnav_item( array(
        'name'                 => 'Cambiar Perfil',
        'slug'                 => 'cambiar_perfil',
        'screen_function'      => 'xprofile_screen_change_cambiar_perfil',
        'position'             => 40,
        'parent_url'           => bp_loggedin_user_domain() . 'profile/',
        'parent_slug'          => 'profile',
        'default_subnav_slug'  => 'all_jobs',
    ) );


}
add_action( 'bp_setup_nav', 'profile_tab_jobs' );
*/
/*
function bp_shortcode_breadcrumbs() {
	// Return variable
  $dato='<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
        '.if(function_exists('bcn_display'))
        {
            bcn_display();
        }.'

        </div>';

  return $dato;
}

add_shortcode( 'shortcode_breadcrumbs', 'bp_shortcode_breadcrumbs' );
*/

/*
add_action( 'wp_footer', function () { ?>
<script>

jquery(function() {
 jquery('#swpm_user_name').attr('required',true);
 jquery('#swpm_user_name').attr('autocomplete','off');
});

</script>
<?php } );
*/



add_action( 'wp_footer', function () { ?>
  <script>
  var cantReg = '10'
  jQuery( document ).ready(function() {
   
    jQuery(document).on("click",".paginacion-bp",function() {
      var indicePag = jQuery(this).attr("data-indice-pag");
      var isActivo = jQuery(this).hasClass("active"); 

      var inicio = 1 
      
      if(indicePag > 1 ){
        inicio = parseInt((parseInt(cantReg) * (parseInt(indicePag)-1))) + 1
      }
      var fin = parseInt(cantReg) * parseInt(indicePag);

 

        if(isActivo == false){
          jQuery(".paginacion-bp").removeClass("active")
          jQuery(this).addClass("active"); 

        //  jQuery(".bp_resultado_bbpp").removeClass("hide") 
          jQuery(".bp_resultado_bbpp").addClass("hide") 

          jQuery(".bp_resultado_bbpp").each(function() {
            var indiceContenido = jQuery(this).attr("data-bp-attr"); 
           
            if(indiceContenido >= inicio && indiceContenido <= fin){
             
              jQuery(this).removeClass("hide");
            }
           
              
          });

        }

    });
    console.log( "ready!" );
});


  </script>
  <?php } );
