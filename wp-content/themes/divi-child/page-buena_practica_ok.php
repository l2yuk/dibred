<?php
/*
Template Name: Buenas Practicas
*/


get_header(); ?>

<div id="primary" class="site-content">
<div id="content" role="main">
<?php echo do_shortcode( '[searchandfilter fields="tipo-practica,nivel-educativo"]' ); ?>
  <h2 class="text-center">Comida por tipo:</h2>
  <div id="filtrar">
    <div class="menu-centered">
      <ul class="menu">
        <?php
        $terms = get_terms( array(
            'taxonomy' => 'nivel-educativo',
        ) );
        foreach($terms as $term) {
          echo "<li><a href='#". $term->slug . "'>" . $term->name ."</a></li>";
        }
        ?>
      </ul>
    </div>
    <div id="platillos">
      <?php
        foreach($terms as $term) {
            filtrar_platillos($term->slug);
        }
      ?>
    </div>
  </div>

<div>


<?php

$args = array(
        'post_type' => 'post',
        'tax_query' => array(
            'relation' => 'AND',
            array(
                'taxonomy' => 'movie_genre',
                'field' => 'slug',
                'terms' => array( 'action', 'comedy' )
            ),
            array(
                'taxonomy' => 'actor',
                'field' => 'id',
                'terms' => array( 103, 115, 206 ),
                'operator' => 'NOT IN'
            )
        )
    );

function filtrar_platillos( $busqueda ) {
    // Get the list of files
    $args = array(
				'posts_per_page' => 4,
				'post_type' => 'buena_practica',
				'order' => 'rand',
				'tax_query' => array(
					array(
						'taxonomy' => 'nivel-educativo',
						'field'    => 'slug',
						'terms'    => $busqueda,
					),
				),
		);

		$platillos = new WP_Query($args);
		if($platillos->have_posts()) {
			echo '<div id="'.$busqueda.'" class="row">';

			while($platillos->have_posts()): $platillos->the_post();
				echo '<div class="small-6 medium-3 columns">';
				echo '<div class="platillo">';

				echo '<a href="'.get_the_permalink($post->ID).'">';
				echo get_the_post_thumbnail( $post->ID, 'platilloBuscado');
				echo '</a>';
				echo '<h2 class="text-center">'.  get_the_title() . '</h2>';
				echo '</div>';
				echo '</div>';
	    endwhile; wp_reset_postdata();
			echo '</div>';
		}
}

?>
<script>
jQuery(function($) {

  $('#platillos > div').not(':first').hide();
  jQuery('#filtrar .menu li:first-child').addClass('active');

  $('#filtrar .menu a').on('click', function() {
      jQuery('#filtrar .menu li').removeClass('active');
      jQuery(this).parent().addClass('active');

      var enlace = $(this).attr('href');

      $('#platillos > div').hide();
      $(enlace).fadeIn();
      return false;
  });
});
</script>
<?php


get_footer(); ?>
