<?php
    //Para wp-config.php o /plugins/bp-custom.php
    // definir nuestras propias url
    function my_custom_bp_mofile( $mofile, $domain ){
        if( 'buddypress' == $domain ){
             $mofile = trailingslashit( WP_LANG_DIR ) . basename( $mofile );
        }
        return $mofile;
    }
    add_filter( 'load_textdomain_mofile', 'my_custom_bp_mofile', 10, 2 );
    ?>