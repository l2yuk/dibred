<?php
/*
Plugin Name: Simple WordPress Membership
Version: 3.9.2
Plugin URI: https://simple-membership-plugin.com/
Author: smp7, wp.insider
Author URI: https://simple-membership-plugin.com/
Description: A flexible, well-supported, and easy-to-use WordPress membership plugin for offering free and premium content from your WordPress site.
Text Domain: simple-membership
Domain Path: /languages/
*/
//EDITADO PROYECTO_BP 2019-10-16
//Direct access to this file is not permitted
if (!defined('ABSPATH')){
    exit("Do not access this file directly.");
}

include_once('classes/class.simple-wp-membership.php');
include_once('classes/class.swpm-cronjob.php');
include_once('swpm-compat.php');

define('SIMPLE_WP_MEMBERSHIP_VER', '3.9.2');
define('SIMPLE_WP_MEMBERSHIP_DB_VER', '1.3');
define('SIMPLE_WP_MEMBERSHIP_SITE_HOME_URL', home_url());
define('SIMPLE_WP_MEMBERSHIP_PATH', dirname(__FILE__) . '/');
define('SIMPLE_WP_MEMBERSHIP_URL', plugins_url('', __FILE__));
define('SIMPLE_WP_MEMBERSHIP_DIRNAME', dirname(plugin_basename(__FILE__)));
define('SIMPLE_WP_MEMBERSHIP_TEMPLATE_PATH', 'simple-membership');
if (!defined('COOKIEHASH')) {
    define('COOKIEHASH', md5(get_site_option('siteurl')));
}
define('SIMPLE_WP_MEMBERSHIP_AUTH', 'simple_wp_membership_' . COOKIEHASH);
define('SIMPLE_WP_MEMBERSHIP_SEC_AUTH', 'simple_wp_membership_sec_' . COOKIEHASH);
define('SIMPLE_WP_MEMBERSHIP_STRIPE_ZERO_CENTS',serialize(array('JPY', 'MGA', 'VND', 'KRW')));


//AGREGADO PROYECTO_BP
#---------------------------------------------------------------------------
# Conexion SQL Server
define('DB_USER_', 'user_padron_L');                    ########################################### -> Usuario BD
define('DB_PASS_', 'user_padron_L');                    ########################################### -> Password BD
define('DB_HOST_', '10.200.2.24\andromeda');                         ########################################### -> Servidor BD
define('DB_NAME_', 'padron');                      ########################################### -> Base de datos
define('DRIVER_SQL_', 'dblib:host='.DB_HOST_);
define("URI_", DRIVER_SQL_ .';dbname=' . DB_NAME_);
#---------------------------------------------------------------------------
  try {
            $conexionDB = new PDO(URI_, DB_USER_, DB_PASS_);
            $conexionDB->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            $conexionDB->query('SET ANSI_NULLS ON');
            $conexionDB->query('SET ANSI_WARNINGS ON');
            //$conexionDB->query('select * from [dbo].[ubigeo]');
        } catch (PDOException $error) {
            echo("ERROR DE CONEXION: ".$error);
            $conexionDB = NULL;
            exit;
        }


/**/


SwpmUtils::do_misc_initial_plugin_setup_tasks();

register_activation_hook(SIMPLE_WP_MEMBERSHIP_PATH . 'simple-wp-membership.php', 'SimpleWpMembership::activate');
register_deactivation_hook(SIMPLE_WP_MEMBERSHIP_PATH . 'simple-wp-membership.php', 'SimpleWpMembership::deactivate');

add_action('swpm_login', 'SimpleWpMembership::swpm_login', 10, 3);

$simple_membership = new SimpleWpMembership();
$simple_membership_cron = new SwpmCronJob();

//Add settings link in plugins listing page
function swpm_add_settings_link($links, $file) {
    if ($file == plugin_basename(__FILE__)) {
        $settings_link = '<a href="admin.php?page=simple_wp_membership_settings">Settings</a>';
        array_unshift($links, $settings_link);
    }
    return $links;
}

add_filter('plugin_action_links', 'swpm_add_settings_link', 10, 2);

//AGREGADO PROYECTO_BP
// 1. Encolamos nuestro script
add_action('wp_enqueue_scripts', 'jiac_public_scripts');

function jiac_public_scripts() {
  global $wp_query;
  wp_enqueue_script('mi-ajax', SIMPLE_WP_MEMBERSHIP_URL . '/js/bp-ajax.js', array('jquery'));

}
// 2. Asociamos una función a la acción
add_action( 'wp_ajax_get_subperfil', 'bp_get_subperfil' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_subperfil', 'bp_get_subperfil' ); // Para usuarios no logeados
// 3. Escribimos la función de callback
function bp_get_subperfil() {
  // Petición a la Base de Datos
  // Preparación de los datos
  $perfil=$_GET["perfil"];
  $subperfil = array(
                  "3"=>array(
                      "1"=>"Docente Educación Básica",
                      "2"=>"Docente Formador",
                      "3"=>"Director"
                  ),
                  "4"=> array(
                    "7"=>"Minedu",
                    "8"=>"DRE",
                    "9"=>"UGEL",
                  )
                );

  $perfil=$subperfil[$perfil];
  //$datos_bd=array("1"=>"dato 1","1"=>"dato 3");
  echo json_encode($perfil);
  die(); // Importante finalizar el script
}

add_action( 'wp_ajax_get_datos_dni', 'bp_get_datos_dni' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_datos_dni', 'bp_get_datos_dni' ); // Para usuarios no logeados

function bp_get_datos_dni() {
  $nro_doc=$_GET["nro_doc"];
  $ubigeo=$_GET["ubigeo"];

  $bp_datos=bp_validate_service_nro($nro_doc,$ubigeo,"1");

  echo json_encode($bp_datos);
  die(); // Importante finalizar el script
}

function bp_validate_service_nro($nro_doc,$ubigeo,$ip){

    //$url_servicio="http://192.168.210.182:8080/prjMINEDU/ReniecWS?wsdl";
    $url_servicio="http://192.168.210.182:8080/MINEDUEPV2/ReniecWS?wsdl";
    $parametros = array(
      'usuario'       => 'USRDEPARTE',
      'clave'         => '0a223763',
      'ipsistema'     => '::1',
      'dni'           => $nro_doc
    );

    $client = new \SoapClient($url_servicio, [
      'trace' => 1,
      'exceptions' => true,
    ]);

    $resultado = $client->buscarDNICascada($parametros);
    $msg="";

    if($resultado){
      $codigo = $resultado->return->codigo;
      $bp_nrodoc=$resultado->return->persona->numDoc;
      $bp_ubigeo=$resultado->return->persona->dptoNacimiento."".$resultado->return->persona->provNacimiento."".$resultado->return->persona->distNacimiento;
      if($codigo[0] == 'R'){
        //echo 'El DNI es Invalido';
        $msg="0";
      }else{

        if($bp_nrodoc==$nro_doc && $bp_ubigeo==$ubigeo ){
          $msg="1";
          $bp_nombres=$resultado->return->persona->nombres;
          $bp_apellidos=$resultado->return->persona->apellidoPaterno." ".$resultado->return->persona->apellidoMaterno;

        }else{
          $msg="2";
        }
        //echo 'El DNI es valido';
      }
      $bp_array= array('msg' =>$msg ,'nombres'=>   $bp_nombres,'apellidos'=>$bp_apellidos);
    }
    return   $bp_array;
}


add_action( 'wp_ajax_get_instituciones', 'bp_get_instituciones' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_instituciones', 'bp_get_instituciones' ); // Para usuarios no logeados

function bp_get_instituciones() {
  $id_nivel=$_GET["id_nivel"];
  $id_centro=$_GET["id_centro"];

  $bp_datos_js=bp_listar_instituciones_js($id_centro,$id_nivel);

  echo json_encode($bp_datos_js);
  die(); // Importante finalizar el script
}


// funcion listar instituciones

function bp_listar_instituciones_js($id_centro,$id_nivel){

  global $wpdb;
  $query="SELECT id,TRIM(descripcion) as descripcion from wp_instituciones where tipo='$id_nivel' and gestion='$id_centro'";
  $registros= $wpdb->get_results($query);
  foreach (  $registros as $key => $value) {
    $bp_datos[] = array(
        'id'=>$value->id,
        'descripcion'=>utf8_encode($value->descripcion)
      );

  }
  //var_dump($bp_datos);
  return  $bp_datos;
}



function get_dre_dropdown_bp($valor_bp){
  global $conexionDB;

  try {
              $stmt = $conexionDB->query("
                  SELECT COD_OI, NOMBRE_OI FROM [dbo].[BP_SEL_DRE] ORDER BY NOMBRE_OI
              ");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

  //var_dump($resultado);
  $perfil_dropdown = '';
  $perfil_dropdown .= "\r\n" . '<option value=""' . ($valor_bp == '' ? ' selected' : '') . '>' . SwpmUtils::_('Seleccionar') . '</option>';

  if(!empty($resultado))
  {
    foreach ($resultado as $c=>$v)
    {
    $perfil_dropdown .= "\r\n" . '<option value="' . $v["COD_OI"] . '"' . (strtolower($v["COD_OI"]) == strtolower($valor_bp) ? ' selected' : '') . '>' . utf8_encode($v["NOMBRE_OI"]) . '</option>';
    }
  }


  return $perfil_dropdown;
}

function get_dre_dropdown_dato($valor_bp){
  global $conexionDB;

  try {
              $stmt = $conexionDB->query("
                  SELECT COD_OI, NOMBRE_OI FROM [dbo].[BP_SEL_DRE] ORDER BY NOMBRE_OI
              ");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

  if(!empty($resultado))
  {
    foreach ($resultado as $c=>$v)
    {
      if($valor_bp==$v["COD_OI"]){
        $dato=$v["NOMBRE_OI"];
      }
    }
  }


  return $dato;
}



add_action( 'wp_ajax_get_ugel', 'bp_get_ugel' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_ugel', 'bp_get_ugel' ); // Para usuarios no logeados

function bp_get_ugel() {
  $id_dre=$_GET["id_dre"];

  $bp_datos_js=bp_listar_ugel_js($id_dre);

  echo json_encode($bp_datos_js);
  die(); // Importante finalizar el script
}


// funcion listar instituciones

function bp_listar_ugel_js($id_dre){

  global $conexionDB;
  try {
              $stmt = $conexionDB->query("SELECT     codigo AS COD_OI, nombreOI AS NOMBRE_OI, tipo
                        FROM         padron.dbo.dre_ugel
                        where tipo='UGEL' and LEFT(codigo,4) like '%$id_dre'");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

  foreach (  $resultado as $key => $value) {
    $bp_datos[] = array(
        'COD_OI'=>$value["COD_OI"],
        'NOMBRE_OI'=>utf8_encode($value["NOMBRE_OI"])
      );

  }


  return  $bp_datos;
}

function bp_listar_ugel_edit($id_dre,$id_ugel){
  global $conexionDB;
//010002
  try {
          $stmt = $conexionDB->query("SELECT     codigo AS COD_OI, nombreOI AS NOMBRE_OI, tipo
              FROM         padron.dbo.dre_ugel
              where tipo='UGEL' and LEFT(codigo,4) like '%$id_dre'");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }
  foreach (  $resultado as $key => $value) {
      $bp_datos[] = array(
            'COD_OI'=>$value["COD_OI"],
            'NOMBRE_OI'=>utf8_encode($value["NOMBRE_OI"])
        );

    }
  $ugel_dropdown = '';
  $ugel_dropdown .= "\r\n" . '<option value=""' . ($id_ugel == '' ? ' selected' : '') . '>' . SwpmUtils::_('Seleccionar') . '</option>';

  if(!empty($bp_datos))
  {
    foreach ($bp_datos as $c=>$v)
    {
    $ugel_dropdown .= "\r\n" . '<option value="' . $v["COD_OI"] . '"' . (strtolower($v["COD_OI"]) == strtolower($id_ugel) ? ' selected' : '') . '>' . $v["NOMBRE_OI"] . '</option>';
    }
  }

  //var_dump($bp_datos);
  return  $ugel_dropdown ;
}

function bp_listar_ugel_datos($id_dre,$id_ugel){
  global $conexionDB;
//010002
  try {
          $stmt = $conexionDB->query("SELECT     codigo AS COD_OI, nombreOI AS NOMBRE_OI, tipo
              FROM         padron.dbo.dre_ugel
              where tipo='UGEL' and LEFT(codigo,4) like '%$id_dre'");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }
  foreach (  $resultado as $key => $value) {
      $bp_datos[] = array(
            'COD_OI'=>$value["COD_OI"],
            'NOMBRE_OI'=>utf8_encode($value["NOMBRE_OI"])
        );

    }
  if(!empty($bp_datos))
  {
    foreach ($bp_datos as $c=>$v)
    {
      if($id_ugel==$v["COD_OI"]){
        $dato=$v["NOMBRE_OI"];
      }

    }
  }

  //var_dump($bp_datos);
  return  $dato ;
}




function get_region_dropdown_bp($valor_bp){

  global $conexionDB;


  try {
              $stmt = $conexionDB->query("SELECT    LEFT(CODDEPA,2) as cod,DEPARTAMENTO
                FROM    padron.dbo.departamento ORDER BY  DEPARTAMENTO ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

  //var_dump($resultado);
  $perfil_dropdown = '';
  $perfil_dropdown .= "\r\n" . '<option value=""' . ($valor_bp == '' ? ' selected' : '') . '>' . SwpmUtils::_('Seleccionar') . '</option>';

  if(!empty($resultado))
  {
    foreach ($resultado as $c=>$v)
    {
    $perfil_dropdown .= "\r\n" . '<option value="' . $v["cod"] . '"' . (strtolower($v["cod"]) == strtolower($valor_bp) ? ' selected' : '') . '>' . $v["DEPARTAMENTO"] . '</option>';
    }
  }


  return $perfil_dropdown ;
}


function get_region_dropdown_datos($valor_bp){

  global $conexionDB;


  try {
              $stmt = $conexionDB->query("SELECT    LEFT(CODDEPA,2) as cod,DEPARTAMENTO
                FROM    padron.dbo.departamento ORDER BY  DEPARTAMENTO ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

  if(!empty($resultado))
  {
    foreach ($resultado as $c=>$v)
    {
      if($valor_bp==$v["cod"]){
        $dato=$v["DEPARTAMENTO"];
      }
    //$perfil_dropdown .= "\r\n" . '<option value="' . $v["cod"] . '"' . (strtolower($v["cod"]) == strtolower($valor_bp) ? ' selected' : '') . '>' . $v["DEPARTAMENTO"] . '</option>';
    }
  }


  return $dato;
}




add_action( 'wp_ajax_get_provincia', 'bp_get_provincia' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_provincia', 'bp_get_provincia' ); // Para usuarios no logeados

function bp_get_provincia() {
  $id_departamento=$_GET["id_departamento"];

  $bp_datos_js=bp_listar_provincia_js($id_departamento);

  echo json_encode($bp_datos_js);
  die(); // Importante finalizar el script
}


// funcion listar instituciones
function bp_listar_provincia_js($id_departamento){

  global $conexionDB;

  try {
              $stmt = $conexionDB->query("
SELECT SUBSTRING(CODPROV,3,2) as CODPROV,PROVINCIA  from provincia where LEFT(CODPROV,2)='$id_departamento' ORDER BY  PROVINCIA ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

  foreach (  $resultado as $key => $value) {
    $bp_datos[] = array(
        'COD'=>$value["CODPROV"],
        'PROVINCIA'=>utf8_encode($value["PROVINCIA"])
      );

  }

  //var_dump($bp_datos);
  return  $bp_datos;
}

function bp_listar_provincia_edit($id_departamento,$id_provincia){
  global $conexionDB;

  try {
              $stmt = $conexionDB->query("
SELECT SUBSTRING(CODPROV,3,2) as CODPROV,PROVINCIA  from provincia where LEFT(CODPROV,2)='$id_departamento' ORDER BY  PROVINCIA ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }
          /*
  foreach (  $resultado as $key => $value) {
    $bp_datos[] = array(
        'COD'=>$value["CODPROV"],
        'PROVINCIA'=>utf8_encode($value["PROVINCIA"])
      );
  }
  */
  $perfil_dropdown = '';
  $perfil_dropdown .= "\r\n" . '<option value=""' . ($id_provincia == '' ? ' selected' : '') . '>' . SwpmUtils::_('Seleccionar') . '</option>';

  if(!empty($resultado))
  {
    foreach ($resultado as $c=>$v)
    {
    $perfil_dropdown .= "\r\n" . '<option value="' . $v["CODPROV"] . '"' . (strtolower($v["CODPROV"]) == strtolower($id_provincia) ? ' selected' : '') . '>' . $v["PROVINCIA"] . '</option>';
    }
  }

  //var_dump($bp_datos);
  return  $perfil_dropdown ;
}


function bp_listar_provincia_datos($id_departamento,$id_provincia){
  global $conexionDB;

  try {
              $stmt = $conexionDB->query("
SELECT SUBSTRING(CODPROV,3,2) as CODPROV,PROVINCIA  from provincia where LEFT(CODPROV,2)='$id_departamento' ORDER BY  PROVINCIA ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

    foreach ($resultado as $key => $value) {
      $bp_datos[] = array(
            'COD'=>$value["CODPROV"],
            'PROVINCIA'=>utf8_encode($value["PROVINCIA"])
        );
    }

  if(!empty($bp_datos))
  {
    foreach ($bp_datos as $c=>$v)
    {
      if($id_provincia==$v["COD"]){
        $dato=$v["PROVINCIA"];
      }
    //$perfil_dropdown .= "\r\n" . '<option value="' . $v["CODPROV"] . '"' . (strtolower($v["CODPROV"]) == strtolower($id_provincia) ? ' selected' : '') . '>' . $v["PROVINCIA"] . '</option>';
    }
  }

  //var_dump($bp_datos);
  return $dato;
}




add_action( 'wp_ajax_get_distrito', 'bp_get_distrito' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_distrito', 'bp_get_distrito' ); // Para usuarios no logeados

function bp_get_distrito() {
  $cod_prov=$_GET["id_provincia"];

  $bp_datos_js=bp_listar_distrito_js($cod_prov);

  echo json_encode($bp_datos_js);
  die(); // Importante finalizar el script
}


// funcion listar instituciones

function bp_listar_distrito_js($cod_prov){

  global $conexionDB;

  try {
              $stmt = $conexionDB->query("select RIGHT(CODDIST,2) AS CODDIST,DISTRITO from distrito where LEFT(CODDIST,4)='$cod_prov'");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }
          foreach (  $resultado as $key => $value) {
            $bp_datos[] = array(
                'COD'=>$value["CODDIST"],
                'DISTRITO'=>utf8_encode($value["DISTRITO"])
              );

          }


  //var_dump($bp_datos);
  return  $bp_datos;
}

function bp_listar_distrito($cod_dist,$id_distrito){

  global $conexionDB;

  try {
              $stmt = $conexionDB->query("select RIGHT(CODDIST,2) AS CODDIST,DISTRITO from distrito where LEFT(CODDIST,4)='$cod_dist'");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }
          /*
          foreach (  $resultado as $key => $value) {
            $bp_datos[] = array(
                'COD'=>$value["CODDIST"],
                'DISTRITO'=>utf8_encode($value["DISTRITO"])
              );

          }
          */

          $perfil_dropdown = '';
          $perfil_dropdown .= "\r\n" . '<option value=""' . ($valor_bp == '' ? ' selected' : '') . '>' . SwpmUtils::_('Seleccionar') . '</option>';

          if(!empty($resultado))
          {
            foreach ($resultado as $c=>$v)
            {
            $perfil_dropdown .= "\r\n" . '<option value="' . $v["CODDIST"] . '"' . (strtolower($v["CODDIST"]) == strtolower($id_distrito) ? ' selected' : '') . '>' . $v["DISTRITO"] . '</option>';
            }
          }

          //var_dump($bp_datos);
          return  $perfil_dropdown ;
}


function bp_listar_distrito_datos($cod_dist,$id_distrito){

  global $conexionDB;

  try {
              $stmt = $conexionDB->query("select RIGHT(CODDIST,2) AS CODDIST,DISTRITO from distrito where LEFT(CODDIST,4)='$cod_dist'");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

          foreach (  $resultado as $key => $value) {
            $bp_datos[] = array(
                'COD'=>$value["CODDIST"],
                'DISTRITO'=>utf8_encode($value["DISTRITO"])
              );

          }



          if(!empty($bp_datos))
          {
            foreach ($bp_datos as $c=>$v)
            {
              if($id_distrito==$v["COD"]){
                $dato=$v["DISTRITO"];
              }
            //$perfil_dropdown .= "\r\n" . '<option value="' . $v["CODDIST"] . '"' . (strtolower($v["CODDIST"]) == strtolower($id_distrito) ? ' selected' : '') . '>' . $v["DISTRITO"] . '</option>';
            }
          }

          //var_dump($bp_datos);
          return  $dato;
}



add_action( 'wp_ajax_get_ie', 'bp_get_ie' ); // Para usuarios logeados
add_action( 'wp_ajax_nopriv_get_ie', 'bp_get_ie' ); // Para usuarios no logeados

function bp_get_ie() {
  $codigo=$_GET["codigo"];

  $bp_datos_js=bp_listar_ie_js($codigo);

  echo json_encode($bp_datos_js);
  die(); // Importante finalizar el script
}


// funcion listar instituciones

function bp_listar_ie_js($codigo){

  global $conexionDB;

  try {
              $stmt = $conexionDB->query("select cod_mod,cen_edu from padron.dbo.padron where codooii='$codigo' ORDER BY  cen_edu ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }

  foreach (  $resultado as $key => $value) {
    $bp_datos[] = array(
        'COD_MOD'=>$value["cod_mod"],
        'CEN_EDU'=>utf8_encode($value["cen_edu"])
      );

  }

  //var_dump($bp_datos);
  return  $bp_datos;
}


function bp_listar_ie_edit($codigo,$id_institucion_educativa){
  global $conexionDB;
//010002
  try {
          $stmt = $conexionDB->query("select cod_mod,cen_edu from padron.dbo.padron where codooii='$codigo' ORDER BY  cen_edu ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }
          foreach (  $resultado as $key => $value) {
            $bp_datos[] = array(
                'COD_MOD'=>$value["cod_mod"],
                'CEN_EDU'=>utf8_encode($value["cen_edu"])
              );

          }
  $dato_dropdown = '';
  $dato_dropdown .= "\r\n" . '<option value=""' . ($id_institucion_educativa == '' ? ' selected' : '') . '>' . SwpmUtils::_('Seleccionar') . '</option>';

  if(!empty($bp_datos))
  {
    foreach ($bp_datos as $c=>$v)
    {
    $dato_dropdown .= "\r\n" . '<option value="' . $v["COD_MOD"] . '"' . (strtolower($v["COD_MOD"]) == strtolower($id_institucion_educativa) ? ' selected' : '') . '>' . $v["CEN_EDU"] . '</option>';
    }
  }

  //var_dump($bp_datos);
  return  $dato_dropdown ;
}


function bp_listar_ie_datos($codigo,$id_institucion_educativa){
  global $conexionDB;
//010002
  try {
          $stmt = $conexionDB->query("select cod_mod,cen_edu from padron.dbo.padron where codooii='$codigo' ORDER BY  cen_edu ASC");
                              //$stmt->execute();
              $resultado = $stmt->fetchAll(PDO::FETCH_ASSOC);
              $stmt->closeCursor();
              //return $resultado;
          } catch (Exception $exc) {
              echo("ERROR DE CONEXION: ".$error);
              $resultado = NULL;
              exit;
          }
          foreach (  $resultado as $key => $value) {
            $bp_datos[] = array(
                'COD_MOD'=>$value["cod_mod"],
                'CEN_EDU'=>utf8_encode($value["cen_edu"])
              );

          }
  if(!empty($bp_datos))
  {
    foreach ($bp_datos as $c=>$v)
    {
      if($id_institucion_educativa==$v["COD_MOD"]){
        $dato=$v["CEN_EDU"];
      }

    }
  }

  //var_dump($bp_datos);
  return  $dato ;
}



//TERMINA PROYECTO_BP
