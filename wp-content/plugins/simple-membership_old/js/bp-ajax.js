jQuery(document).on({
  mouseenter: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-facebook-on.png'); },
  mouseleave: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-facebook-off.png'); }
}, '.activa_fb');

jQuery(document).on({
  mouseenter: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-twitter-on.png'); },
  mouseleave: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-twitter-off.png'); }
}, '.activa_tw');

jQuery(document).on({
  mouseenter: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-linkedin-on.png'); },
  mouseleave: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-linkedin-off.png'); }
},'.activa_li');

jQuery(document).on({
  mouseenter: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-whatsapp-on.png'); },
  mouseleave: function() { jQuery(this).attr('src', 'https://observatorio.minedu.gob.pe/almacenamiento/2019/12/ico-noticias-socmedia-whatsapp-off.png'); }
}, '.activa_wa');

jQuery(document).ready(function(){
  jQuery('#swpm_user_name').attr('required',true);
  jQuery('#swpm_user_name').attr('autocomplete','off');

  jQuery('#ajax-contact-form').submit(function(e){
    /*
    var valor1 = jQuery("#name_w").val();
    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'contact_form',
      valor1: valor1,
    },
    function(data) {
      console.log(data);
    });
    */

    jQuery.ajax({
         data: {action: 'contact_form'},
         type: 'post',
         url: "https://observatorio.minedu.gob.pe/acceder/admin-ajax.php",
         success: function(data) {
              alert(data); // This prints '0', I want this to print whatever name the user inputs in the form.

        }
    });

  })


  jQuery("#tipo_doc").change(function(){
    jQuery('.bp_title2').html("CENTRO LABORAL");

    jQuery("#user_name").removeAttr( "size" );
    jQuery("#user_name").removeAttr( "maxlength" );
    jQuery("#user_name").attr("size","12");
    jQuery("#user_name").attr("maxlength","12");
    jQuery("#user_name").removeAttr("onkeypress");
    //jQuery("#user_name").removeAttr( "id" );
    jQuery('.swpm-registration-firstname-row').show();
    jQuery('.swpm-registration-lastname-row').show();
  });

  jQuery("#edit").removeAttr( "href" );
  jQuery("#edit").attr("href","https://observatorio.minedu.gob.pe/membership-login/membership-profile/");
  //Listamos Departamento
  jQuery("#id_departamento").change(function(){
    var id_departamento = jQuery(this).find(":selected").val();

    jQuery("#id_provincia").empty();
    jQuery("#id_distrito").empty();
    bpListarProvincia(id_departamento);

  });
  //FUNCION PARA LISTAR PROVINCIAS
  function bpListarProvincia(valor1){

    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'get_provincia',
      id_departamento: valor1,
    },
    function(data) { //Procesamiento de los datos de vuelta
      //console.log(data);
      jQuery('#id_provincia').empty();
      jQuery("#id_provincia").append('<option value=""> Seleccionar </option>');
      jQuery.each(data,function(key, values) {
        jQuery("#id_provincia").append('<option value='+values.COD+'>'+values.PROVINCIA+'</option>');
      });

    });
  }

  //listamos los institutos
  jQuery("#id_provincia").change(function(){
    var id_provincia = jQuery(this).find(":selected").val();
    var id_departamento=jQuery("#id_departamento").find(":selected").val();
    var id_distrito = jQuery("#id_distrito").find(":selected").val();

      bpListarDistrito(id_departamento+id_provincia);
      //bpListarIE(id_departamento+id_provincia+id_distrito);
  });

  //FUNCION PARA LISTAR PROVINCIAS
  function bpListarDistrito(valor1){

    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'get_distrito',
      id_provincia: valor1,
    },
    function(data) { //Procesamiento de los datos de vuelta

      jQuery('#id_distrito').empty();
      jQuery("#id_distrito").append('<option value=""> Seleccionar </option>');
      jQuery.each(data,function(key, values) {
        jQuery("#id_distrito").append('<option value='+values.COD+'>'+values.DISTRITO+'</option>');
      });

       //jQuery("#bp_mensaje").html(bp_msg)
       /**  jQuery("#bp_mensaje").html(bp_msg);*/

    });
  }



  jQuery(".wpcf7-tel").keydown(function(e)
  {
      var key = e.charCode || e.keyCode || 0;
      // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal
      return (
          key == 8 ||
          key == 9 ||
          key == 46 ||
          key == 110 ||
          key == 190 ||
          (key >= 35 && key <= 40) ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105));
  });

  jQuery("#phone").keydown(function(e)
  {
      var key = e.charCode || e.keyCode || 0;
      // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal
      return (
          key == 8 ||
          key == 9 ||
          key == 46 ||
          key == 110 ||
          key == 190 ||
          (key >= 35 && key <= 40) ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105));
  });

  //ellipsis_box(".et_pb_slide_title", 60);
  //ellipsis_box(".et_pb_slide_content", 150);
  //ellipsis_box(".bp_estilo_1", 80);
  //ellipsis_box(".category-content-bp", 189);

  function ellipsis_box(elemento, max_chars){
    //console.log("et_pb_slide_title");
    limite_text="";
    limite="";
  	limite_text = jQuery(elemento).text();
  	if (limite_text.length > max_chars)
  	{
  	//limite = limite_text.substr(0, max_chars)+" ...";
    limite = limite_text.substr(0, max_chars);
  	jQuery(elemento).text(limite);
  	}
  }

  jQuery(".bp_cerrar").click(function() {
    window.close();
  });
  //jQuery(".et-divi-customizer-global-cached-inline-styles").remove();

  jQuery("link").removeAttr("onerror");
  console.log("ok");
  /*
  var yasrRaterVVInDom1 = document.getElementsByClassName('yasr-rater-stars-visitor-votes');

    if (yasrRaterVVInDom1.length > 0) {
        //yasrVisitorVotesFront(yasrRaterVVInDom);
        //  alert("prueba");
    }
    */
  var bp_id_page_1=jQuery("#bp_id_page").val();
  getAverageRating(bp_id_page_1);
  jQuery(".yasr-rater-stars-visitor-votes").click(function() {
    //alert("prueba");
    var bp_id_page=jQuery("#bp_id_page").val();
    //console.log(bp_id_page);
    //llamar la funcion de reporte de valoracion.
    getAverageRating(bp_id_page);


  });
  function getAverageRating(valor1){

    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'getAverageRating',
      bp_id_page: valor1,
    },
    function(data) { //Procesamiento de los datos de vuelta
    //  console.log(data);
      //var bp_msg="";

      jQuery('.AverageRating').html(data);
    });
  }


    jQuery("#id_perfil").change(function(){
      id = jQuery(this).find(":selected").val();


      jQuery("#user_name").attr("onkeypress","return event.charCode >= 48 && event.charCode <= 57");

      //actualizamos el capcha
      var URLactual = location.origin;
      document.querySelector(".captcha-image").src = URLactual+'/wp-content/plugins/simple-membership/views/captcha.php?' + Date.now();


      jQuery('#bp_mensaje').hide();
      jQuery('#id_departamento option:selected').removeAttr('selected');
      jQuery('#id_carrera option:selected').removeAttr('selected');
      jQuery('#id_carrera option:selected').empty();
      //jQuery('#tipo_oficina option:selected').empty();
      jQuery('#tipo_oficina option:selected').removeAttr('selected');
      jQuery('#id_dre option:selected').removeAttr('selected');
      jQuery('#id_cargo option:selected').removeAttr('selected');
      jQuery('#captcha').val('');
      jQuery("#centro_laboral").val("");
      jQuery("#user_name").val("");
      jQuery("#ubigeo").val("");


      jQuery('#grupo_1').hide();
      jQuery('.swpm-registration-id_nivel-row').hide();
      jQuery('.swpm-registration-id_dre-row').hide();
      jQuery('.swpm-registration-id_ugel-row').hide();
      jQuery('.swpm-registration-id_institucion-row').hide();
      jQuery('.swpm-registration-id_carrera-row').hide();
      jQuery('.swpm-registration-codigo_modular-row').hide();
      jQuery('.swpm-registration-centro_laboral-row').hide();
      jQuery('.swpm-registration-tipo_oficina-row').hide();
      jQuery('.swpm-registration-id_cargo-row').hide();

      jQuery('#id_nivel').removeAttr('required');
      jQuery('#id_dre').removeAttr('required');
      jQuery('#id_ugel').removeAttr('required');
      jQuery('#id_institucion').removeAttr('required');
      jQuery('#id_carrera').removeAttr('required');
      jQuery('#codigo_modular').removeAttr('required');
      jQuery('#centro_laboral').removeAttr('required');
      jQuery('#tipo_oficina').removeAttr('required');
      jQuery('#id_cargo').removeAttr('required');
      jQuery('#tipo_doc').removeAttr('required');
      jQuery('#id_institucion_educativa').removeAttr('required');

      jQuery('#id_provincia option:selected').removeAttr('selected');
      jQuery('#id_distrito option:selected').removeAttr('selected');
      jQuery('#id_departamento option:selected').removeAttr('selected');
      /*
      jQuery('#id_provincia').empty();
      jQuery('#id_distrito').empty();
      jQuery('#id_institucion').empty();
      */

      jQuery('#id_ugel').empty();
      jQuery('#id_institucion_educativa').empty();

      jQuery('#id_nivel').empty();
      jQuery("#id_nivel").append('<option value="">Seleccionar</option>');
      jQuery("#id_nivel").append('<option value="1">Inicial</option>');
      jQuery("#id_nivel").append('<option value="2">Primaria</option>');
      jQuery("#id_nivel").append('<option value="3">Secundaria</option>');
      jQuery("#id_nivel").append('<option value="4">Instituto</option>');
      jQuery("#id_nivel").append('<option value="5">Universidad</option>');

      jQuery('#id_centro').empty();
      jQuery("#id_centro").append('<option value="">Seleccionar</option>');
      jQuery("#id_centro").append('<option value="1">Pública</option>');
      jQuery("#id_centro").append('<option value="2">Privada</option>');

  });
  jQuery('.swpm-registration-firstname-row').hide();
  jQuery('.swpm-registration-lastname-row').hide();
  /**
   * Validacion del formulario Iniciar session
   */
   jQuery('#swpm_user_name').on('change invalid', function() {
      var campotexto = jQuery(this).get(0);
      campotexto.setCustomValidity('');

      if (!campotexto.validity.valid) {
        campotexto.setCustomValidity('El número de DNI es obligatorio');
      }
    });

    jQuery('#swpm_password').on('change invalid', function() {
       var campotexto = jQuery(this).get(0);
       campotexto.setCustomValidity('');

       if (!campotexto.validity.valid) {
         campotexto.setCustomValidity('El campo Contraseña es obligatorio');
       }
     });
     jQuery('#captcha').on('change invalid', function() {
        var campotexto = jQuery(this).get(0);
        campotexto.setCustomValidity('');

        if (!campotexto.validity.valid) {
          campotexto.setCustomValidity('Ingrese el Código de Seguridad');
        }
      });



  /**
    * Validacion del formulario de registro
    */

    jQuery('.swpm-registration-tipo_doc-row').hide();

    jQuery('.swpm-registration-subperfil-row').hide();

    jQuery('#grupo_1').hide();
    jQuery('.swpm-registration-id_centro-row').hide();
    jQuery('.swpm-registration-id_nivel-row').hide();
    jQuery('.swpm-registration-id_dre-row').hide();
    jQuery('.swpm-registration-id_ugel-row').hide();
    jQuery('.swpm-registration-id_institucion-row').hide();
    jQuery('.swpm-registration-id_institucion_educativa-row').hide();
    jQuery('.swpm-registration-id_carrera-row').hide();
    jQuery('.swpm-registration-codigo_modular-row').hide();
    jQuery('.swpm-registration-centro_laboral-row').hide();
    jQuery('.swpm-registration-tipo_oficina-row').hide();
    jQuery('.swpm-registration-id_cargo-row').hide();
    jQuery('.swpm-registration-oficina-row').hide();
    jQuery('.swpm-registration-cargo-row').hide();
    jQuery('#bp_mensaje_ext').hide();
    jQuery('.swpm-registration-tipo_doc-row').hide();
    jQuery('.bp_titulo_2').hide();

  jQuery( ".extranjeros" ).click(function() {
    jQuery("#user_name").val("");
    //jQuery('#id_perfil').hide();
    jQuery('#bp_mensaje_ext').hide();
    //habilitamos los nuevos campos

    jQuery('.swpm-registration-tipo_doc-row').show();
    //quitamos los campos dni y ubigeo
    jQuery('.swpm-registration-username-row').show();


    jQuery('.swpm-registration-tipo_doc-row').removeAttr('required');
    jQuery('.swpm-registration-username-row').removeAttr('required');

    jQuery('.swpm-registration-ubigeo-row').hide();
    jQuery('.swpm-registration-icono-row').hide();

    jQuery('.swpm-registration-firstname-row').hide();
    jQuery('.swpm-registration-lastname-row').hide();

  });




    jQuery("#id_perfil").change(function(){
      var id="";
       id = jQuery(this).find(":selected").val();

      if (id=='5'){
        jQuery('#bp_mensaje_ext').show();

      }else{
        jQuery("#user_name").removeAttr( "size" );
        jQuery("#user_name").removeAttr( "maxlength" );
        jQuery("#user_name").attr("size","8");
        jQuery("#user_name").attr("maxlength","8");
        //jQuery("#tipo_doc").empty();
        jQuery('#tipo_doc option:selected').removeAttr('selected');

          jQuery('#bp_mensaje_ext').hide();
          jQuery('.swpm-registration-username-row').show();
          jQuery('.swpm-registration-ubigeo-row').show();
          jQuery('.swpm-registration-icono-row').show();
          jQuery('.swpm-registration-tipo_doc-row').hide();
      }

      switch (id) {
        case '1': //Investigador

          jQuery('.bp_title2').html("CENTRO LABORAL");
          jQuery('.swpm-registration-id_centro-row').show();
          jQuery('#grupo_1').show();
          jQuery('.swpm-registration-centro_laboral-row').show();

          jQuery('.swpm-registration-id_nivel-row').hide();
          jQuery('.swpm-registration-id_institucion-row').hide();
          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-tipo_oficina-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('.swpm-registration-firstname-row').hide();
          jQuery('.swpm-registration-lastname-row').hide();

          jQuery('.swpm-registration-id_institucion_educativa-row').hide();
          jQuery('#id_institucion_educativa').attr('required', '');

          jQuery('#id_subperfil').removeAttr('required');
          jQuery('#id_centro').attr('required', '');
          jQuery('#id_departamento').attr('required', '');
          jQuery('#id_provincia').attr('required', '');
          jQuery('#id_distrito').attr('required', '');
          jQuery('#centro_laboral').attr('required', '');


          jQuery('.bp_titulo_2').show();

          break;
        case '2':// Estudiante
          jQuery('.bp_title2').html("CENTRO DE ESTUDIOS");
          jQuery('#id_nivel').empty();
          jQuery("#id_nivel").append('<option value="">Seleccionar</option>');
          jQuery("#id_nivel").append('<option value="4">Instituto</option>');
          jQuery("#id_nivel").append('<option value="5">Universidad</option>');

          jQuery('.swpm-registration-id_centro-row').show();
          jQuery('#grupo_1').show();
          jQuery('.swpm-registration-id_nivel-row').show();
          jQuery('.swpm-registration-id_institucion-row').show();
          jQuery('.swpm-registration-id_carrera-row').show();

          jQuery('.swpm-registration-id_institucion_educativa-row').hide();
          jQuery('.swpm-registration-codigo_modular-row').hide();
          jQuery('.swpm-registration-id_dre-row').hide();
          jQuery('.swpm-registration-id_ugel-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();
          jQuery('.swpm-registration-tipo_oficina-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('.swpm-registration-firstname-row').hide();
          jQuery('.swpm-registration-lastname-row').hide();

          jQuery('.bp_titulo_2').show();

          jQuery('#id_centro').attr('required', '');
          jQuery('#id_departamento').attr('required', '');
          jQuery('#id_provincia').attr('required', '');
          jQuery('#id_distrito').attr('required', '');

          jQuery('#id_nivel').attr('required', '');
          jQuery('#id_institucion').attr('required', '');
          jQuery('#id_carrera').attr('required', '');

          jQuery('#id_subperfil').removeAttr('required');


          break;
        case '3': //Docente
          jQuery('.bp_title2').html("INSTITUCIÓN EDUCATIVA");
          jQuery('.swpm-registration-id_centro-row').show();
          jQuery('.swpm-registration-id_nivel-row').show();

          jQuery('#id_centro').attr('required', '');
          jQuery('#id_nivel').attr('required', '');
          //seleccionamos el nivel educativo
          jQuery("#id_nivel").change(function(){
            //jQuery(id_nivel).empty();
            var id_nivel = jQuery(this).find(":selected").val();
            if(id_nivel=='1' || id_nivel=='2'  || id_nivel=='3'){
                jQuery('#user_name').empty();
                /*
                jQuery('#user_name').on('input', function () {
                      this.value = this.value.replace(/[^0-9]/g,'');
                  });
                  */
              jQuery('.swpm-registration-id_dre-row').show();
              jQuery('.swpm-registration-id_ugel-row').show();
              //jQuery('#grupo_1').show();
              //jQuery('.swpm-registration-id_institucion_educativa-row').show();
              //jQuery('.swpm-registration-codigo_modular-row').show();

              jQuery('.swpm-registration-id_institucion-row').hide();
              jQuery('.swpm-registration-id_carrera-row').hide();


              //datos requeridos


              //jQuery('#codigo_modular').attr('required', '');
              jQuery('#id_dre').attr('required', '');
              jQuery('#id_ugel').attr('required', '');
              //jQuery('#id_departamento').attr('required', '');
              //jQuery('#id_provincia').attr('required', '');
              //jQuery('#id_distrito').attr('required', '');
            }else{
                jQuery('#user_name').empty()
              jQuery('.swpm-registration-id_dre-row').hide();
              jQuery('.swpm-registration-id_ugel-row').hide();
              jQuery('.swpm-registration-id_institucion_educativa-row').hide();
              jQuery('.swpm-registration-codigo_modular-row').hide();

              jQuery('.swpm-registration-id_institucion-row').show();
              jQuery('.swpm-registration-id_carrera-row').show();
              jQuery('#grupo_1').show();

              jQuery('#id_institucion').attr('required', '');
              jQuery('#id_departamento').attr('required', '');
              jQuery('#id_provincia').attr('required', '');
              jQuery('#id_distrito').attr('required', '');
              jQuery('#id_carrera').attr('required', '');
            }
          });
          jQuery('.bp_titulo_2').show();

          //jQuery('.swpm-registration-id_institucion-row').show();




          //jQuery('#grupo_1').show();
          //jQuery('.swpm-registration-id_nivel-row').show();
          //jQuery('.swpm-registration-id_institucion-row').show();
          //jQuery('.swpm-registration-id_carrera-row').show();


          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();
          jQuery('.swpm-registration-tipo_oficina-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('.swpm-registration-firstname-row').hide();
          jQuery('.swpm-registration-lastname-row').hide();

          /*
          jQuery('#id_nivel').attr('required', '');
          jQuery('#id_institucion').attr('required', '');
          jQuery('#codigo_modular').attr('required', '');
          jQuery('#id_dre').attr('required', '');
          jQuery('#id_ugel').attr('required', '');
          */
          break;
        case '4': //Funcionario
        jQuery('.bp_title2').html("CENTRO LABORAL");
        jQuery('.swpm-registration-tipo_oficina-row').show();
        jQuery('.swpm-registration-id_dre-row').show();
        jQuery('.swpm-registration-id_ugel-row').show();
        //jQuery('.swpm-registration-id_institucion_educativa-row').show();
        jQuery('.swpm-registration-id_cargo-row').show();
        jQuery('.bp_titulo_2').show();
          jQuery('#grupo_1').hide();
          jQuery('.swpm-registration-id_nivel-row').hide();
          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();

          jQuery('.swpm-registration-firstname-row').hide();
          jQuery('.swpm-registration-lastname-row').hide();

          jQuery('#id_institucion_educativa').attr('required', '');
          jQuery('#codigo_modular').attr('required', '');
          jQuery('#id_dre').attr('required', '');
          jQuery('#id_ugel').attr('required', '');
          jQuery('#tipo_oficina').attr('required', '');
          jQuery('#id_cargo').attr('required', '');
          break;
        case '5': // Otros
        jQuery('.bp_title2').html("CENTRO LABORAL");
        jQuery('.swpm-registration-id_centro-row').show();
        jQuery('#grupo_1').show();
        jQuery('.bp_titulo_2').show();
        jQuery('.swpm-registration-centro_laboral-row').show();

        jQuery('.swpm-registration-id_nivel-row').hide();
        jQuery('.swpm-registration-id_institucion-row').hide();
        jQuery('.swpm-registration-id_carrera-row').hide();
        jQuery('.swpm-registration-tipo_oficina-row').hide();
        jQuery('.swpm-registration-id_cargo-row').hide();

        jQuery('.swpm-registration-firstname-row').hide();
        jQuery('.swpm-registration-lastname-row').hide();

        jQuery('.swpm-registration-id_institucion_educativa-row').hide();
        jQuery('#id_institucion_educativa').attr('required', '');

        jQuery('#id_subperfil').removeAttr('required');
        jQuery('#id_centro').attr('required', '');
        jQuery('#id_departamento').attr('required', '');
        jQuery('#id_provincia').attr('required', '');
        jQuery('#id_distrito').attr('required', '');
        jQuery('#centro_laboral').attr('required', '');


        break;
      default:

          jQuery('#grupo_1').hide();
          jQuery('.swpm-registration-id_nivel-row').hide();
          jQuery('.swpm-registration-id_dre-row').hide();
          jQuery('.swpm-registration-id_ugel-row').hide();
          jQuery('.swpm-registration-id_institucion-row').hide();
          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-codigo_modular-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();
          jQuery('.swpm-registration-tipo_oficina-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('#id_nivel').removeAttr('required');
          jQuery('#id_dre').removeAttr('required');
          jQuery('#id_ugel').removeAttr('required');
          jQuery('#id_institucion').removeAttr('required');
          jQuery('#id_carrera').removeAttr('required');
          jQuery('#codigo_modular').removeAttr('required');
          jQuery('#centro_laboral').removeAttr('required');
          jQuery('#tipo_oficina').removeAttr('required');
          jQuery('#id_cargo').removeAttr('required');
          jQuery('#tipo_doc').removeAttr('required');
          jQuery('#id_institucion_educativa').removeAttr('required');

      }
  });




      jQuery( ".swpm-registration-submit" ).click(function() {
        //jQuery(".bp_listar_evidencias").show();
        var id_nivel = jQuery("#id_nivel").find(":selected").val();
        //console.log(id_nivel);
        if(id_nivel=='1' || id_nivel=='2'  || id_nivel=='3'){
          var id_ie = jQuery("#id_institucion_educativa").find(":selected").val();
          if (id_ie ==""){
            jQuery('#id_institucion_educativa').attr('required', '');
            //return false;
          }else{
            jQuery('#id_institucion_educativa').removeAttr('required');
            return true;
          }
        }

      });

      //Listamos UGEL
      jQuery("#id_dre").change(function(){
        var id_dre = jQuery(this).find(":selected").val();

          bpListarUGEL(id_dre);
      });

      //Listamos UGEL
      jQuery("#id_ugel").change(function(){
        var id_ugel = jQuery(this).find(":selected").val();

          jQuery('.swpm-registration-id_institucion_educativa-row').show();
          bpListarIE(id_ugel);
      });

      //listamos los institutos
      jQuery("#id_nivel").change(function(){
        var id_nivel = jQuery(this).find(":selected").val();
        var id_centro=jQuery("#id_centro").find(":selected").val()

          bpListarInstituciones(id_nivel,id_centro);
      });
      //listamos los institutos
      jQuery("#id_centro").change(function(){
        var id_centro = jQuery(this).find(":selected").val();
        var id_nivel=jQuery("#id_nivel").find(":selected").val()

          bpListarInstituciones(id_nivel,id_centro);
      });





      //FUNCION PARA UGEL
      function bpListarUGEL(valor1){

        jQuery.getJSON("/acceder/admin-ajax.php",{
          action: 'get_ugel',
          id_dre: valor1,
        },
        function(data) { //Procesamiento de los datos de vuelta
          //console.log(JSON.stringify(data));
          //var bp_msg="";

          jQuery('#id_ugel').empty();
          jQuery("#id_ugel").append('<option value=""> Seleccionar </option>');
          jQuery.each(data,function(key, values) {
            jQuery("#id_ugel").append('<option value='+values.COD_OI+'>'+values.NOMBRE_OI+'</option>');
          });

    	     //jQuery("#bp_mensaje").html(bp_msg)
           /**  jQuery("#bp_mensaje").html(bp_msg);*/

        });
      }

      //FUNCION PARA IE
      function bpListarIE(valor1){

        jQuery.getJSON("/acceder/admin-ajax.php",{
          action: 'get_ie',
          codigo: valor1,
        },
        function(data) { //Procesamiento de los datos de vuelta
          //console.log(JSON.stringify(data));
          //var bp_msg="";

          jQuery('#id_institucion_educativa').empty();
          jQuery("#id_institucion_educativa").append('<option value=""> Seleccionar </option>');
          jQuery.each(data,function(key, values) {
            jQuery("#id_institucion_educativa").append('<option value='+values.COD_MOD+'>'+values.CEN_EDU+'</option>');
          });

    	     //jQuery("#bp_mensaje").html(bp_msg)
           /**  jQuery("#bp_mensaje").html(bp_msg);*/

        });
      }

      //listamos los institutos
      jQuery("#id_institucion_educativa").change(function(){
        var cod = jQuery(this).find(":selected").val();
            jQuery('#codigo_modular').val(cod);
      });




      //FUNCION PARA VALIDAR DATOS DEL definitions
      function bpListarInstituciones(valor1,valor2){

        jQuery.getJSON("/acceder/admin-ajax.php",{
          action: 'get_instituciones',
          id_nivel: valor1,
          id_centro:valor2
        },
        function(data) { //Procesamiento de los datos de vuelta
          jQuery('#id_institucion').empty();
          jQuery("#id_institucion").append('<option value=""> Seleccionar </option>');
          jQuery.each(data,function(key, values) {
            jQuery("#id_institucion").append('<option value='+values.id+'>'+values.descripcion+'</option>');
          });

    	     //jQuery("#bp_mensaje").html(bp_msg)
           /**  jQuery("#bp_mensaje").html(bp_msg);*/

        });
      }










    jQuery("#id_perfil").change(function(){

    var id = jQuery(this).find(":selected").val();
    jQuery.getJSON("/acceder/admin-ajax.php",{
        action: 'get_subperfil',
        perfil: id
      },
        function(data) { //Procesamiento de los datos de vuelta

          if(!jQuery.isEmptyObject(data)){
            jQuery('.swpm-registration-subperfil-row').show();
          }else{
            jQuery('.swpm-registration-subperfil-row').hide();
          }

          jQuery('#id_subperfil').empty();
          var $select = jQuery('#id_subperfil');

          //alert(options);
          $select.append('<option value="">Seleccionar</option>');
          jQuery.each(data, function(i,n) {
            //console.log(i);
            $select.append('<option value=' + i + '>' + n + '</option>');
          });



        });
  });
/*
  jQuery("#ubigeo").keyup(function(a){
  	if(jQuery(this).val().length!=6 ){
  		jQuery("#bp_mensaje").html("El ubigeo tiene que contener 6 digitos");
  	}else{
      bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}
  });
  jQuery("#ubigeo").blur(function(a){
    if(jQuery(this).val().length!=6 ){
  		jQuery("#bp_mensaje").html("El ubigeo tiene que contener 6 digitos");
  	}else{
      bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}
  });
  */

  jQuery("#ubigeo").keyup(function(a){
  	if(jQuery(this).val().length==6 && jQuery("#user_name").val().length==8){
      jQuery('#bp_mensaje').show();
  		bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  		jQuery("#bp_mensaje").html("");
  	}
  });

  jQuery("#user_name").keyup(function(a){
    jQuery('#bp_mensaje').show();
  	if(jQuery(this).val().length==8 && jQuery("#ubigeo").val().length==6){
  	   bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  	jQuery("#bp_mensaje").html("");
  	}
  });


  jQuery("#user_name").blur(function(a){
    jQuery('#bp_mensaje').show();
  	if(jQuery(this).val().length==8 && jQuery("#ubigeo").val().length==6){
  	   bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  	   jQuery("#bp_mensaje").html("");
  	}
  });

  jQuery("#ubigeo").blur(function(a){
    jQuery('#bp_mensaje').show();
  	if(jQuery(this).val().length==6 && jQuery("#user_name").val().length==8){
  	   bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  	   jQuery("#bp_mensaje").html("");
  	}
  });



  //FUNCION PARA VALIDAR DATOS DEL definitions
  function bpValidarDatos(valor1,valor2){

    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'get_datos_dni',
      nro_doc: valor1,
      ubigeo:valor2
    },
    function(data) { //Procesamiento de los datos de vuelta
      var bp_msg="";
      if(data.msg=="0"){
			  bp_msg="El DNI es Invalido";
		  }else{
        if(data.msg=="1"){
            //bp_msg=data.nombres;
            //bp_msg="Datos correctos"
            bp_msg=data.nombres+' '+data.apellidos;
        }else{
            bp_msg="<span style='color:#ff0000!important;font-family: Calibri Regular,Helvetica,Arial,Lucida,sans-serif!important;font-size: 16px!important;margin-top: -2px;'>El DNI o código de Ubigeo no son válidos. Intente nuevamente.</span>";
        }
      }
	     //jQuery("#bp_mensaje").html(bp_msg)
      jQuery("#bp_mensaje").html(bp_msg);
       jQuery('input[name="first_name"]:hidden').val(data.nombres);
       jQuery('input[name="last_name"]:hidden').val(data.apellidos);
      /*jQuery('#first_name').val(data.nombres);
      jQuery('#last_name').val(data.apellidos);*/
    });
  }


/* Validacion para el registro*/


/* Termina*/
//jQuery("#nro_doc").validarNumero();

  jQuery("#nro_doc").keydown(function(e)
  {
      var key = e.charCode || e.keyCode || 0;
      // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal
      return (
          key == 8 ||
          key == 9 ||
          key == 46 ||
          key == 110 ||
          key == 190 ||
          (key >= 35 && key <= 40) ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105));
  });



  jQuery("#ubigeo").keydown(function(e)
  {
      var key = e.charCode || e.keyCode || 0;
      // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal
      return (
          key == 8 ||
          key == 9 ||
          key == 46 ||
          key == 110 ||
          key == 190 ||
          (key >= 35 && key <= 40) ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105));
  });

var filter = jQuery('#filter');

bpSearch(filter);


jQuery('#filter').submit(function(){


  var filter = jQuery('#filter');
  bpSearch(filter);
  return false;
});

//FUNCION PARA VALIDAR DATOS DEL definitions
function bpSearch(filter){
  jQuery.ajax({
      url:filter.attr('action'),
      data:filter.serialize(), // form data
      type:filter.attr('method'), // POST
      beforeSend:function(xhr){
          filter.find('button').text('PROCESANDO...'); // changing the button label
      },
      success:function(data){
          filter.find('button').text('BUSCAR'); // changing the button label back
          jQuery('#response').html(data); // insert data
      }
  });

}


jQuery('#brand').on('change', function() {
  var tipo_practica = jQuery(this).find(":selected").val();
  console.log(tipo_practica);
  //alert( this.value );
  if(tipo_practica!=''){
    //alert("vacio");
      if(tipo_practica=='0'){
        jQuery('#brand').empty();
        jQuery("#brand").append('<option value="">Tipo de Práctica</option>');
        jQuery("#brand").append('<option value="0">&nbsp; Ver Todos</option>');
        jQuery("#brand").append('<option value="directivos">&nbsp; Directivos</option>');
        jQuery("#brand").append('<option value="docentes">&nbsp; Docentes</option>');
        jQuery('#size').empty();
        jQuery("#size").append('<option value="">Nivel Educativo</option>');
        jQuery("#size").append('<option value="0">&nbsp; Ver Todos</option>');
        jQuery("#size").append('<option value="inicial">&nbsp; Inicial</option>');
        jQuery("#size").append('<option value="primaria">&nbsp; Primaria</option>');
        jQuery("#size").append('<option value="secundaria">&nbsp; Secundaria</option>');
        jQuery("#filter").submit();
        var filter = jQuery('#filter');
        bpSearch(filter);
      }else{
        jQuery("#filter").submit();
        var filter = jQuery('#filter');
        bpSearch(filter);
      }
  }



});

jQuery('#size').on('change', function() {
  var nivel_educativo = jQuery(this).find(":selected").val();
  //alert( this.value );
  if(nivel_educativo!=''){
    if(nivel_educativo=='0'){
      jQuery('#brand').empty();
      jQuery("#brand").append('<option value="">Tipo de Práctica</option>');
      jQuery("#brand").append('<option value="0">&nbsp; Ver Todos</option>');
      jQuery("#brand").append('<option value="directivos">&nbsp; Directivos</option>');
      jQuery("#brand").append('<option value="docentes">&nbsp; Docentes</option>');
      jQuery('#size').empty();
      jQuery("#size").append('<option value="">Nivel Educativo</option>');
      jQuery("#size").append('<option value="0">&nbsp; Ver Todos</option>');
      jQuery("#size").append('<option value="inicial">&nbsp; Inicial</option>');
      jQuery("#size").append('<option value="primaria">&nbsp; Primaria</option>');
      jQuery("#size").append('<option value="secundaria">&nbsp; Secundaria</option>');
      jQuery("#filter").submit();
      var filter = jQuery('#filter');
      bpSearch(filter);
    }else{
      jQuery("#filter").submit();
      var filter = jQuery('#filter');
      bpSearch(filter);
    }
  }

});

/*********************************************/
var filter_bp = jQuery('#filter_bp');
//bpSearch2(filter_bp);

function bpSearch2(filter_bp){
    //var filter_bp = jQuery('#filter_bp');
    jQuery.ajax({
        url:filter_bp.attr('action'),
        data:filter_bp.serialize(), // form data
        type:filter_bp.attr('method'), // POST
        beforeSend:function(xhr){
            filter_bp.find('button').text('LOAD...'); // changing the button label
        },
        success:function(data){
            filter_bp.find('button').text('BUSCAR'); // changing the button label back
            jQuery('#response_search').html(data); // insert data
        }
    });
	};
  jQuery('#brand2').on('change', function() {
    //alert( this.value );
    //jQuery("#filter_bp").submit();
    var filter_bp = jQuery('#filter_bp');
    bpSearch2(filter_bp);
    jQuery("#search_post_bp").hide();
  });

  jQuery('#size2').on('change', function() {
    //alert( this.value );
    //jQuery("#filter_bp").submit();
    var filter_bp = jQuery('#filter_bp');
    bpSearch2(filter_bp);
    jQuery("#search_post_bp").hide();
  });

jQuery("#search_bp").keyup(function(a){
  var filter_bp = jQuery('#filter_bp');
  bpSearch2(filter_bp);
  jQuery("#search_post_bp").hide();
});


jQuery("#search_bp").blur(function(a){
  var filter_bp = jQuery('#filter_bp');
  bpSearch2(filter_bp);
  jQuery("#search_post_bp").hide();
});

jQuery('#category').on('change', function() {
  //alert( this.value );
  //jQuery("#filter_bp").submit();
  var filter_bp = jQuery('#filter_bp');
  bpSearch2(filter_bp);

    jQuery("#search_post_bp").hide();
});

/**
  * eventos para listar imagens buena practica
  */
  jQuery(".bp_listar_evidencias").hide();
  jQuery(".bp_mensaje_out").hide();

  jQuery( ".bp_button_evidencia" ).click(function() {
    //jQuery(".bp_listar_evidencias").show();
    jQuery(".bp_listar_evidencias").toggle();
    return false;
  });
  jQuery( ".bp_evidencia_close" ).click(function() {
    //jQuery(".bp_listar_evidencias").show();
    jQuery(".bp_listar_evidencias").hide();
    return false;
  });
  jQuery( ".bp_button_evidencia_out" ).click(function() {
    jQuery(".bp_mensaje_out").hide()
    jQuery(".bp_mensaje_out").show(1500);
    return false;
  });
  jQuery( ".bp_update_datos_out" ).click(function() {
    jQuery(".bp_mensaje_out").hide()
    jQuery(".bp_mensaje_out").show(1500);
    return false;
  });
  jQuery( ".bp_descargar_out" ).click(function() {
    jQuery(".bp_mensaje_out").hide()
    jQuery(".bp_mensaje_out").show(1500);
    return false;
  });

  jQuery(".bp_msg_mi").hide();
/*
$("#booking_Form #submit_Booking").one("click", function(event) {
    //do something
});
*/
  var n = 0;
  jQuery(".bp_update_datos").click(function() {
    var bp_id_page=jQuery("#bp_id_page").val();
    var bp_id_user=jQuery("#bp_id_user").val();
    var bp_nro_doc=jQuery("#bp_nro_doc").val();
    //$(this).attr('disabled','disabled');
    //var n=1;

    n++;


    console.log(n);
    if(n>1){
      return false;
    }

    alert(1);
    //alert(bp_id_page);
    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'get_update_datos',
      bp_id_page: bp_id_page,
      bp_id_user:bp_id_user,
      bp_nro_doc:bp_nro_doc

    },

    function(data) { //Procesamiento de los datos de vuelta
      if(data!='0'){
          jQuery(".bp_msg_mi").show().delay(5000).fadeOut();
          jQuery(".bp_disable").removeClass("bp_update_datos");
          //jQuery(".bp_update_datos").attr('disabled','disabled');
          //jQuery(".bp_update_datos").hide();

      }

    });

  });


  var n2 = 0;
  jQuery(".bp_update_descargar").click(function() {
    var bp_id_page=jQuery("#bp_id_page").val();
    var bp_id_user=jQuery("#bp_id_user").val();
    var bp_nro_doc=jQuery("#bp_nro_doc").val();
    //$(this).attr('disabled','disabled');
    //var n=1;

    n2++;


    console.log(n);
    if(n2>1){
      return false;
    }

    //alert(bp_id_page);
    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'get_update_descargar',
      bp_id_page: bp_id_page,
      bp_id_user:bp_id_user,
      bp_nro_doc:bp_nro_doc,
      flag:"1"
    },

    function(data) { //Procesamiento de los datos de vuelta
      if(data!='0'){
          //jQuery(".bp_msg_mi").show().delay(5000).fadeOut();
          //jQuery(".bp_disable").removeClass("bp_update_datos");
          //jQuery(".bp_update_datos").attr('disabled','disabled');
          //jQuery(".bp_update_datos").hide();

      }

    });

  });


  /* actualizamos mi perfil*/
  jQuery(".swpm-mi-subperfil-row").hide();
  jQuery(".bp_info").hide();

  jQuery(".btn_perfil").click(function() {
    var id_perfil_ = jQuery("#id_perfil_bp").find(":selected").val();
    var id_subperfil_ = jQuery("#id_subperfil_bp").find(":selected").val();
    var texto_perfil = jQuery("#id_perfil_bp").find(":selected").text();
    //console.log(texto_perfil);
    //var total=0;
    //validamos los datos
    jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'miperfil'
    },
    function(data) { //Procesamiento de los datos de vuelta
      //total=data;
      //console.log("total"+data);
      if (data<2){
        jQuery.getJSON("/acceder/admin-ajax.php",{
          action: 'change_miperfil',
          bp_id_perfil: id_perfil_,
          bp_id_subperfil:id_subperfil_

        },
          function(data) { //Procesamiento de los datos de vuelta
            console.log(data)
            jQuery(".bp_info").show();
            jQuery('.mensaje_perfil').html(data); // insert data
            jQuery('.text_perfil').html(texto_perfil);

          });
      }else{
        jQuery(".bp_info").show();
        jQuery('.mensaje_perfil').html("No puedes realizar mas cambio de perfil"); // insert data
      }

    });
    //console.log(total);


  });

  jQuery("#id_perfil_bp").change(function(){

  var id = jQuery(this).find(":selected").val();
  jQuery.getJSON("/acceder/admin-ajax.php",{
      action: 'get_subperfil',
      perfil: id
    },
      function(data) { //Procesamiento de los datos de vuelta

        if(!jQuery.isEmptyObject(data)){
          jQuery('.swpm-mi-subperfil-row').show();
        }else{
          jQuery('.swpm-mi-subperfil-row').hide();
        }

        jQuery('#id_subperfil_bp').empty();
        var $select = jQuery('#id_subperfil_bp');

        //alert(options);
        $select.append('<option value="">Seleccionar</option>');
        jQuery.each(data, function(i,n) {
          //console.log(i);
          $select.append('<option value=' + i + '>' + n + '</option>');
        });



      });
});

jQuery("#id_subperfil").change(function(){
//jQuery(document).on("change","#id_subperfil",function(){ 
  var id = jQuery(this).find(":selected").val();
  //console.log("id >> : " + id)
  if(id == 3){
    jQuery("#id_nivel option[value='4']").remove();
    jQuery("#id_nivel option[value='5']").remove();
  }else{
    jQuery('#id_nivel').empty();
      jQuery("#id_nivel").append('<option value="">Seleccionar</option>');
      jQuery("#id_nivel").append('<option value="1">Inicial</option>');
      jQuery("#id_nivel").append('<option value="2">Primaria</option>');
      jQuery("#id_nivel").append('<option value="3">Secundaria</option>');
      jQuery("#id_nivel").append('<option value="4">Instituto</option>');
      jQuery("#id_nivel").append('<option value="5">Universidad</option>');
  }
});

jQuery("#id_perfil").change(function(){
  var id = jQuery(this).find(":selected").val();
  if(id == 5){
    jQuery('.swpm-registration-email-row').css('margin-top',20);
  }else{
    jQuery('.swpm-registration-email-row').css('margin-top', '');
  }
});

jQuery(".extranjeros").click(function(){
  jQuery('.swpm-registration-email-row').css('margin-top',90);
});

jQuery("#tipo_doc").change(function(){

  jQuery('.swpm-registration-firstname-row').css('margin-top', 90);
  jQuery('.swpm-registration-email-row').css('margin-top', '');
});

});
