<?php
$auth = SwpmAuth::get_instance();
$user_data = (array) $auth->userData;
$user_data['membership_level_alias'] = $auth->get('alias');
extract($user_data, EXTR_SKIP);
$settings=SwpmSettings::get_instance();
$force_strong_pass=$settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    //$pass_class="validate[custom[strongPass],minSize[8]]";
} else {
    $pass_class="";
}
/*
if (!$auth->is_logged_in()) {
  wp_redirect( site_url() );
  return;
}
*/
//SimpleWpMembership::enqueue_validation_scripts();
//The admin ajax causes an issue with the JS validation if done on form submission. The edit profile doesn't need JS validation on email. There is PHP validation which will catch any email error.
//SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData'=>'&action=swpm_validate_email&member_id='.SwpmAuth::get_instance()->get('member_id'))));
$perfil = array(
                "3"=> "Docente",
                "2"=> "Estudiante",
                "4"=>"Funcionario",
                "1"=>"Investigador",
                "5"=> "Otros"
              );

if ($id_perfil=='3'){
  $sub_perfil = array(
          "1"=>"Docente Educacion Basica",
          "2"=>"Formador de Docentes",
          "3"=>"Director"
          /*
          "4"=>"Especialista de UGEL",
          "5"=>"Especialista de DRE",
          "6"=>"Especialista de DRE"
          */
  );

  foreach ($sub_perfil as $key => $value) {
    if($id_subperfil == $key) {
      $dato_subperfil=$value;
    }
  }
}

if ($id_perfil=='4'){
  $sub_perfil = array(
          "7"=>"Minedu",
          "8"=>"DRE",
          "9"=>"UGEL"
  );
  foreach ($sub_perfil as $key => $value) {
    if($id_subperfil == $key) {
      $dato_subperfil=$value;
    }
  }
}

foreach ($perfil as $key => $value) {
  if($id_perfil == $key) {
    $dato_perfil=$value;
  }
}
//$indice = in_array("3", $perfil ); // true
$indice =array_search("3", $perfil);
?>
<div class="bp_content_edit">
  <div class="bp_content_edit_1">
    <div class="swpm-edit-profile-form">
        <form id="swpm-editprofile-form" name="swpm-editprofile-form" method="post" action="" class="swpm-validate-form">
          <?php wp_nonce_field('swpm_profile_edit_nonce_action', 'swpm_profile_edit_nonce_val') ?>
          <?php apply_filters('swpm_edit_profile_form_before_username', ''); ?>

          <div class="bp_edit_text"><p>Datos Personales</p></div>

          <div class="swpm-profile-username-row bp_frm_registration" >
            <label for="username"><?php echo SwpmUtils::_('Username') ?></label><br>
            <?php echo $user_name;?>
          </div>

          <div class="swpm-profile-username-row bp_frm_registration" >
            <label for="perfil"><?php echo SwpmUtils::_('Perfil de Usuario') ?></label><br>
            <?php echo $dato_perfil;?>
          </div>
          <?php if (!empty($id_subperfil)){?>
          <div class="swpm-profile-username-row bp_frm_registration" >
            <label for="SubPerfil"><?php echo SwpmUtils::_('Sub perfil de Usuario') ?></label><br>
            <?php echo $dato_subperfil;?>
          </div>
          <?php }?>

          <!--
          <div class="swpm-profile-perfil-row bp_frm_registration">
            <label for="perfil"><?php //echo SwpmUtils::_('Perfil de Usuario') ?></label>
            <select id="id_perfil" name="id_perfil" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil" disabled><?php //echo SwpmMiscUtils::get_perfil_dropdown($id_perfil) ?></select>
          </div>
          -->
          <!--
          <div class="swpm-profile-subperfil-row bp_frm_registration">
            <label for="subperfil"><?php //echo SwpmUtils::_('SubPerfil de Usuario') ?></label>
            <select id="id_subperfil" name="id_subperfil" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil" disabled><?php //echo SwpmMiscUtils::get_subperfil_dropdown($id_subperfil) ?></select>
          </div>
          -->

          <div class="swpm-profile-email-row bp_frm_registration">
            <label for="email"><?php echo SwpmUtils::_('Email') ?></label>
            <input type="email" autocomplete="off" id="email" class="" value="<?php echo esc_attr($email); ?>" size="50" name="email" placeholder="Correo electrónico" required/>
          </div>


          <div class="swpm-profile-password-row bp_frm_registration">
            <label for="password"><?php echo SwpmUtils::_('Password') ?></label>
            <input type="password" autocomplete="off" id="password" class="<?php //echo $pass_class; ?>" value="" size="50" name="password" placeholder="Contraseña" />
          </div>

          <div class="swpm-profile-password-retype-row bp_frm_registration">
            <label for="password_re"><?php echo SwpmUtils::_('Confirmación de contraseña') ?></label>
            <input type="password" autocomplete="off" id="password_re" value="" size="50" name="password_re" placeholder="Confirmación de contraseña" />
          </div>

          <div class="swpm-profile-phone-row bp_frm_registration">
            <label for="phone"><?php echo SwpmUtils::_('Número de celular') ?></label>
            <input type="text" id="phone" value="<?php echo $phone; ?>" size="50" name="phone" class="input" placeholder="Número de celular" required/>
          </div>

          <div class="bp_edit_text"><p>Información Centro Laboral / Centro de Estudios</p></div>

          <div id="perfil_1">

            <div class="swpm-profile-tipo_oficina-row bp_frm_registration">
              <label for="tipo_oficina"><?php echo SwpmUtils::_('Ministerio de Educación') ?></label>
              <select id="tipo_oficina" name="tipo_oficina" class="input" data-required_mark="required" data-field_type="select" data-original_id="tipo_oficina">
                <?php echo SwpmMiscUtils::get_tipo_oficina_dropdown($tipo_oficina) ?></select>
            </div>

            <div class="swpm-profile-id_centro-row bp_frm_registration">
              <label for="id_centro"><?php echo SwpmUtils::_('Gestión') ?></label>
              <select id="id_centro" name="id_centro" class="input" data-required_mark="required" data-field_type="select" data-original_id="Gestión"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select>
            </div>



          <div class="swpm-profile-id_nivel-row bp_frm_registration">
            <label for="id_nivel"><?php echo SwpmUtils::_('Nivel Educativo') ?></label>
            <select id="id_nivel_" name="id_nivel"  class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" >
            <?php
            echo SwpmMiscUtils::get_estudios_dropdown($id_nivel)
            ?>
          </select>
          </div>

          <div class="swpm-profile-id_dre-row bp_frm_registration">
            <label for="id_dre"><?php echo SwpmUtils::_('DRE') ?></label>
            <select id="id_dre" name="id_dre" class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" >
              <?php
              //echo SwpmMiscUtils::get_dre_dropdown($id_dre)
              echo get_dre_dropdown_bp($id_dre);
            ?></select>
          </div>

          <div class="swpm-profile-id_ugel-row bp_frm_registration">
              <label for="id_ugel"><?php echo SwpmUtils::_('UGEL') ?></label>
              <select id="id_ugel" name="id_ugel" class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" >
                <?php
              //  echo SwpmMiscUtils::get_ugel_dropdown($id_ugel)
                echo bp_listar_ugel_edit($id_dre,$id_ugel);
                ?></select>
          </div>


          <div id="grupo_2">
            <div class="swpm-profile-id_departamento-row bp_frm_registration">
                <label for="id_departamento"><?php echo SwpmUtils::_('Departamento') ?></label>
                <?php //echo SwpmMiscUtils::get_region_dropdown($id_region)
                //echo $id_departamento;
                ?>
                <select id="id_departamento" name="id_departamento" class="input" data-required_mark="required" data-field_type="select" data-original_id="departamento"  >
                  <?php //echo SwpmMiscUtils::get_region_dropdown($id_region)
                  echo get_region_dropdown_bp($id_departamento);
                  ?>
                </select>
            </div>

            <div class="swpm-profile-id_provincia-row bp_frm_registration">
                <label for="id_provincia"><?php echo SwpmUtils::_('Provincia') ?></label>
                <select id="id_provincia" name="id_provincia" class="input" data-required_mark="required" data-field_type="select" data-original_id="provincia"  >
                  <?php //echo bp_listar_provincia_edit($id_departamento.''.$id_provincia)
                        echo bp_listar_provincia_edit($id_departamento,$id_provincia);
                  ?>
                </select>
            </div>

            <div class="swpm-profile-id_Distrito-row bp_frm_registration">
                <label for="id_distrito"><?php echo SwpmUtils::_('Distrito') ?></label>
                <select id="id_distrito" name="id_distrito" class="input" data-required_mark="required" data-field_type="select" data-original_id="distrito"  >
                  <?php
                  //echo SwpmMiscUtils::get_distrito_dropdown($id_distrito)
                  echo bp_listar_distrito($id_departamento.''.$id_provincia,$id_distrito);
                  ?></select>
            </div>

          </div>


          <div class="swpm-profile-id_institucion-row bp_frm_registration">
              <label for="id_provincia"><?php echo SwpmUtils::_('Institución') ?></label>
              <?php //echo $id_institucion;?>
              <select id="id_institucion" name="id_institucion" class="input"  data-required_mark="required" data-field_type="select" >
                <?php
                echo SwpmMiscUtils::get_institucion_dropdown($id_institucion)
                //
                //echo bp_listar_instituciones_js($id_centro,$id_nivel);
                ?></select>
          </div>

          <div class="swpm-profile-id_institucion_educativa-row bp_frm_registration">
              <label for="id_institucion_educativa"><?php echo SwpmUtils::_('Institución Educativa') ?></label>
              <select id="id_institucion_educativa" name="id_institucion_educativa" class="input"  data-required_mark="required" data-field_type="select" >
                <?php
                //echo SwpmMiscUtils::get_institucion_educativa_dropdown($id_institucion_educativa)
                echo bp_listar_ie_edit($id_ugel,$id_institucion_educativa);
                ?>
              </select>
          </div>


          <div class="swpm-profile-id_carrera-row bp_frm_registration">
              <label for="id_carrera"><?php echo SwpmUtils::_('Carrera Profesional') ?></label>
              <select id="id_carrera" name="id_carrera" class="input"  data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_carrera_dropdown($id_carrera) ?></select>
          </div>

          <div class="swpm-profile-codigo_modular-row bp_frm_registration">
              <label for="codigo_modular"><?php echo SwpmUtils::_('Código modular IE') ?></label>
              <input type="text" id="codigo_modular" value="<?php echo $codigo_modular; ?>" size="50" name="codigo_modular" class="input" placeholder="Código modular IE" readonly/>
          </div>

          <div class="swpm-profile-centro_laboral-row bp_frm_registration">
            <label for="centro_laboral"><?php echo SwpmUtils::_('Centro Laboral') ?></label>
            <input type="text" id="centro_laboral" value="<?php echo $centro_laboral; ?>" size="50" name="centro_laboral" class="input" placeholder="Centro Laboral" />
          </div>


          <div class="swpm-profile-id_cargo-row bp_frm_registration">
            <label for="id_cargo"><?php echo SwpmUtils::_('Cargo') ?></label>
            <select id="id_cargo" name="id_cargo" class="input" data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_cargo_dropdown($id_cargo) ?></select>
          </div>



          </div>



          <?php apply_filters('swpm_edit_profile_form_before_submit', ''); ?>
          <p class="swpm-edit-profile-submit-section">
              <input type="submit" id="bp_enviar" value="<?php echo SwpmUtils::_('Update') ?>" class="swpm-edit-profile-submit" name="swpm_editprofile_submit" />
          </p>
          <?php echo SwpmUtils::delete_account_button(); ?>

          <input type="hidden" name="action" value="custom_posts" />


      </form>


    </div>
  </div>
</div>


<?php WP_CSP::ob_start(); ?>
<script>

var bp_id_perfil1='<?php echo $id_perfil;?>';
console.log(bp_id_perfil1);

if(bp_id_perfil1 == '2'){
  //jQuery("#id_nivel_").append('<option value=""> Seleccionar </option>');
  jQuery("#id_nivel_ option[value='1']").remove();
  jQuery("#id_nivel_ option[value='2']").remove();
  jQuery("#id_nivel_ option[value='3']").remove();
}

//listamos los institutos
jQuery("#id_nivel_").change(function(){
  var id_nivel = jQuery(this).find(":selected").val();
  var id_centro=jQuery("#id_centro").find(":selected").val()

    bpListarInstituciones_(id_nivel,id_centro);
});
//listamos los institutos
jQuery("#id_centro").change(function(){
  var id_centro = jQuery(this).find(":selected").val();
  var id_nivel=jQuery("#id_nivel_").find(":selected").val()

    bpListarInstituciones_(id_nivel,id_centro);
});
//jQuery('.swpm-profile-id_institucion-row').hide();

//FUNCION PARA VALIDAR DATOS DEL definitions
function bpListarInstituciones_(valor1,valor2){

  jQuery.getJSON("/acceder/admin-ajax.php",{
    action: 'get_instituciones',
    id_nivel: valor1,
    id_centro:valor2
  },
  function(data) { //Procesamiento de los datos de vuelta
    jQuery('#id_institucion').empty();
    jQuery("#id_institucion").append('<option value=""> Seleccionar </option>');
    jQuery.each(data,function(key, values) {
      jQuery("#id_institucion").append('<option value='+values.id+'>'+values.descripcion+'</option>');
    });

     //jQuery("#bp_mensaje").html(bp_msg)
     /**  jQuery("#bp_mensaje").html(bp_msg);*/

  });
}


switch (bp_id_perfil1) {
  case '1': //Investigador
    jQuery('.swpm-profile-id_centro-row').show();
    jQuery('#grupo_2').show();
    jQuery('.swpm-profile-centro_laboral-row').show();

    jQuery('.swpm-profile-id_dre-row').hide();
    jQuery('.swpm-profile-id_ugel-row').hide();
    jQuery('.swpm-profile-id_institucion_educativa-row').hide();
    jQuery('.swpm-profile-codigo_modular-row').hide();


    jQuery('.swpm-profile-id_nivel-row').hide();
    jQuery('.swpm-profile-id_institucion-row').hide();
    jQuery('.swpm-profile-id_carrera-row').hide();
    jQuery('.swpm-profile-tipo_oficina-row').hide();
    jQuery('.swpm-profile-id_cargo-row').hide();

    jQuery('#id_subperfil').removeAttr('required');
    jQuery('#id_centro').attr('required', '');
    jQuery('#id_departamento').attr('required', '');
    jQuery('#id_provincia').attr('required', '');
    jQuery('#id_distrito').attr('required', '');
    jQuery('#centro_laboral').attr('required', '');

    break;
  case '2':// Estudiante

    jQuery('#id_nivel').empty();
    jQuery("#id_nivel").append('<option value="">Nivel Educativo</option>');
    jQuery("#id_nivel").append('<option value="4">Instituto</option>');
    jQuery("#id_nivel").append('<option value="5">Universidad</option>');

    jQuery('.swpm-profile-id_centro-row').show();
    jQuery('#grupo_2').show();
    jQuery('.swpm-profile-id_nivel-row').show();
    jQuery('.swpm-profile-id_institucion-row').show();
    jQuery('.swpm-profile-id_carrera-row').show();

    jQuery('.swpm-profile-subperfil-row').hide();
    jQuery('.swpm-profile-id_institucion_educativa-row').hide();
    jQuery('.swpm-profile-codigo_modular-row').hide();
    jQuery('.swpm-profile-id_dre-row').hide();
    jQuery('.swpm-profile-id_ugel-row').hide();
    jQuery('.swpm-profile-centro_laboral-row').hide();
    jQuery('.swpm-profile-tipo_oficina-row').hide();
    jQuery('.swpm-profile-id_cargo-row').hide();


    jQuery('#id_centro').attr('required', '');
    jQuery('#id_departamento').attr('required', '');
    jQuery('#id_provincia').attr('required', '');
    jQuery('#id_distrito').attr('required', '');

    jQuery('#id_nivel').attr('required', '');
    jQuery('#id_institucion').attr('required', '');
    jQuery('#id_carrera').attr('required', '');

    jQuery('#id_subperfil').removeAttr('required');


    break;
  case '3': //Docente

    jQuery('.swpm-profile-id_centro-row').show();
    jQuery('.swpm-profile-id_nivel-row').show();

    jQuery('#id_centro').attr('required', '');
    jQuery('#id_nivel_').attr('required', '');
    jQuery('#id_institucion').attr('required', '');
    //var id_nivel = jQuery("#id_nivel_").val();
  //  console.log(id_nivel);
    //seleccionamos el nivel educativo
    jQuery("#id_nivel_").change(function(){
      //jQuery(id_nivel).empty();
      var id_nivel = jQuery(this).find(":selected").val();
      if(id_nivel=='1' || id_nivel=='2'  || id_nivel=='3'){

        jQuery('.swpm-profile-id_dre-row').show();
        jQuery('.swpm-profile-id_ugel-row').show();
        jQuery('.swpm-profile-id_institucion_educativa-row').show();
        jQuery('.swpm-profile-codigo_modular-row').show();

        jQuery('.swpm-profile-id_institucion-row').hide();
        jQuery('.swpm-profile-id_carrera-row').hide();
        jQuery('#grupo_2').hide();
        jQuery('#id_dre').attr('required', '');
        jQuery('#id_ugel').attr('required', '');
        jQuery('#id_institucion').removeAttr('required');

      }else{
        jQuery('#user_name').empty()
        jQuery('.swpm-profile-id_dre-row').hide();
        jQuery('.swpm-profile-id_ugel-row').hide();
        jQuery('.swpm-profile-id_institucion_educativa-row').hide();
        jQuery('.swpm-profile-codigo_modular-row').hide();

        jQuery('.swpm-profile-id_institucion-row').show();
        jQuery('.swpm-profile-id_carrera-row').show();
        jQuery('#grupo_2').show();

        jQuery('#id_institucion').attr('required', '');
        jQuery('#id_departamento').attr('required', '');
        jQuery('#id_provincia').attr('required', '');
        jQuery('#id_distrito').attr('required', '');
        jQuery('#id_carrera').attr('required', '');
      }
    });

    var id_nivel = jQuery("#id_nivel_").find(":selected").val();
    //alert(id_nivel);
    if(id_nivel=='1' || id_nivel=='2'  || id_nivel=='3'){

      jQuery('.swpm-profile-id_dre-row').show();
      jQuery('.swpm-profile-id_ugel-row').show();
      jQuery('.swpm-profile-id_institucion_educativa-row').show();
      jQuery('.swpm-profile-codigo_modular-row').show();

      jQuery('.swpm-profile-id_institucion-row').hide();
      jQuery('.swpm-profile-id_carrera-row').hide();
      jQuery('#grupo_2').hide();
      jQuery('#id_dre').attr('required', '');
      jQuery('#id_ugel').attr('required', '');
      jQuery('#id_institucion').removeAttr('required');
    }else{
      jQuery('.swpm-profile-id_dre-row').hide();
      jQuery('.swpm-profile-id_ugel-row').hide();
      jQuery('.swpm-profile-id_institucion_educativa-row').hide();
      jQuery('.swpm-profile-codigo_modular-row').hide();

      jQuery('.swpm-profile-id_institucion-row').show();
      jQuery('.swpm-profile-id_carrera-row').show();
      jQuery('#grupo_2').show();

      jQuery('#id_institucion').attr('required', '');
      jQuery('#id_departamento').attr('required', '');
      jQuery('#id_provincia').attr('required', '');
      jQuery('#id_distrito').attr('required', '');
      jQuery('#id_carrera').attr('required', '');
    }
    //jQuery('.swpm-registration-id_institucion-row').show();




    //jQuery('#grupo_1').show();
    //jQuery('.swpm-registration-id_nivel-row').show();
    //jQuery('.swpm-registration-id_institucion-row').show();
    //jQuery('.swpm-registration-id_carrera-row').show();

    jQuery('.swpm-profile-tipo_oficina-row').hide();
    /*
    jQuery('#grupo_2').hide();
    jQuery('.swpm-profile-id_institucion-row').hide();
    jQuery('.swpm-profile-id_carrera-row').hide();
    */
    jQuery('.swpm-profile-centro_laboral-row').hide();
    jQuery('.swpm-profile-id_cargo-row').hide();

    jQuery('.swpm-profile-id_carrera-row').hide();
    jQuery('.swpm-profile-centro_laboral-row').hide();
    jQuery('.swpm-profile-tipo_oficina-row').hide();
    jQuery('.swpm-profile-id_cargo-row').hide();

    break;
  case '4': //Funcionario

    jQuery('.swpm-profile-tipo_oficina-row').show();
    jQuery('.swpm-profile-id_dre-row').show();
    jQuery('.swpm-profile-id_ugel-row').show();
    //jQuery('.swpm-registration-id_institucion_educativa-row').show();
    jQuery('.swpm-profile-id_cargo-row').show();

    jQuery('#grupo_2').hide();

    jQuery('.swpm-profile-id_centro-row').hide();
    jQuery('.swpm-profile-id_nivel-row').hide();
    jQuery('.swpm-profile-id_carrera-row').hide();
    jQuery('.swpm-profile-centro_laboral-row').hide();
    jQuery('.swpm-profile-id_institucion-row').hide();
    jQuery('.swpm-profile-codigo_modular-row').hide();

    jQuery('#id_institucion_educativa').attr('required', '');
    jQuery('#codigo_modular').attr('required', '');
    jQuery('#id_dre').attr('required', '');
    jQuery('#id_ugel').attr('required', '');
    jQuery('#tipo_oficina').attr('required', '');
    jQuery('#id_cargo').attr('required', '');
    break;
  case '5': // Otros
  jQuery('.swpm-profile-id_centro-row').show();
  jQuery('#grupo_2').show();
  jQuery('.swpm-profile-centro_laboral-row').show();

  jQuery('.swpm-profile-id_dre-row').hide();
  jQuery('.swpm-profile-id_ugel-row').hide();
  jQuery('.swpm-profile-id_institucion_educativa-row').hide();
  jQuery('.swpm-profile-codigo_modular-row').hide();


  jQuery('.swpm-profile-id_nivel-row').hide();
  jQuery('.swpm-profile-id_institucion-row').hide();
  jQuery('.swpm-profile-id_carrera-row').hide();
  jQuery('.swpm-profile-tipo_oficina-row').hide();
  jQuery('.swpm-profile-id_cargo-row').hide();

  jQuery('#id_subperfil').removeAttr('required');
  jQuery('#id_centro').attr('required', '');
  jQuery('#id_departamento').attr('required', '');
  jQuery('#id_provincia').attr('required', '');
  jQuery('#id_distrito').attr('required', '');
  jQuery('#centro_laboral').attr('required', '');


  break;
default:

    jQuery('#grupo_2').hide();
    jQuery('.swpm-profile-id_nivel-row').hide();
    jQuery('.swpm-profile-id_dre-row').hide();
    jQuery('.swpm-profile-id_ugel-row').hide();
    jQuery('.swpm-profile-id_institucion-row').hide();
    jQuery('.swpm-profile-id_carrera-row').hide();
    jQuery('.swpm-profile-codigo_modular-row').hide();
    jQuery('.swpm-profile-centro_laboral-row').hide();
    jQuery('.swpm-profile-tipo_oficina-row').hide();
    jQuery('.swpm-profile-id_cargo-row').hide();

    jQuery('#id_nivel_').removeAttr('required');
    jQuery('#id_dre').removeAttr('required');
    jQuery('#id_ugel').removeAttr('required');
    jQuery('#id_institucion').removeAttr('required');
    jQuery('#id_carrera').removeAttr('required');
    jQuery('#codigo_modular').removeAttr('required');
    jQuery('#centro_laboral').removeAttr('required');
    jQuery('#tipo_oficina').removeAttr('required');
    jQuery('#id_cargo').removeAttr('required');
    jQuery('#tipo_doc').removeAttr('required');
    jQuery('#id_institucion_educativa').removeAttr('required');

}

</script>
<?php WP_CSP::ob_end_flush(); ?>



<?php WP_CSP::ob_start(); ?>
<script type='text/javascript' src='https://observatorio.minedu.gob.pe/wp-content/plugins/simple-membership/js/jquery.validate.js'></script>
<!--
<script type='text/javascript' src='<?php //echo site_url('/wp-content/plugins/simple-membership/js/jquery.validate.js');?>'></script>
-->
<?php WP_CSP::ob_end_flush(); ?>

<?php WP_CSP::ob_start(); ?>
<script >
jQuery(document).ready(function () {
        function validaNumericos(event) {
            if(event.charCode >= 48 && event.charCode <= 57){
              return true;
             }
             return false;
        }

        jQuery(".swpm-validate-form").validate();
        /*
        jQuery("#id_perfil").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Perfil es obligatorio</h4>"
            }
        });

        jQuery("#id_subperfil").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo SubPerfil es obligatorio</h4>"
            }
        });
        */
        /*
        jQuery("#user_name").rules("add", {
            required: true,
            minlength: 8,
            messages: {
                required: "<h4>El campo Usuario es obligatorio</h4>",
                minlength: jQuery.validator.format("<h4>El valor ingresado tiene que ser {0} digitos.</h4>")
            }
        });
        */
        /*
        jQuery("#ubigeo").rules("add", {
            required: true,
            minlength: 6,
            messages: {
                required: "<h4>El campo Ubigeo es obligatorio</h4>",
                minlength: jQuery.validator.format("<h4>El Ubigeo es un código de {0} dígitos que se encuentra en el DNI, de un clic en la imagen con el signo de interrogación ?</h4>")
            }
        });
        */

        jQuery("#email").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Email es obligatorio</h4>",
                email:"<h4>Debe ingresar un email válido</h4>"
            }
        });
        jQuery("#password").keydown(function(e)
        {
          jQuery.validator.addMethod("validaPass1", function(value, element) {
            //return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/i.test(value);
            return /^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*([^\w\s]|[_]))\S{8,}$/i.test(value);
          });

          jQuery.validator.addMethod("validaPassM", function(value, element) {
            return /[A-Z]/i.test(value);
          }, "Ingrese una letra Mayuscula.");

          jQuery("#password").rules("add", {
              validaPass1:true,
              required: true,
              minlength: 8,
              messages: {
                  required: "<h4>El campo Contraseña es obligatorio</h4>",
                  minlength: jQuery.validator.format("<h4>El largo mínimo permitido es {0} caracteres.</h4>"),
                  validaPass1:"La contraseña debe contener al menos una letra  minúscula, una letra mayúscula, un número y al menos un caracter especial."

              },

          });

        jQuery("#password_re").rules("add", {
            equalTo : '[name="password"]',
            required: true,
            minlength: 8,
            messages: {
                required: "<h4>El campo Contraseña es obligatorio</h4>",
                minlength: jQuery.validator.format("<h4>El largo mínimo permitido es {0} caracteres.</h4>"),
                equalTo:"<h4>Introduzca la misma contraseña nuevamente.</h4>",
            }
        });
      });


      jQuery("#phone").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Telefono es obligatorio</h4>"
            }
        });
        jQuery("#id_centro").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Gestión es obligatorio</h4>"
            }
        });
        jQuery("#id_nivel_").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Nivel Educativo es obligatorio</h4>"
            }
        });

        jQuery("#id_dre").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo DRE es obligatorio</h4>"
            }
        });

        jQuery("#id_ugel").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo ugel es obligatorio</h4>"
            }
        });

        jQuery("#id_departamento").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo departamento es obligatorio</h4>"
            }
        });
        jQuery("#id_provincia").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo provincia es obligatorio</h4>"
            }
        });
        jQuery("#id_distrito").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo distrito es obligatorio</h4>"
            }
        });

        jQuery("#id_institucion").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo institución es obligatorio</h4>"
            }
        });

        jQuery("#id_institucion_educativa").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo institución es obligatorio</h4>"
            }
        });

        jQuery("#tipo_oficina").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo captcha es obligatorio</h4>"
            }
        });

        jQuery("#id_cargo").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo captcha es obligatorio</h4>"
            }
        });
        /*
        jQuery("#captcha").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo captcha es obligatorio</h4>"
            }
        });
        */
        jQuery("#centro_laboral").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo centro laboral es obligatorio</h4>"
            }
        });

        /*
        jQuery("#accept_terms").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Terminos y condiciones es obligatorio</h4>"
            }
        });
        */



        jQuery('#bp_enviar').click(function () {
            jQuery(".swpm-validate-form").valid();
            jQuery(".swpm-validate-form").submit();
        });


      });


</script>
<?php WP_CSP::ob_end_flush(); ?>


<style>
.bp_content_edit{
      position: relative;
      /*width: 90%;*/
      width: 795px;
      max-width: 1080px;
      margin: auto;
      background-color: rgba(247,247,247,0.92);
      border-radius: 8px 8px 8px 8px;
      overflow: hidden;
      margin-left: 40px;
}
.bp_content_edit_1{
      background-color: rgba(247,247,247,0.92);
      width:100%;
      /*max-width: 750px;*/
      padding-right: 2vw!important;
      padding-left: 2vw!important;
      padding-top: 4.2415%;
      padding-bottom: 4.2415%;
}
.bp_content_edit .bp_frm_registration select{
    width: 100%;
    margin-bottom: 3%;
    padding: 0 0 0 3%;
    background-position: center;
    background-size: cover;
    border-radius: 5px 5px 5px 5px;
    overflow: hidden;
    border-color: #d0e8d9;
    font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #ffffff;
}
.bp_content_edit .bp_frm_registration input{
    width: 100%;
  margin-bottom: 3%;
  padding: 0 0 0 3%;
  background-position: center;
  background-size: cover;
  border-radius: 5px 5px 5px 5px;
    overflow: hidden;
    border-color: #d0e8d9;
    font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #ffffff;
}

label {
  /*display: none;*/
  font-size: 16px;
  color:#b7b2b2;
}
.bp_edit_text{
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
  font-size: 20px;
  margin-bottom: 14px!important;
  color: #7580c4!important;
}
.swpm-edit-profile-submit{
  color: #ffffff!important;
    border-width: 0px!important;
    border-color: #ffffff;
    border-radius: 25px;
    letter-spacing: 1px;
    font-size: 15px;
    font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;
    padding-left: 0.7em;
    padding-right: 2em;
    background-color: #ff9f0f;
    position: relative;
    padding: .3em 1em;
    border: 2px solid;
    font-weight: 500;
    line-height: 1.7em!important;
    -webkit-transition: all .2s;
    -moz-transition: all .2s;
    transition: all .2s;
    float: right;
    cursor: pointer;
}
.swpm-edit-profile-submit-section{
  padding-bottom: 40px!important;
}
.swpm-profile-username-row{
  padding-bottom: 10px!important;
}
.swpm-profile-ubigeo-row{
  width: 42%;
  float: left;
}

.swpm-profile-id_departamento-row{
  width: 30%;
  float: left;
    margin-right: 5%;
}

.swpm-profile-id_provincia-row{
  width: 30%;
  float: left;
    margin-right: 5%;
}

.swpm-profile-id_Distrito-row{
  width: 30%;
  float: left;
      margin-bottom: 15px;
}
.swpm-profile-update-success{
  position: absolute;
  top: 20px;
  left: 325px;
}
#password-error {
  font-family: 'Calibri Regular',Helvetica,Arial,Lucida,sans-serif!important;
    font-size: 16px!important;
    color: #ff0000!important;
}
.swpm-profile-update-success{
  background-color:#d1a9ca !important;
  color: #fff!important;
  width: 795px;
  font-family: 'Calibri',Helvetica,Arial,Lucida,sans-serif!important;
  font-size: 18px!important;
  font-weight:inherit!important;
  padding-left: 10px!important;
}
.bp_link_regresar{
  font-family: 'Calibri Bold',Helvetica,Arial,Lucida,sans-serif!important;
  font-size: 18px!important;
  color: #f9d800 !important;
}
</style>
<!--
<div class="swpm-edit-profile-form">
    <form id="swpm-editprofile-form" name="swpm-editprofile-form" method="post" action="" class="swpm-validate-form">
        <?php //wp_nonce_field('swpm_profile_edit_nonce_action', 'swpm_profile_edit_nonce_val') ?>
        <table>
            <?php //apply_filters('swpm_edit_profile_form_before_username', ''); ?>
            <tr class="swpm-profile-username-row">
                <td><label for="user_name"><?php //echo SwpmUtils::_('Username'); ?></label></td>
                <td><?php //echo $user_name ?></td>
            </tr>
            <tr class="swpm-profile-email-row">
                <td><label for="email"><?php //echo SwpmUtils::_('Email'); ?></label></td>
                <td><input type="text" id="email" name="email" size="50" autocomplete="off" class="" value="<?php //echo $email; ?>" /></td>
            </tr>
            <tr class="swpm-profile-password-row">
                <td><label for="password"><?php //echo SwpmUtils::_('Password'); ?></label></td>
                <td><input type="password" id="password" value="" size="50" name="password" class="<?php //echo $pass_class;?>" autocomplete="off" placeholder="<?php //echo SwpmUtils::_('Leave empty to keep the current password'); ?>" /></td>
            </tr>
            <tr class="swpm-profile-password-retype-row">
                <td><label for="password_re"><?php //echo SwpmUtils::_('Repeat Password'); ?></label></td>
                <td><input type="password" id="password_re" value="" size="50" name="password_re" autocomplete="off" placeholder="<?php //echo SwpmUtils::_('Leave empty to keep the current password'); ?>" /></td>
            </tr>
            <tr class="swpm-profile-firstname-row">
                <td><label for="first_name"><?php //echo SwpmUtils::_('First Name'); ?></label></td>
                <td><input type="text" id="first_name" value="<?php //echo $first_name; ?>" size="50" name="first_name" /></td>
            </tr>
            <tr class="swpm-profile-lastname-row">
                <td><label for="last_name"><?php //echo SwpmUtils::_('Last Name'); ?></label></td>
                <td><input type="text" id="last_name" value="<?php //echo $last_name; ?>" size="50" name="last_name" /></td>
            </tr>
            <tr class="swpm-profile-phone-row">
                <td><label for="phone"><?php //echo SwpmUtils::_('Phone'); ?></label></td>
                <td><input type="text" id="phone" value="<?php //echo $phone; ?>" size="50" name="phone" /></td>
            </tr>
            -->
            <!--
            <tr class="swpm-profile-street-row">
                <td><label for="address_street"><?php //echo SwpmUtils::_('Street'); ?></label></td>
                <td><input type="text" id="address_street" value="<?php //echo $address_street; ?>" size="50" name="address_street" /></td>
            </tr>
            <tr class="swpm-profile-city-row">
                <td><label for="address_city"><?php //echo SwpmUtils::_('City'); ?></label></td>
                <td><input type="text" id="address_city" value="<?php //echo $address_city; ?>" size="50" name="address_city" /></td>
            </tr>
            <tr class="swpm-profile-state-row">
                <td><label for="address_state"><?php //echo SwpmUtils::_('State'); ?></label></td>
                <td><input type="text" id="address_state" value="<?php //echo $address_state; ?>" size="50" name="address_state" /></td>
            </tr>
            <tr class="swpm-profile-zipcode-row">
                <td><label for="address_zipcode"><?php //echo SwpmUtils::_('Zipcode'); ?></label></td>
                <td><input type="text" id="address_zipcode" value="<?php //echo $address_zipcode; ?>" size="50" name="address_zipcode" /></td>
            </tr>
            <tr class="swpm-profile-country-row">
                <td><label for="country"><?php //echo SwpmUtils::_('Country'); ?></label></td>
                <td><select id="country" name="country"><?php //echo SwpmMiscUtils::get_countries_dropdown($country) ?></select></td>
            </tr>
            <tr class="swpm-profile-company-row">
                <td><label for="company_name"><?php //echo SwpmUtils::_('Company Name'); ?></label></td>
                <td><input type="text" id="company_name" value="<?php //echo $company_name; ?>" size="50" name="company_name" /></td>
            </tr>
          -->
          <!--
            <tr class="swpm-profile-membership-level-row">
                <td><label for="membership_level"><?php //echo SwpmUtils::_('Membership Level'); ?></label></td>
                <td>
                    <?php //echo $membership_level_alias; ?>
                </td>
            </tr>
        </table>
        <?php //apply_filters('swpm_edit_profile_form_before_submit', ''); ?>
        <p class="swpm-edit-profile-submit-section">
            <input type="submit" value="<?php //echo SwpmUtils::_('Update') ?>" class="swpm-edit-profile-submit" name="swpm_editprofile_submit" />
        </p>
        <?php //echo SwpmUtils::delete_account_button(); ?>

        <input type="hidden" name="action" value="custom_posts" />

    </form>
</div>
-->
