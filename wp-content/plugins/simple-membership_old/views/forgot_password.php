
<div class="bp_content bp_content_rc">
  <div class="bp_content_1">
    <div class="swpm-pw-reset-widget-form">
      <div class="bt_titulo">
      <h1 class="et_pb_contact_main_title">Reestablecer Contraseña <div class="bt_linea_reset_bp"></div></h1>
      </div>
        <form id="swpm-pw-reset-form" name="swpm-reset-form" method="post" action="">
            <div class="swpm-pw-reset-widget-inside">
                <div class="swpm-pw-reset-email swpm-margin-top-10">
                    <label for="swpm_reset_email" class="swpm_label swpm-pw-reset-email-label"><?php echo SwpmUtils::_('Email Address') ?></label>
                </div>
                <div class="swpm-pw-reset-email-input swpm-margin-top-10 bp_frm_login">
                    <input type="email" name="swpm_reset_email" class="swpm-text-field swpm-pw-reset-text" id="swpm_reset_email"  value="" size="60" />
                </div>

                <div class="swpm-before-login-submit-section swpm-margin-top-10 bp_frm_login"><?php echo apply_filters('swpm_before_pass_reset_form_submit_button', ''); ?></div>
                <div class="swpm-pw-reset-submit-button swpm-margin-top-10">
                    <input type="submit" name="swpm-reset" id="bp_enviar" class="swpm-pw-reset-submit bp_button" value="<?php echo SwpmUtils::_('ENVIAR'); ?>" />
                </div>
            </div>
        </form>
    </div>
  </div>
</div>
<?php //WP_CSP::ob_start(); ?>
<script type='text/javascript' src='<?php echo site_url('/wp-content/plugins/simple-membership/js/jquery.validate.js');?>'></script>
<?php //WP_CSP::ob_end_flush(); ?>
<?php //WP_CSP::ob_start(); ?>
<script type='text/javascript' >
jQuery(document).ready(function () {
    jQuery("#swpm-pw-reset-form").validate();
    console.log("Reestablecer contraseña");
    jQuery("#swpm_reset_email").rules("add", {
        required: true,
        messages: {
            required: "<h4>El campo Email es obligatorio</h4>",
            email:"<h4>Debe ingresar un email válido</h4>"
        }
    });
    jQuery('.bp_button').click(function () {
        jQuery("#swpm-pw-reset-form").valid();
        jQuery("#swpm-pw-reset-form").submit();
    });
});
</script>
<?php //WP_CSP::ob_end_flush(); ?>
<style>
/* Estilos del modulo iniciar session */
#main-content {
  background-blend-mode: multiply;
  background-image: url(<?php echo site_url('/wp-content/uploads/2019/10/fondo-miperfil-duotono.jpg');?>),linear-gradient(0deg,#a6a6c1 0%,#ffffff 100%)!important;
  padding-top: 5vw;
  padding-bottom: 8vw;
  background-color: inherit!important;
}
.error h4 {
    font-family: 'Calibri Regular',Helvetica,Arial,Lucida,sans-serif!important;
    font-size: 16px;
    color: #ff0000;
    margin-top: 15px;
}
#swpm_message{
  margin-top: -50px!important;
  padding-bottom: 10px;
}
.swpm-reset-pw-error{
  font-family: 'Gotham Rounded medium',Helvetica,Arial,Lucida,sans-serif!important;
  font-size: 18px!important;
  color:#ffffff!important;
  font-weight: inherit!important;
}
.swpm-reset-pw-error-email{
  font-family: 'Gotham Rounded medium',Helvetica,Arial,Lucida,sans-serif!important;
  font-size: 18px!important;
  color:#ffffff!important;
}
.bp_content_1 .bp_frm_login input {
    padding: 14px!important;
}

</style>
