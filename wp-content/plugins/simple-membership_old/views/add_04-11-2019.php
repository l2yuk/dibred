
<?php
SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData' => '&action=swpm_validate_email&member_id=' . filter_input(INPUT_GET, 'member_id'))));
$settings = SwpmSettings::get_instance();
$force_strong_pass = $settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    $pass_class = "validate[required,custom[strongPass],minSize[8]]";
} else {
    $pass_class = "";
}
?>



              <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
                <div class="et_pb_text_inner">
                  <h1>Formulario de Registro</h1>
                </div>
              </div> <!-- .et_pb_text -->

              <div class="et_pb_module et_pb_divider et_pb_divider_0 et_pb_divider_position_ et_pb_space">
                <div class="et_pb_divider_internal"></div>
              </div>


          <div class="swpm-registration-widget-form">
            <form id="swpm-registration-form" class="swpm-validate-form" name="swpm-registration-form" method="post" action="">
              <input type ="hidden" name="level_identifier" value="<?php echo $level_identifier ?>" />

          <div class="et_pb_row et_pb_row_1_bp">
            <div class="et_pb_column et_pb_column_4_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">
              <div id="" class="et_pb_with_border et_pb_module et_pb_contact_form_0 et_pb_contact_form_container clearfix" data-form_unique_num="0">
                <h1 class="et_pb_contact_main_title">Datos Personales</h1>
                <div class="et-pb-contact-message"></div>
                <div class="et_pb_contact">
                    <p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_last" data-id="perfil" data-type="select">

                      <label for="et_pb_contact_perfil_0" class="et_pb_contact_form_label"><?php echo SwpmUtils::_('Perfil de Usuario'); ?> </label>
                      <select id="id_perfil" name="id_perfil" class="et_pb_contact_select input" name="et_pb_contact_perfil_0" data-required_mark="required" data-field_type="select" data-original_id="perfil"><?php echo SwpmMiscUtils::get_perfil_dropdown($id_perfil) ?></select>

                    </p>

                    <p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_last" data-id="perfil" data-type="select">

                      <label for="et_pb_contact_perfil_0" class="et_pb_contact_form_label"><?php echo SwpmUtils::_('Perfil de Usuario'); ?> </label>
                      <select id="id_subperfil" name="id_subperfil" class="et_pb_contact_select input" name="et_pb_contact_perfil_0" data-required_mark="required" data-field_type="select" data-original_id="perfil"><?php echo SwpmMiscUtils::get_subperfil_dropdown($id_subperfil) ?></select>

                    </p>

                    <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="dni" data-type="input">

                      <label for="et_pb_contact_dni_0" class="et_pb_contact_form_label">Número de documento</label>
                      <input type="text" id="user_name" class="validate[required,custom[noapostrophe],custom[SWPMUserName],minSize[4],ajax[ajaxUserCall]] input" value="<?php echo esc_attr($user_name); ?>"  name="user_name" maxlength="8"  data-required_mark="required" data-field_type="input" data-original_id="Número de documento" placeholder="Número de documento"/>

                    </p>

                    <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="ubigeo" data-type="input">
                      <label for="et_pb_contact_ubigeo_0" class="et_pb_contact_form_label">Ubigeo</label>
                      <input type="text" id="ubigeo" value="<?php echo esc_attr($ubigeo); ?>"  name="ubigeo" maxlength="6"  class="input" data-required_mark="required" data-field_type="input" data-original_id="ubigeo" placeholder="Ubigeo"/>

                    </p>

                    <div id="bp_mensaje" style="/*! margin: 20px 0px 10px 10px; */margin-left: 25px;margin-bottom: 20px;color: red;font-weight: bold;"></div>

                    <p class="et_pb_contact_field et_pb_contact_field_3 et_pb_contact_field_last" data-id="nombres" data-type="input">
                      <label for="et_pb_contact_nombres_0" class="et_pb_contact_form_label">Nombres</label>
                      <input type="text" id="first_name" value="<?php echo esc_attr($first_name); ?>" size="50" name="first_name" class="input" data-required_mark="required" data-field_type="input" data-original_id="nombres" placeholder="Nombres" readonly/>

                    </p>

                    <p class="et_pb_contact_field et_pb_contact_field_3 et_pb_contact_field_last" data-id="nombres" data-type="input">
                      <label for="et_pb_contact_nombres_0" class="et_pb_contact_form_label">Apellidos</label>
                      <input type="text" id="last_name" value="<?php echo esc_attr($last_name); ?>" size="50" name="last_name" class="input" data-required_mark="required" data-field_type="input" data-original_id="nombres" placeholder="Apellidos" readonly/>

                    </p>

                    <p class="et_pb_contact_field et_pb_contact_field_4 et_pb_contact_field_last" data-id="correo" data-type="input">


                      <label for="et_pb_contact_correo_0" class="et_pb_contact_form_label">Correo  electrónico:</label>
                      <input type="text" autocomplete="off" id="email" class="validate[required,custom[email],ajax[ajaxEmailCall]] input" value="<?php echo esc_attr($email); ?>" size="50" name="email" data-required_mark="required" data-field_type="input" data-original_id="correo" placeholder="Correo  electrónico" />

                    </p>
                    <!--
                    <p class="et_pb_contact_field et_pb_contact_field_5 et_pb_contact_field_last" data-id="confirmar-correo" data-type="input">
                      <label for="et_pb_contact_confirmar-correo_0" class="et_pb_contact_form_label">Confirmar correo  electrónico:</label>
                      <input type="text" id="et_pb_contact_confirmar-correo_0" class="input" value="" name="et_pb_contact_confirmar-correo_0" data-required_mark="required" data-field_type="input" data-original_id="confirmar-correo" placeholder="Confirmar correo  electrónico:">
                    </p>
                    -->
                    <p class="et_pb_contact_field et_pb_contact_field_6 et_pb_contact_field_last" data-id="contraseña" data-type="input">


                      <label for="et_pb_contact_contraseña_0" class="et_pb_contact_form_label">Contraseña</label>
                      <input type="password" autocomplete="off" id="password" class="<?php echo $pass_class; ?> input" value="" size="50" name="password" data-required_mark="required" data-field_type="input" data-original_id="contraseña" placeholder="Contraseña" />

                    </p>

                    <p class="et_pb_contact_field et_pb_contact_field_7 et_pb_contact_field_last" data-id="confirmación_de_contraseña" data-type="input">

                      <label for="et_pb_contact_confirmación_de_contraseña_0" class="et_pb_contact_form_label">Confirmación de contraseña</label>
                      <input type="password" autocomplete="off" id="password_re" value="" size="50" name="password_re" class="input" data-required_mark="required" data-field_type="input" data-original_id="confirmación_de_contraseña" placeholder="Confirmación de contraseña" />

                    </p>

                    <p class="et_pb_contact_field et_pb_contact_field_8 et_pb_contact_field_last" data-id="número_de_celular" data-type="input">

                      <label for="et_pb_contact_número_de_celular_0" class="et_pb_contact_form_label">Número de celular</label>
                      <input type="text" id="phone" value="<?php echo $phone; ?>" size="50" name="phone" class="input" data-required_mark="required" data-field_type="input" data-original_id="número_de_celular" placeholder="Número de celular"/>

                    </p>



                </div> <!-- .et_pb_contact -->
              </div> <!-- .et_pb_contact_form_container -->
              <div class="et_pb_module et_pb_divider et_pb_divider_1 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
              <div id="" class="et_pb_with_border et_pb_module et_pb_contact_form_1 et_pb_contact_form_container clearfix" data-form_unique_num="1">


                <h1 class="et_pb_contact_main_title">Información donde Labora</h1>


                <div class="et_pb_contact">
                  <p class="et_pb_contact_field et_pb_contact_field_13 et_pb_contact_field_last" data-id="nivel" data-type="select">
                    <label for="et_pb_contact_nivel_1" class="et_pb_contact_form_label">Gestión</label>
                    <select id="id_centro" name="id_centro" class="et_pb_contact_select input" data-required_mark="required"
                    data-field_type="select" data-original_id="Gestión"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select>

                  </p>

                  <p class="et_pb_contact_field et_pb_contact_field_13 et_pb_contact_field_last" data-id="nivel" data-type="select">
                    <label for="et_pb_contact_nivel_1" class="et_pb_contact_form_label">Nivel Educativo</label>
                    <select id="id_nivel" name="id_nivel"  class="et_pb_contact_select input" data-required_mark="required" data-field_type="select" data-original_id="nivel"><?php echo SwpmMiscUtils::get_estudios_dropdown($id_centro) ?></select>

                  </p>

                  <p class="et_pb_contact_field et_pb_contact_field_13 et_pb_contact_field_last" data-id="nivel" data-type="select">
                    <label for="et_pb_contact_nivel_1" class="et_pb_contact_form_label">DRE</label>
                    <select id="id_dre" name="id_dre" class="et_pb_contact_select input" data-required_mark="required" data-field_type="select" data-original_id="nivel"><?php echo SwpmMiscUtils::get_dre_dropdown($id_dre) ?></select>

                  </p>

                  <p class="et_pb_contact_field et_pb_contact_field_13 et_pb_contact_field_last" data-id="nivel" data-type="select">
                    <label for="et_pb_contact_nivel_1" class="et_pb_contact_form_label">UGEL</label>
                    <select id="id_ugel" name="id_ugel" class="et_pb_contact_select input" data-required_mark="required" data-field_type="select" data-original_id="nivel"><?php echo SwpmMiscUtils::get_centro_dropdown($id_ugel) ?></select>

                  </p>


                    <p class="et_pb_contact_field et_pb_contact_field_9 et_pb_contact_field_last" data-id="departamento" data-type="select">
                      <label for="et_pb_contact_departamento_1" class="et_pb_contact_form_label">Departamento</label>
                      <select id="id_departamento" name="id_departamento" class="et_pb_contact_select input" data-required_mark="required" data-field_type="select" data-original_id="departamento" style="width: 195px;"><?php echo SwpmMiscUtils::get_region_dropdown($id_region) ?></select>

                    </p>
                    <p class="et_pb_contact_field et_pb_contact_field_10 et_pb_contact_field_last" data-id="provincia" data-type="select">
                      <label for="et_pb_contact_provincia_1" class="et_pb_contact_form_label">Provincia</label>
                      <select id="id_provincia" name="id_provincia" class="et_pb_contact_select input" data-required_mark="required" data-field_type="select" data-original_id="provincia" style="width: 195px;"><?php echo SwpmMiscUtils::get_provincia_dropdown($id_provincia) ?></select>

                    </p>


                    <p class="et_pb_contact_field et_pb_contact_field_11 et_pb_contact_field_last" data-id="distrito" data-type="select">
                      <label for="et_pb_contact_distrito_1" class="et_pb_contact_form_label">Distrito</label>
                      <select id="id_distrito" name="id_distrito" class="et_pb_contact_select input" data-required_mark="required" data-field_type="select" data-original_id="distrito" style="width: 195px;"><?php echo SwpmMiscUtils::get_distrito_dropdown($id_distrito) ?></select>
                    </p>

                    <!--
                    <p class="et_pb_contact_field et_pb_contact_field_12 et_pb_contact_field_last" data-id="gestión" data-type="radio">

                      <label for="et_pb_contact_gestión_1" class="et_pb_contact_form_label">Gestión</label>
                      <span class="et_pb_contact_field_options_wrapper">
                        <span class="et_pb_contact_field_options_title">Gestión</span>
                        <span class="et_pb_contact_field_options_list"><span class="et_pb_contact_field_radio">
                          <input type="radio" id="et_pb_contact_gestión_1_12_0" class="input" value="Pública" name="et_pb_contact_gestión_1" data-required_mark="required" data-field_type="radio" data-original_id="gestión" data-id="-1">
                          <label for="et_pb_contact_gestión_1_12_0"><i></i>Pública</label>
                        </span><span class="et_pb_contact_field_radio">
                          <input type="radio" id="et_pb_contact_gestión_1_12_1" class="input" value="Privada" name="et_pb_contact_gestión_1" data-required_mark="required" data-field_type="radio" data-original_id="gestión" data-id="0">
                          <label for="et_pb_contact_gestión_1_12_1"><i></i>Privada</label>
                        </span>
                      </span>
                    </span>
                  </p>
                  -->


                  <p class="et_pb_contact_field et_pb_contact_field_14 et_pb_contact_field_last" data-id="centro-estudios" data-type="select">

                    <label for="et_pb_contact_centro-estudios_1" class="et_pb_contact_form_label">Institución Educativa</label>
                    <select id="id_institucion" name="id_institucion" class="et_pb_contact_select input" name="et_pb_contact_centro-estudios_1" data-required_mark="required" data-field_type="select" data-original_id="centro-estudios"><?php echo SwpmMiscUtils::get_institucion_dropdown($id_institucion) ?></select>

                  </p>


                          <?php
                          //echo $membership_level_alias; //Show the level name in the form.
                          //Add the input fields for the level data.
                          echo '<input type="hidden" value="' . $membership_level . '" size="50" name="membership_level" id="membership_level" />';
                          //Add the level input verification data.
                          $swpm_p_key = get_option('swpm_private_key_one');
                          if (empty($swpm_p_key)) {
                              $swpm_p_key = uniqid('', true);
                              update_option('swpm_private_key_one', $swpm_p_key);
                          }
                          $swpm_level_hash = md5($swpm_p_key . '|' . $membership_level); //level hash
                          echo '<input type="hidden" name="swpm_level_hash" value="' . $swpm_level_hash . '" />';
                          ?>

                  <?php
                  //check if we need to display Terms and Conditions checkbox
                  $terms_enabled = $settings->get_value('enable-terms-and-conditions');
                  if (!empty($terms_enabled)) {
                      $terms_page_url = $settings->get_value('terms-and-conditions-page-url');
                      ?>

                              <label style="margin: 20px;"><input type="checkbox" id="accept_terms" name="accept_terms" class="validate[required]" value="1"> <?php echo SwpmUtils::_('I accept the ') ?> <a href="<?php echo $terms_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Terms and Conditions') ?></a></label>

                      <?php
                  }
                  //check if we need to display Privacy Policy checkbox
                  $pp_enabled = $settings->get_value('enable-privacy-policy');
                  if (!empty($pp_enabled)) {
                      $pp_page_url = $settings->get_value('privacy-policy-page-url');
                      ?>

                              <label><input type="checkbox" id="accept_pt" name="accept_pp" class="validate[required]" value="1"> <?php echo SwpmUtils::_('I agree to the ') ?> <a href="<?php echo $pp_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Privacy Policy') ?></a></label>

                      <?php
                  }
                  ?>
                  <!--
                  <p class="et_pb_contact_field et_pb_contact_field_16 et_pb_contact_field_last" data-id="terminos" data-type="checkbox">

                    <label for="et_pb_contact_terminos_1" class="et_pb_contact_form_label"> </label>
                    <input class="et_pb_checkbox_handle" type="hidden" name="et_pb_contact_terminos_1" data-required_mark="required" data-field_type="checkbox" data-original_id="terminos">
                    <span class="et_pb_contact_field_options_wrapper">
                      <span class="et_pb_contact_field_options_title"> </span>
                      <span class="et_pb_contact_field_options_list"><span class="et_pb_contact_field_checkbox">
                        <input type="checkbox" id="et_pb_contact_terminos_16_0" class="input" value="Acepto los términos y condiciones de uso" data-id="-1">
                        <label for="et_pb_contact_terminos_16_0"><i></i>
                          <label><input type="checkbox" id="accept_terms" name="accept_terms" class="validate[required] input" value="1"> <?php //echo SwpmUtils::_('I accept the ') ?> <a href="<?php echo $terms_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Terms and Conditions') ?></a></label>
                        </label>
                      </span>
                    </span>
                  </span>
                </p>
              -->
                <!--
                <p class="et_pb_contact_field et_pb_contact_field_17 et_pb_contact_field_half" data-id="et_number" data-type="input">
                  <label for="et_pb_contact_et_number_1" class="et_pb_contact_form_label">Number</label>
                  <input type="text" id="et_pb_contact_et_number_1" class="input" value="" name="et_pb_contact_et_number_1" data-required_mark="required" data-field_type="input" data-original_id="et_number" placeholder="Number" tabindex="-1">
                </p>
-->

            </div> <!-- .et_pb_contact -->
          </div> <!-- .et_pb_contact_form_container -->
          <div class="et_pb_module dsm_contact_form_7 dsm_contact_form_7_0">


          </div>
          <div class="et_pb_module dsm_caldera_forms dsm_caldera_forms_0  dsm_cf_custom_radio dsm_cf_custom_checkbox">

          </div>

          <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_right et_pb_button_alignment_phone_center et_pb_module ">
            <div class="swpm-before-registration-submit-section" align="center"><?php echo apply_filters('swpm_before_registration_submit_button', ''); ?></div>

            <div class="swpm-registration-submit-section" align="center">
                <input type="submit" value="<?php echo SwpmUtils::_('Register') ?>" class="swpm-registration-submit" name="swpm_registration_submit" />
            </div>

            <input type="hidden" name="action" value="custom_posts" />
          </div>
        <!--
          <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_right et_pb_button_alignment_phone_center et_pb_module ">
            <a class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" href="">REGÍSTRATE</a>
          </div>
        -->

        </div> <!-- .et_pb_column -->

      </div> <!-- .et_pb_row -->
    </form>
    </div>



<style id="et-core-unified-cached-inline-styles-2">


@font-face{font-family:"Gotham Rounded Medium Italic";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Medium-Italic.otf") format("opentype")}@font-face{font-family:"Calibri";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/calibri.ttf") format("truetype")}@font-face{font-family:"Gotham Rounded Medium";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Medium.otf") format("opentype")}@font-face{font-family:"Gotham Rounded Light";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Light.otf") format("opentype")}

div.et_pb_section.et_pb_section_0{
  /*background-blend-mode:multiply;*/
  background-image:url(http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/observatorio-buenas-practicas-fondo-login-variante.jpg)!important
}

body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:1.6em;color:#ffffff}
.et_pb_contact_form_1 .input::-ms-input-placeholder{color:#797979}.et_pb_contact_form_1 .input::-moz-placeholder{color:#797979}.et_pb_contact_form_1 .input::-webkit-input-placeholder{color:#797979}.et_pb_contact_form_1 .input,.et_pb_contact_form_1 .input[type="checkbox"]+label,.et_pb_contact_form_1 .input[type="radio"]+label,.et_pb_contact_form_1 .input[type="checkbox"]:checked+label i:before,.et_pb_contact_form_1 .input::placeholder{color:#797979}.et_pb_contact_form_1 .input:focus,.et_pb_contact_form_1 .input[type="checkbox"]:active+label i,.et_pb_contact_form_1 .input[type="radio"]:active+label i{background-color:#ffffff}.et_pb_contact_form_1 .input,.et_pb_contact_form_1 .input[type="checkbox"]+label i,.et_pb_contact_form_1 .input[type="radio"]+label i{background-color:#ffffff}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{background-image:initial!important;background-color:#ffdb0f!important}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{color:}body.et_button_custom_icon #page-container .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:16px}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button{color:#7a5500!important;border-width:0px!important;border-color:rgba(0,0,0,0);border-radius:25px;letter-spacing:0px;font-size:16px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;background-color:#ffc30f}.et_pb_contact_form_1 p .input:focus::-webkit-input-placeholder{color:#797979}.et_pb_contact_form_1.et_pb_contact_form_container .input,.et_pb_contact_form_1.et_pb_contact_form_container .input[type="checkbox"]+label i,.et_pb_contact_form_1.et_pb_contact_form_container .input[type="radio"]+label i{border-radius:5px 5px 5px 5px;overflow:hidden;border-color:rgba(0,0,0,0)}.et_pb_contact_form_1.et_pb_contact_form_container{background-color:rgba(0,0,0,0);padding-top:30px;padding-right:3vw;padding-bottom:33px;padding-left:3vw;margin-top:0px!important;margin-right:0px!important;margin-bottom:0px!important;margin-left:0px!important}.et_pb_contact_form_1.et_pb_contact_form_container .input:-ms-input-placeholder{font-size:18px}.et_pb_contact_form_1.et_pb_contact_form_container .input::-moz-placeholder{font-size:18px}.et_pb_contact_form_1.et_pb_contact_form_container .input::-webkit-input-placeholder{font-size:18px}.et_pb_contact_form_1.et_pb_contact_form_container .input,.et_pb_contact_form_1.et_pb_contact_form_container .input::placeholder,.et_pb_contact_form_1.et_pb_contact_form_container .input[type=checkbox]+label,.et_pb_contact_form_1.et_pb_contact_form_container .input[type=radio]+label{font-size:18px}.et_pb_contact_form_1.et_pb_contact_form_container .et_pb_contact_captcha_question{font-family:'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;font-size:33px;color:#00877e!important}.et_pb_contact_form_1.et_pb_contact_form_container h1,.et_pb_contact_form_1.et_pb_contact_form_container h2.et_pb_contact_main_title,.et_pb_contact_form_1.et_pb_contact_form_container h3.et_pb_contact_main_title,.et_pb_contact_form_1.et_pb_contact_form_container h4.et_pb_contact_main_title,.et_pb_contact_form_1.et_pb_contact_form_container h5.et_pb_contact_main_title,.et_pb_contact_form_1.et_pb_contact_form_container h6.et_pb_contact_main_title{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:20px;color:#7580c4!important}.et_pb_section_0.et_pb_section{padding-top:5vw;padding-bottom:8vw;background-color:rgba(0,0,0,0)!important}.et_pb_contact_form_1 .input:focus,.et_pb_contact_form_1 .input[type="checkbox"]:active+label,.et_pb_contact_form_1 .input[type="radio"]:active+label,.et_pb_contact_form_1 .input[type="checkbox"]:checked:active+label i:before{color:#797979}.et_pb_contact_form_1 p .input:focus::-moz-placeholder{color:#797979}.et_pb_contact_field_9{Max-width:33.2%;Float:left;Clear:none!important}body #page-container .et_pb_section .et_pb_button_0:after{font-size:1.6em;color:#ffffff}.et_pb_text_1.et_pb_text{color:#9789dd!important}.et_pb_row_2.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_section_1.et_pb_section{padding-top:24px;padding-right:0px;padding-bottom:24px;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}div.et_pb_section.et_pb_section_1{background-image:linear-gradient(2deg,#1d0038 0%,#462a66 100%)!important}.et_pb_button_0,.et_pb_button_0:after{transition:all 300ms ease 0ms}body #page-container .et_pb_section .et_pb_button_0:hover{background-image:initial!important;background-color:#ffdb0f!important}body #page-container .et_pb_section .et_pb_button_0:hover:after{color:}body.et_button_custom_icon #page-container .et_pb_button_0:after{font-size:16px}body #page-container .et_pb_section .et_pb_button_0{color:#7a5500!important;border-width:0px!important;border-color:#ffffff;border-radius:25px;letter-spacing:1px;font-size:16px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;background-color:#ffc30f}.et_pb_contact_form_1 p .input:focus:-ms-input-placeholder{color:#797979}.et_pb_button_0_wrapper{margin-top:0px!important;margin-right:3vw!important;margin-bottom:10px!important}.dsm_caldera_forms_0 .dsm-cf-html p{line-height:1.7em;line-height:1.7em}.dsm_contact_form_7_0 .wpcf7-form p:nth-last-of-type(1){text-align:}.et_pb_contact_form_1 .input[type="radio"]:checked:active+label i:before{background-color:#797979}.et_pb_contact_form_1 .input[type="radio"]:checked+label i:before{background-color:#797979}.et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button{transition:background-color 300ms ease 0ms}.et_pb_contact_form_1 p textarea:focus:-ms-input-placeholder{color:#797979}.et_pb_contact_form_1 p textarea:focus::-moz-placeholder{color:#797979}.et_pb_contact_form_1 p textarea:focus::-webkit-input-placeholder{color:#797979}.et_pb_contact_field_10{Max-width:33.2%;Float:left;Clear:none!important}.et_pb_contact_field_11{Max-width:33.2%;Float:left;Clear:none!important}.et_pb_divider_1:hover:before{border-top-width:px}.et_pb_contact_form_0.et_pb_contact_form_container h1,.et_pb_contact_form_0.et_pb_contact_form_container h2.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h3.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h4.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h5.et_pb_contact_main_title,.et_pb_contact_form_0.et_pb_contact_form_container h6.et_pb_contact_main_title{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:20px;color:#7580c4!important}.et_pb_divider_1:before{border-top-color:rgba(191,66,244,0.26);border-top-style:dotted;border-top-width:2px;width:auto;top:0px;right:3vw;left:3vw}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{color:#002d1e!important;border-width:0px!important;border-color:rgba(0,0,0,0);border-radius:25px;letter-spacing:0px;font-size:16px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;background-color:#f9af0e}.et_pb_contact_form_0.et_pb_contact_form_container .input,.et_pb_contact_form_0.et_pb_contact_form_container .input[type="checkbox"]+label i,.et_pb_contact_form_0.et_pb_contact_form_container .input[type="radio"]+label i{border-radius:5px 5px 5px 5px;overflow:hidden;border-color:#d0e8d9}.et_pb_contact_form_0.et_pb_contact_form_container{background-color:rgba(0,0,0,0);padding-top:35px;padding-right:3vw;padding-bottom:20px;padding-left:3vw;margin-top:0px!important;margin-right:0px!important;margin-bottom:0px!important;margin-left:0px!important}.et_pb_contact_form_0.et_pb_contact_form_container .input:-ms-input-placeholder{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input::-moz-placeholder{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input::-webkit-input-placeholder{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .input,.et_pb_contact_form_0.et_pb_contact_form_container .input::placeholder,.et_pb_contact_form_0.et_pb_contact_form_container .input[type=checkbox]+label,.et_pb_contact_form_0.et_pb_contact_form_container .input[type=radio]+label{font-size:18px}.et_pb_contact_form_0.et_pb_contact_form_container .et_pb_contact_captcha_question{font-family:'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;font-size:33px;color:#00877e!important}
.et_pb_row_1_bp,body #page-container .et-db #et-boc .et_pb_row_1_bp.et_pb_row,body.et_pb_pagebuilder_layout.single #page-container #et-boc .et_pb_row_1_bp.et_pb_row,body.et_pb_pagebuilder_layout.single.et_full_width_page #page-container #et-boc .et_pb_row_1_bp.et_pb_row{max-width:750px}
body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{color:}
.et_pb_row_1_bp{background-image:linear-gradient(180deg,rgba(255,255,255,0.86) 0%,rgba(221,221,221,0.91) 100%);background-color:rgba(0,0,0,0);border-radius:15px 15px 15px 15px;overflow:hidden}
.et_pb_divider_0:hover:before{border-top-width:px}
.et_pb_divider_0:before{border-top-color:#ff89ca;border-top-width:6px;width:auto;top:0px;right:0px;left:0px}
.et_pb_divider_0{background-color:rgba(0,0,0,0);padding-top:0px;margin-top:-28px!important;width:14%;max-width:100%}
.et_pb_text_0{padding-bottom:6px!important}.et_pb_text_0 h1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:40px;color:#ffffff!important}
.et_pb_text_0.et_pb_text{color:#ffffff!important}.et_pb_row_0,body #page-container .et-db #et-boc .et_pb_row_0.et_pb_row,body.et_pb_pagebuilder_layout.single #page-container #et-boc .et_pb_row_0.et_pb_row,body.et_pb_pagebuilder_layout.single.et_full_width_page #page-container #et-boc .et_pb_row_0.et_pb_row{max-width:750px}.et_pb_row_0.et_pb_row{padding-top:0px!important;padding-bottom:27px!important;margin-top:-5px!important;padding-top:0px;padding-bottom:27px}body.et_button_custom_icon #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:16px}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{font-size:1.6em;color:#ffffff}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{background-image:initial!important;background-color:#fcd00f!important}.et_pb_contact_form_0 p textarea:focus::-webkit-input-placeholder{color:#797979}.et_pb_divider_1{background-color:rgba(0,0,0,0);padding-right:3vw;padding-left:3vw;margin-top:27px!important;margin-bottom:0px!important}.et_pb_contact_form_0 .input[type="radio"]:checked:active+label i:before{background-color:#797979}.et_pb_contact_form_0 .input[type="radio"]:checked+label i:before{background-color:#797979}.et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{transition:background-color 300ms ease 0ms}.et_pb_contact_form_0 p textarea:focus:-ms-input-placeholder{color:#797979}.et_pb_contact_form_0 p textarea:focus::-moz-placeholder{color:#797979}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label i,.et_pb_contact_form_0 .input[type="radio"]+label i{background-color:#ffffff}.et_pb_text_1{font-family:'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;font-size:13px;letter-spacing:1px;padding-top:2px!important;padding-right:0px!important;padding-bottom:0px!important;padding-left:0px!important;margin-top:0px!important;margin-right:0px!important;margin-bottom:0px!important;margin-left:0px!important}.et_pb_contact_form_0 p .input:focus:-ms-input-placeholder{color:#797979}.et_pb_contact_form_0 .input::-moz-placeholder{color:#797979}.et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label i,.et_pb_contact_form_0 .input[type="radio"]:active+label i{background-color:#ffffff}.et_pb_contact_form_0 p .input:focus::-moz-placeholder{color:#797979}.et_pb_contact_form_0 .input::-webkit-input-placeholder{color:#797979}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label,.et_pb_contact_form_0 .input[type="radio"]+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked+label i:before,.et_pb_contact_form_0 .input::placeholder{color:#797979}.et_pb_contact_form_0 .input::-ms-input-placeholder{color:#797979}.et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label,.et_pb_contact_form_0 .input[type="radio"]:active+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked:active+label i:before{color:#797979}.et_pb_contact_form_0 p .input:focus::-webkit-input-placeholder{color:#797979}p.et_pb_contact_field_6{padding-bottom:0px}.et_pb_contact_form_container .et_pb_contact_field_16.et_pb_contact_field{margin-bottom:18px!important}p.et_pb_contact_field_16{padding-bottom:0px}.et_pb_contact_form_container .et_pb_contact_field_15.et_pb_contact_field{margin-bottom:18px!important}.et_pb_column_0{z-index:9;position:relative}p.et_pb_contact_field_15{padding-bottom:0px}.et_pb_column_2{z-index:9;position:relative}.et_pb_contact_form_container .et_pb_contact_field_6.et_pb_contact_field{margin-bottom:18px!important}.et_pb_column_1{z-index:9;position:relative}@media only screen and (max-width:980px){.et_pb_contact_form_0.et_pb_contact_form_container{padding-top:7vw;padding-right:6vw;padding-left:6vw}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}.et_pb_contact_form_1.et_pb_contact_form_container{padding-top:7vw;padding-right:6vw;padding-left:6vw}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}body #page-container .et_pb_section .et_pb_button_0{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_button_0:hover:after{opacity:1}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}}@media only screen and (max-width:767px){.et_pb_contact_form_0.et_pb_contact_form_container{padding-top:6vw;padding-right:4vw;padding-left:4vw}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}.et_pb_contact_form_1.et_pb_contact_form_container{padding-top:6vw;padding-right:4vw;padding-left:4vw}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_contact_form_1.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after{opacity:1}body #page-container .et_pb_section .et_pb_button_0{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:after{display:inline-block;opacity:0}body #page-container .et_pb_section .et_pb_button_0:hover:after{opacity:1}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}}


.container{width:100%;max-width:100%;top:0px}
  #main-content .container {
      padding-top: 0px;
  }
  .et_pb_contact_select {
    width: 100%;
  }
  .et_pb_contact p input, .et_pb_contact p textarea, .et_pb_subscribe .et_pb_contact_field input, .et_pb_subscribe .et_pb_contact_field textarea {
    width: 100%;
  }
  .swpm-registration-widget-form input[type="text"], .swpm-registration-widget-form input[type="password"] {
    width: 100%;
}
.et_pb_contact_form_0.et_pb_contact_form_container {

    padding-left: 2vw;

}
.swpm-registration-submit{
    color: #7a5500!important;
    border-width: 0px!important;
    border-color: #ffffff;
    border-radius: 25px;
    letter-spacing: 1px;
    font-size: 16px;
    font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;
    background-color: #ffc30f;
    padding: .8em 1em;
    cursor: pointer;
    align:left;
}

.swpm-registration-submit:hover {
    background-image: initial!important;
    background-color: #ffdb0f!important;
    padding: .3em 2em .3em .7em;
    border: 2px solid transparent;
padding: .8em 1em;
cursor: pointer;
}

.et_pb_row .et_pb_row_1_bp{
  width: 100%;
}
.et_pb_row_1_bp > p{
  background: none;
}
</style>
