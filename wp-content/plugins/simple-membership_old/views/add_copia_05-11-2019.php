<?php
SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData' => '&action=swpm_validate_email&member_id=' . filter_input(INPUT_GET, 'member_id'))));
$settings = SwpmSettings::get_instance();
$force_strong_pass = $settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    $pass_class = "validate[required,custom[strongPass],minSize[8]]";
} else {
    $pass_class = "";
}
$ubigeo="";
?>
<div class="bp_content">
  <div class="bp_content_1">
<div class="swpm-registration-widget-form">
    <form id="swpm-registration-form" class="swpm-validate-form" name="swpm-registration-form" method="post" action="">
        <input type ="hidden" name="level_identifier" value="<?php echo $level_identifier ?>" />

        <div class="swpm-registration-perfil-row bp_frm_registration">
          <label for="perfil"><?php echo SwpmUtils::_('Perfil de Usuario') ?></label>
          <select id="id_perfil" name="id_perfil" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil" required><?php echo SwpmMiscUtils::get_perfil_dropdown($id_perfil) ?></select>
        </div>

        <div class="swpm-registration-subperfil-row bp_frm_registration">
          <label for="subperfil"><?php echo SwpmUtils::_('SubPerfil de Usuario') ?></label>
          <select id="id_subperfil" name="id_subperfil" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil"><?php echo SwpmMiscUtils::get_subperfil_dropdown($id_subperfil) ?></select>
        </div>

        <div class="swpm-registration-username-row bp_frm_registration">
          <label for="user_name"><?php echo SwpmUtils::_('Número de documento') ?></label>
          <input type="text" id="user_name" class="validate[required,custom[noapostrophe],custom[SWPMUserName],minSize[4],ajax[ajaxUserCall]]" value="<?php echo esc_attr($user_name); ?>" size="50" name="user_name" placeholder="Número de documento" />
        </div>

        <div class="swpm-registration-ubigeo-row bp_frm_registration">
          <label for="ubigeo"><?php echo SwpmUtils::_('Ubigeo') ?></label>
          <input type="text" id="ubigeo" value="<?php echo esc_attr($ubigeo); ?>"  name="ubigeo" maxlength="6"  class="input" placeholder="Ubigeo" required/>
        </div>

        <div id="bp_mensaje" style="/*! margin: 20px 0px 10px 10px; */margin-left: 25px;margin-bottom: 20px;color: red;font-weight: bold;"></div>

        <div class="swpm-registration-email-row bp_frm_registration">
          <label for="email"><?php echo SwpmUtils::_('Email') ?></label>
          <input type="text" autocomplete="off" id="email" class="validate[required,custom[email],ajax[ajaxEmailCall]]" value="<?php echo esc_attr($email); ?>" size="50" name="email" placeholder="Correo electrónico" />
        </div>

        <div class="swpm-registration-password-row bp_frm_registration">
          <label for="password"><?php echo SwpmUtils::_('Password') ?></label>
          <input type="password" autocomplete="off" id="password" class="<?php echo $pass_class; ?>" value="" size="50" name="password" placeholder="Contraseña" />
        </div>

        <div class="swpm-registration-password-retype-row bp_frm_registration">
          <label for="password_re"><?php echo SwpmUtils::_('Confirmación de contraseña') ?></label>
          <input type="password" autocomplete="off" id="password_re" value="" size="50" name="password_re" placeholder="Confirmación de contraseña" />
        </div>

        <div class="swpm-registration-phone-row bp_frm_registration">
          <label for="phone"><?php echo SwpmUtils::_('Número de celular') ?></label>
          <input type="text" id="phone" value="<?php echo $phone; ?>" size="50" name="phone" class="input" placeholder="Número de celular" required/>
        </div>



        <div id="perfil_1">

        <div id="grupo_1">
          <div class="swpm-registration-id_centro-row bp_frm_registration">
            <label for="id_centro"><?php echo SwpmUtils::_('Gestión') ?></label>
            <select id="id_centro" name="id_centro" class="input" data-required_mark="required" data-field_type="select" data-original_id="Gestión"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select>
          </div>

          <div class="swpm-registration-id_departamento-row bp_frm_registration">
              <label for="id_departamento"><?php echo SwpmUtils::_('Departamento') ?></label>
              <select id="id_departamento" name="id_departamento" class="input" data-required_mark="required" data-field_type="select" data-original_id="departamento" style="width: 195px;" ><?php echo SwpmMiscUtils::get_region_dropdown($id_region) ?></select>
          </div>

          <div class="swpm-registration-id_provincia-row bp_frm_registration">
              <label for="id_provincia"><?php echo SwpmUtils::_('Provincia') ?></label>
              <select id="id_provincia" name="id_provincia" class="input" data-required_mark="required" data-field_type="select" data-original_id="provincia" style="width: 195px;" ><?php echo SwpmMiscUtils::get_provincia_dropdown($id_provincia) ?></select>
          </div>

          <div class="swpm-registration-id_Distrito-row bp_frm_registration">
              <label for="id_distrito"><?php echo SwpmUtils::_('Distrito') ?></label>
              <select id="id_distrito" name="id_distrito" class="input" data-required_mark="required" data-field_type="select" data-original_id="distrito" style="width: 195px;" ><?php echo SwpmMiscUtils::get_distrito_dropdown($id_distrito) ?></select>
          </div>
        </div>

        <div class="swpm-registration-id_nivel-row bp_frm_registration">
          <label for="id_nivel"><?php echo SwpmUtils::_('Nivel Educativo') ?></label>
          <select id="id_nivel" name="id_nivel"  class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" ><?php echo SwpmMiscUtils::get_estudios_dropdown($id_nivel) ?></select>
        </div>

        <div class="swpm-registration-id_dre-row bp_frm_registration">
          <label for="id_dre"><?php echo SwpmUtils::_('DRE') ?></label>
          <select id="id_dre" name="id_dre" class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" ><?php echo SwpmMiscUtils::get_dre_dropdown($id_dre) ?></select>
        </div>

        <div class="swpm-registration-id_ugel-row bp_frm_registration">
            <label for="id_ugel"><?php echo SwpmUtils::_('UGEL') ?></label>
            <select id="id_ugel" name="id_ugel" class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" ><?php echo SwpmMiscUtils::get_centro_dropdown($id_ugel) ?></select>
        </div>



        <div class="swpm-registration-id_institucion-row bp_frm_registration">
            <label for="id_provincia"><?php echo SwpmUtils::_('Institución Educativa') ?></label>
            <select id="id_institucion" name="id_institucion" class="input"  data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_institucion_dropdown($id_institucion) ?></select>
        </div>


        <div class="swpm-registration-id_carrera-row bp_frm_registration">
            <label for="id_carrera"><?php echo SwpmUtils::_('Carrera Profesional') ?></label>
            <select id="id_carrera" name="id_carrera" class="input"  data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_carrera_dropdown($id_carrera) ?></select>
        </div>

        <div class="swpm-registration-codigo_modular-row bp_frm_registration">
            <label for="codigo_modular"><?php echo SwpmUtils::_('Código modular IE') ?></label>
            <input type="text" id="codigo_modular" value="<?php echo $codigo_modular; ?>" size="50" name="codigo_modular" class="input" placeholder="Código modular IE" />
        </div>

        <div class="swpm-registration-centro_laboral-row bp_frm_registration">
          <label for="centro_laboral"><?php echo SwpmUtils::_('Centro Laboral') ?></label>
          <input type="text" id="centro_laboral" value="<?php echo $centro_laboral; ?>" size="50" name="centro_laboral" class="input" placeholder="Centro Laboral" />
        </div>

        <div class="swpm-registration-id_ministerio-row bp_frm_registration">
          <label for="id_ministerio"><?php echo SwpmUtils::_('Ministerio de Educación') ?></label>
          <select id="id_ministerio" name="id_ministerio" class="input" data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_ministerio_dropdown($id_ministerio) ?></select>
        </div>

        <div class="swpm-registration-id_cargo-row bp_frm_registration">
          <label for="id_cargo"><?php echo SwpmUtils::_('Cargo') ?></label>
          <select id="id_cargo" name="id_cargo" class="input" data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_cargo_dropdown($id_cargo) ?></select>
        </div>
        <!--
        <div class="swpm-registration-fec_nac-row bp_frm_registration">
          <label for="fec_nac"><?php //echo SwpmUtils::_('Fecha de Nacimiento') ?></label>
          <input type="text" id="fec_nac" value="<?php //echo $fec_nac; ?>" size="50" name="fec_nac" class="input" placeholder="Fecha de Nacimiento" />
        </div>


        <div class="swpm-registration-tipo_doc-row bp_frm_registration">
          <label for="tipo_doc"><?php //echo SwpmUtils::_('Tipo de documento') ?></label>
          <select id="tipo_doc" name="tipo_doc" class="input" data-required_mark="required" data-field_type="select" ><?php //echo SwpmMiscUtils::get_tipo_documento_dropdown($tipo_doc) ?></select>
        </div>
      -->

        </div>

            <!--
            <tr class="swpm-registration-firstname-row">
                <td><label for="first_name"><?php //echo SwpmUtils::_('First Name') ?></label></td>
                <td><input type="text" id="first_name" value="<?php //echo esc_attr($first_name); ?>" size="50" name="first_name" /></td>
            </tr>
            <tr class="swpm-registration-lastname-row">
                <td><label for="last_name"><?php //echo SwpmUtils::_('Last Name') ?></label></td>
                <td><input type="text" id="last_name" value="<?php //echo esc_attr($last_name); ?>" size="50" name="last_name" /></td>
            </tr>
          -->
            <div class="swpm-registration-membership-level-row">
                <!--
                <label for="membership_level"><?php //echo SwpmUtils::_('Membership Level') ?></label>
                -->
                    <?php
                    //echo $membership_level_alias; //Show the level name in the form.
                    //Add the input fields for the level data.
                    echo '<input type="hidden" value="' . $membership_level . '" size="50" name="membership_level" id="membership_level" />';
                    //Add the level input verification data.
                    $swpm_p_key = get_option('swpm_private_key_one');
                    if (empty($swpm_p_key)) {
                        $swpm_p_key = uniqid('', true);
                        update_option('swpm_private_key_one', $swpm_p_key);
                    }
                    $swpm_level_hash = md5($swpm_p_key . '|' . $membership_level); //level hash
                    echo '<input type="hidden" name="swpm_level_hash" value="' . $swpm_level_hash . '" />';
                    ?>

            </div>
            <?php
            //check if we need to display Terms and Conditions checkbox
            $terms_enabled = $settings->get_value('enable-terms-and-conditions');
            if (!empty($terms_enabled)) {
                $terms_page_url = $settings->get_value('terms-and-conditions-page-url');
                ?>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <label><input type="checkbox" id="accept_terms" name="accept_terms" class="validate[required]" value="1"> <?php echo SwpmUtils::_('I accept the ') ?> <a href="<?php echo $terms_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Terms and Conditions') ?></a></label>
                    </td>
                </tr>
                <?php
            }
            //check if we need to display Privacy Policy checkbox
            $pp_enabled = $settings->get_value('enable-privacy-policy');
            if (!empty($pp_enabled)) {
                $pp_page_url = $settings->get_value('privacy-policy-page-url');
                ?>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <label><input type="checkbox" id="accept_pt" name="accept_pp" class="validate[required]" value="1"> <?php echo SwpmUtils::_('I agree to the ') ?> <a href="<?php echo $pp_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Privacy Policy') ?></a></label>
                    </td>
                </tr>
                <?php
            }
            ?>


        <div class="swpm-before-registration-submit-section" align="center"><?php echo apply_filters('swpm_before_registration_submit_button', ''); ?></div>

        <div class="swpm-registration-submit-section" align="center">
            <input type="submit" value="<?php echo SwpmUtils::_('Register') ?>" class="swpm-registration-submit" name="swpm_registration_submit" />
        </div>

        <input type="hidden" name="action" value="custom_posts" />

    </form>
</div>
</div>
</div>
<style>
#main-content {
  background-blend-mode: multiply;
  background-image: url(http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/fondo-miperfil-duotono.jpg),linear-gradient(0deg,#a6a6c1 0%,#ffffff 100%)!important;
  padding-top: 5vw;
  padding-bottom: 8vw;
  background-color: inherit!important;
}
label {
  /*display: none;*/
}
/*
input:required:invalid {
  border: 2px solid red!important;
}

input:required:valid {
  border: 2px solid green!important;
}
*/
</style>
