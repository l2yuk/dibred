
<?php
$auth = SwpmAuth::get_instance();
//EDITADO PROYECTO_BP 2019-10-15
//var_dump($auth );
?>



				<div class="et_pb_row">
					<div class="et_pb_column et_pb_column_1_3 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">


				<div class="et_pb_module et_pb_team_member et_pb_team_member_0 et_pb_bg_layout_light clearfix  et_pb_text_align_center">


				<div class="et_pb_team_member_image et-waypoint et_pb_animation_off"><img src="http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/usuario-icono.png" alt="Juana Contreras" srcset="http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/usuario-icono.png 200w, http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/usuario-icono-150x150.png 150w" sizes="(max-width: 200px) 100vw, 200px" /></div>
				<div class="et_pb_team_member_description">
					<h4 class="et_pb_module_header"><?php echo $auth->get('first_name'); ?></h4>
					<p class="et_pb_member_position">(Docente)</p>
					<div><p><span>Etiam feugiat finibus ipsum eu aliquet. Maecenas quis dignissim dui. Sed nec ultricies nisi. Duis quis dolor semper, rutrum dui ut, sagittis massa.</span></p></div>
					<ul class="et_pb_member_social_links"><li><a href="#" class="et_pb_font_icon et_pb_facebook_icon"><span>Facebook</span></a></li><li><a href="#" class="et_pb_font_icon et_pb_twitter_icon"><span>Twitter</span></a></li><li><a href="#" class="et_pb_font_icon et_pb_linkedin_icon"><span>LinkedIn</span></a></li></ul>
				</div> <!-- .et_pb_team_member_description -->
			</div> <!-- .et_pb_team_member --><div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
				<a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_pb_bg_layout_light" href="http://10.200.8.76/~observatorio/wordpress/?page_id=51619" data-icon="&#x6c;">EDITAR PERFIL</a>
			</div>
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_2_3 et_pb_column_1   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough et-last-child">


				<div class="et_pb_row_inner et_pb_row_inner_0">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_0 et-last-child">


				<div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p style="font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size: 23px;color:#575756">Información de Contacto</p></div>
			</div> <!-- .et_pb_text -->
			<div class="et_pb_module  et_pb_text_1_bp " style="margin-bottom: 0px;">


				<div class="et_pb_text_inner"><p>Nombres</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p><?php echo $auth->get('first_name'); ?></p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>Apellidos</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p><?php echo $auth->get('last_name'); ?></p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>Tipo de Documento</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>DNI - <?php echo $auth->get('user_name'); ?></p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>Número de Celular</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p><?php echo $auth->get('phone'); ?></p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>Correo Electrónico</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p><?php echo $auth->get('email'); ?></p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>Institución Educativa</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_12 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>56004 Julio Alberto Ponce Atúnez de Mayolo (JAPAM)</p></div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->


			</div> <!-- .et_pb_row_inner --><div class="et_pb_row_inner et_pb_row_inner_1">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_1 et-last-child">


				<div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_13 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner" ><p style="font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size: 23px;color:#575756">Información Adicional</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_14 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>Habilidades</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p><span>Cras imperdiet nibh non lacus placerat, vel maximus nibh dignissim.</span></p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_16 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p>Intereses</p></div>
			</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_17 et_pb_bg_layout_light  et_pb_text_align_left">


				<div class="et_pb_text_inner"><p><span>Etiam imperdiet, mauris sed arcu, dignissim, ut laoreet nunc, accumsan. Etiam imperdiet mauris sed arcu dignissim, ut laoreet nunc accumsan. Nam eu nibh rhoncus, vestibulum lorem sit amet. </span></p></div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->


			</div> <!-- .et_pb_row_inner -->
			</div> <!-- .et_pb_column -->
				</div> <!-- .et_pb_row -->




<style id="et-core-unified-cached-inline-styles-2">@font-face{font-family:"Gotham Rounded Medium Italic";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Medium-Italic.otf") format("opentype")}@font-face{font-family:"Calibri";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/calibri.ttf") format("truetype")}@font-face{font-family:"Gotham Rounded Medium";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Medium.otf") format("opentype")}@font-face{font-family:"Gotham Rounded Light";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Light.otf") format("opentype")}@font-face{font-family:"Calibri Bold";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/calibrib.ttf") format("truetype")}div.et_pb_section.et_pb_section_0{background-image:url(http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/fondo-miperfil-duotono.jpg)!important}.et_pb_text_15{font-size:22px;padding-top:0px!important;margin-top:-30px!important;margin-bottom:20px!important}.et_pb_text_9.et_pb_text{color:#aaaaaa!important}.et_pb_text_10{font-size:22px;padding-top:0px!important;margin-top:-30px!important;margin-bottom:20px!important}.et_pb_text_11.et_pb_text{color:#aaaaaa!important}.et_pb_text_12{font-size:22px;padding-top:0px!important;padding-bottom:0px!important;margin-top:-30px!important}.et_pb_row_inner_1{background-color:rgba(255,255,255,0.85);border-radius:8px 8px 8px 8px;overflow:hidden;box-shadow:0px 2px 13px 0px rgba(0,0,0,0.17)}.et_pb_row_inner_1.et_pb_row_inner{padding-top:3vw!important;padding-right:3vw!important;padding-bottom:3vw!important;padding-left:3vw!important;margin-top:28px!important}.et_pb_column .et_pb_row_inner_1{padding-top:3vw;padding-right:3vw;padding-bottom:3vw;padding-left:3vw}.et_pb_text_13{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:23px;border-bottom-width:1px;border-bottom-color:#f4afff;padding-top:0px!important;margin-top:0px!important}.et_pb_text_14.et_pb_text{color:#aaaaaa!important}.et_pb_text_16.et_pb_text{color:#aaaaaa!important}.et_pb_section_0.et_pb_section{padding-bottom:93px;background-color:rgba(0,0,0,0.67)!important}.et_pb_text_17{font-size:22px;padding-top:0px!important;margin-top:-30px!important}div.et_pb_section.et_pb_section_1{background-image:linear-gradient(2deg,#1d0038 0%,#462a66 100%)!important}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}.et_pb_section_1.et_pb_section{padding-top:3vw;padding-right:0px;padding-bottom:2vw;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px}div.et_pb_section.et_pb_section_2{background-image:linear-gradient(2deg,#1d0038 0%,#462a66 100%)!important}.et_pb_section_2{border-top-width:7px;border-top-color:#f8ae0d}.et_pb_section_2.et_pb_section{padding-top:3vw;padding-right:0px;padding-bottom:2vw;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px}.et_pb_row_0.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}.et_pb_text_18.et_pb_text{color:#9789dd!important}.et_pb_text_8{font-size:22px;padding-top:0px!important;margin-top:-30px!important;margin-bottom:20px!important}.et_pb_text_7.et_pb_text{color:#aaaaaa!important}.et_pb_text_6{font-size:22px;padding-top:0px!important;margin-top:-30px!important;margin-bottom:20px!important}.et_pb_text_5.et_pb_text{color:#aaaaaa!important}.et_pb_team_member_0.et_pb_team_member h4,.et_pb_team_member_0.et_pb_team_member h1.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h2.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h3.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h5.et_pb_module_header,.et_pb_team_member_0.et_pb_team_member h6.et_pb_module_header{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:26px;color:#7082e0!important;line-height:29px}.et_pb_team_member_0.et_pb_team_member .et_pb_member_position{font-family:'Calibri Bold',Helvetica,Arial,Lucida,sans-serif;font-size:20px;color:#b784ce!important;line-height:28.1px}.et_pb_team_member_0.et_pb_team_member{background-color:rgba(255,255,255,0.85);border-radius:8px 8px 8px 8px;overflow:hidden;padding-top:2vw!important;padding-right:1vw!important;padding-bottom:2vw!important;padding-left:1vw!important;margin-top:-30px!important;margin-right:0px!important;margin-bottom:0px!important;margin-left:0px!important}.et_pb_team_member_0{box-shadow:0px 2px 13px 0px rgba(0,0,0,0.17)}.et_pb_button_0_wrapper{margin-top:18px!important}body #page-container .et_pb_section .et_pb_button_0{color:#ffffff!important;border-width:0px!important;border-color:#ffffff;border-radius:25px;letter-spacing:1px;font-size:15px;font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;padding-left:0.7em;padding-right:2em;background-color:#f47c92}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em;color:#ffe30f}body #page-container .et_pb_section .et_pb_button_0:after{color:#ffffff;line-height:inherit;font-size:inherit!important;opacity:1;margin-left:.3em;left:auto}.et_pb_button_0,.et_pb_button_0:after{transition:all 300ms ease 0ms}.et_pb_text_18{font-family:'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;font-size:13px;letter-spacing:1px;padding-top:2px!important;padding-right:0px!important;padding-bottom:0px!important;padding-left:0px!important;margin-top:0px!important;margin-right:0px!important;margin-bottom:0px!important;margin-left:0px!important}.et_pb_row_inner_0{background-color:rgba(255,255,255,0.85);border-radius:8px 8px 8px 8px;overflow:hidden;box-shadow:0px 2px 13px 0px rgba(0,0,0,0.17)}.et_pb_row_inner_0.et_pb_row_inner{padding-top:3vw!important;padding-right:3vw!important;padding-bottom:3vw!important;padding-left:3vw!important}.et_pb_column .et_pb_row_inner_0{padding-top:3vw;padding-right:3vw;padding-bottom:3vw;padding-left:3vw}.et_pb_text_0{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:23px;border-bottom-width:1px;border-bottom-color:#f4afff;padding-top:0px!important;margin-top:0px!important}
.et_pb_text_1.et_pb_text{color:#aaaaaa!important}
.et_pb_text{color:#aaaaaa!important}
/*
	.et_pb_text_2{font-size:22px;padding-top:0px!important;margin-top:-30px!important;margin-bottom:20px!important}
	*/
	.et_pb_text_3.et_pb_text{color:#aaaaaa!important}.et_pb_text_4{font-size:22px;padding-top:0px!important;margin-top:-30px!important;margin-bottom:20px!important}.et_pb_column_inner_1{z-index:9;position:relative}.et_pb_column_inner_0{z-index:9;position:relative}.et_pb_column_2{z-index:9;position:relative}.et_pb_column_0{z-index:9;position:relative}.et_pb_column_1{z-index:9;position:relative}@media only screen and (max-width:980px){.et_pb_team_member_0.et_pb_team_member{padding-top:5vw!important;padding-right:3vw!important;padding-bottom:3vw!important;padding-left:5vw!important;margin-left:0px!important}body #page-container .et_pb_section .et_pb_button_0{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_0:before{display:none}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_row_inner_0.et_pb_row_inner{padding-top:5vw!important;padding-right:6vw!important;padding-bottom:7vw!important;padding-left:6vw!important}.et_pb_column .et_pb_row_inner_0{padding-top:5vw!important;padding-right:6vw!important;padding-bottom:7vw!important;padding-left:6vw!important}.et_pb_text_0{border-bottom-width:1px;border-bottom-color:#f4afff}.et_pb_row_inner_1.et_pb_row_inner{padding-top:5vw!important;padding-right:6vw!important;padding-bottom:8vw!important;padding-left:6vw!important}.et_pb_column .et_pb_row_inner_1{padding-top:5vw!important;padding-right:6vw!important;padding-bottom:8vw!important;padding-left:6vw!important}.et_pb_text_13{border-bottom-width:1px;border-bottom-color:#f4afff}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}.et_pb_section_2{border-top-width:7px;border-top-color:#f8ae0d}}@media only screen and (max-width:767px){.et_pb_team_member_0.et_pb_team_member{padding-right:5vw!important}body #page-container .et_pb_section .et_pb_button_0{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_section .et_pb_button_0:after{line-height:inherit;font-size:inherit!important;margin-left:.3em;left:auto;display:inline-block;opacity:1;content:attr(data-icon);font-family:"ETmodules"!important}body #page-container .et_pb_section .et_pb_button_0:before{display:none}body #page-container .et_pb_section .et_pb_button_0:hover:after{margin-left:.3em;left:auto;margin-left:.3em}.et_pb_row_inner_0.et_pb_row_inner{padding-top:7vw!important;padding-right:7vw!important;padding-bottom:8vw!important;padding-left:7vw!important}.et_pb_column .et_pb_row_inner_0{padding-top:7vw!important;padding-right:7vw!important;padding-bottom:8vw!important;padding-left:7vw!important}.et_pb_text_0{border-bottom-width:1px;border-bottom-color:#f4afff}.et_pb_row_inner_1.et_pb_row_inner{padding-top:7vw!important;padding-right:7vw!important;padding-bottom:9vw!important;padding-left:7vw!important}.et_pb_column .et_pb_row_inner_1{padding-top:7vw!important;padding-right:7vw!important;padding-bottom:9vw!important;padding-left:7vw!important}.et_pb_text_13{border-bottom-width:1px;border-bottom-color:#f4afff}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}.et_pb_section_2{border-top-width:7px;border-top-color:#f8ae0d}}.et_pb_button{display:none}</style>
        <style id="pum-admin-bar-styles">
            /* Layer admin bar over popups. */
            #wpadminbar {
                z-index: 999999999999;
            }

            #wp-admin-bar-popup-maker > .ab-item::before {
                background: url("http://10.200.8.76/~observatorio/wordpress/wp-content/plugins/popup-maker//assets/images/admin/icon-info-21x21.png") center center no-repeat transparent !important;
                top: 3px;
                content: "";
                width: 20px;
                height: 20px;
            }

            #wp-admin-bar-popup-maker:hover > .ab-item::before {
                background-image: url("http://10.200.8.76/~observatorio/wordpress/wp-content/plugins/popup-maker//assets/images/admin/icon-info-21x21.png") !important;
            }

						.et_pb_row {

    position: relative;
    width: 100%;
    max-width: 1080px;
    margin: auto;
        margin-top: auto;
        margin-bottom: auto;

}

.et_pb_column .et_pb_row_inner_0 {

margin-top: -30px;
}

.et_pb_team_member_description span {
	font-size: 14px;
	font-family: 'Calibri',Helvetica,Arial,Lucida,sans-serif;

}

.et_pb_text_inner> p{
	font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
    font-size: 23px;
}
.et_pb_column .et_pb_text_inner p{
	font-family: 'Calibri',Helvetica,Arial,Lucida,sans-serif;
	font-size: 18px;
}
.et_pb_text_1{
	color: #aaaaaa !important;
	font-size: 22px !important;
}
.et_pb_text_2 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_4 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_6 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_8 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_10 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_12 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_15 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_17 {
	color: #575756 !important;
	font-size: 22px !important;
}
.et_pb_text_1_bp .et_pb_module .et_pb_text_1_bp {
	margin-bottom: 0px !important;
	font-size: 22px !important;
}


        </style>
