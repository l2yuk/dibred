jQuery(document).ready(function(){

	// Generate a password string
	function randString(id){
	  var dataSet = jQuery(id).attr('data-character-set').split(',');
	  var possible = '';
	  if(jQuery.inArray('a-z', dataSet) >= 0){
		possible += 'abcdefghijklmnopqrstuvwxyz';
	  }
	  if(jQuery.inArray('A-Z', dataSet) >= 0){
		possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	  }
	  if(jQuery.inArray('0-9', dataSet) >= 0){
		possible += '0123456789';
	  }
	  if(jQuery.inArray('#', dataSet) >= 0){
		possible += '![]{}()%&*$#^<>~@|';
	  }
	  var text = '';
	  for(var i=0; i < jQuery(id).attr('data-size'); i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	  }
	  return text;
	}

	// Create a new password on page load
	//jQuery('input[rel="gp"]').each(function(){
	 // jQuery(this).val(randString(jQuery(this)));
	//});

	// Create a new password
	jQuery(".getNewPass").click(function(){
	  var field = jQuery(this).closest('div').find('input[rel="gp"]');
	  field.val(randString(field));
	  jQuery('.password_id').show();
	});

	// Auto Select Pass On Focus
	//jQuery('input[rel="gp"]').on("click", function () {

	   //jQuery(this).select();

	//});

});
