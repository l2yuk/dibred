<?php
//echo "Prueba";

function cr_openCypher ($action='encrypt',$string=false)
{
    $action = trim($action);
    $output = false;

    $myKey = 'oW%c76+jb2';
    $myIV = 'A)2!u467a^';
    $encrypt_method = 'AES-256-CBC';

    $secret_key = hash('sha256',$myKey);
    $secret_iv = substr(hash('sha256',$myIV),0,16);

    if ( $action && ($action == 'encrypt' || $action == 'decrypt') && $string )
    {
        $string = trim(strval($string));

        if ( $action == 'encrypt' )
        {
            $output = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
        };

        if ( $action == 'decrypt' )
        {
            $output = openssl_decrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
        };
    };

    return $output;
};


function cr_db_plugin() {
  global $wpdb;
  //$nombreTabla = $wpdb->prefix . "demotabla";
	$created = dbDelta("
	DROP TABLE IF EXISTS `cr_pass_users`;
	CREATE TABLE cr_pass_users (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `user_pass` varchar(255) NOT NULL,
      `user_id` varchar(60) NOT NULL,
      UNIQUE KEY id (id)
    )ENGINE=InnoDB;");
}
