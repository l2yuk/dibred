msgid ""
msgstr ""
"Project-Id-Version: WP jCryption Security v0.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2015-01-17 22:16:14+0000\n"
"Last-Translator: andrey <andrey271@bigmir.net>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%100/10==1 ? 2 : n%10==1 ? 0 : (n+9)%10>3 ? 2 : 1;\n"
"X-Generator: CSL v1.x\n"
"X-Poedit-Language: UkrainianX-Poedit-Country: UKRAINE\n"
"X-Poedit-Country: \n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: \n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: wp-jcryption.php:110
#@ wpjc
msgid "Public and private keys have not been generated yet."
msgstr "Публічний і приватний ключи ще не були створені."

#: wp-jcryption.php:169
#@ wpjc
msgid "Form selectors"
msgstr "Селектори форм"

#: wp-jcryption.php:172
#@ wpjc
msgid "List forms which data are to be encrypted (in jQuery style, separated by commas), e. g.:"
msgstr "Список форм (в стилі jQuery, через кому), дані яких мають бути зашифрованими, наприклад:"

#: wp-jcryption.php:186
#@ wpjc
msgid "Current Key Pair"
msgstr "Діюча пара ключей"

#: wp-jcryption.php:193
#@ wpjc
msgid "Private key is not shown here but it is stored in the database."
msgstr "Приватний ключ не показано, але він зберігається в базі даних."

#: wp-jcryption.php:202
#@ wpjc
msgid "Generate new key pair"
msgstr "Створити нову пару ключей"

#: wp-jcryption.php:248
#@ wpjc
msgid "Public or private keys have not been generated correctly."
msgstr "Публічний або приватний ключ не було створено правильно."

#: wp-jcryption.php:260
#, php-format
#@ wpjc
msgid "Public and private keys have been generated successfully, %s."
msgstr "Публічний і приватний ключі було створено успішно, %s."

#: wp-jcryption.php:190
#, php-format
#@ wpjc
msgid "Generation date: %s. Current RSA public key length: %d."
msgstr "Час створення: %s. Довжина діючого публічного RSA ключа: %d."

#: wp-jcryption.php:194
#@ wpjc
msgid "New key length:"
msgstr "Довжина нового ключа:"

#: wp-jcryption.php:252
#@ wpjc
msgid "plugin settings"
msgstr "налаштування плагіна"

#: wp-jcryption.php:253
#, php-format
#@ wpjc
msgid "Maybe you want to change some %s."
msgstr "Можливо, ви бажаєте змінити деякі %s."

#: wp-jcryption.php:127
#, php-format
#@ wpjc
msgid "RSA keys have been created before: %s"
msgstr "RSA-ключі вже було створено раніше: %s"

#: wp-jcryption.php:174
#@ wpjc
msgid "Also use in admin area (user profile form data will be encrypted anyway)"
msgstr "Також застосовувати в адмінці (дані форми користувача будуть зашифровані завжди)"

#: wp-jcryption.php:177
#@ wpjc
msgid "Indicate secured form inputs with color"
msgstr "Виділити кольором поля захищених форм"

#: wp-jcryption.php:180
#@ wpjc
msgid "Fix button id=&#34;submit&#34; and name=&#34;submit&#34; by replacing this with &#34;wpjc-submit&#34;"
msgstr "Виправляти в кнопках id=&#34;submit&#34; і name=&#34;submit&#34;, замінюючи їх на &#34;wpjc-submit&#34;"

#: class-jcryption.php:19
#, php-format
#@ wpjc
msgid "Unable to read RSA keys. WP jCryption plugin has been deactivated. %s"
msgstr "Не вдалося прочитати RSA-ключі. Плагін WP jCryption деактивовано. %s"

#: class-jcryption.php:20
#@ wpjc
msgid "Back to admin area"
msgstr "Повернутися в адмінку"

#: wp-jcryption.php:104
#@ wpjc
msgid "WP jCryption is not working currently because it requires PHP 5.3+ with OpenSSL extension."
msgstr "WP jCryption зараз не працює, тому що для цього необхідно PHP 5.3+ з підтримкою OpenSSL."

