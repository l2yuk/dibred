<?php
/*
Title: Equipo Docente Slides
Post Type: buena_practica
*/

/* Display shortcode tag for current Carousel (only if it has been published) */
/*
if ( 'publish' === get_post_status($post->ID) ) {
    $shortcode = '<pre style="margin:0;">[ntrk-carousel-cposts id=' . $post->ID . ']</pre>';
    piklist('field', array(
        'type' => 'html',
        'label' => 'Shortcode',
        'value' => $shortcode
    ));
}
*/
/* Carousel options (values for basic Slick settings) */
/*
piklist('field', array(
    'type' => 'group',
    'label' => __('Carousel Settings', 'buena_practica'),
    'fields' => array(
        array(
            'type' => 'checkbox',
            'field' => 'infinite',
            'label' => __('Infinite loop', 'buena_practica'),
            'value' => 1,
            'choices' => array(
              1 => __('Yes', 'buena_practica'),
            ),
            'columns' => 3,
        ),
        array(
            'type' => 'checkbox',
            'field' => 'dots',
            'label' => __('Navigation dots', 'buena_practica'),
            'value' => 1,
            'choices' => array(
              1 => __('Yes', 'buena_practica'),
            ),
            'columns' => 3
        ),
        array(
            'type' => 'number',
            'field' => 'slides_to_show',
            'label' => __('Slides to show', 'buena_practica'),
            'value' => 3,
            'columns' => 3
        ),
        array(
            'type' => 'number',
            'field' => 'slides_to_scroll',
            'label' => __('Slides to scroll', 'buena_practica'),
            'value' => '1',
            'columns' => 3
        ),
    )
));
*/
/* Responsive display (values for Slick settings) */
/*
piklist('field', array(
    'type' => 'group',
    'field' => 'responsive_settings',
    'label' => __('Responsive Settings', 'buena_practica'),
    'fields' => array(
        array(
            'type' => 'group',
            'field' => 'resp_option',
            'add_more' => true,
            'fields' => array(
                array(
                    'type' => 'number',
                    'field' => 'width',
                    'label' => __('Breakpoint (px)', 'buena_practica'),
                    'columns' => 4,
                ),
                array(
                    'type' => 'number',
                    'field' => 'slides_to_show',
                    'label' => __('Slides to show', 'buena_practica'),
                    'value' => '',
                    'columns' => 4
                ),
                array(
                    'type' => 'number',
                    'field' => 'slides_to_scroll',
                    'label' => __('Slides to scroll', 'buena_practica'),
                    'value' => '',
                    'columns' => 4
                ),
            )
        )
    )
));
*/

/* Carousel Slides */
piklist('field', array(
    'type' => 'group',
    'field' => 'slides',
    'label' => __('Slides', 'buena_practica'),
    'add_more' => true,
    'fields' => array(
        array(
            'type' => 'file',
            'field' => 'image',
            'label' => __('Image', 'buena_practica'),
            'options' => array(
                'button' => __('Add image', 'buena_practica'),
                'modal_title' => __('Select image', 'buena_practica'),
            ),
        ),
        array(
            'type' => 'text',
            'field' => 'text',
            'label' => __('Nombres:', 'buena_practica'),
            'columns' => 12,
            'attributes' => array(
                'rows' => 5,
                'placeholder' => __('Insert text', 'buena_practica'),
            )
        ),
        array(
            'type' => 'text',
            'field' => 'text_institucion',
            'label' => __('Institución Educativa:', 'buena_practica'),
            'columns' => 12,
            'attributes' => array(
                'rows' => 5,
                'placeholder' => __('Insert text', 'buena_practica'),
            )
        ),
    ),
));

?>
