<?php
$auth = SwpmAuth::get_instance();
$setting = SwpmSettings::get_instance();
$password_reset_url = $setting->get_value('reset-page-url');
//$join_url = $setting->get_value('join-us-page-url');
$registration_url = $setting->get_value('registration-page-url');


//EDITADO PROYECTO_BP 2019-10-15
?>
<div  class="et_pb_with_border et_pb_module et_pb_contact_form_0_bp et_pb_contact_form_container_bp" >
  <div class="swpm-login-widget-form_">

    <h1 class="et_pb_contact_main_title">Ingresa a tu cuenta</h1>
    <div class="et-pb-contact-message">
      <?php echo "<p style='color:red'>".$auth->get_message()."<p>"; ?>
    </div>
    <div class="et_pb_contact">

      <form id="swpm-login-form" name="swpm-login-form" method="post" action="">
          <div class="swpm-login-form-inner">

              <p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_last" data-id="correo_electrónico" data-type="input">
                <label for="et_pb_contact_correo_electrónico_0" class="et_pb_contact_form_label"><?php echo SwpmUtils::_('Número de documento') ?></label>
                <!--
                <input type="text" id="et_pb_contact_correo_electrónico_0" class="input" value="" name="et_pb_contact_correo_electrónico_0" data-required_mark="required" data-field_type="input" data-original_id="correo_electrónico" placeholder="Correo Electrónico">
                -->
                <input type="text" class="swpm-text-field swpm-username-field input" id="swpm_user_name" value="" size="25" name="swpm_user_name" data-required_mark=""  data-original_id="Usuario" placeholder="Número de documento"/>
              </p>

              <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_last" data-id="contraseña" data-type="input">
                <label for="et_pb_contact_contraseña_0" class="et_pb_contact_form_label"><?php echo SwpmUtils::_('Contraseña') ?></label>
                <!--
                <input type="text" id="et_pb_contact_contraseña_0" class="input" value="" name="et_pb_contact_contraseña_0" data-required_mark="required" data-field_type="input" data-original_id="contraseña" placeholder="Contraseña">
                -->
                <input type="password" class="swpm-text-field swpm-password-field input" id="swpm_password" value="" size="" name="swpm_password"  data-required_mark="" placeholder="Contraseña"/>
              </p>

              <?php //echo do_shortcode('[FC_captcha_fields]'); ?>
              <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last" data-id="field_5" data-type="checkbox">
        				<label for="et_pb_contact_field_5_0" class="et_pb_contact_form_label"> </label>
                <!--
        				<input class="et_pb_checkbox_handle" type="hidden" name="et_pb_contact_field_5_0" data-required_mark="required" data-field_type="checkbox" data-original_id="field_5">
              -->
        					<span class="et_pb_contact_field_options_wrapper">
        						<span class="et_pb_contact_field_options_title"> </span>
        						<span class="et_pb_contact_field_options_list"><span class="et_pb_contact_field_checkbox">
                      <!--
                      <div class="swpm-remember-me">
                        <span class="swpm-remember-checkbox"><input type="checkbox" name="rememberme" class="input" value="checked='checked'"  data-id="-1"></span>
                        <span class="swpm-rember-label"> <?php //echo SwpmUtils::_('Remember Me') ?></span>
                      </div>
                    -->
        							<input type="checkbox" id="et_pb_contact_field_5_2_0" name="rememberme" class="input" value="checked='checked'" data-id="-1">
        							<label for="et_pb_contact_field_5_2_0"><i></i>Recordarme</label>
        						</span></span>
        					</span>
			        </p>

              <div class="swpm-before-login-submit-section"><?php echo apply_filters('swpm_before_login_form_submit_button', ''); ?></div>



              <div class="et_contact_bottom_container_">
                <!--
                <button type="submit" name="et_builder_submit_button" class="et_pb_contact_submit et_pb_button" 3="">INICIAR SESIÓN</button>
                -->
                <div class="swpm-login-submit">
                    <input type="submit" class="swpm-login-form-submit et_pb_contact_submit et_pb_button" name="swpm-login" value="<?php echo SwpmUtils::_('INICIAR SESIÓN') ?>"/>
                </div>

              </div>


              </div>
              <!--
              <div class="swpm-forgot-pass-link">
                  <a id="forgot_pass" class="swpm-login-form-pw-reset-link"  href="<?php //echo $password_reset_url; ?>"><?php //echo SwpmUtils::_('Olvidé mi contraseña ?') ?></a>
              </div>
              <div class="swpm-join-us-link">
                  <a id="register" class="swpm-login-form-register-link" href="<?php //echo $join_url; ?>"><?php //echo SwpmUtils::_('¿Nuevo Usuario? REGÍSTRATE') ?></a>
              </div>
            -->

          </div>
      </form>

    </div>

  </div>
</div>


<div class="et_pb_module et_pb_divider et_pb_divider_0 et_pb_divider_position_ et_pb_space">
  <div class="et_pb_divider_internal"></div>
</div>

<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_center">
  <div class="et_pb_text_inner"><p>
  <a id="forgot_pass" class="swpm-login-form-pw-reset-link"  href="<?php echo $password_reset_url; ?>"><?php echo SwpmUtils::_('Olvidé mi contraseña ?') ?></a>
  </p></div>
</div> <!-- .et_pb_text -->

<div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_center">
  <div class="et_pb_text_inner"><p>
    <a id="register" class="swpm-login-form-register-link" href="<?php echo $registration_url; ?>"><?php echo SwpmUtils::_('¿Nuevo Usuario? <strong>REGÍSTRATE</strong>') ?></a>

  </p>
</div>
</div>

<style id="">@font-face{

  font-family:"Gotham Rounded Medium Italic";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Medium-Italic.otf") format("opentype")}@font-face{font-family:"Calibri";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/calibri.ttf") format("truetype")}@font-face{font-family:"Gotham Rounded Medium";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Medium.otf") format("opentype")}@font-face{font-family:"Gotham Rounded Light";src:url("http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/et-fonts/Gotham-Rounded-Light.otf") format("opentype")}
  div.et_pb_section.et_pb_section_0{
    background-blend-mode:multiply;background-image:url(http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/observatorio-buenas-practicas-fondo-login-duotono.jpg)!important}
    .et_pb_divider_0:hover:before{border-top-width:px}
    .et_pb_contact_form_0 p .input:focus:-ms-input-placeholder{color:#797979}
    .et_pb_contact_form_0 p textarea:focus::-webkit-input-placeholder{color:#797979}
    .et_pb_contact_form_0 p textarea:focus::-moz-placeholder{color:#797979}
    .et_pb_contact_form_0 p textarea:focus:-ms-input-placeholder{color:#797979}
    .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button{transition:background-color 300ms ease 0ms}
    .et_pb_contact_form_0 .input[type="radio"]:checked+label i:before{background-color:#797979}
    .et_pb_contact_form_0 .input[type="radio"]:checked:active+label i:before{background-color:#797979}
    .et_pb_divider_0{background-color:rgba(0,0,0,0);padding-right:3vw;padding-left:3vw;margin-top:27px!important;margin-bottom:0px!important}
    .et_pb_divider_0:before{border-top-color:rgba(43,52,155,0.26);border-top-style:dotted;border-top-width:2px;width:auto;top:0px;right:3vw;left:3vw}
    .et_pb_text_0.et_pb_text{color:#474747!important}
    .et_pb_section_0.et_pb_section{padding-top:5vw;padding-bottom:8vw;background-color:#b5b5b5!important}
    .et_pb_text_0{font-family:'Calibri',Helvetica,Arial,Lucida,sans-serif;font-size:16px;background-color:rgba(0,0,0,0);padding-top:0px!important;padding-bottom:0px!important;
  /*padding-left:3vw!important;*/
  margin-top:0px!important;margin-bottom:0px!important}
  .et_pb_text_0 h1{color:#3b777c!important}
  .et_pb_text_1.et_pb_text{color:#ff5683!important}
  .et_pb_text_1{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:16px;background-color:rgba(0,0,0,0);padding-top:10px!important;padding-bottom:10px!important;/*padding-left:3vw!important;*/margin-top:0px!important;margin-bottom:0px!important}.et_pb_text_1 h1{color:#ffffff!important}div.et_pb_section.et_pb_section_1{background-image:linear-gradient(2deg,#1d0038 0%,#462a66 100%)!important}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}.
  et_pb_section_1.et_pb_section{padding-top:24px;padding-right:0px;padding-bottom:24px;padding-left:0px;margin-top:0px;margin-right:0px;margin-bottom:0px;margin-left:0px}
  .et_pb_row_1.et_pb_row{padding-top:0px!important;padding-bottom:0px!important;margin-top:0px!important;margin-bottom:0px!important;padding-top:0px;padding-bottom:0px}
  .et_pb_text_2.et_pb_text{color:#9789dd!important}
  .et_pb_contact_form_0 p .input:focus::-moz-placeholder{color:#797979}
  .et_pb_contact_form_0 p .input:focus::-webkit-input-placeholder{color:#797979}
  .et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label,.et_pb_contact_form_0 .input[type="radio"]:active+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked:active+label i:before{color:#797979}.et_pb_contact_form_0 .input::-ms-input-placeholder{color:#797979}
  .et_pb_row_0{background-image:linear-gradient(180deg,rgba(255,255,255,0.86) 0%,#dddddd 100%);background-color:rgba(0,0,0,0);border-radius:15px 15px 15px 15px;overflow:hidden}
  .et_pb_row_0,body #page-container .et-db #et-boc .et_pb_row_0.et_pb_row,body.et_pb_pagebuilder_layout.single #page-container #et-boc .et_pb_row_0.et_pb_row,body.et_pb_pagebuilder_layout.single.et_full_width_page #page-container #et-boc .et_pb_row_0.et_pb_row{max-width:610px}
  .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp h1,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp h2.et_pb_contact_main_title,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp h3.et_pb_contact_main_title,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp h4.et_pb_contact_main_title,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp h5.et_pb_contact_main_title,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp h6.et_pb_contact_main_title{font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size:20px;color:#7580c4!important}
  .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .et_pb_contact_captcha_question{font-family:'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;font-size:33px;color:#7580c4!important}
  .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input::placeholder,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input[type=checkbox]+label,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input[type=radio]+label{font-size:18px}
  .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input::-webkit-input-placeholder{font-size:18px}.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input::-moz-placeholder{font-size:18px}.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input:-ms-input-placeholder{font-size:18px}
  .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp{background-color:rgba(0,0,0,0);padding-top:5vw;padding-right:3vw;padding-bottom:52px;padding-left:3vw;margin-top:0px!important;margin-right:0px!important;margin-bottom:0px!important;margin-left:0px!important}
  .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input[type="checkbox"]+label i,.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp .input[type="radio"]+label i{border-radius:5px 5px 5px 5px;overflow:hidden;border-color:#d0e8d9;background: #ffffff}
  body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button{color:#7a5500!important;border-width:0px!important;border-color:rgba(0,0,0,0);border-radius:25px;letter-spacing:0px;
    /*font-size:16px;*/
    font-family:'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;background-color:#f9af0e}body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:after{font-size:1.6em;color:#ffffff}body.et_button_custom_icon #page-container .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:after{font-size:16px}body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:hover:after{color:}body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:hover{background-image:initial!important;background-color:#fcd00f!important}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label i,.et_pb_contact_form_0 .input[type="radio"]+label i{background-color:#ffffff}.et_pb_contact_form_0 .input:focus,.et_pb_contact_form_0 .input[type="checkbox"]:active+label i,.et_pb_contact_form_0 .input[type="radio"]:active+label i{background-color:#ffffff}.et_pb_contact_form_0 .input,.et_pb_contact_form_0 .input[type="checkbox"]+label,.et_pb_contact_form_0 .input[type="radio"]+label,.et_pb_contact_form_0 .input[type="checkbox"]:checked+label i:before,.et_pb_contact_form_0 .input::placeholder{color:#797979}.et_pb_contact_form_0 .input::-webkit-input-placeholder{color:#797979}.et_pb_contact_form_0 .input::-moz-placeholder{color:#797979}.et_pb_text_2{font-family:'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;font-size:13px;letter-spacing:1px;padding-top:2px!important;padding-right:0px!important;padding-bottom:0px!important;padding-left:0px!important;margin-top:0px!important;margin-right:0px!important;margin-bottom:0px!important;margin-left:0px!important}.et_pb_column_0{z-index:9;position:relative}.et_pb_column_1{z-index:9;position:relative}@media only screen and (max-width:980px){.et_pb_contact_form_0_bp.et_pb_contact_form_container_bp{padding-top:7vw;padding-right:6vw;padding-left:6vw}body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}
/*
    body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:after{
      display:inline-block;opacity:0}*/
    /*body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:hover:after{opacity:1}*/
    .et_pb_text_0{padding-right:0vw!important;padding-bottom:55px!important;padding-left:0vw!important;margin-bottom:-10px!important}
    .et_pb_text_1{padding-right:0vw!important;padding-bottom:55px!important;padding-left:0vw!important;margin-bottom:-10px!important}
    .et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}}@media only screen and (max-width:767px){
      .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp{padding-top:6vw;padding-right:4vw;padding-left:4vw}
      body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button{padding-left:1em;padding-right:1em}
      body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:hover{padding-left:0.7em;padding-right:2em}
      /*body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:after{display:inline-block;opacity:0}*/
      /*body #page-container .et_pb_section .et_pb_contact_form_0_bp.et_pb_contact_form_container_bp.et_pb_module .et_pb_button:hover:after{opacity:1}*/
      .et_pb_text_0{padding-bottom:30px!important}.et_pb_text_1{padding-bottom:30px!important}.et_pb_section_1{border-top-width:7px;border-top-color:#f8ae0d}}



</style>	<!--[if lte IE 8]>
<![endif]-->
