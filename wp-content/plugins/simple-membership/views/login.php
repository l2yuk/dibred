<?php
$auth = SwpmAuth::get_instance();
$setting = SwpmSettings::get_instance();
$password_reset_url = $setting->get_value('reset-page-url');
//$join_url = $setting->get_value('join-us-page-url');
$registration_url = $setting->get_value('registration-page-url');
$bp_s=$_GET["bp_s"];
if($bp_s=='true'){
  $bp_mensaje='<h3 style="margin-top:-25px;padding-bottom:40px;color:#ff668e">Registro con éxito.</h3>';
  //wp_redirect( site_url() );
}
if($bp_s=='red'){
  /*
  $bp_mensaje2='<div class="bp_mensaje_red"><div class="bp_mensaje_red_1">
  <p>
  El Observatorio de Buenas Prácticas es un espacio virtual  donde la comunidad educativa accede a prácticas innovadoras que responden a la motivación de  los docentes y directivos y a la particularidad de cada una de sus instituciones educativas. Además, ofrece una red de Docentes Innovadores, herramientas de comunicación y recursos necesarios para que los docentes puedan desarrollar todo su potencial.
  </p>
  </div>
  <div class="bp_mensaje_red_2">
  <p>
  Para participar en cada uno de sus espacios de interacción debes iniciar tu sesión de usuario, si no estás registrado deberás crear una cuenta.
  </p>

  </div>
  </div>';
  */
}
?>
<?php echo $bp_mensaje2 ?>
<?php if($bp_s=='red'){ ?>
<div class="bp_content_registration">
  <div class="bp_content_registration_0">
    <div class="bp_text"><p>El Observatorio de Buenas Prácticas es un espacio virtual donde la comunidad educativa accede a prácticas innovadoras que responden a la motivación de los docentes y directivos y a la particularidad de cada una de sus instituciones educativas. Además, ofrece una red de Docentes Innovadores, herramientas de comunicación y recursos necesarios para que los docentes puedan desarrollar todo su potencial.</p></div>
    <div class="bp_module">
      <div class="bp_text bp_text_3">
        <p>Para participar en cada uno de sus espacios de interacción debes iniciar tu sesión de usuario, si no estás registrado deberás crear una cuenta.</p>
      </div>
		</div>

  </div>
<?php }?>
<div class="bp_content">
  <div class="bp_content_1">


<div class="swpm-login-widget-form">
    <form id="swpm-login-form" name="swpm-login-form" method="post" action="">
        <div class="swpm-login-form-inner">

            <div class="bt_titulo">
                <?php echo $bp_mensaje ?>
            <h1 class="et_pb_contact_main_title">Ingresa a tu cuenta</h1>
            <!--
            <div class="bt_linea_login_bp"></div>
          -->
            </div>

            <div class="swpm-login-action-msg <?php if($auth->get_message()){echo 'bp_msg';}?>">
                <span class="swpm-login-widget-action-msg"><?php echo $auth->get_message(); ?></span>

            </div>


            <div class="swpm-username-label">
                <label for="swpm_user_name" class="swpm-label"><?php echo SwpmUtils::_('Username or Email') ?></label>
            </div>
            <div class="swpm-username-input bp_frm_login">
                <input type="text" class="swpm-text-field swpm-username-field" id="swpm_user_name" value="" size="25" maxlength="12" name="swpm_user_name" placeholder="Número de DNI" required/>
            </div>
            <div class="swpm-password-label">
                <label for="swpm_password" class="swpm-label"><?php echo SwpmUtils::_('Password') ?></label>
            </div>
            <div class="swpm-password-input bp_frm_login">
                <input type="password" class="swpm-text-field swpm-password-field" id="swpm_password" value="" size="25" name="swpm_password" placeholder="Ingresa su contraseña" required />
            </div>

            <div class="elem-group">
              <img src="<?php echo plugins_url("/simple-membership/views/captcha.php"); ?>" alt="CAPTCHA" class="captcha-image"><i class="fas fa-redo refresh-captcha"></i>  <br>
              <label for="captcha">Código de Seguridad:</label>
                <br>
              <input type="text" id="captcha" name="captcha_challenge" pattern="[A-Z]{6}" required>
            </div>
              <?PHP //ECHO $_SESSION['captcha_text'] ?>

            <!--
            <div class="swpm-remember-me">
                <span class="swpm-remember-checkbox"><input type="checkbox" name="rememberme" value="checked='checked'"></span>
                <span class="swpm-rember-label"> <?php //echo SwpmUtils::_('Remember Me') ?></span>
            </div>
          -->
            <div class="swpm-before-login-submit-section "><?php echo apply_filters('swpm_before_login_form_submit_button', ''); ?></div>

            <div class="swpm-login-submit">
                <input type="submit" class="swpm-login-form-submit bp_button " name="swpm-login" value="<?php echo SwpmUtils::_('INICIAR SESIÓN') ?>"/>
                  <!--
                  <span class="dashicons dashicons-arrow-right-alt2"></span>
                -->
                </input>
            </div>
          <div class="bp_divider">  </div>
            <div class="swpm-forgot-pass-link">
                <a id="forgot_pass" class="swpm-login-form-pw-reset-link"  href="<?php echo $password_reset_url; ?>"><?php echo SwpmUtils::_('¿Olvidé mi contraseña?') ?></a>
            </div>
            <div class="swpm-join-us-link">
                <a id="register" class="swpm-login-form-register-link" href="<?php echo $registration_url ?>" style="color: #705a87!important;"><?php echo SwpmUtils::_('¿Nuevo Usuario? REGÍSTRATE') ?></a>
            </div>

        </div>
    </form>
</div>
</div>
</div>
<?php if($bp_s=='red'){ ?>
</div>
<?php } ?>
<style>
#main-content {
  background-blend-mode: multiply;
  background-image: url(<?php echo site_url('/wp-content/uploads/2019/10/fondo-miperfil-duotono.jpg');?>),linear-gradient(0deg,#a6a6c1 0%,#ffffff 100%)!important;
  padding-top: 5vw;
  padding-bottom: 8vw;
  background-color: inherit!important;
}

.bp_mensaje_red_1 {
    color: #ffffff!important;
    font-family: 'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;
    font-size: 14px;
    letter-spacing: 0.5px;
    line-height: 20px;
    max-width: 1080px;
    margin-bottom: 2.75%;
    line-height: 20px;
    margin-top: -50px;
    text-align: center;
  }
.bp_mensaje_red_2 {
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
    font-size: 20px;
    letter-spacing: 0.2px;
    line-height: 25px;
    padding-top: 0px!important;
    margin-top: -17px!important;
    max-width: 900px;
    color: #ffffff;
    text-align: center;
    line-height: 25px;
    margin-left: 85px;
    padding-bottom:25px;
}
#captcha{
  width: 100% !important;
  margin-bottom: 3% !important;
  padding: 0 0 0 3% !important;
  background-position: center !important;
  background-size: cover !important;
  border-radius: 5px 5px 5px 5px !important;
  overflow: hidden !important;
  border-color: #d0e8d9 !important;
  font-size: 18px !important;
  padding-top: 10px !important;
  padding-bottom: 10px !important;
  background-color: #ffffff !important;
}

.bp_content_registration .bp_frm_registration select{
  width: 100%;
  margin-bottom: 3%;
  padding: 0 0 0 3%;
  background-position: center;
  background-size: cover;
  border-radius: 5px 5px 5px 5px;
    overflow: hidden;
    border-color: #d0e8d9;
    font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #ffffff;
}
.bp_content_registration .bp_frm_registration input{
    width: 100%;
  margin-bottom: 3%;
  padding: 0 0 0 3%;
  background-position: center;
  background-size: cover;
  border-radius: 5px 5px 5px 5px;
    overflow: hidden;
    border-color: #d0e8d9;
    font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #ffffff;
}


.bp_content_registration_0 {
  width: 29.667%;
  margin-right: -3%;
    float: left;
    position: relative;
    z-index: 9;
    background-position: center;
    background-size: cover;
}

.bp_content_registration_0{
    float: left;
    text-align: right;
    /*margin-top: 11.6% !important;*/
}
.bp_text{
  color: #ffffff!important;
  font-family: 'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;
  font-size: 14px;
  line-height: 20px;
}
.bp_module{
  margin-top: 10px;
  border-top-color: #ff9b0f;
  border-top-style: dashed;
  border-top-width: 2px;
}
.bp_text_3{
  color: #ffe993!important;
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
  font-size: 20px;
  line-height: 28px;
  margin-top: 10px;
}
.bt_titulo h1{
    font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;
    font-size: 20px!important;
    color: #7580c4!important;
}
.entry-title {
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
  font-size: 40px;
  color: #ffffff!important;
  <?php if($bp_s=='red'){ ?>
  margin-left: 380px;
  <?php }else{ ?>
    margin-left: 235px;
  <?php } ?>
}
.linea_inicio_sesion{
  border-top-color: #ff89ca;
  border-top-width: 6px;
  border-top-style: solid;
  width: 8%;
  <?php if($bp_s=='red'){ ?>
  margin-left: 380px;
  <?php }else{ ?>
    margin-left: 235px;
  <?php } ?>
  margin-top: -4px;
  padding-bottom: 20px
}

.post-51618 .entry-title{
  margin-top: 0px!important;
}
body, input, textarea, select {
    font-family: 'Calibri Regular',Helvetica,Arial,Lucida,sans-serif!important;
    font-size: 18px!important;
    color: #797979!important;
}
</style>
