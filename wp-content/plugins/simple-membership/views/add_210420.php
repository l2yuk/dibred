﻿<?php
session_start();
SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData' => '&action=swpm_validate_email&member_id=' . filter_input(INPUT_GET, 'member_id'))));
$settings = SwpmSettings::get_instance();
$force_strong_pass = $settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    //$pass_class = "validate[required,custom[strongPass],minSize[8]]";
} else {
    $pass_class = "";
}
$ubigeo="";
?>

<div class="bp_content_registration">
  <div class="bp_content_registration_0">
    <div class="bp_text"><p>Regístrate para continuar accediendo a los contenidos más relevantes del&nbsp;</p></div>
    <div class="bp_text bp_text_1"><p>Observatorio de Buenas Prácticas e Innovación Educativa.</p></div>
    <div class="bp_text bp_text_2"><p>Además, con tu cuenta podrás:</p></div>
    <div class="bp_module">
      <div class="bp_text bp_text_3">
        <p>Descargar las Buenas Prácticas e innovaciones.</p>
        <p>Ser parte de nuestra comunidad y red de docentes y directivos.</p>
        <p>Participar en foros de debate.</p>
        <p>Recibir notificaciones sobre convocatorias y nuestras últimas publicaciones.</p></div>
			</div>

  </div>
<div class="bp_content_registration_grupo">
  <div class="bp_module_1">
      <div class="bp_text bp_text_4"><h1>Formulario de Registro<div class="bt_linea_add_bp"></div></h1></div>

  </div>

  <div class="bp_content_registration_1" id="bp_content_registration_1">

    <div class="swpm-registration-widget-form">
      <h1 class="bp_main_title">Datos Personales</h1>

    <form id="swpm-registration-form" class="swpm-validate-form" autocomplete="nope" name="swpm-registration-form" method="post" action="">
        <input type ="hidden" name="level_identifier" value="<?php echo $level_identifier ?>" />

        <div class="swpm-registration-perfil-row bp_frm_registration">
          <label for="perfil"><?php echo SwpmUtils::_('Perfil de Usuario') ?></label>
          <select id="id_perfil" name="id_perfil" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil" required><?php echo SwpmMiscUtils::get_perfil_dropdown($id_perfil) ?></select>
        </div>

        <div id="bp_mensaje_ext" style="margin-bottom: 10px;margin-top: 10px;color: #7f6ba5;font-weight: bold;">
        Si Ud. es extranjero, por favor ingrese <a href="#" class="extranjeros">aquí</a></div>
        <div id="perfil_1">
          <div class="swpm-registration-tipo_doc-row bp_frm_registration">
            <label for="tipo_doc"><?php echo SwpmUtils::_('Tipo de documento') ?></label>
            <select id="tipo_doc" name="" class="input" data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_tipo_documento_dropdown($tipo_doc) ?></select>
          </div>

        </div>


        <div class="swpm-registration-subperfil-row bp_frm_registration">
          <label for="subperfil"><?php echo SwpmUtils::_('SubPerfil de Usuario') ?></label>
          <select id="id_subperfil" name="id_subperfil" class="input"  data-required_mark="required" data-field_type="select" data-original_id="perfil" required><?php echo SwpmMiscUtils::get_subperfil_dropdown($id_subperfil) ?></select>
        </div>

        <div class="swpm-registration-username-row bp_frm_registration">
          <label for="user_name"><?php echo SwpmUtils::_('Número de documento') ?></label>
          <input type="text"  autocomplete="off" id="user_name" class="" value="<?php echo esc_attr($user_name); ?>" size="8" maxlength="8" name="user_name" placeholder="" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required/>
        </div>

        <div class="swpm-registration-ubigeo-row bp_frm_registration">
          <label for="ubigeo"><?php echo SwpmUtils::_('Ubigeo') ?></label>
          <input type="text" autocomplete="off" id="ubigeo" value="<?php echo esc_attr($ubigeo); ?>"  name="ubigeo" maxlength="6"  class="input" placeholder="" required/>
          <input type="hidden" id="first_name" value="<?php echo esc_attr($first_name); ?>" size="50" name="first_name" />
          <input type="hidden" id="last_name" value="<?php echo esc_attr($last_name); ?>" size="50" name="last_name" />
        </div>

        <div class="swpm-registration-icono-row bp_frm_registration">
          <?php
            $url=wp_upload_dir();
            //echo var_dump($url);
            $url_imagen_bp=$url['baseurl'].'/2019/11/btn-ico-lupa-off.png';
            $ubigeo_popup .='<img src="'.$url_imagen_bp.'" alt="" width="30" height="30" style="padding-bottom: 10px;">';
          ?>
          <a class="ubigeo_popup" href="#" style="margin-left:20px" ><?php echo $ubigeo_popup;?></a>
        </div>

        <div id="bp_mensaje" style="margin-bottom: 20px;color: #7f6ba5;"></div>

        <div class="swpm-registration-firstname-row bp_frm_registration">
          <label for="first_name"><?php echo SwpmUtils::_('First Name') ?></label>
          <input type="text" autocomplete="off" id="first_name" value="<?php echo esc_attr($first_name); ?>" size="50" name="first_name" />
        </div>

        <div class="swpm-registration-lastname-row bp_frm_registration">
          <label for="last_name"><?php echo SwpmUtils::_('Last Name') ?></label>
          <input type="text" autocomplete="off" id="last_name" value="<?php echo esc_attr($last_name); ?>" size="50" name="last_name" />
        </div>


        <div class="swpm-registration-email-row bp_frm_registration">
          <label for="email"><?php echo SwpmUtils::_('Email') ?></label>
          <input type="email" autocomplete="off" id="email" class="" value="<?php echo esc_attr($email); ?>" size="50" name="email"  style="font-size:18px" placeholder="" required/>
        </div>

        <div class="swpm-registration-password-row bp_frm_registration">
          <label for="password"><?php echo SwpmUtils::_('Password') ?></label>
          <input type="password" autocomplete="off" id="password" class="<?php echo $pass_class; ?>" value="" size="50" name="password"  style="font-size:18px" placeholder="" required/>
        </div>

        <div class="swpm-registration-password-retype-row bp_frm_registration">
          <label for="password_re"><?php echo SwpmUtils::_('Confirmación de contraseña') ?></label>
          <input type="password" autocomplete="off" id="password_re" value="" size="50" name="password_re" placeholder=""  style="font-size:18px" required/>
        </div>

        <div class="swpm-registration-phone-row bp_frm_registration">
          <label for="phone"><?php echo SwpmUtils::_('Número de celular') ?></label>
          <input type="text" autocomplete="off" id="phone" value="<?php echo $phone; ?>" size="50" name="phone" class="input" placeholder=""  style="font-size:18px" required/>
        </div>



        <div id="perfil_1">
          <div class="bp_titulo_2">
            <h1 class="bp_main_title bp_title2"><?php //echo $ti?></h1>
          </div>
          <div class="swpm-registration-tipo_oficina-row bp_frm_registration">
            <label for="tipo_oficina"><?php echo SwpmUtils::_('Ministerio de Educación') ?></label>
            <select id="tipo_oficina" name="tipo_oficina" class="input" data-required_mark="required" data-field_type="select" data-original_id="tipo_oficina">
              <?php echo SwpmMiscUtils::get_tipo_oficina_dropdown($oficina) ?></select>
          </div>

          <div class="swpm-registration-id_centro-row bp_frm_registration">
            <label for="id_centro"><?php echo SwpmUtils::_('Gestión') ?></label>
            <select id="id_centro" name="id_centro" class="input" data-required_mark="required" data-field_type="select" data-original_id="Gestión"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select>
          </div>



        <div class="swpm-registration-id_nivel-row bp_frm_registration">
          <label for="id_nivel"><?php echo SwpmUtils::_('Nivel Educativo') ?></label>
          <select id="id_nivel" name="id_nivel"  class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" ><?php echo SwpmMiscUtils::get_estudios_dropdown($id_nivel) ?></select>
        </div>

        <div class="swpm-registration-id_dre-row bp_frm_registration">
          <label for="id_dre"><?php echo SwpmUtils::_('DRE') ?></label>
          <select id="id_dre" name="id_dre" class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" >
            <?php
          //echo SwpmMiscUtils::get_dre_dropdown($id_dre)
          echo get_dre_dropdown_bp($id_dre);
          ?></select>
        </div>

        <div class="swpm-registration-id_ugel-row bp_frm_registration">
            <label for="id_ugel"><?php echo SwpmUtils::_('UGEL') ?></label>
            <select id="id_ugel" name="id_ugel" class="input" data-required_mark="required" data-field_type="select" data-original_id="nivel" >
              <?php
              //echo SwpmMiscUtils::get_ugel_dropdown($id_ugel)
              //echo get_ugel_dropdown_bp($id_ugel);
              ?></select>
        </div>


        <div id="grupo_1">
          <div class="swpm-registration-id_departamento-row bp_frm_registration">
              <label for="id_departamento"><?php echo SwpmUtils::_('Departamento') ?></label>
              <select id="id_departamento" name="id_departamento" class="input" data-required_mark="required" data-field_type="select" data-original_id="departamento"  >
                <?php //echo SwpmMiscUtils::get_region_dropdown($id_region)
                echo get_region_dropdown_bp($id_region);
                ?>
              </select>
          </div>

          <div class="swpm-registration-id_provincia-row bp_frm_registration">
              <label for="id_provincia"><?php echo SwpmUtils::_('Provincia') ?></label>
              <select id="id_provincia" name="id_provincia" class="input" data-required_mark="required" data-field_type="select" data-original_id="provincia"  ><?php //echo SwpmMiscUtils::get_provincia_dropdown($id_provincia) ?></select>
          </div>

          <div class="swpm-registration-id_Distrito-row bp_frm_registration">
              <label for="id_distrito"><?php echo SwpmUtils::_('Distrito') ?></label>
              <select id="id_distrito" name="id_distrito" class="input" data-required_mark="required" data-field_type="select" data-original_id="distrito"  ><?php //echo SwpmMiscUtils::get_distrito_dropdown($id_distrito) ?></select>
          </div>

        </div>


        <div class="swpm-registration-id_institucion-row bp_frm_registration">
            <label for="id_provincia"><?php echo SwpmUtils::_('Institución') ?></label>
            <select id="id_institucion" name="id_institucion" class="input"  data-required_mark="required" data-field_type="select" ><?php //echo SwpmMiscUtils::get_institucion_dropdown($id_institucion) ?></select>
        </div>

        <div class="swpm-registration-id_institucion_educativa-row bp_frm_registration">
            <label for="id_institucion_educativa"><?php echo SwpmUtils::_('Institución Educativa') ?></label>
            <select id="id_institucion_educativa" name="id_institucion_educativa" class="input"  data-required_mark="required" data-field_type="select" >
              <?php
              //echo SwpmMiscUtils::get_institucion_educativa_dropdown($id_institucion_educativa)
              //echo get_ie_dropdown_bp($id_institucion_educativa);
              ?>
            </select>
        </div>


        <div class="swpm-registration-id_carrera-row bp_frm_registration">
            <label for="id_carrera"><?php echo SwpmUtils::_('Carrera Profesional') ?></label>
            <select id="id_carrera" name="id_carrera" class="input"  data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_carrera_dropdown($id_carrera) ?></select>
        </div>

        <div class="swpm-registration-codigo_modular-row bp_frm_registration">
            <label for="codigo_modular"><?php echo SwpmUtils::_('Código modular IE') ?></label>
            <input type="text" autocomplete="off" id="codigo_modular" value="<?php echo $codigo_modular; ?>" size="50" name="codigo_modular" class="input" placeholder="" readonly/>
        </div>

        <div class="swpm-registration-centro_laboral-row bp_frm_registration">
           <label id="centroLaboral" for="centro_laboral"><?php echo SwpmUtils::_('Centro Laboral') ?></label>
          <input type="text" autocomplete="off" id="centro_laboral" value="<?php echo $centro_laboral; ?>" size="50" name="centro_laboral" class="input" placeholder="" />
        </div>
 
        <div class="swpm-registration-oficina_laboral-row bp_frm_registration" style="display:none">
          <label id="oficinaLaboral" for="oficina_laboral"><?php echo SwpmUtils::_('Dirección u Oficina') ?></label>
          <select id="centro_laboral_2" name="centro_laboral_2" class="input"  data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_oficina_dropdown('') ?></select>
        </div>



        <div class="swpm-registration-id_cargo-row bp_frm_registration">
          <label for="id_cargo"><?php echo SwpmUtils::_('Cargo') ?></label>
          <select id="id_cargo" name="id_cargo" class="input" data-required_mark="required" data-field_type="select" ><?php echo SwpmMiscUtils::get_cargo_dropdown($id_cargo) ?></select>
        </div>


        <div class="elem-group">
          <img src="<?php echo plugins_url("/simple-membership/views/captcha.php"); ?>" alt="CAPTCHA" class="captcha-image"><i class="fas fa-redo refresh-captcha"></i>          <br>
          <label for="captcha">Por favor ingresar el código de seguridad:</label>

          <input type="text" autocomplete="off" id="captcha" name="captcha_challenge" pattern="[A-Z]{6}" required>
        </div>
          <?PHP //ECHO $_SESSION['captcha_text'] ?>

        </div>




            <div class="swpm-registration-membership-level-row">
                <!--
                <label for="membership_level"><?php //echo SwpmUtils::_('Membership Level') ?></label>
                -->
                    <?php
                    //echo $membership_level_alias; //Show the level name in the form.
                    //Add the input fields for the level data.
                    echo '<input type="hidden" value="' . $membership_level . '" size="50" name="membership_level" id="membership_level" />';
                    //Add the level input verification data.
                    $swpm_p_key = get_option('swpm_private_key_one');
                    if (empty($swpm_p_key)) {
                        $swpm_p_key = uniqid('', true);
                        update_option('swpm_private_key_one', $swpm_p_key);
                    }
                    $swpm_level_hash = md5($swpm_p_key . '|' . $membership_level); //level hash
                    echo '<input type="hidden" name="swpm_level_hash" value="' . $swpm_level_hash . '" />';
                    ?>

            </div>
            <?php
            //check if we need to display Terms and Conditions checkbox
            $terms_enabled = $settings->get_value('enable-terms-and-conditions');
            if (!empty($terms_enabled)) {
                $terms_page_url = $settings->get_value('terms-and-conditions-page-url');
                ?>
                <div class="bp_accept_terms">
                  <input type="checkbox" id="accept_terms" name="accept_terms" class="" value="1"> <?php echo SwpmUtils::_('Acepto los') ?>
                  <a href="<?php echo $terms_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Términos y Condiciones.') ?>
                  </a>
                </div>
                <?php
            }
            //check if we need to display Privacy Policy checkbox
            $pp_enabled = $settings->get_value('enable-privacy-policy');
            if (!empty($pp_enabled)) {
                $pp_page_url = $settings->get_value('privacy-policy-page-url');
                ?>
                <input type="checkbox" id="accept_pt" name="accept_pp" class="" value="1"> <?php echo SwpmUtils::_('I agree to the ') ?> <a href="<?php echo $pp_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Privacy Policy') ?></a>
                <?php
            }
            ?>


        <div class="swpm-before-registration-submit-section" align="center"><?php echo apply_filters('swpm_before_registration_submit_button', ''); ?></div>

        <div class="swpm-registration-submit-section" align="center">
            <input type="submit" value="<?php echo SwpmUtils::_('Register') ?>"  id="bp_enviar" class="swpm-registration-submit bp_button" name="swpm_registration_submit" />
        </div>

        <input type="hidden" name="action" value="custom_posts" />

    </form>
</div>
</div>
</div>
</div>

<?php //WP_CSP::ob_start(); ?>
<script type='text/javascript' src='<?php echo site_url('/wp-content/plugins/simple-membership/js/jquery.validate.js');?>'></script>
<?php //WP_CSP::ob_end_flush(); ?>
<?php //WP_CSP::ob_start(); ?>
<script type='text/javascript' >
jQuery(document).ready(function () {
        function validaNumericos(event) {
            if(event.charCode >= 48 && event.charCode <= 57){
              return true;
             }
             return false;
        }

        jQuery("#swpm-registration-form").validate();

        jQuery("#id_perfil").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Perfil es obligatorio</h4>"
            }
        });

        jQuery("#id_subperfil").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo SubPerfil es obligatorio</h4>"
            }
        });

        jQuery("#user_name").rules("add", {
            required: true,
            minlength: 8,
            messages: {
                required: "<h4>El campo Usuario es obligatorio</h4>",
                minlength: jQuery.validator.format("<h4>El valor ingresado tiene que ser {0} digitos.</h4>")
            }
        });

        jQuery("#ubigeo").rules("add", {
            required: true,
            minlength: 6,
            messages: {
                required: "<h4>El campo Ubigeo es obligatorio</h4>",
                minlength: jQuery.validator.format("<h4>El Ubigeo es un código de {0} dígitos que se encuentra en el DNI, de un clic en la imagen con el signo de interrogación ?</h4>")
            }
        });

        jQuery("#email").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Email es obligatorio</h4>",
                email:"<h4>Debe ingresar un email válido</h4>"
            }
        });

          jQuery.validator.addMethod("validaPass1", function(value, element) {
            //return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/i.test(value);
            return /^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*([^\w\s]|[_]))\S{8,}$/i.test(value);
          });
          /*
          $.validator.addMethod("validaPassM", function(value, element) {
            return /[A-Z]/i.test(value);
          }, "Ingrese una letra Mayuscula.");
*/

          jQuery("#password").rules("add", {
              validaPass1:true,
              required: true,
              minlength: 8,
              messages: {
                  required: "<h4>El campo Contraseña es obligatorio</h4>",
                  minlength: jQuery.validator.format("<h4>El largo mínimo permitido es {0} caracteres.</h4>"),
                  validaPass1:"La contraseña debe contener al menos una letra  minúscula, una letra mayúscula, un número y al menos un caracter especial."

              },

          });

        jQuery("#password_re").rules("add", {
            equalTo : '[name="password"]',
            required: true,
            minlength: 8,
            messages: {
                required: "<h4>El campo Contraseña es obligatorio</h4>",
                minlength: jQuery.validator.format("<h4>El largo mínimo permitido es {0} caracteres.</h4>"),
                equalTo:"<h4>Introduzca la misma contraseña nuevamente.</h4>",
            }
        });

      jQuery("#phone").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Telefono es obligatorio</h4>"
            }
        });
        jQuery("#id_centro").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Gestión es obligatorio</h4>"
            }
        });
        jQuery("#id_nivel").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Nivel Educativo es obligatorio</h4>"
            }
        });

        jQuery("#id_dre").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo DRE es obligatorio</h4>"
            }
        });

        jQuery("#id_ugel").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo ugel es obligatorio</h4>"
            }
        });

        jQuery("#id_departamento").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo departamento es obligatorio</h4>"
            }
        });
        jQuery("#id_provincia").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo provincia es obligatorio</h4>"
            }
        });
        jQuery("#id_distrito").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo distrito es obligatorio</h4>"
            }
        });

        jQuery("#id_institucion").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo institución es obligatorio</h4>"
            }
        });

        jQuery("#id_institucion_educativa").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo institución es obligatorio</h4>"
            }
        });

        jQuery("#tipo_oficina").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo captcha es obligatorio</h4>"
            }
        });

        jQuery("#id_cargo").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo captcha es obligatorio</h4>"
            }
        });

        jQuery("#captcha").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo captcha es obligatorio</h4>"
            }
        });

        jQuery("#accept_terms").rules("add", {
            required: true,
            messages: {
                required: "<h4>El campo Terminos y condiciones es obligatorio</h4>"
            }
        });




        jQuery('#bp_enviar').click(function () {
            jQuery("#swpm-registration-form").valid();
            jQuery("#swpm-registration-form").submit();
        });



      });


</script>
<?php //WP_CSP::ob_end_flush(); ?>
<style>
#main-content {
  background-blend-mode: multiply;
  background-image: url(<?php echo site_url('/wp-content/uploads/2019/10/fondo-miperfil-duotono.jpg');?>),linear-gradient(0deg,#a6a6c1 0%,#ffffff 100%)!important;
  padding-top: 5vw;
  padding-bottom: 8vw;
  background-color: inherit!important;
}
label {
  /*display: none;*/
  font-size: 16px;
  color:#b7b2b2;
}
.error{
  color: #ff4c00!important;
    font-size: 16px;
    font-family: 'Calibri',Helvetica,Arial,Lucida,sans-serif!important;
}

.bp_content_registration{
    position: relative;
      width: 100%;
      max-width: 1080px;
      margin: auto;
}
.bp_content_registration_1{
    background-image: linear-gradient(180deg,rgba(255,255,255,0.86) 0%,rgba(221,221,221,0.91) 100%);
      background-color: rgba(0,0,0,0);
      border-radius: 15px 15px 15px 15px;
      overflow: hidden;
      width: 60%;
      max-width: 750px;
      padding-right: 4vw!important;
      padding-left: 4vw!important;
      padding-top: 4.2415%;
      padding-bottom: 4.2415%;
}
p.et_pb_contact_field {
    margin-bottom: 3%;
    padding: 0 0 0 3%;
    background-position: center;
    background-size: cover;
}
.bp_content_registration .bp_frm_registration select{
  width: 100%;
  margin-bottom: 3%;
  padding: 0 0 0 3%;
  background-position: center;
  background-size: cover;
  border-radius: 5px 5px 5px 5px;
    overflow: hidden;
    border-color: #d0e8d9;
    font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #ffffff;
}
.bp_content_registration .bp_frm_registration input{
    width: 100%;
  margin-bottom: 3%;
  padding: 0 0 0 3%;
  background-position: center;
  background-size: cover;
  border-radius: 5px 5px 5px 5px;
    overflow: hidden;
    border-color: #d0e8d9;
    font-size: 18px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-color: #ffffff;
}


.bp_content_registration_0 {
  width: 29.667%;
  margin-right: 5.5%;
    float: left;
    position: relative;
    z-index: 9;
    background-position: center;
    background-size: cover;
}

.bp_content_registration_0{
    float: left;
    text-align: right;
    margin-top: 11.6% !important;
}
.bp_content_registration_1{
    float: left;

}


.bp_main_title{
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
    font-size: 20px;
    color: #7580c4!important;
    position: relative;
    padding-bottom: 16px;
    font-weight: 500;
    padding-top: 40px;

}

.swpm-registration-widget-form{
    background-color: rgba(0,0,0,0);
    padding-top: 18px;
    padding-right: 0px;
    padding-bottom: 0px;
    padding-left: 0px;
    margin-top: 0px!important;
    margin-right: 0px!important;
    margin-bottom: 0px!important;
    margin-left: 0px!important;
}

.bp_text{
  color: #ffffff!important;
  font-family: 'Gotham Rounded Light',Helvetica,Arial,Lucida,sans-serif;
  font-size: 14px;
  line-height: 20px;
}

.bp_text_1{
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
  line-height: 22px;
  font-size: 18px;
  margin-bottom: 3%;
      /*margin-top: -27px!important;*/
}
.bp_text_2{
  margin-top: 9.27%;
  margin-bottom: 5%;
}
.bp_text_3{
  color: #ffe993!important;
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
  font-size: 18px;
  line-height: 1.4em;
}

.bp_module{
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
    background-color: rgba(0,0,0,0);
    border-style: dashed;
    border-color: #ff9b0f;
    border-right-width: 2px;
    padding-top: 0px!important;
    padding-right: 15px!important;
    padding-bottom: 0px!important;
    padding-left: 0px!important;
    margin-top: 0px!important;
    margin-right: 0px!important;
    margin-left: 0px!important;
}

.bp_text_4 h1 {
    font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;
    font-size: 40px;
    color: #ffffff!important;
    margin-bottom: 4%;
    padding-top: 15px!important;
}

body .bp_content_registration .bp_button {
  color: #603e00!important;
  border-width: 0px!important;
  border-color: #ffffff;
  border-radius: 25px;
  letter-spacing: 1px;
  font-size: 15px;
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;
  background-color: #ffc30f;
  padding: .3em 1em;
  text-transform: uppercase;
}
body .bp_content_registration  .bp_button:hover {
    background-image: initial!important;
    background-color: #fcd00f!important;

}

.bp_accept_terms{
  margin-top: 2%;
  margin-bottom: 5%;
}


.swpm-registration-username-row{
  width: 42%;
  margin-right: 17px;
  float: left;
}
.swpm-registration-ubigeo-row{
  width: 42%;
  float: left;
}

.swpm-registration-id_departamento-row{
  width: 30%;
  float: left;
    margin-right: 5%;
}

.swpm-registration-id_provincia-row{
  width: 30%;
  float: left;
    margin-right: 5%;
}

.swpm-registration-id_Distrito-row{
  width: 30%;
  float: left;
      margin-bottom: 15px;
}
.swpm-registration-icono-row{
      margin-top: 35px;
      margin-left: 20px;
      padding-left: 20px;
}
#swpm_message{
  text-align: left !important;
  margin-left: 385px !important;
  color: #ff668e !important;
  margin-top: -40px !important;
  background: #ffffff !important;
padding-left: 40px !important;
/* margin-top: 180px!important; */
padding-bottom: 10px !important;
padding-top: 10px !important;
padding-bottom: 10px !important;
width: 59% !important;
border-radius: 5px !important;
font-family: 'Calibri Regular',Helvetica,Arial,Lucida,sans-serif!important;
font-size: 16px!important;


}

*:focus {outline: none;}
::-webkit-validation-bubble-message {
    padding: 1em;
}
.swpm-validate-form input:focus:invalid, .swpm-validate-form select:focus:invalid { /* when a field is considered invalid by the browser */
    box-shadow: 0 0 5px #d45252;
    border-color: #ff4c00
}
.swpm-validate-form input:required:valid, .swpm-validate-form select:required:valid { /* when a field is considered valid by the browser */
    box-shadow: 0 0 5px #5cd053;
    border-color: #28921f;
}

.swpm-registration-tipo_doc-row {
    width: 49%;
    float: left;
    margin-right: 5%;
}

.formErrorArrow{
  display: none;
}

.formError .formErrorContent {
    width: 100%;
    background: #ee0101;
    position: relative;
    color: #fff;
    min-width: 120px;
    font-size: 11px;
    border: 0px!important;
    box-shadow: 0px!important;
    -moz-box-shadow: 0 0 6px #000;
    -webkit-box-shadow: 0px!important;
    -o-box-shadow: 0 0 6px #000;
    padding: 4px 10px 4px 10px;
    border-radius: 6px;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    -o-border-radius: 6px;
}
.formErrorContent{
  margin-left: -175px!important;
  margin-top: 190px!important;
}
.user_nameformError {
  margin-top: -140px!important;
}
.emailformError{
      margin-top: -140px!important;
      margin-left: -270px!important;
}
.passwordformError {
  margin-left: -272px!important;
  margin-top: -141px!important;
  width: 25%;
}


#captcha{
  width: 100% !important;
  margin-bottom: 3% !important;
  padding: 0 0 0 3% !important;
  background-position: center !important;
  background-size: cover !important;
  border-radius: 5px 5px 5px 5px !important;
  overflow: hidden !important;
  border-color: #d0e8d9 !important;
  font-size: 18px !important;
  padding-top: 10px !important;
  padding-bottom: 10px !important;
  background-color: #ffffff !important;
}
body, input, textarea, select {
    font-family: 'Calibri Regular',Helvetica,Arial,Lucida,sans-serif!important;
    font-size: 18px!important;
    color: #797979!important;
}
#user_name-error h4{
  margin-top: 5px!important;
}

#ubigeo-error h4{
  margin-top: 5px!important;
}
input, select{
    border: none !important;
    box-shadow: none !important;
}
#password-error{
    color: #f00 !important;
}
</style>
<?php
  ob_end_flush();

 ?>
