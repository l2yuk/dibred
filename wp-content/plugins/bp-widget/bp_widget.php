<?php
ob_start();
/*
Plugin Name: BP Widget
Plugin URI: http://wordpress.org/extend/plugins/#
Description: widget para mostrar resumen de perfil de usuario
Author: walter
Version: 1.0
Author URI: http://example.com/
*/


//$auth = SwpmAuth::get_instance();
//$bp_edit_url = $setting->get_value('registration-page-url');

add_action( 'widgets_init', function(){
	register_widget( 'Bp_Widget' );
});


class Bp_Widget extends WP_Widget {
	// class constructor
	public function __construct() {

    $widget_ops = array(
  		'classname' => 'bp_widget',
  		'description' => 'widget para mostrar resumen de perfil de usuario',
	   );
	    parent::__construct( 'bp_widget', 'My Widget', $widget_ops );



  }

	// output the widget content on the front-end
	public function widget( $args, $instance ) {
    //obtener imagen del usuario

    //obtener nombre del usuario y id
    if (is_user_logged_in()){
		    $cu = wp_get_current_user();

    }
    //
	 	$nombre = explode(' ', trim($cu->user_firstname));
		$apellido = explode(' ', trim($cu->user_lastname));
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		global $current_user;
		$url_link=get_bloginfo('url') . '/members/'. $current_user->user_login . '/profile';
    ?>
    <div class="bbp-logged-in" style="padding-bottom:40px;width: 250px;padding-right: 20px;">
				<a href="<?php echo $url_link;//bbp_user_profile_url( bbp_get_current_user_id() ); ?>" class="submit user-submit"><?php echo get_avatar( bbp_get_current_user_id(), '200' ); ?></a>
				<h4 style="font-size:26px;text-transform: uppercase">
        <?php //bbp_user_profile_link( bbp_get_current_user_id() );
        echo $nombre["0"].' '.$apellido["0"];
        ?>
      	</h4>

				<?php //bbp_logout_link(); ?>
				<a href="https://observatorio.minedu.gob.pe/?swpm-logout=true" class="button logout-link-editar">SALIR</a>
        <a href="<?php echo $url_link;?>" class="button logout-link-editar">MI PERFIL</a>
			</div>
			<style>
			<?php if (is_user_logged_in()){ ?>
					#top-menu li.menu-item-red>a{
					  color: #f9d800!important;
					}
			  <?php } ?>
			</style>
    <?php
    /*
    echo '
    <div id="bbp_login_widget-5" class="widget-2 widget-even widget-alt et_pb_widget bbp_widget_login">
			<div class="bbp-logged-in">
				<a href="https://observatorio.minedu.gob.pe/members/46487505/" class="submit user-submit"><img alt="" src="//www.gravatar.com/avatar/be19bfef98cbb32bf9593a21377abcba?s=40&amp;r=g&amp;d=mm" srcset="//www.gravatar.com/avatar/be19bfef98cbb32bf9593a21377abcba?s=40&amp;r=g&amp;d=mm 2x" class="avatar avatar-40 photo" height="40" width="40"></a>
				<h4><a href="https://observatorio.minedu.gob.pe/members/46487505/">46487505</a></h4>
				<a href="https://observatorio.minedu.gob.pe/wp-login.php?action=logout&amp;_wpnonce=bbfce4d920&amp;redirect_to=https%3A%2F%2Fobservatorio.minedu.gob.pe%2Fmembership-login%2F%3Floggedout%3Dtrue" class="button logout-link">Editar</a>
        <a href="https://observatorio.minedu.gob.pe/wp-login.php?action=logout&amp;_wpnonce=bbfce4d920&amp;redirect_to=https%3A%2F%2Fobservatorio.minedu.gob.pe%2Fmembership-login%2F%3Floggedout%3Dtrue" class="button logout-link">Salir</a>			</div>
		</div>';
    */
  }

	// output the option form field in admin Widgets screen
	public function form( $instance ) {



  }

	// save options
	public function update( $new_instance, $old_instance ) {

  }
}
