<?php
/*
 *
 */
setlocale(LC_TIME, 'es_PE');
 add_shortcode('listar_noticias', 'bp_listar_noticias');

function bp_listar_noticias() {
  //Obtenemos los 3 primeros post
  $args = Array(
    'cat' => 59,
      "orderby"=>"ID",
      "order"=>"DESC",
      "posts_per_page"=>4
  );
  $the_query = new WP_Query($args);
  $dato_bp1=array();
  if ( $the_query->have_posts() ) :
    while ( $the_query->have_posts() ) : $the_query->the_post();
        $post = get_post();
        $dato_bp1[]=$post->ID;

    endwhile;
  else :
    _e( 'Sorry, no posts matched your criteria.' );
  endif;

  //listamos los post que no se encuentran en la primera lista
  $args_filtro = Array(
      'cat' => 59,
      "orderby"=>"ID",
      "order"=>"DESC",
      "posts_per_page"=>3,
      'post__not_in' =>  $dato_bp1
  );

  $the_query3 = new WP_Query($args_filtro);
  /*si hay post, entra en el bucle*/
  if ( $the_query3->have_posts() ) :
  /*el loop*/
  $html= '';

  //unset($post->post_title);
//  $i=0;
    while ( $the_query3->have_posts() ) :
      //$i++;
      $the_query3->the_post();

      $post3 = get_post();

        $getlength = strlen($post3->post_title);
        $thelength = 80;

        $dato_bp=mb_substr($post3->post_title,0,$thelength,'UTF-8');
        $dato=$dato_bp;

        if ($getlength > $thelength) $dato=$dato."...";
        if ($getlength < $thelength){
          //$bp_estilo='style="margin-bottom: 25px;"';
        }else{
          $bp_estilo='';
        }
        $bp_estilo='style="margin-bottom: 25px;"';
        $thumbID = get_post_thumbnail_id( $post3->ID );
        $imgDestacada = wp_get_attachment_image_src( $thumbID, 'attachment-post-thumbnail' ); // Thumbnail, medium, large, full
        /*
        if ( has_post_thumbnail() ) {
            $bp_imagen=the_post_thumbnail();
        }
        */

          $bp_link=post_permalink($post3->ID);
    //$dato=$post3->post_title;
    $html.= ' 
	  
		<style>
      /*
      .et_pb_top_inside_divider{
        display: none!important ;
      }
      */

      div.et_pb_section.et_pb_section_0 {
      display: none!important;
      }
	  .et_pb_row.et_pb_row_11.et_pb_ab_subject.et_pb_ab_subject_id-1168_2{
		  display: none!important;
	  }

      </style><div class="et_pb_column et_pb_column_1_3 et_pb_column_15  et_pb_css_mix_blend_mode_passthrough">';
      $html.= '
          				<div class="et_pb_module et_pb_image et_pb_image_3">
  				            <span class="et_pb_image_wrap ">
                      <img src="'.$imgDestacada[0].'" >
                      </span>
  			           </div>
                  <div class="et_pb_module et_pb_text et_pb_text_16 et_pb_bg_layout_light  et_pb_text_align_left">
                    <div class="et_pb_text_inner">
                      <p '.$bp_estilo.'>
                      <a class="bp_estilo_1" href="'.$bp_link.'" style="color: #575756!important;">
                      '.$dato.'
                      </a>
                      </p>
                      </div>
  			          </div> <!-- .et_pb_text -->
                  <div class="et_pb_module et_pb_divider et_pb_divider_4 et_pb_divider_position_center et_pb_space"><div class="et_pb_divider_internal"></div></div>
                  <div class="et_pb_module et_pb_text et_pb_text_17 et_pb_bg_layout_light  et_pb_text_align_left">
                    <div class="et_pb_text_inner"><p><span>'.strftime("%d %B %G", strtotime($post3->post_date)).'</span></p></div>
  			          </div> <!-- .et_pb_text -->



              ';
              $html.= '</div>
				
              ';
			  
      endwhile; /*fin del loop */
    /*si no hay post de búsqueda*/
    else:

    endif;



  return $html;
}



add_shortcode('listar_bbpp', 'bp_listar_bbpp');

function bp_listar_bbpp() {
  $html='<div class="bp_content_bbpp">';

  //$currentPage = get_query_var('paged');
  $html.=$currentPage;
  // Top pagination (pagination arguments)


  $html.='
      <form action="'.site_url().'/wp-admin/admin-ajax.php" method="POST" id="filter">
        <div class="bp_select_bbpp_s">
      <div class="bp_select_bbpp" data-type="select">
              <select class="brands-list bp_select_bbpp_0" id="brand" name="brand">
                <option selected="selected" value="">Tipo de Práctica</option>
                <option value="0">&nbsp; Ver Todos</option>
            ';
            if( $brands = get_terms( array( "taxonomy" => "tipo-practica" ) ) ) :
            foreach( $brands as $brand ) {
              $html.='<option value="'. $brand->slug . '">&nbsp; '.$brand->name.'</option>';

            }
            $html.='</select>';
            endif;
            $html.='</div><div class="bp_select_bbpp" data-type="select">';

            $html.='<select class="sizes-list bp_select_bbpp_0" id="size" name="size" style="">
            <option selected="selected" value="">Nivel Educativo</option>
            <option value="0">&nbsp; Ver Todos</option>';

                    if( $sizes = get_terms( array( 'taxonomy' => 'nivel-educativo' ) ) ) :
                          //var_dump($brands);
                      foreach( $sizes as $size ) :
                          $html.='<option value="'. $size->slug . '">&nbsp; '.$size->name.'</option>';
                      endforeach;
                            $html.='</select>';
                  endif;


$html.='</div>
</div>
        <button class="bp_button bp_button_bbpp ">BUSCAR</button>
        <input type="hidden" name="action" value="myfilter">
        </form>
        </div>
        <br>
    <div id="response" class="bp_response_bbpp" >
    </div>
    ';
    return $html;
}





add_shortcode('search_bbpp', 'bp_search_bbpp');

function bp_search_bbpp() {

$html='<div class="bp_content_bbpp">';
//$html='';
  //$currentPage = get_query_var('paged');
  //$html.=$currentPage;
  // Top pagination (pagination arguments)


  $html.='
      <form action="'.site_url().'/wp-admin/admin-ajax.php" method="POST" id="filter_bp">
        <input type="text" id="search_bp" name="search_bp" placeholder="Ingresa tu búsqueda" class="et_pb_s" style="padding-right: 123px;width:92%; margin-left: -10px;">
        <button class="bp_button bp_button_bbpp bp_button_search">BUSCAR</button>
        <div class="bp_select_bbpp_s">
          <div class="bp_select_bbpp bp_select_search" data-type="select">
              <select class="brands-list bp_select_bbpp_0" id="brand2" name="brand2">
                <option selected="selected" value="">Tipo de Práctica</option>
                <option value="0">&nbsp; Ver Todos</option>
            ';
            if( $brands = get_terms( array( "taxonomy" => "tipo-practica" ) ) ) :
            foreach( $brands as $brand ) {
              $html.='<option value="'. $brand->slug . '">&nbsp; '.$brand->name.'</option>';

            }
            $html.='</select>';
            endif;
            $html.='</div>
              <div class="bp_select_bbpp bp_select_search" data-type="select">';

                $html.='<select class="sizes-list bp_select_bbpp_0" id="size2" name="size2" style="">
                <option selected="selected" value="">Nivel Educativo</option>
                <option value="0">&nbsp; Ver Todos</option>';

                    if( $sizes = get_terms( array( 'taxonomy' => 'nivel-educativo' ) ) ) :
                          //var_dump($brands);
                      foreach( $sizes as $size ) :
                          $html.='<option value="'. $size->slug . '">&nbsp; '.$size->name.'</option>';
                      endforeach;
                            $html.='</select>';
                  endif;


$html.='     </div>';

$html.='</div>
  <div class="bp_select_bbpp bp_select_search" data-type="select">';

    $html.='<select class="sizes-list bp_select_bbpp_0" id="category" name="category" style="">
    <option selected="selected" value="">Categoría</option>';

        if( $sizes = get_terms( array( 'taxonomy' => 'category' ) ) ) :
              //var_dump($brands);
          foreach( $sizes as $size ) :
              $html.='<option value="'. $size->slug . '">&nbsp; '.$size->name.'</option>';
          endforeach;
                $html.='</select>';
      endif;

$html.='
        </div>
        <input type="hidden" name="action" value="myfilterbp">


        </form>
        </div>
        <br>
    <div id="response_search" class="bp_response_bbpp" >
    </div>
    ';

    //$html="</div>";
    return $html;
}
