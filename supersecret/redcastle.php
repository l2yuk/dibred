<?php
/**
 * Configuración básica de WordPress.
 *
 * El script de creación utiliza este fichero para la creación del fichero wp-config.php durante el
 * proceso de instalación. Usted no necesita usarlo en su sitio web, simplemente puede guardar este fichero
 * como "wp-config.php" y completar los valores necesarios.
 *
 * Este fichero contiene las siguientes configuraciones:
 *
 * * Ajustes de MySQL
 * * Claves secretas
 * * Prefijo de las tablas de la Base de Datos
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solcite esta información a su proveedor de alojamiento web. ** //
/** El nombre de la base de datos de WordPress */
define('DB_NAME', 'wp_observatorio');

/** Nombre de usuario de la base de datos de MySQL */
define('DB_USER', '');

/** Contraseña del usuario de la base de datos de MySQL */
define('DB_PASSWORD', '!');

/** Nombre del servidor de MySQL (generalmente es localhost) */
define('DB_HOST', '');

/** Codificación de caracteres para usar en la creación de las tablas de la base de datos. */
define('DB_CHARSET', 'latin1');

/** El tipo de cotejamiento de la base de datos. Si tiene dudas, no lo modifique. */
define('DB_COLLATE', '');

/** Restringir live editing */
define('DISALLOW_FILE_EDIT', true);



/**#@+
 * Claves únicas de autenticación y salts.
 *
 * ¡Defina cada clave secreta con una frase aleatoria distinta!
 * Usted puede generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress.org}
 * Usted puede cambiar estos valores en cualquier momento para invalidar todas las cookies existentes. Esto obligará a todos los usuarios a iniciar sesión nuevamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ic[9IyBJ!*?j3Eb=rHs+X7lT_mz{$cdtiG?B@)qzS]v`|X,L|NL z}-->5qS]P(6');
define('SECURE_AUTH_KEY',  '-:s|}Ly#5z,RV<tt6O6.O^1#oT}E,irh3|W89c(ir- =$!/@KqoyzmMC=IUURGL^');
define('LOGGED_IN_KEY',    'D06c+OL$+xXJn_y)?a-5I2H=@81HYlK[Ry[~H4_dvX26K9yUR7>}%9?f!m;^u;q(');
define('NONCE_KEY',        ' NK@anZNvbUch3]$X*=U)_~C^TzxF;Q>GKS 1(^9]4|rB_hHA^zh`J].-]2x7N-h');
define('AUTH_SALT',        'bV!9?2zlwgSL9c;X8?pBZ?$jcipg?TH 4,wZZ$5t#3aa4oIfujNO6hM;$LU?GE1#');
define('SECURE_AUTH_SALT', 'i5uUI5<!-sa50+c1dLs7sws~*NhAc6i|{QHS=61Xmf5[3Y<>-Ea5|_e1]fU!;hwQ');
define('LOGGED_IN_SALT',   ':V{ZMy**px0@2`!oDpoBE03HnHLLoyvy&Q3>!5zz^9h^+|PU|(GbB[)/4YXGwaK4');
define('NONCE_SALT',       'ovIHcm~,3LCuO[2Q!cjdZZ-f?n%$.671M&bwTWo/&<fTl:]l{0,h06{ZUHJ- .uF');

/**#@-*/

/**
 * Prefijo de las tablas de la base de datos de WordPress.
 *
 * Usted puede tener múltiples instalaciones en una sóla base de datos si a cada una le da
 * un único prefijo. ¡Por favor, emplee sólo números, letras y guiones bajos!
 */
$table_prefix  = 'wp_';



/**
 * Para los desarrolladores: modo de depuración de WordPress.
 *
 * Cambie esto a true para habilitar la visualización de noticias durante el desarrollo.
 * Se recomienda encarecidamente que los desarrolladores de plugins y temas utilicen WP_DEBUG
 * en sus entornos de desarrollo.
 *
 * Para obtener información acerca de otras constantes que se pueden utilizar para la depuración,
 * visite el Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG',false);

/* ¡Eso es todo, deje de editar! Disfrute de su sitio. */
//define( 'ALLOW_UNFILTERED_UPLOADS', true );
/** Ruta absoluta al directorio de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Establece las vars de WordPress y los ficheros incluidos. */
require_once(ABSPATH . 'wp-settings.php');
